A Man Among Giants
{{Infobox film name           = A Man Among Giants image          =  caption        =  director       = Rod Webber producer       = Ali Bell Matthew D. Ferrel writer         = Rod Webber screenplay     =  story          =  based on       =   starring       = Tiny The Terrible Rod Webber Jay Tunstall Joseph James Bellamy music          = Rod Webber cinematography = Greyson Welch editing        = David T. Grophear Rod Webber studio         =  distributor    =  released       =  runtime        = 94 minutes country        = United States language       = English budget         =  gross          = 
}}
 WWF pro-wrestler, and toy store elf, Tiny, a self-described "ghetto republican" has a difficult time being taken seriously.  The fact that he is accused of having called in a threat to a local radio station doesnt help either. And yet, with the undying tenacity of the man, there is something bizarrely endearing about Tunstall. In the words of The Boston Globe, "He is afraid of no one and, in more lucid moments, he advocates for America’s have-nots in the heartfelt words of a guy who knows whereof he speaks."  

==Plot== WWF Monday Night Raw and The Jerry Springer Show, Tiny leaps into a political system where he is in over his head.  Living on a fixed income, Tiny cant do much more than get out and shake hands with the voters. But when Dave Lewis, a wealthy backer comes to manage his campaign, things begin to take on a new life and a new struggle, this time within the campaign. In the words of Todd Cioffi of Fest21.com, "Somehow, this odd little man, with his half-formed policy ideas and his misshapen sense of politics, connects with people... Ultimately, warts and all, he is strangely compelling... In the end, Tunstall shows the truth to the cliché that life is about the journey, not the destination and that, sometimes, even a loser can be a winner." 

==Aftermath==
After the completion of the documentary, Tiny was arrested for making threats to public officials. 

==Screenings==
An early cut of A Man Among Giants premiered June 4, 2008 at The Hoboken International Film Festival.
 

The completed cut premiered at Reel Fest in July 2009.
 {{cite news|url=http://www.boston.com/ae/movies/articles/2009/07/05/duxbury_arts_center_teams_up_with_the_coolidge/ Boston Globe | first=Linda|last=Matchan|date=July 5, 2009}}  {{cite web|url=http://www.highbeam.com/doc/1P2-20524421.html Boston Globe}} 

==References==
 

==External links==
*  

 
 
 