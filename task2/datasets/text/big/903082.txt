The Kentucky Fried Movie
 
{{Infobox film
| name = The Kentucky Fried Movie
| image = Kentucky Fried Movie movie poster.jpg
| alt = high top sneaker, with a statue of liberty sticking out of it, and a rocket pack on the side.
| caption = The Kentucky Fried Movie theatrical poster
| director = John Landis
| producer = Kim Jorgensen Larry Kostroff Robert K. Weiss David Zucker Jerry Zucker
| starring = Bill Bixby George Lazenby Evan C. Kim Bong Soo Han Donald Sutherland Henry Gibson Barry Dennen Agneta Eckemyr
| music = Igo Kantor
| cinematography = Robert E. Collins Stephen M. Katz
| editing = George Folsey Jr.
| distributor = United Film Distribution  Anchor Bay Entertainment (DVD)
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = United States dollar|$650,000 JACK MATHEWS: FRIED MOVIE PRODUCER AT IT AGAIN
Los Angeles Times (1923-Current File)   15 Sep 1987: H1.  
| gross = $20,000,000 (Global) = $15,000,000 (USA) + $5,000,000 (Non-USA)
}}

The Kentucky Fried Movie is an 1977 American anthology comedy film, released in 1977 and directed by John Landis.
 David Zucker, Jim Abrahams and Jerry Zucker, who would go on to write and direct Airplane!, Top Secret! and the Police Squad! television series and its film spinoffs, The Naked Gun|The Naked Gun films. The "feature presentation" portion of the film stars Evan C. Kim and hapkido Grand Master Bong Soo Han.
 Stephen Bishop, and the voice of Shadoe Stevens. According to David Zucker in the DVD commentary track, David Letterman auditioned for the role of the newscaster, but was not selected. The film also features many former members of The Groundlings theater, as well as some from The Second City.

The Kentucky Fried Movie marked the first film appearances of a number of actors who later became famous as well as being the vehicle that launched the careers of the Zucker brothers, Abrahams and John Landis|Landis. It was Landiss work on this film that was largely responsible for his being recommended to direct National Lampoons Animal House in 1978.   

==Production-background== improvisational troupe billed as Kentucky Fried Theater, they decided to make the movie on their own. 

A wealthy real estate investor offered to finance the film if they would write a script. After completion of the screenplay, the investor had second thoughts and decided he did not want to finance the film alone. He said he would try to attract other investors if the three filmmakers would produce a ten-minute excerpt of the film, which he would finance. When the trio presented a budget of the short film to the investor, he backed out. Litwak, p. 136 

The prospect of shooting the short film so excited the trio that they decided to pay for it themselves. The ten-minute film cost $35,000, and with it they again approached the Hollywood studios. This time they attached young director John Landis to the project. However, once again, the studios turned them down. 

Curious as to how audiences would react to their film, they persuaded exhibitor Kim Jorgenson to show it before one of his regularly scheduled movies. When Jorgenson saw the short, he "fell out of his seat laughing." He was so impressed that he offered to raise the money needed to make the full-length version. By having his fellow exhibitors screen the film before audiences in their theaters, he convinced them to put up the $650,000 budget. When released, Kentucky Fried Movie was a box-office success, returning domestic American rentals of $7.1 million. 

==Description== sketches that parody various film genres, including exploitation films. The films longest segment spoofs early kung-fu films, primarily Enter the Dragon; its title, A Fistful of Yen, refers to A Fistful of Dollars. Parodies of disaster films (Thats Armageddon), blaxplotation (Cleopatra Schwartz) and softcore porn/Women in prison film|women-in-prison films (Catholic High School Girls in Trouble) are presented as "Coming Attraction" trailer (promotion)|trailers. The fictional films are said to have been produced by "Samuel L. Bronkowitz" (a conflation of Samuel Bronston and Joseph L. Mankiewicz). The sketch See You Next Wednesday mocks theater-based gimmicks like Sensurround by depicting a dramatic film presented in "Feel-a-Round", which involves an usher physically accosting the patron. Other sketches spoof TV commercials and programs, news broadcasts, and classroom educational films. The city of Detroit and its high crime rate are a running gag portraying the city as Hell on Earth; in "A Fistful of Yen", the evil drug lord orders a captured CIA agent to be sent to Detroit, and the agent screams and begs to be killed instead.
 spoof and mockumentary genres of filmmaking.

===Sketch selection=== Sketch Details 

# 11 OClock News (Part 1) (:04)
# Argon Oil (1:13)
# A.M. Today (6:05)
# His New Car (:24)
# Catholic High School Girls in Trouble (2:00)
# See You Next Wednesday in Feel-a-Round (4:52)
# Nytex P.M. (:35)
# High Adventure (3:01)
# 11 OClock News (Part 2) (:05)
# Headache Clinic (:40)
# Household Odors (:40)
# The Wonderful World of Sex (4:55)
# A Fistful of Yen (31:34)
# Willer Beer (:58)
# 11 OClock News (Part 3) (:05)
# Scot Free (:58)
# Thats Armageddon (2:17)
# United Appeal for the Dead (1:42)
# "Courtroom" (Part 1) (4:35)
# Nesson Oil (:14)
# "Courtroom" (Part 2) (3:02)
# Cleopatra Schwartz (1:24)
# Zinc Oxide and You (1:59)
# "Danger Seekers" (1:02)
# Eyewitness News (4:24)
# 11 OClock News (Part 4) (:09)

====Sketch details====
{| class="wikitable collapsible collapsed" width=100% 
! colspan=2 | Sketch details
|-
| width=20% |
11 OClock News (Part 1) (:04) A news Film at 11.
|- Argon Oil (1:13) A commercial US fast food.
|-
|A.M. Today (6:05) A news astrologer reminds Rick Baker). The gorilla, who has been unable to mate, becomes progressively enraged by its female handlers explanations and suddenly tears off the handlers shirt. Studio hands try unsuccessfully to restrain the gorilla as it runs amok and smashes the camera.
|- His New Car (:24) When a man enters a car, multiple alarms go off. As he goes through a succession of actions (locking his door, buckling his seatbelt, and so on) the alarms shut off one by one until only one remains.  Eventually he reaches down and zips the zipper on his jeans. The last alarm then stops.
|-
|Catholic High School Girls in Trouble (2:00) A parody penetrates her from behind. Also, a rather shocked teenager interrupts his intercourse when he realizes it is actually his girlfriends middle aged mother under the sheets. She claims people always mistake her for her teenage daughter. Another teenager learns in horror that masturbation has made his hands hairy.
|-
|(See You Next Wednesday in) Feel-A-Round (4:52) A guy usher takes Deep Throat. The viewer runs away screaming.
|- Nytex P.M. (:35) A commercial for a drug that cures headaches by rendering the receiver unconscious.
|-
|
High Adventure (3:01) A talk boom operator finds the French adventurer guest boring, and causes troubles with his boom mike to both the guest and the host.
|- 11 OClock News (Part 2) (:05) Moscow is nuclear warheads Film at 11.
|- Headache Clinic (:40) A commercial hosted by Bill Bixby shows how a clinics scientists demonstrate their headache curing drug Sanhedrin by pounding on peoples heads. It is claimed the people are not affected by the pain.
|- Household Odors (:40) A commercial for household deodorizer that claims if you do not buy it, people will humiliate you by telling you right in your face that your house stinks.
|- The Wonderful World of Sex (4:55) A couple plays back a phonograph record of a how-to guide for sex. After disrobing to their underwear, the overly detailed record eventually instructs them to kiss and begin foreplay. After the male experiences premature ejaculation, the recording sends "Big Jim Slade," a briefs-wearing, muscular "tight end for the Kansas City Chiefs" to carry the woman away and finish up the job of her humiliated partner, backed by a vigorous choral rendition of the song "Heiveinu Shalom Aleichem."
|-
|A Fistful of Yen (31:34) One missile Dark Lord The Wizard of Oz, Loo is sent back to Kansas after his victory and learns it may have all been a dream (or not).
|- Willer Beer (:58) A beer Hare Krishna monks.
|- 11 OClock News (Part 3) (:05) All kinds Film at 11.
|- Scot Free (:58) A commercial for a board game based on US president John F. Kennedy assassination conspiracy theories.
|-
|Thats Armageddon (2:17) A parody of the then-common disaster film genre, with "Donald Sutherland as the clumsy waiter."
|- United Appeal for the Dead (1:42) A commercial for an association that supports keeping around corpses of dead people and still treating them like part of the family.
|-
|"Courtroom" (Part 1) (4:35) A spoof Jerry Zucker) and Wally Cleaver (Tony Dow, reprising his role from the original Leave It to Beaver) get into trouble in the jury stand.
|- Nesson Oil (:14) A commercial for cooking oil in which a little girl is "cooking the cat in pure Nesson oil".
|-
|"Courtroom" (Part 2) (3:02) Beaver and Wally continue to make trouble, while the trials "surprise" witness recognizes the TV announcer himself as the offender in a car accident.
|-
|Cleopatra Schwartz (1:24) A parody of blaxploitation films. A love and marriage story of a Pam Grier–like character and a Rabbi. Despite their differences, they live a passionate life (highlighted by the couple sitting in bed with satisfied expressions, with her topless).
|-
|Zinc Oxide and You (1:59) A parody instantly sag gas control valves on her stove disappear, her kitchen catches on fire, and everything that can stop the fire also disappears. Ends with a brief announcement of the next film in the series, Rebuilding Your House.
|-
|"Danger Seekers" (1:02) A parody daredevil Rex Kramer vows to take on the most dangerous situations possible "for the sake of adventure." Rex walks into the middle of a group of African American men playing Cee-lo (dice game)|Cee-lo in an alley, screams a racial epithet, then flees as they angrily chase him.

(The name Rex Kramer would later be given to Robert Stacks character in Airplane!.) 
|- Eyewitness News (4:24) A couple the woman mounts the man. Almost immediately, the men from the production crew return to the news announcer. They all share a collective orgasm with the woman.
|- 11 OClock News (Part 4) (:09) The news Film at 11.
|}

==Release==
The film was released on DVD in the U.S. by   and full-frame (1.33:1), and includes commentary by John Landis; writers Jerry Zucker, David Zucker, and Jim Abrahams; and producer Robert K. Weiss.

On July 4, 2011 Arrow Video in the United Kingdom released a two-disc special edition DVD, with the following special features:
* Feature presented in widescreen 1.85:1 and full-frame 1.33:1
* Original mono audio
* The audio recollections of director John Landis; writers Jerry Zucker, David Zucker, Jim Abrahams; and producer Robert K. Weiss
* A conversation with David and Jerry Zucker: A feature length interview with the co-creators of The Kentucky Fried Movie, Airplane! and The Naked Gun about their lives and career, from growing up and starting out in show business to their comedy influences and spoofing Midnight Cowboy
* Jerry Zuckers on-set home video shot during the making of the movie
* Behind-the-scenes photo gallery
* Original trailer
* Four-panel reversible sleeve with original and newly commissioned artwork
* Double-sided fold-out artwork poster
* Collectors booklet featuring brand new writing on director John Landis by critic and author Calum Waddell

On July 2, 2013, Shout! Factory released the film on Blu-ray disc, in a 1.85:1 aspect widescreen transfer, with the original theatrical trailer, and, ported over from the above-mentioned Arrow DVD release: the filmmaker commentary, and the Zucker brothers interview.

== Critical response ==
The film received favorable reviews. Review aggregation website Rotten Tomatoes gives the film a score of 80% based on 30 reviews. 

At the time, Variety (magazine)|Variety described the film as having "excellent production values and some genuine wit" but also noted that film was juvenile and tasteless. 

J.C. Maçek III of PopMatters wrote "The Kentucky Fried Movie is, however, profane, experimental, violent, silly, hilarious and occasionally quite sexually explicit (all of which surely helped its success over the years)." 

Writing three decades later in 2008, Ian Nathan of Empire Magazine called the film "occasionally funny" ... "in a scattershot and puerile way" and he concludes the film is "smart and satirical but very dated". 

==See also==
* Amazon Women on the Moon
* Disco Beaver from Outer Space
* The Groove Tube Everything You Always Wanted to Know About Sex*
* UHF (film)|UHF Tunnel Vision

== References ==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 