Jolly Boy
 
{{Infobox film
| name           = Jolly Boy
| image          =
| caption        =
| director       = Sabapathy Dekshinamurthy
| producer       = K. Manju
| writer         = D. Sabapathy Malavalli Saikrishna (dialogues)
| starring       = Diganth Rekha Vedavyas Devaraj
| music          = Yuvan Shankar Raja
| cinematography = Raana
| editing        = 
| studio         = K Manju Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         =
| gross          = 
}} Tara and Sudha Rani.    A remake of the directors 2011 Tamil film Pathinaaru, it is produced by K. Manju and features the music  by Yuvan Shankar Raja. 

== Cast ==
* Diganth
* Rekha Vedavyas
* Devaraj Tara
* Sudha Rani
* Avinash
* Faisal mohammed
* Archana Sasikala
* Jayakrishna
* Bank Janardhan
* Bhagya
* Maste Amog
* Baby Bhavana

== Soundtrack ==
{{Infobox album
| Name          = Jolly Boy
| Type          = soundtrack
| Artist        = Yuvan Shankar Raja
| Released      = 12 April 2011
| Recorded      = 2008 - 2010 Feature film soundtrack
| Length        = 22:58
| Label         = Anand Audio
| Producer      = Yuvan Shankar Raja
| Chronology    = 
| Last album    = Vaanam (2011)
| This album    = Jolly Boy (2011)
| Next album    = Avan Ivan (2011)
}}
 original Tamil version. The album was released in April 2011 by director Indrajit Lankesh at the Abhimani Convention Center, Bangalore.  

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 22:58
| all_lyrics      = K. Kalyan

| title1          = Baa Endhare
| extra1          = Rajesh Krishnan
| length1         = 6:36

| title2          = Baanu Nammadhe
| extra2          = Ajay Warrior
| length2         = 5:08

| title3          = Kallu Kodada Kavithe
| extra3          = Rajesh Krishnan
| length3         = 4:30

| title4          = Yedaya Olegondhu
| extra4          = Hemanth, Nanditha
| length4         = 4:21

| title5          = Theme Music
| extra5          = Yuvan Shankar Raja
| length5         = 2:23
}}

== References ==
 

 
 
 
 
 


 