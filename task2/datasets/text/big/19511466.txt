Rekrut 67, Petersen
 
{{Infobox film
| name           = Rekrut 67, Petersen
| image          = Rekrut 67, Petersen.jpg
| caption        = Film poster
| director       = Poul Bang
| producer       = John Olsen
| narrator       =
| starring       = Lily Broberg
| music          = Sven Gyldmark
| cinematography = Jørgen Christian Jensen Ole Lytken
| editing        = Anker Sørensen
| studio         = Saga Studios
| distributor    =
| released       =  
| runtime        = 78 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Rekrut 67, Petersen is a 1952 Danish family film directed by Poul Bang.

==Cast==
* Lily Broberg - Grete Petersen
* Gunnar Lauring - Kaptajn Fang
* Kate Mundt - Anna Mogensen
* Ib Schønberg - Dr. Christiansen
* Buster Larsen - Peter Rasmussen
* Dirch Passer - Lillebilchauffør Larsen Rasmus Christiansen - Viceværten Henry Nielsen - Mælkemanden
* Henny Lindorff Buckhøj - Fru Rasmussen
* Ove Sprogøe - Rekrut 68
* Valdemar Skjerning - Direktør I stormagasinet
* Svend Pedersen - Programleder
* Vibeke Warlev - Pianistinden
* Marie Bisgaard - Koncertsangerinde
* Inge Ketti - Alma, stuepige på Krogerup
* Robert Eiming - Direktionssekretær
* Inge-Lise Grue - Ekspeditrice
* Else Jarlbak - Kunde I stormagasin
* Agnes Phister-Andresen - Kunde I stormagasin
* Edith Hermansen
* Ib Conradi - Skildvagt
* Jytte Gratwohl - Pige på kasernen
* Ib Fürst - Premierløjtnant Knudsen
* Ernst Schou - Løjtnant Bagger

==External links==
* 

 
 
 
 
 


 