Escape by Night (1953 film)
 
{{Infobox film
| name           = Escape By Night
| image          = Simone Silva screencap Escape by Night.png
| caption        = Simon Silva in screen
| director       = John Gilling     
| producer       = Robert S. Baker Monty Berman
| writer         = John Gilling Ted Ray Simone Silva
| music          = Stanley Black
| cinematography = Monty Berman
| editing        = Gerald Landau
| studio         = Southall Studios
| distributor    = Eros Films
| released       = December 1953
| runtime        = 79 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}

Escape by Night is a 1953 British crime film directed and written by John Gilling.

==Plot==
Gino Rossi (James) is an Italian crime boss on the run who hides in a theater with a British journalist (Colleano) who wants to extract his life story.

==Cast==
*Bonar Colleano .....Tom Buchan
*Andrew Ray .....Joey Weston
*Sid James .....Gino Rossi
*Ted Ray .....Mr. Weston
*Simone Silva .....Rosetta Mantania
*Patrick Barr .....Inspector Frampton
*Peter Sinclair .....MacNaughton
*Avice Landone .....Mrs. Weston Ronald Adam .....Tallboy Eric Berry .....Con Blair Martin Benson .....Guillio

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 
 