Naan Aanaiyittal
{{Infobox film
| name           = Naan Aanaiyittal
| image          = Naan Aanaiyittal .jpeg
| caption        =  Chanakya
| producer       = R. M. Veerappan
| writer         = Ve. Lakshmanan Na. Pandurangan
| screenplay     = R. M. Veerappan
| story          = Manjula Nedumaran
| starring       = M. G. Ramachandran B. Saroja Devi K. R. Vijaya M. N. Nambiar R. S. Manohar S. A. Ashokan Nagesh
| music          = M. S. Viswanathan
| cinematography = P. N. Sundaram
| editing        = C. P. Jambulingam
| studio         = Sathya Movies
| distributor    = Sathya Movies
| released       =  
| runtime        = 182 minutes
| country        = India
| language       = Tamil
| budget         = 
}}
 1966 Tamil language drama film directed by Tapi Chanakya |Chanakya. The film features M. G. Ramachandran and B. Saroja Devi in lead roles. The film, produced by R. M. Veerappan under Sathya Movies, had musical score by M. S. Viswanathan.  The film ran for 60 days. 

==Cast==
* M. G. Ramachandran
* B. Saroja Devi
* K. R. Vijaya
* M. N. Nambiar
* R. S. Manohar
* S. A. Ashokan
* Nagesh
* O. A. K. Devar
* Sethupathi

==Soundtrack==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kodukka Kodukka || P. Susheela, M. S. Viswanathan || Ma. Lakshmanan || 03:11
|-
| 2 || Megangal || Seerkazhi Govindarajan, P. Susheela || Alangudi Somu || 03:36
|- Vaali (poet) Vaali || 03:43
|-
| 4 || Nalla Velai || T. M. Soundararajan || 03:19
|-
| 5 || Odi Vanthu || Seerkazhi Govindarajan || 03:35
|-
| 6 || Pattu Varum || T. M. Soundararajan, P. Susheela || 03:46
|-
| 7 || Pirandha Idam || L. R. Eswari || Alangudi Somu || 02:52
|- Vaali || 03:32
|}

==References==
 

==External links==
 

 
 
 
 


 