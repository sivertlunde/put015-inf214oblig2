Nasty Baby
 
{{Infobox film
| name           = Nasty Baby
| image          = 
| caption        = 
| director       = Sebastián Silva (director)|Sebastián Silva
| producer       = Charlie Dibe David Hinojosa Juan de Dios Larraín Pablo Larraín Julia Oh
| writer         = Sebastián Silva
| starring       = Kristen Wiig Sebastián Silva Tunde Adebimpe
| music          = Danny Bensi Saunder Jurriaans
| cinematography = Sergio Armstrong
| editing        = Sofía Subercaseaux
| studio         = Fabula Funny Balloons
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = United States Chile
| language       = English
| budget         = 
}}

Nasty Baby is a 2015 Chilean-American drama film directed by Sebastián Silva (director)|Sebastián Silva. The film premiered at the 2015 Sundance Film Festival and was screened in the Panorama section of the 65th Berlin International Film Festival,     where it was named winner of the Teddy Award for best LGBT-themed feature film.  

The film centers on Freddy (Silva) and Mo (Tunde Adebimpe), a gay couple trying to have a baby with the help of their friend Polly (Kristen Wiig).

==Cast==
* Kristen Wiig as Polly
* Alia Shawkat as Wendy
* Mark Margolis as Richard
* Reg E. Cathey as The Bishop
* Tunde Adebimpe as Mo
* Sebastián Silva (director)|Sebastián Silva as Freddy
* Neal Huff as Gallery Owner
* Judy Marte as Battered Woman
* Anthony Chisholm as David
* Agustín Silva (actor)|Agustín Silva as Chino

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 