Doddmane Hudga
{{Infobox film
| italic title    = no
| name            = Doddmane Huduga
| image           = Dodmane Huduga Poster.jpg
| starring        = Puneeth Rajkumar Ambareesh Bharathi Vishnuvardhan Sumalatha Srinivasa Murthy Radhika Pandit P. Ravi Shankar Chikkanna
| director        = Duniya Soori
| writer          = Duniya Soori Ravikiran Vikas
| cinematography  = Satya Hegde
| producer        = M. Govindu
| editing         = Deepu S Kumar
| studio          = Ajay Pictures
| distributor     = 
| country         = India
| released        = 
| runtime         =
| language        = Kannada
| music           = V. Harikrishna 
| budget          = 
| gross           = 
}} Kannada movie co-written and directed by Duniya Soori. This film features Puneeth Rajkumar, Radhika Pandit in lead roles. Apart from them, the film features an ensemble cast of Ambareesh, Sumalatha, Bharathi Vishnuvardhan, Srinivasa Murthy, P. Ravi Shankar, Chikkanna, Rangayana Raghu in important roles.

This film marks third collaboration of Appu with Soori. The film is being produced by M.Govindu under Ajay Pictures banner. The Soundtrack for the film will be composed by V. Harikrishna. Shashidhar Adapa has been recruited as art director for the film. The principal photography commenced on 5 March 2015.

== Cast ==
* Puneeth Rajkumar
* Radhika Pandit
* Ambareesh
* Sumalatha
* Bharathi Vishnuvardhan
* Srinivasa Murthy
* P. Ravi Shankar
* Rangayana Raghu
* Chikkanna
* Santosh
* Udaya Raghav
* Avinash

==Production==

===Development===
In February 2014, it was reported that Duniya Soori and Puneeth Rajkumar would work together again under Ajay Pictures.  V. Harikrishna was selected to compose the music for this film.The song recording of the was launched at the Prasad Recording Studios in Bangalore on 24 April 2014. Art director Shashidhar Adapa was selected to handle art direction for the film. Satya Hegde was selected to handle the films cinematography.

===Casting===
In July 2014, it was reported that Ramya would play the female lead of the movie but later she walked out of the movie citing "remuneration problems".  After Ramyas exit Radhika Pandit was roped to play lead actress role which marked her second collaboration with Puneeth Rajkumar.  Rebel star Ambareesh and Sumalatha were signed for portraying the parents role of Puneeth Rajkumar in the movie.  Veteran actress Bharathi Vishnuvardhan was chosen to play the role of Ambareeshs sister in the movie.  Srinivasa Murthy, Chikkanna, Rangayana Raghu, P. Ravi Shankar, Avinash, Udaya Raghav and Santosh Aryavardan of Bigboss 2 fame were chosen to play crucial role in the movie. 

===Filming===
The film went on the floors on 5 March 2015, the ‘Holi’ festival day, at the famous Ganesha temple in front of Puneeth Rajkumar in Sadashivanagar, Bangalore. Parvathamma Rajkumar, crazy star V. Ravichandran, Shiva Rajkumar, Raghavendra Rajkumar,Srinivasa Murthy, Bharathi Vishnuvardhan, Radhika Pandit, Vinay Rajkumar, Imran Sardariya and other sandalwood celebrities graced the movie launch event.  Parvathamma Rajkumar did the formal pooja V. Ravichandran gave the clap while Shiva Rajkumar switched on the camera. The Principal Photography of the movie commenced from 5 March 2015 at Bangalore and the first schedule of the movie is expeted to continue for 20 days near Bangalore surroundings.

==Music==
V. Harikrishna was signed in to compose the soundtrack album and background score for the film. The song recording of the movie commenced on 24 April 2014, the day of birth anniversary of legendary Rajkumar (actor)|Dr.Rajkumar at Prasad Studios in Bangalore. Jayanth Kaykini and Yogaraj Bhat will be writing the lyrics for the movie. 

==References==
 

 
 
 
 


 