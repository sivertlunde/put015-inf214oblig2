Netrikkann
{{Infobox film
| name = Netri Kann
| image = Netri Kann.jpg
| caption =
| director = SP. Muthuraman
| writer = K. Balachander Visu (dialogues)
| story  = Visu Lakshmi Saritha Menaka Vijayashanti Sarath Babu
| producer = Rajam Balachander Pushpa Kandaswamy
| music = Ilaiyaraaja
| cinematography = Babu
| editing = R. Vittal
| studio = Kavithalayaa Productions
| distributor = Kavithalayaa Productions
| released = 15 August 1981
| runtime =
| country = India Tamil
| budget =
}}
 1981 Tamil Lakshmi and Saritha in the lead roles. 

== Plot ==
The story revolves around Chakravarthy (Older Rajini) who is a successful textile businessman in Coimbatore. He is a great womaniser (perhaps his only weakness) and picks up any women he wants.  He has wife Meenakshi (Lakshmi), his son Santosh (also Rajini) and his daughter Sangeetha (Vijayashanthi). It doesnt take long for Santosh to find his fathers provocative behavior and tries to mend his fathers ways. Radha (Saritha) gets introduced as a candidate for PRO (Public Relations Officer) interview and eventually gets selected and is sent to Hong Kong for training. Chakravarthy unable to tolerate his sons growing menace also sets off to Hong Kong for a vacation. Here he meets Radha and at one point ends up raping her. Chakravarthy flies back to India where he is met with a number of changes which all points out to the new General Manager. This person turns out be Radha who has joined with Santosh to teach Chakravarthy a lesson for lifetime. How the duo succeed in changing Chakravarthys behaviour forms the crux of the story.

== Cast ==

*Rajinikanth as Chakravarthy/Santhosh (dual roles - father and son) Lakshmi as Meenakshi (Chakravarthys wife)
*Saritha as Radha (the girl who is raped by Chakravarthy) Menaka as Menaka (Santhoshs girlfriend)
*Vijayashanti as Sangeetha (Chakravarthys daughter/Santhoshs sister)
*Goundamani as Singaram (Chakravarthys driver)
*Sarath Babu as Yuvaraj (Guest appearance)
*Thengai Srinivasan (Guest appearance)

==Production==
Netrikann had Rajini playing the roles of father and son. Usually in such films, the father turns out to be the good guy and the son is a bit of a wastrel. Here it is the father who is lecherous and the son is the good guy. Balachander said that the film had a lot of scope for Rajini.  Muthuraman was initially hesitant to direct the film but Balachander encouraged him to direct.  Cameraman Babu introduced the mask shots through this film the film had 90 mask shots. 

== Soundtrack ==
{|class="wikitable"
! Song !! Singer !! Duration !! Lyricist
|---
| Mappillaikku
| Malaysia Vasudevan, Uma Ramanan
| 4:15 Kannadasan
|---
| Raja Rani
| Malaysia Vasudevan, S. P. Sailaja
| 4:09
|---
| Ramanin Mohanam
| K. J. Yesudas, S. Janaki
| 4:13
|---
| Theeratha
| S.P. Balasubrahmanyam
| 4:13
|---
|}

==Reception==
Cinemalead wrote:"Netrikkan, one of the toughest roles ever portrayed by Rajini as a pervert father and righteous son.   Father character was like walking on the edge of the knife as it had chances to go exhibit vulgarity but Rajini gave a mature performance".  Indiaglitz wrote that the film: "trumpeted Rajini is one of the finest wholesome actors of Tamil cinema". 

==Legacy== same name. Vishals characterisation of playboy was similar to Rajinis character from the film. Violin theme of Netrikann was used in this film.

== References ==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 
 


 