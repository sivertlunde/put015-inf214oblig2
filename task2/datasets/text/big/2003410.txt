Shenandoah (film)
{{Infobox film 
| name         = Shenandoah
| image        = Shenandoah 1965 poster.jpg
| caption      = 1965 cinema poster Robert Arthur
| director     = Andrew V. McLaglen
| writer       = James Lee Barrett
| starring     = James Stewart Doug McClure Glenn Corbett Patrick Wayne Katharine Ross  Rosemary Forsyth Frank Skinner
| cinematography = William H. Clothier
| editing      = Otho Lovering Universal
| released     =  
| runtime      = 105 minutes
| language     = English
| country      = United States
| gross        = $17,268,889 
}}

Shenandoah is a 1965 American Civil War film starring James Stewart, Doug McClure, Glenn Corbett, Patrick Wayne, and, in her film debut, Katharine Ross. 
 picture was directed by Andrew V. McLaglen. The cast also includes Rosemary Forsyth in her film debut. The American folk song "Oh Shenandoah" features prominently in the films soundtrack.

Though set during the American Civil War, the films strong antiwar and humanitarian themes resonated with audiences in later years as attitudes began to change toward the Vietnam War. Upon its release, the film was praised for its themes as well as its technical production.

==Plot==
In the state of Virginia in 1864, during the American Civil War, family patriarch Charlie Anderson (James Stewart) and his six sons run the family farm, while his daughter Jennie (Rosemary Forsyth) and daughter-in-law Ann (Katharine Ross) take care of the housework. The family has no slaves. Charlies oldest son Jacob (Glenn Corbett) wants to join the war, but Charlie repeatedly tells his family that they wont join the war until it concerns them. Although a few of the boys want to join, they respect their fathers wishes and remain on the farm. 

Charlies daughter Jennie is courted by a young Confederate officer named Sam (Doug McClure). He wants to marry Jennie, and when Charlie gives his permission, the wedding occurs a few days later. As soon as the vows are said, a corporal rides up and announces that Sam is wanted back immediately. Sam leaves, much to the sorrow of his new bride.
 the river. When a union patrol comes on them, Boy is taken as a prisoner of war. Gabriel runs to tell the Andersons what happened. When Charlie hears the news, he and his sons and daughter Jennie leave to look for Boy, leaving James (Patrick Wayne) and his wife Ann at the farm with their young baby.

Boy is taken to a prisoner of war camp. He is befriended by rebel soldier Carter (James Best), who plans to escape and decides to let Boy come along. They and a few other men successfully make it out of the camp and start heading south.

Charlie Anderson, still looking for Boy, overpowers the Union soldiers on a train carrying prisoners. He looks through the boxcars, searching for his son. Boy is not there, so he mounts his horse to leave. As he looks up, he sees young Sam coming through the crowd. Jennie is overjoyed to see her husband. Sam leaves with the Andersons, telling the soldiers to burn the train and go home. 

Coming onto a Confederate camp, they are involved in a skirmish. Carter is killed, and Boy himself is shot in the leg. When a Union soldier comes along about to kill him, he looks up into the face of his friend, Gabriel. Gabriel helps him off into a bush to hide until after the battle.

Back at the farm, scavengers raid the place, killing both James and Ann. On their way home, the Andersons run across a Confederate patrol. A young sentry, startled by the sound of horses, takes a shot at Jacob, killing him instantly. Charlie starts to kill the sentry, but stops to ask him his age. The sentry replies, "Sixteen, sir." Charlie, remembering that his youngest son is sixteen, emotionally tells the sentry that he wants him to live and be an old man and have many sons. He wants him to know what it feels like to lose one of them.

When the Andersons return home, the doctor tells them what happened to James and Ann. Their child Martha is still alive, and Charlie takes her in his arms. Next day at the breakfast table, Charlie begins his standard prayer, but is so heartbroken that he cant finish it. He goes out to the family graveyard to see his wifes grave. He sees the graves of James, Jacob and Ann alongside hers, and he hears church bells ringing in the distance.  

At the farmhouse, he demands to know why no one told him it was Sunday. The whole family gets dressed and ready for church, arriving as the singing begins. As the congregation completes the first song, the pastor (Denver Pyle) starts to announce the next hymn. Boy stumbles through the back door on a crutch. The whole congregation looks, and Charlie Anderson turns to see what is happening. His face lights up, and he helps his son to the pew. Everyone joyously sings in unison as the story ends.

==James Stewart==
In view of the decidedly anti-war tone of this film, and that of the character of Charlie Anderson, it is worth noting that James Stewart was a brigadier general in the US Air Force Reserve at the time of its filming, and had been a decorated bomber pilot and squadron commander in World War II; he would fly as an observer and additional pilot on one B-52 mission in Vietnam as part of his reserve duty a year after the films release.

Stewarts own son Ronald (adopted from his wifes first marriage) would be killed in action in Vietnam as a US Marine Corps officer a few years later. Stewart had four children, including twin daughters Kelly Stewart and Judy Stewart-Murray, who once appeared with their parents on the 1960s television quiz show Password. He adopted his wife Glorias two sons from a previous marriage — Ronald (5) and Michael (2) — when they married.

The anti-Vietnam War connection was not made by the average moviegoer at the time of the films release in 1965.  The beginning of the American ground war in Vietnam is generally considered to be March 8, 1965, when 3,500 United States Marines were dispatched to South Vietnam. During this initial period, U.S. public opinion overwhelmingly supported the deployment. Anti-war sentiments were still several years away. 

== Cast ==
* James Stewart as Charlie Anderson
* Doug McClure as Sam
* Glenn Corbett as Jacob Anderson
* Patrick Wayne as James Anderson
* Rosemary Forsyth as Jennie Anderson
* Phillip Alford as Boy Anderson
* Katharine Ross as Ann Anderson Charles Robinson as Nathan Anderson
* Jim McMullan as John Anderson
* Tim McIntire as Henry Anderson
* Eugene Jackson Jnr. as Gabriel
* Paul Fix as Dr. Tom Witherspoon
* Denver Pyle as Pastor Bjoerling
* George Kennedy as Col. Fairchild
* James Best as Carter, Rebel Soldier
* Tom Simcox as Lt. Johnson
* Berkeley Harris as Capt. Richards
* Harry Carey, Jr. as Jenkins (rebel soldier)
* Kevin Hagen as Mule (deserter)
* Dabbs Greer as Abernathy
* Strother Martin as Train Engineer
* Kelly Thordsen as Federal Purchasing Agent Carroll

==Awards== Academy Award Best Sound (Waldon O. Watson).    For her part in Shenandoah, Rosemary Forsyth was nominated for a Golden Globe for Most Promising Newcomer - Female.

==Adaptations==
 Broadway musical in 1975, which won John Cullum his first Tony Award for Best Actor .

==Production notes==
* Location scenes filmed near Eugene, Oregon
* Working titles: Fields of Honor and Shenandoah Crossing.
* The film broke box office records in Virginia, the storys locale.

==References==
 

== External links ==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 