Urudhi Mozhi
{{Infobox film
| name           = Urudhi Mozhi
| image          =
| caption        =
| director       = R. V. Udayakumar
| producer       = Ravi Yadav
| writer         = 
| screenplay     = 
| story          =  Prabhu Geetha Geetha Sivakumar Bharathi
| music          = Ilayaraja
| cinematography = 
| editing        = 
| studio         = Yaathavaalayaa
| distributor    = Yaathavaalayaa
| released       =  
| runtime        = 
| country        = India Tamil
}}
 1990 Cinema Indian Tamil Tamil film, Bharathi in lead roles. The film had musical score by Ilayaraja.  

==Cast== Prabhu
* Geetha
* Sivakumar Bharathi
* Sarath Kumar

==Soundtrack==
The music was composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Chithra || RV. Udayakumar || 04.37
|- Janaki || RV. Udayakumar || 04.31
|- Janaki || RV. Udayakumar || 03.15
|-
| 4 || Maarelo Mari ||  || RV. Udayakumar || 02.17
|-
| 5 || Anbu Kathai || S. P. Balasubrahmanyam || RV. Udayakumar || 04.36
|-
| 6 || Takkara Achadi || S. P. Balasubrahmanyam || RV. Udayakumar || 04.50
|-
| 7 || Dhinakku Dhinakku (Pathos) || Ilaiyaraaja || RV. Udayakumar || 02.29
|}

==References==
 

==External links==
*  
 

 
 
 
 
 


 