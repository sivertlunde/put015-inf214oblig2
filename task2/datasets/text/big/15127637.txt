Stowaway (1936 film)
{{Infobox film
| name = Stowaway
| image =
| caption =
| director = William A. Seiter
| producer = Buddy G. DeSylva
| writer = Screenplay:   Robert Young Alice Faye
| music = Harry Revel Mack Gordon
| cinematography = Arthur C. Miller
| editing = Lloyd Nosler
| distributor = 20th Century Fox
| released =  
| runtime = 87 minutes
| country = United States
| language = English
}}
 1936 American Robert Young) in Shanghai and then accidentally stows away on the ocean liner he is travelling on. The film was hugely successful,   and is available on videocassette and DVD.

==Plot==
Barbara "Ching-Ching" Stewart is an orphan living in Sanchow, China. When bandits threaten, she is sent to Shanghai for safety. Accidentally separated from her guide, Ching-Ching finds herself in Shanghai all alone with her dog until she meets a westerner, Tommy Randall, a rich playboy traveling about the world by ocean liner. Ching-Ching then accidentally becomes a stowaway on his ship. When discovered, she is provided for by Tommy and Susan Parker, a passenger on the ship engaged to the son of her traveling companion, Mrs Ruth Hope. Susan and Tommy become romantically involved. Ching-Ching plays Cupid in furthering their romance. The couple realize they adore Ching-Ching and want to do the best for her after learning she will be put off the ship and sent to an orphans asylum. Susan breaks her engagement with Richard Hope, Ruths son, after discovering his selfish nature and marries Tommy. The two adopt Ching-Ching.

==Cast==
*Shirley Temple as Barbara "Ching-Ching" Stewart, a young American girl orphaned in China Robert Young as Tommy Randall, a playboy and world traveler, Barbaras adoptive father
*Alice Faye as Susan Parker, Mrs Hopes shipboard companion and the wife of Tommy Randall, Barbaras adoptive mother
*Helen Westley as Mrs Hope, Susans companion 
*Allan Lane as Richard Hope, the son of Mrs. Hope and Susans ex-fiance
*Eugene Pallette as the Colonel, Tommys friend
*Astrid Allwyn as Kay Swift, Tommys friend
*Jayne Regan as Dora Day, Tommys friend
*Arthur Treacher as Atkins, Tommys valet
*Philip Ahn as Sun Lo, Barbaras friend in Sanchow
*Willie Fung as Chang, Sun-los friend and a boatman
*Robert Greig as Captain of SS Victoria 
*J. Edward Bromberg as Judge J. D. Booth
*Layne Tom Jr. as Chinese Boy in the Musical Band (uncredited)

==Production==
Temple learned forty words in Mandarin Chinese for the film, later stating the learning process required six months of instruction. She was taught by UCLA student Bessie Nyi. She encountered problems in her communication with the extras on the set however, as she found out they were actually speaking a south Chinese dialect. In the film, she impersonates Ginger Rogers (with a life-sized male doll fixed to her toes), Eddie Cantor, and Al Jolson singing “My Mammy|Mammy”. In the first take, the elastic band of the dummy, which she named Tommy Wonder, snapped off her foot. In preparation for the Jolson imitation, she had to listen and watch Jolson, something she did not enjoy doing. 

Production of the movie was held up for close to four weeks while first Alice Faye then Shirley Temple came down with the flu.  

The dog in the film, a miniature Chinese Pekinese which was owned by the wife of a local photographer, was given to Temple and renamed Ching-Ching (after her character in the movie). Temples mother worked out a trade in which Temple and her father would agree to pose for the photographer in exchange for the dog. 
 genius classification. 

==Music== Myra Breckinridge. Other songs are "Please" by Ralph Rainger and Leo Robin (sung by a Chinese performer in a theater), and the films finale, "Thats What I Want for Christmas" by Gerald Marks and Irving Caesar (sung by Temple).

==Release==

===Critical reception===
Variety (magazine)|Variety remarked, “It’s a nifty Shirley Temple comedy with musical trimmings.”  Variety commented, “Whether or not due to Seiter’s efforts,   does not appear to have outgrown   the Little Miss Marker stage in this one as she had in her last pictures”. 

The New York Times applauded the film, noting that Temple had “an amusing script behind her, an agreeable adult troupe with her, and a clever director before her.” The reviewer thought the film the best from Temple since Little Miss Marker 

===Home media===
In 2009, Stowaway was available on videocassette and DVD in the black and white original and in computer-colorized versions of the original. Some versions featured theatrical trailers and other special features.

==See also==
* Shirley Temple filmography

== Notes ==
 

== References ==

*  
*  

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 