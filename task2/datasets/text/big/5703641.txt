Wild America (film)
 
{{Infobox film
| name           = Wild America
| image          = Wild_America_poster.jpg
| caption        = Wild America theatrical poster
| director       = William Dear
| producer       = Gary Barber James G. Robinson Irby Smith Mark Stouffer Steve Tisch Bill Todman Jr.
| writer         = David Michael Wieger
| starring       = Jonathan Taylor Thomas Devon Sawa Scott Bairstow
| music          = Joel McNeely
| cinematography = David Burr
| editing        = O. Nicholas Brown
| studio         = Morgan Creek Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 106 min.
| country        = United States English
| budget         = 
| gross          = $7,324,662 
}} 1997 adventure comedy film directed by William Dear, written by David Michael Wieger, and starring Jonathan Taylor Thomas, Devon Sawa and Scott Bairstow.

==Plot==
  Marty (Scott Bairstow). The two oldest brothers love using Marshall to film him in stunts, which he dislikes. Occasionally Marty and Mark will show footage of their antics in their garage to all their friends. Mark and Marty have a dream of filming dangerous animals around the country, and the dream starts when they find a rare, special camera in a shop. Agnes (their mother) loans them the money she was saving up and they begin planning their trip. Their father is against this idea.

The brothers then want to take the next step by traveling across the country, but again, the father says no. Mark and Marty finally convince their father to let them go. All set, Marty and Mark begin their trip using the familys beat up 1956 Chevrolet Suburban and are surprised to find out Marshall couldnt leave them and comes along. The mother is worried, but lets him go. The overall goal of the trip in Martys mind is to find a legendary cave located somewhere in the western states said to be filled with hundreds of bears. Marty learns of it when reading an article in a wildlife magazine, which took it from a trappers journal from the 19th century.

The three brothers start camping. First, they miss a shot at catching an eagle, then go to film some alligators, and start by seeing a man who was attacked by an alligator. As they go in a swamp on a boat, Mark throws some bait but it lands in the trees, trying to retrieve it, his clothing gets stuck in a branch underwater and he starts to drown, Marshall and Marty drive the boat, but it crashes into another branch, which sends Marshall flying into the water. Marshall gets a knife from Marty and cuts Mark loose, but Marshall is now dealing with a bigger problem; he and the alligator are face to face. Marshall is able to get back on the boat in time. When they get back to the hut, the alligator man (Strango) tells them about how back when he served in the Korean War he befriended a fellow soldier named Phil. Strango and Phil would exchange stories about their wilderness adventures. Strango would talk about hunting alligators and Phil would tell tall tales about bears. This rouses Martys attention and he asks about it. Strango states that Phil was talking about a cave full of hundreds of bears somewhere "out West."

The brothers then stop at a beachside grill on the southern coast of Louisiana where they get a small dose of the Vietnam War fueled counter culture of the 1960s. Mark catches the eye of two British women who are hippies. They take the brothers to a nude beach. Marty and Mark and the two hippies go swimming while Marshall guards the truck and the camera. After the swim the brothers get back on the road much to Marks reluctance.

They drive northwest until they reach Devils Playground in Colorado, "the last home of the wild American wolf." Devils Playground is located on government protected land. They catch footage of a wolf creeping up on a doe. Then as the wolf is about to ambush the doe there is a series of explosions. The brothers look up and see two F-4 Phantoms flying overhead. The pilots see the brothers and turn around, firing missiles at them eventually hitting a giant boulder knocking the three down. As they get up a herd of wild horses comes thundering towards them. They get in the truck just in time to film it. When the horses pass Marshall sees an owl that looks a lot like his owl Leona. The three follow it and discover a cave. On the wall of the cave is an ancient Indian drawing of a cave filled with bear-shaped figures. Marty and Mark draw it on Marshalls chest and show it to an old Indian woman. The woman tells them that its located near Arapaho Peak in Montana.

The brothers try to attract bears, but end up attracting a baby moose that Marshall thinks is a deer. As Marshall starts to pet it, its mother comes and starts chasing him. Marshall tries to escape it, but he gets on its head and antlers as the moose runs into a river. Marshall is about to drown until Bigfoot the Mountain Man saves him. That night around the campfire Marty and Bigfoot talk about bears and the cave. Bigfoot dismisses the cave as a myth but tells them to talk to Carrie Stokes whose husband was killed by a bear. The next day the brothers drive to Stokess cabin in Willis Peak. Stokes tells the story of how she was mauled by a bear. Her fiance, Judd then went after the bears in their cave but was killed by them two days before the two were to be married (September 3). When they return to the truck the brothers discover they were robbed of their food, sleeping bags, and money, except for the film. This leads to a fight between Mark and Marty, which ends up with Mark having a minor fracture in his leg.

Dismayed by Marks broken leg, the brothers begin to head home. As they drive Marshall sees a woman laying down flowers near a grave, and reminds the brothers what Carrie does on that day, and they go back and follow her to the cave. They get in and film the bears, but wake all of them up, making them angry. They get some good shots, and sing a song their dad would sing which puts the bears to sleep. Just as they are ready to leave, bats come and attack them. Marshall distracts one of the grizzlies, and tells the other two to leave, because he "has a way out".

The two brothers get out safely, but Marshall has a problem trying to escape the bear as he crawls through a small tunnel in the cave. He gets out, and gets the camera back. The three brothers are home, but Marshall (who was supposed to fly a plane with his dad one day) is driving, and imagines himself flying a plane. He crashes into a few mailboxes, and their dad has to pick them up.
They get back home, and their mom tells them she rented the school gym for them to show what they filmed.

While driving, their father crashes his truck and ends up in the hospital. Marshall teaches himself how to fly a plane, and flies over the hospital so his dad can see. This made his mom amused, as he talks to his dad about how he should let his boys have freedom and let Mark and Marty do what they want to do.

They display their film, and everyone claps, but when their enemy DC makes a rude comment, their dad begins to applaud, having the crowd cheer and clap.
Marshall and his Dad smile at each other at the end.

==Cast==
*Jonathan Taylor Thomas as Marshall Stouffer
*Devon Sawa as Mark Stouffer Marty Stouffer Jr.
*Frances Fisher as Mrs. Agnes Stouffer
*Jamey Sheridan as Mr. Marty Stouffer, Sr.
*Tracey Walter as Leon, the Stouffers farmhand and neighbor
*Zack Ward as D.C.
*Marshall Stouffer (uncredited) as Ruffian #1
*Mark Stouffer (uncredited) as Ruffian #2
*Marty Stouffer (uncredited) as Ruffian #3
*Danny Glover (uncredited) as Bigfoot the Mountain Man

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 