Demonic (film)
{{Infobox film
| name           = Demonic
| image          = Demonic.movieposter.jpg
| alt            = 
| caption        = 
| director       = Will Canon 
| producer       = Lee Clay James Wan
| story          = James Wan Max La Bella
| screenplay     = Max La Bella Doug Simon Will Canon 
| starring       = Maria Bello Frank Grillo Cody Horn Dustin Milligan Megan Park Scott Mechlowicz Aaron Yoo Tyson Sullivan Alex Goode
| music          = Dan Marocco 
| cinematography = Michael Fimognari 
| editing        = Josh Schaeffer 
| studio         = Icon Entertainment International First Point Entertainment IM Global
| distributor    = Dimension Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $3 million 
| gross          = 
}}
 2015 United American horror film directed by Will Canon and written by Max La Bella, Doug Simon and Will Canon. The film stars Maria Bello, Frank Grillo, Cody Horn, Dustin Milligan, Megan Park, Scott Mechlowicz, Aaron Yoo, and Alex Goode. 

==Plot==
A violent slaughter occurs in an abandoned house in Louisiana where six college students are found dead. Detective Mark Lewis (Frank Grillo) examines the crime scene and finds a shocked survivor, John (Dustin Milligan) before calling reinforcements. Several police cars then arrive the house alongside with an ambulance and a psychologist, Dr. Elizabeth Klein (Maria Bello). She talks to John, who tells her that a séance performed in the house called the attention of many spirits, including one he identified as his mother.

The following scenes consist of two interlaced plots: one shows the interrogation of John by Dr. Klein and the investigation of videotapes found in the house by police officers, and the other shows the actual events (often filmed by a camera) that John and the five other visitors of the house experienced, including several paranormal phenomena.

== Cast ==		
*Maria Bello as Dr. Elizabeth Klein
*Frank Grillo as Detective Mark Louis
*Cody Horn as Michelle
*Dustin Milligan as John
*Megan Park as Jules
*Scott Mechlowicz as Bryan 
*Aaron Yoo as Donnie
*Tyson Sullivan as Luke Elton
*Alex Goode as Sam
*Ashton Leigh as Sara Mathews
*Terence Rosemore as Jenkins
*Jesse Steccato as Peter
*Meyer DeLeeuw as Henry
*Griff Furst as Reeves

== Production ==
On May 13, 2011, the film was announced under the title House of Horror, with James Wan set to produce.  On November 22, 2011, it was announced Xavier Gens would direct the film.  On January 23, 2013, Maria Bello and Frank Grillo joined the cast.  On October 8, 2013, the film was retitled Demonic.  On July 31, 2014, it was announced that the film would be released on December 12, 2014.  On September 19, 2014, the film was pushed back to an unknown date. 

== Release ==
The film premiered on 12 February 2015 in Brazil under the title A Casa dos Mortos. 

== References ==
 

== External links ==
*  

 
 
 
 
 