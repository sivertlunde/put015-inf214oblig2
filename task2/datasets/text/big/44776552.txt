Samara (film)
{{Infobox film|
| name = Samara
| image = 
| caption =
| director = Chi Guru Dutt
| writer =  J. Damodaran
| story = J. Damodaran
| starring = Shivrajkumar  Sudharani   Devaraj
| producer = Praveen A Gurjer   Varun A Gurjer   Anil Pagdi
| music = Kausthubha   Sax Raja ( )
| cinematography = R. Madhusudhan
| editing = P. Bhaktavatsalam
| studio = A A Combines
| released =  
| runtime = 149 minutes
| language = Kannada
| country = India
| budget =
}} Kannada action film directed by Chi Guru Dutt with the association of P. Sheshadri. The film features Shivarajkumar, Sudharani and Devaraj in the lead roles.   

== Cast ==
* Shivarajkumar
* Sudharani
* Devaraj
* Srinivasa Murthy
* Nandini Singh
* Thej Sapru
* Jai Jagadish
* Ajay Gundurao
* Malavika Avinash

== Soundtrack ==
The soundtrack of the film was composed by Kousthubha and the background score was by Sax Raja. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| collapsed = no
| title1 = Mai Marethu
| extra1 = S. P. Balasubrahmanyam& K. S. Chithra
| lyrics1 = 
| length1 = 
| title2 = Aa Aaa Ee Eee
| extra2 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics2 = 
| length2 = 
| title3 = Kanasinalu Neene
| extra3 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics3 = 
| length3 = 
| title4 = Ide Dim Dima
| extra4 = Manjula Gururaj
| lyrics4 = 
| length4 = 
| title5 = Kannadada Maathe Chenda Rajkumar
| lyrics5 = 
| length5 = 
}}

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 

 