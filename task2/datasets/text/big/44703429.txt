Neelakanta (film)
{{Infobox film
| name = Neelakanta
| image = 
| caption =
| director = Om Sai Prakash
| writer = Rajkiran
| starring = V. Ravichandran  Namitha   Sridevika 
| producer = K. Bala Muttaiah
| music = V. Ravichandran
| cinematography = G. S. V. Seetharam
| editing = S. Manohar
| studio = Sri Dhanalakshmi Creations
| released =  
| runtime = 153 minutes
| language = Kannada
| country = India
| budget =
}}
 romantic drama drama film directed by Om Sai Prakash and produced by K. Bala Mutthaiah. The film stars  V. Ravichandran, Namitha and Sridevika in the leading roles. 
 Tamil film Aranmanai Kili (1993) directed and written by Rajkiran.  The music was composed by V. Ravichandran.

The film was dubbed and released in Tamil as Brahmandam.

== Cast ==
* V. Ravichandran as Neelakanta
* Namitha 
* Sridevika  Sujatha 
* Umashri
* Avinash
* Lamboo Nagesh
* Kashi
* Sadhu Kokila
* Ramesh Bhat
* M.N Lakshmi Devi
* Vijay Kashi
* Ashalatha
* Muniraj

== Soundtrack ==
The music was composed and lyrics written by V. Ravichandran.  A total of 6 tracks have been composed for the film and the audio rights brought by Jhankar Music.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| collapsed = no
| title1 = Hennige Seere Yaake Anda
| extra1 = Srinivas (singer)|Srinivas, Suma Shastry
| lyrics1 = V. Ravichandran
| length1 = 
| title2 = Ammammmamo
| extra2 = S. P. Balasubrahmanyam, Nanditha
| lyrics2 = V. Ravichandran
| length2 = 
| title3 = Malla Malla Malla
| extra3 = Chaitra H. G.
| lyrics3 = V. Ravichandran
| length3 = 
| title4 = Devaru Bareda Katheyali
| extra4 = S. P. Balasubrahmanyam, Nanditha
| lyrics4 = V. Ravichandran
| length4 = 
| title5 = Andada Bombege
| extra5 = S. P. Balasubrahmanyam
| lyrics5 = V. Ravichandran
| length5 = 
| title6 = Devaru Bareda (sad)
| extra6 = S. P. Balasubrahmanyam
| lyrics6 = V. Ravichandran
| length6 = 
|}}

== References ==
 

 

== External links ==
*  
*  

 
 
 
 
 
 


 

 