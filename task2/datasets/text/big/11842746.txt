Ladies They Talk About
{{Infobox film
| name           = Ladies They Talk About
| image          = Ladies They Talk About.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Howard Bretherton
* William Keighley
}}
| producer       = Raymond Griffith  
| screenplay     = {{Plainlist|
* Brown Holmes
* William McGrath
* Sidney Sutherland
}}
| story          = {{Plainlist|
* Dorothy Mackaye
* Carlton Miles
}}
| starring       = {{Plainlist|
* Barbara Stanwyck
* Preston Foster
* Lyle Talbot
}}
| music          = Cliff Hess   John Seitz
| editing        = Basil Wrangell
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Ladies They Talk About is a 1933 Pre-Code American crime drama directed by Howard Bretherton
and William Keighley, and starring Barbara Stanwyck, Preston Foster, and Lyle Talbot. Based on the play Women in Prison by Dorothy Mackaye and Carlton Miles, the film is about an attractive woman who is a member of a bank-robbery gang. She confesses her crime to an acquaintance, who sends her to prison because he loves her.

==Plot==
Nan Taylor (Barbara Stanwyck) is a member of a gang of bank robbers, posing as an regular customer to distract the security guard while her accomplices take the money. Her cover is blown by a policeman who had arrested her before, and she is arrested. Reform-minded radio star David Slade (Preston Foster) falls in love with her and gets her released as a favor from District Attorney Simpson (Robert McWade). However, when she confesses that she is guilty, Simpson has her imprisoned. 
 escape plot and Don is shot dead as he gets to Nans cell to break her out.  Nan is given another year, and is not allowed visitors, but vows to seek revenge on Slade.

When she is released, Nan goes to a revivial meeting of Slades group.  He is glad to see her, and she is escorted to a back room where he professes his love for her.  She scoffs and accuses him of turning in her bank robber accomplices, and she shoots him in the arm hoping to kill him.  Linda sees this from outside via a keyhole, but Slade denies that he has been shot and Slade and Nan announce their intention to marry.

== Cast ==
* Barbara Stanwyck as Nan Taylor 
* Preston Foster as David Slade 
* Lyle Talbot as Gangster Don
* Dorothy Burgess as Sister Susie
* Lillian Roth as Prisoner Linda
* Maude Eburne as Aunt Maggie
* Ruth Donnelly as Prison Matron 
* Harold Huber as Lefty Simons
* Robert McWade as District Attorney Simpson

==Production notes==
Unlike most films of the women in prison genre, Taylors fellow inmates are criminals, rather than innocents in prison by mistake.   

== Reception ==
The New York Times said "When a reformer and a dashing female bank bandit fall in love, their home life may be somewhat as illustrated in the lingering finale of Ladies They Talk About,   After a torrid argument in which Nan, the gun-girl, accuses her beloved of frustrating a jail-break in which two of her pals were killed, she loses her temper, draws a gun from her handbag and shoots him. I didnt mean to do that, Nan remarks a moment later as David Slade falls to the floor with a bullet in his shoulder. Why, thats all right, Nan, responds her husband-to-be. Its nothing."   "It is in the prison scenes that the film provides some interesting drama. Ladies They Talk About is effective when it is describing the behavior of the prisoners, the variety of their misdemeanors, their positions in the social whirl outside, their ingenuity in giving an intimate domestic touch to the prison, and their frequently picturesque way of exhibiting pride, jealousy, vanity and other untrammeled feminine emotions." 

==Remake==
The film was remade in 1942 under the title Lady Gangster, starring Faye Emerson.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 