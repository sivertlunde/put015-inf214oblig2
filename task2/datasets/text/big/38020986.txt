Who Killed Santa Claus?
{{Infobox film
| name           = Who Killed Santa Claus?
| image          = WhoKilledSantaClaus.jpg
| image_size     = 
| caption        = Film poster
| director       = Christian-Jaque
| producer       = Continental Films
| writer         = Charles Spaak
| starring       = Harry Baur Raymond Rouleau Robert Le Vigan Fernand Ledoux
| music          = Henri Verdun
| cinematography = Armand Thirard
| editing        = René Le Hénaff
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
}}
Who Killed Santa Claus? (LAssassinat du père Noël) is a 1941 French drama film by Christian-Jaque.  This adaption of Pierre Vérys novel of the same name was the first film produced by Continental Films.

==Plot==
On the evening of December 24, in a small village of Savoy buried under snow, Gaspard Cornusse, a maker of globes, is preparing to play each year the role of Santa Claus, while his daughter Catherine is sewing doll dresses while dreaming of a prince charming. Meanwhile the return to his castle of the mysterious Baron Roland is the subject of many a conversation…

==Cast==
* Harry Baur as Gaspard Cornusse
* Raymond Rouleau as Roland de La Faille
* Renée Faure as Catherine Cornusse
* Robert Le Vigan as Léon Villard, the schoolmaster
* Fernand Ledoux as Noirgoutte, the mayor
* Jean Brochard as Ricomet, the pharmacist
* Héléna Manson as Marie Coquillot
* Arthur Devère as Tairraz, the watchmaker
* Marcel Pérès as Rambert
* Georges Chamarat as Constable Gercourt
* Bernard Blier as police sergeant
* Jean Sinoël as Noblet
* Marie-Hélène Dasté as Mother Michel
* Jean Parédès as Kappel, the beadle

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 