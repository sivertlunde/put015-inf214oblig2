Children of the Pyre
 

{{Infobox film
| name           = Children of the Pyre
| image          = 
| image_size     =
| caption        =
| director       = Rajesh S. Jala
| producer       = Rajesh S. Jala
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Rajesh S. Jala
| editing        = 
| distributor    = 
| released       = 2008
| runtime        = 74 minutes
| country        = India
| language       = Hindi
| budget         = US$250,000
| preceded_by    =
| followed_by    =
}}

Children of the Pyre (Ta paidia tis pyras) is a 2008 film documentary directed and produced by Rajesh S. Jala. The film documents the stories of seven children who cremate dead bodies and steal cremation shrouds at Indias largest crematorium, Manikarnika, on the banks of the Ganges.   Jalal, also the cinematographer, shot over 100 hours of footage at the crematorium and surrounding sites, including candid interviews with the seven children described in the film, who discuss their difficult life cremating dead bodies and stealing shrouds from the bodies brought to the crematorium and reselling them to merchants for a nominal fee.  In 2009, the filmmaker launched a project to improve the lives of the 300 children working at the crematorium. 

The film was awarded "Best Documentary" at the 2008 Montréal World Film Festival, 2008 Sao Paulo International Film Festival, 2008 Indian Film Festival of Los Angeles, 2009 Asiatica Film Mediale, 2009 IDPA, and the "Silver Lotus Award" for "Best Audiography" at the 56th National Film Awards. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 