Almost Friends
{{Infobox film
| name           = Almost Friends
| image          = 
| caption        =
| director       = Anne Le Ny
| producer       = Bruno Levy   Etienne Mallet   Nicolas Lesage   David Gauquie   Franck Elbase   Julien Deris 
| writer         = Anne Le Ny Axelle Bachman 
| starring       = Karin Viard   Emmanuelle Devos   Roschdy Zem   Anne Le Ny
| music          = Éric Neveux
| cinematography = Jérôme Alméras 	 
| editing        = Guerric Catala 
| studio         = Move Movie   Mars Films   Cinéfrance 1888   France 2 Cinéma
| distributor    = Mars Distribution
| released       =  
| runtime        = 91 minutes
| country        = France
| language       = French
| budget         = € 5.41 million 
| gross          = 
}}

Almost Friends ( )  is a 2014 French comedy-drama film written, directed by, and starring Anne Le Ny, along with Karin Viard, Emmanuelle Devos and Roschdy Zem. It was shot in and around Orléans. 

== Plot ==
Marithé works in a training centre and her task is to assist people who are seeking alternative job options. One day, Carole arrives at the centre. She has never completed her studies and is starting to feel overshadowed by her husband, Sam, a talented Michelin-starred chef. It seems that it is not a job that Carole needs, but rather about her asserting her own independence and to leave her husband; and Marithé does all to help Carole to set out down a new path. However, things become complicated when the man Marithé is secretly attracted to is Sam.

== Cast ==
* Karin Viard as Marithé Bressy 
* Emmanuelle Devos as Carole Drissi
* Roschdy Zem as Sam Drissi
* Anne Le Ny as Nathalie Perusel 
* Philippe Rebbot as Pierre Perusel 
* Philippe Fretun as Michel
* Annie Mercier as Jackie
* Marion Lécrivain as Dorothée
* Yan Tassin as Théo
* Marion Malenfant as Cynthia
* Xavier de Guillebon as Vincent
* Xavier Béja as Pascal
* Alain Stern as The real estate agent
* Pierre Diot as The jogger
* Adeline Moreau as The waitress Jonathan Cohen as The supervisor

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 