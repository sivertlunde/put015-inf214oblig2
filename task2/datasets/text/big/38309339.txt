Sunbai Oti Bharun Ja
 

{{Infobox film
| name           = Sunbai Oti Bharun Ja - Marathi Movie
| image          = Sunbai Oti Bharun Ja.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Dinkar D.Patil
| producer       = Ram Karve
| screenplay     = 
| story          = 
| starring       = Usha Chavan Chandrakant Madre Avinash Masurekar Rajshekhar Nilu Phule
| music          = Bal Palsule
| cinematography = 
| editing        = 
| studio         = Everest Entertainment
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Sunbai Oti Bharun Ja is a Marathi movie released on 11 April 1979.  Produced by Ram Karve and directed by Dinkar D.Patil. The movie is based on a tale of a womans true love and dignity, with lots of twists and turns.

== Synopsis ==
Vishwanath and Shekhar are the best of friends and Vishwanath is in love with Chanchal, who, true to her name is like a chameleon. Watching his friend trapped in a love affair, Shekhar hires Champa, a tamasha dancer to act as Vishwanaths lover. Soon this turns into reality, but somehow Vishwanath’s father manages to separate them, unaware of the fact that Champa is carrying Vishwanath’s child.

One day when she is out hunting for a job she is saved from a gang of goons by Dada, who takes her to his house little knowing that this same woman is his daughter in law. On learning the identity of Dada, Champa leaves the house.

== Cast ==

The cast includes Usha Chavan,  Chandrakant Madre, Avinash Masurekar, Rajshekhar, Nilu Phule & Others.

==Soundtrack==
The music is provided by Bal Palsule. 

== References ==
 
 

== External links ==
*  

 
 
 


 