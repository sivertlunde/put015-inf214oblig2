First Lady (film)
{{Infobox Film
| name           = First Lady
| image_size     = 
| image	=	First Lady FilmPoster.jpeg
| caption        = 
| director       = Stanley Logan
| producer       = Harry Joe Brown (uncredited associate producer)
| writer         = George S. Kaufman (play) Katharine Dayton (play) Rowland Leigh
| narrator       = 
| starring       = Kay Francis Preston Foster Anita Louise Walter Connolly Verree Teasdale
| music          = 
| cinematography = Sidney Hickox
| editing        = 
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       = December 4, 1937
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $485,000   accessed 16 March 2014 
| gross          = $424,000  
}} play of the same name by George S. Kaufman and Katharine Dayton.

==Plot== Secretary of State Stephen Wayne (Preston Foster). She tries to gain the support of rising Senator Gordon Keane (Victor Jory), a victory that would be doubly sweet inasmuch as he is the protégé of her despised arch-rival, Irene Hibbard (Verree Teasdale). 
 Supreme Court Grant Mitchell) adds his support, and Carter is indeed offered the nomination.

Lucy learns that Prince Boris Gregoravitch (Gregory Gaye), Irenes ex-husband, is in Washington for negotiations. Learning something interesting from the prince, she has her husband invite the foreign envoy to the dinner in which Carter is to announce his acceptance of the nomination. Gregoravitch is delighted to see Irene and gives her some "good" news. On behalf of his country, he has reached an agreement with the United States in which both sides will recognize each others laws. Once the treaty is signed, he and Irene will be considered divorced by the American legal system. Until then however, Irene is technically a Bigamy|bigamist. Lucy blackmails Irene into getting Carter to decline the nomination, leaving the way free for her husband.

==Cast==
 
 
*Kay Francis as Lucy Chase Wayne
*Preston Foster as Stephen Wayne
*Anita Louise as Emmy Page, Lucys niece
*Walter Connolly as Carter Hibbard
*Verree Teasdale as Irene Hibbard
*Victor Jory as Senator Gordon Keane
*Marjorie Rambeau as Belle Hardwick
*Marjorie Gateson as Sophy Prescott
*Louise Fazenda as Lavinia Mae Creevey
*Henry ONeill as Judge George Mason Grant Mitchell as Ellsworth T. Banning
*Eric Stanley as Senator Tom Hardwicke
*Lucile Gleason as Mrs. Mary Ives (as Lucille Gleason)
*Sara Haden as Mrs. Mason Harry Davenport as Charles
*Gregory Gaye as Prince Boris Gregoravitch
*Olaf Hytten as Bleeker

==See also==
*Rachel Jackson, who married future President Andrew Jackson in the mistaken belief that she was divorced
==Reception==
According to Warner Bros records the film only earned $322,000 in the US and Canada and $102,000 elsewhere. 
==References==
 
==External links==
 
* 
* 
* 

 
 
 
 
 
 