Petey Wheatstraw (film)
 
 
{{Infobox Film name = Petey Wheatstraw image_size = 
| image	=	Petey Wheatstraw FilmPoster.jpeg caption =  director = Cliff Roquemore producer = Burt Steiger   Theodore Toney writer = Cliff Roquemore narrator =  starring = Rudy Ray Moore Jimmy Lynch Leroy Daniels G. Tito Shaw Ernest Mayhand music =  cinematography =  editing =  distributor =  released = North America: November, 1977 runtime = 94 min. country = United States language = English budget =  gross =  followed_by =
}}

Petey Wheatstraw (1977) aka Petey Wheatstraw, the Devils Son-In-Law is a Blaxploitation film written by Cliff Roquemore, starring popular Blaxploitation genre comedian Rudy Ray Moore, along with Jimmy Lynch, Leroy Daniels, Ernest Mayhand, and Ebony Wright. It is typical of Moores other films Dolemite and The Human Tornado from the same era, in that Rudy Ray Moore rhymes nearly every sentence in the movie with the next one.

== Plot ==

Petey Wheatstraw (Rudy Ray Moore) is born during a great Miami hurricane, and after a difficult labor by his mother, emerges as a talking, diaper-wearing six-year-old boy who promptly attacks the doctor, and then his father for "disturbing me in my sleep every night."  His mother puts him in his place, and names him "Petey Wheatstraw."

While a teenager, Petey meets a mentor "Bantu" who teaches him the philosophy of "Kung Fu," and self-respect - vowing to bow before no man.  Petey grows up to become a successful nightclub comedian, who books a series of shows at a club in Los Angeles called "Steves Den" - much to the dismay of comedy rivals Leroy and Skillet, who have just borrowed a large sum of money from the Mob to finance their own opening at another club.  Realizing that their show is likely to fail with Petey in town (and therefore default on the loan), they beg Petey to delay his act.  When Petey refuses to do so, Leroy and Skillet send out their henchmen to convince Petey otherwise.  

Leroy and Skillets henchmen gun down Peteys business partner Teds little brother Larry, and then attempt to wipe out Petey himself at the boys funeral by machine gunning the entire party.  Mortally wounded, Petey is visited by "Lou Cipher" - the Devil himself - who tells him that his death was a mistake.  He is willing to undo Peteys death on one condition - that Petey marry the Devils daughter and provide him with a grandson.  Petey nearly quashes the deal when he sees the picture of the Devils ugly daughter, but hears the words of Bantu - and decides to make a deal.

Petey and his friends are brought back to life, and Petey tells them of the Devils deal and his plans to gain revenge on Leroy and Skillet - and trick the Devil by not marrying his daughter.

Armed with the Devils own magic "Pimp Cane," Petey sets out to exact his revenge.

==External links==
* 

 
 
 
 
 
 
 
 