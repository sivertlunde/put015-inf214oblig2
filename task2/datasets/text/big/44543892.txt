Huli Hejje
{{Infobox film 
| name           = Huli Hejje
| image          =  
| caption        = 
| director       = K. S. L. Swamy (Ravee)
| producer       = Shashirekha
| writer         = K. Dharmaraj
| screenplay     = K. Dharmaraj Vishnuvardhan Tiger Prabhakar Vijayalakshmi Singh Srigeetha
| music          = Vijaya Bhaskar
| cinematography = Purushottham B Valke
| editing        = Yadav Victor
| studio         = Navanidhi Chithra
| distributor    = Navanidhi Chithra
| released       =  
| runtime        = 116 minutes
| country        = India Kannada
}}
 1984 Cinema Indian Kannada Kannada film, directed by K. S. L. Swamy (Ravee) and produced by Shashirekha. The film stars Vishnuvardhan (actor)|Vishnuvardhan, Tiger Prabhakar, Vijayalakshmi Singh and Srigeetha in lead roles. The film had musical score by Vijaya Bhaskar.  

==Cast==
  Vishnuvardhan
*Tiger Prabhakar
*Vijayalakshmi Singh
*Srigeetha
*Jai Jagadish
*M. V. Vasudeva Rao
*Sundar Krishna Urs
*Sudheer
*Dinesh
*Doddanna
*Master Ravi
*Pramila Joshai
*Shashikala
*Theresamma
*Suman
*Chayadevi
*Mallika
*Shivaprakash
*Bhatti Mahadevappa
*Ashwath Narayan
 

==Soundtrack==
The music was composed by Vijayabhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vishnuvardhan || Dodda Range Gowda || 04.27
|-
| 2 || Hoovu Mullu || S. P. Balasubrahmanyam, Vani Jayaram || R. N. Jayagopal || 04.36
|-
| 3 || Ahamevaasmi || PB. Srinivas, Vani Jayaram || PB. Srinivas || 04.14
|-
| 4 || Amma Amma || S. P. Balasubrahmanyam || R. N. Jayagopal || 04.52
|-
| 5 || Begane Ba || Narayan, Veena || SU. Rudramurthy Shastry || 04.46
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 