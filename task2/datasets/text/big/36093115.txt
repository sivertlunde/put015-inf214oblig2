Evaraina Epudaina
 
{{Infobox film 
| name        = Evaraina Epudaina 
| image       =
| caption     = 
| writer    = Marthand K Shankar
| starring    = Varun Sandesh Vimala Raman Giri Babu Rama Prabha Ali
| director    = Marthand K Shankar
| producer    = M. Saravanan (film producer)|M.Sarvanan   MS Guhan
| distributor = AVM Productions
| cinematography = Venugopaalan
| released    =  
| country     = India
| language    = Telugu
| music    = Mani Sharma
|}}
Evaraina Epudaina ( English: Anyone Anytime) is a 2009 Telugu romantic comedy film directed and written by Marthand K Shankar. It stars Varun Sandesh and Vimala Raman, released on 26 June 2009.

==Plot==
Venkat (Varun Sandesh) is care free guy, he is a smart youngster who looks for people around him who are in love, he blackmail to tell their family and takes money from the couples. He gets into a fight because he took money from a couple by blackmailing, there he sees a girl Madhumita (Vimala Raman) and it is love at first sight for Venkat. Meanwhile his grandma gets hurt and he takes her to a hospital, an old woman is admitted in the same room, later he finds out she is Madhumitas grandma (Rama Prabha). He gets an opportunity to impress her, he pretends to save her grandma and they both become good friends. Venkats close friend (Adarsh) confesses him that he is in love with the daughter of Madhumitas father (Giri Babu). He thinks that his friend is going to be engaged to Madhumita. This prompts Venkat to play a spoilsport in the engagement ceremony, he tries to break the engagement and also succeeds in beaking the engagement. Soon after the engagement breaks he realised that his friend is in love with Madhumitas sister and was getting engaged to her. Venkat is shocked and is in a fixed situation as he cant tell the truth to Madhumita and her family as they would hate him after knowing the truth. He gets close to her family in meantime, also Madhumita falls in love with him. In temple she overhears Venkats friend telling him that it was a good plan to break Adarsh and Madumitas sisters engagement. Venkat sees Madhumita and tries to explain to her that it was an misunderstanding but she was not ready to listen. Venkat then become a responsible person and joins his brothers office, he respects lovers and understands others feeling. He also manages to get Adarsh and Madhumitas sister together in meantime. He then goes to Madhumita and explains to her and try to convince her that everything was a misunderstanding, he tells her that he is changed person and has got a good job for her. She understands him and his feelings, the film ends with Venkat and Madhumita getting together.

==Cast==
* Varun Sandesh as Venkat
* Vimala Raman as Madhumita
* Rama Prabha as Madhumitas grandma
* Giri Babu as Madhumitas father
* Ali as Shivam (Madhumitas brother-in-law)

==Soundtrack==
Evaraina Epudaina held its audio launch on the night of the 1st of June 2009, the event was held in Green park Hotel, Hyderabad.

{{tracklist
| headline        = Tracklist (Telugu version)
| extra_column    = Singer (s)
| total_length    = 
| lyrics_credits  = yes
| title1  = Akasamalo
| lyrics1 = Krishna Chaitanya Tippu
| title2  = Malli Malli
| lyrics2 = Rahaman
| extra2 = Rita, Rahul Nambiar
| title3  = Na Manase
| lyrics3 = Bhaskarabhatla Ranjith 
| title4  = Madhurayathana Veturi
| extra4 = Rita
| title5  = Varevaa
| lyrics5 = Rahaman
| extra5 = Rita
| title6  = Nelalu Garu
| lyrics6 = Vennelakanti Malavika
| title7  = Vaanemo Thadisi 
| lyrics7 = Krishna Chaitanya
| extra7 = Varun Sandesh
| title8  = Nara Naramentilaa 
| lyrics8 = Krishna Chaitanya
| extra8 = Rahul Nambiar
}}

 

 
 