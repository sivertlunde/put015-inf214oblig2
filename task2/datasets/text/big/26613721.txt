3 Apples Fell from the Sky
{{Infobox film
| name           = 3 Apples Fell from the Sky
| image          = 3ApplesFilmPoster.jpg
| caption        = Theatrical poster
| director       = Raşit Çelikezer
| producer       = Raşit Çelikezer 
| writer         = Raşit Çelikezer
| starring       = Ismail Hacioglu Bennu Yildirimlar Ayten Uncuoglu
| cinematography = Mustafa Nuri Eser
| studio         = Defne Film
| distributor    = Tiglon Film
| released       =  
| runtime        = 88 minutes
| country        = Turkey
| language       = Turkish
}}
3 Apples Fell from the Sky ( ) is a 2008 Turkish drama film produced, written and directed by Raşit Çelikezer.

== Plot ==
Ali, a young petty thief runs away from home and seeks refuge with his grandfather in Istanbul. His grandfather, an ex-military disciplinarian who at first did not even recognize his own grandson, is involved in a feud with his upstairs neighbor Nilgun, a middle aged prostitute. We learn they are not quite what they appear to be in a spiraling chain of events that makes these three seemingly antagonistic characters come closer and closer together.

== See also ==
* 2008 in film
* Turkish films of 2008

==References==
 

==External links==
*  

 
 
 
 

 
 