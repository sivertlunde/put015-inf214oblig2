Sindhoora Thilaka
{{Infobox film
| name           = Sindhoora Thilaka
| image          = 
| caption        = 
| director       = Om Sai Prakash
| producer       = K. Satyaranayana
| screenplay     = 
| story          = R. V. Udayakumar Shruti
| music          = Upendra Kumar
| cinematography = Johny Lal
| editing        = Narasaiah
| studio         = R S Arts
| distributor    = 
| released       =  
| runtime        = 133 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}	

Sindhoora Thilaka ( ) is a 1992 Indian Kannada language film directed by Om Sai Prakash, starring Sunil and Malashri in the lead roles. The supporting cast features Shruti (actress)|Shruti, Jaggesh and Abhijeeth. The music was composed by Upendra Kumar.  
 Tamil film Kizhakku Vasal directed by R. V. Udayakumar and starring Karthik Muthuraman|Karthik, Revathy and Kushboo (actress)|Kushboo. 

==Cast==
* Sunil
* Malashri Shruti
* Jaggesh
* Mukhyamantri Chandru
* Abhijeeth
* Jai Jagadish
* Umashri
* Asha Rani
* Padma Kumuta
* Om Sai Prakash

==Soundtrack==
{{Infobox album
| Name        = Sindhoora Thilaka
| Type        = Soundtrack
| Artist      = Upendra Kumar
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Lahari Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Upendra Kumar composed the films background score and music for the soundtracks. The lyrics were written by R. N. Jayagopal. The album consists of six soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Suggi Bandare
| lyrics1 =R. N. Jayagopal
| extra1 = S. P. Balasubrahmanyam
| length1 = 
| title2 = Sindhoora Thilaka
| lyrics2 =R. N. Jayagopal
| extra2 = Sangeetha Katti
| length2 = 
| title3 = Naa Seeti Hodidare
| lyrics3 = R. N. Jayagopal
| extra3 = S. P. Balasubrahmanyam
| length3 = 
| title4 = Pancharangi Giniye
| lyrics4 = R. N. Jayagopal
| extra4 = S. P. Balasubrahmanyam
| length4 = 
| title5 = Sobaana Haaduve
| lyrics5 = R. N. Jayagopal
| extra5 = S. P. Balasubrahmanyam
| length5 = 
| title6 = Sobaana
| lyrics6 = R. N. Jayagopal
| extra6 = Chorus
| length6 =
}}

==References==
 

==External source==
*  

 
 
 
 
 
 

 