Sweet Sixteen (2002 film)
 
 
{{Infobox film
| name           = Sweet Sixteen
| image          = Sweet sixteen film.jpg
| director       = Ken Loach
| producer       = Rebecca OBrien
| writer         = Paul Laverty
| starring       = Martin Compston Annmarie Fulton William Ruane
| music          = George Fenton
| cinematography = Barry Ackroyd
| editing        = Jonathan Morris
| studio         = Alta Films BBC Films Road Movies Filmproduktion Scottish Screen Sixteen Films Tornasol Films
| distributor    = Icon Film Distribution
| released       =  
| runtime        = 106 minutes   
| country        = United Kingdom Germany Spain
| language       = Scots English
| gross          = $316,319 
}}
Sweet Sixteen is a 2002 drama film directed by Ken Loach. The film tells the story of a teenage boy from a troubled background, Liam, who dreams of starting afresh with his mother who is completing a prison term. Liams attempts to raise money for the two of them are set against the backdrop of Greenock, Port Glasgow and the coast at Gourock.

==Plot== Scottish teenager Liam will turn 16. The film opens with him using his tripod-mounted telescope outdoors on a clear night to show other children the stars and planets. He and his friends exemplify the violent "Ned (Scottish)|ned" subculture; they no longer attend school, but instead, hang around isolated areas or wander about all day long. They get money by illicitly selling untaxed cigarettes in a pub, and defy the police. Liams mother is currently in prison, for a crime she did not commit. She will be released in a few weeks, in time for her sons 16th birthday. She has a boyfriend named Stan, who works as a drug dealer with Liams grandfather, Rab.
 Cornton Vale Prison, and try to force him to smuggle drugs to his mother while they create a distraction. In the event, Liam refuses to cooperate by passing the drugs over. When driving home his companions beat him up; he fights back and gets away. Liam arrives back to find that he has been expelled from his grandfathers apartment|flat, and his belongings thrown down into the front garden (including his telescope, which has been broken). Liam then moves to his sister Chantelles nearby home in Port Glasgow. Chantelle agrees to let Liam live in her house if hes good for Chantelles little son, Callum. She has been taking free evening classes to get work in a call centre, and implores Liam to do the same because she wants Liam to do something more constructive with his own life.
 joyriding along caravan (trailer) park where Liam sees a caravan for sale in a spot overlooking the scenic Firth of Clyde. Liam, who loves his mother very much, fantasizes that he, his sister, and his mother can  escape to the seaside and live in the caravan, away from Stan and Rabs wrath. To purchase it he and Pinball steal a delivery of drugs from Stans house and sell them, doing the very things Liam once hated – claiming that they will never go anywhere by selling cheap cigarettes. They soon they develop entrepreneur skills and raise several thousand pounds, which they pay as a deposit towards the caravan in Liams mothers name.

Liams efforts attract the attention of the local drug godfather. Liam, who only wanted a peaceful life with his mother, agrees to work with them after the local godfather tells him to stay away from our shops. Pinball, meanwhile, is thrown into the health club showers due to his disrespectful manner towards the dealer, and vows revenge. Liam and Pinball carry on selling drugs to the local area, with the help of Liams other friends who deliver pizzas. Liam and Pinball meet again with members of the drug godfathers gang, and Liam joins them in their car. Pinball is kicked out, angering him further; the gang members advise Liam to dump Pinball for good. They take Liam to a Glasgow nightclub and instruct him that he has to kill someone to join the gang. Liam attempts to do so, but is stopped by the gang, who inform him it was a test (which he has passed).

Liam, Chantelle, Callum and Suzanne (Chantelles friend) drive to the caravan to have a picnic, only to discover that its burned down. Liam believes it was Stan who burnt it down, and throws a rock through his window. That evening, Pinball turns up in the drug godfathers (stolen) car, telling Liam that he wants revenge. He proceeds to crash the car into the health club. Liam speaks to the godfather in the morning and, to his chagrin, is ordered to "take care of" the Pinball problem (i.e. to kill his friend). The next morning, Pinball - aware of Liams intentions - first tries to stab Liam, then proudly tells him that hes the one who burnt down the caravan, not Stan. He then cuts his own face in rage. Liam is seen reassuring his injured friend after phoning for an ambulance, but in the next scene he notifies the godfather that the deed has "been done", leaving a viewer to infer that he has indeed murdered his friend.  

The godfather promises to buy Liam an upscale apartment, and on the day before his birthday Liams mother is released from the prison and taken to this new house on the coast of Gourock where she is welcomed with a party. She appears uneasy, and the next morning is found to have escaped to Stans house. Liam blames this on Chantelle. Chantelle, now fully aware that Liam is dealing drugs, attempts to warn her little brother about their mother probably not being so thankful for Liams efforts because she is too devoted to Stan, but this only provokes Liam even further. An enraged Liam goes to Stans house, trying to convince his mother to get back to their new home, only to get receive insults from Stan. In a struggle, Liam stabs Stan.

Liam is then seen walking alone on the stony beach. He is phoned by Chantelle, who reminds him that the day is his 16th birthday. She also tells him that the police has been looking for him, but that after everything that he has done, Chantelle still loves him. He walks towards the sea.

==Cast==
* Martin Compston as Liam
* Annmarie Fulton as Chantelle
* William Ruane as Pinball

==Reception==
Sweet Sixteen received very positive reviews, currently holding a 97% "fresh" rating on Rotten Tomatoes; the consensus states: "A bleak, but heartbreaking coming-of-age tale that resonates with truth." 

Sweet Sixteen won the Best Screenplay Award at the 2002 Cannes Film Festival.

Sweet Sixteen inspired Italian singer-songrwriter Lucio Dalla for the 2007 song Liam.

===Criticism of BBFC classification===
Use of the word "fuck" (313 times) and "cunt" (about 20 times) led the British Board of Film Classification to forbid the film to viewers under 18. Spain followed this decision, but other countries, like France or Germany (not under 12) had a different rating system. Ken Loach and Paul Laverty protested against the British procedure in the Guardian.

Laverty asserted that this was "censorship" and "class prejudice" because he got a lot of information to write his scenario from people around Scotland, many of whom were not 18, and were therefore denied the opportunity to see the film.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 