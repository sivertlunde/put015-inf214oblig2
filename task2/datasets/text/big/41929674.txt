Dance Raja Dance
{{Infobox film|
| name = Dance Raja Dance
| image = 
| caption =
| director = Dwarakish
| writer = Dwarakish
| starring = Vinod Raj  Divya   Devaraj
| producer = Dwarakish
| music = Vijay Anand
| cinematography = R. Deviprasad
| editing = Gowtham Raju
| studio = Dwarakish Chitra
| released = 1987
| runtime = 134 min
| language = Kannada
| country =  India
| budgeBold textt =
}}
 dance film directed and produced by Dwarakish for his Dwarakish Chitra banner. The film starred newcomers Vinod Raj, Divya and Sangeetha in lead roles.

The film released to positive response by critics and audience and the lead star Vinod Raj was lauded for his dancing skills. The dance moves of the song "Dance Dance Raja Dance" from this film was an inspiration to the widely popular video game    For this movies climax was shot by Helicopter, apart from that they used 5 cameras to shoot this climax scene. Actually it was Dwarakishs Dream to shoot mysore from aerial angle. He dropped that idea due to financial crisis. But Then Chief minister Mr.Ramakrishna hegde came to know this dream and he helped Dwarakish to do so. He called his minister Siddaramayya ( present chief minister) to go mysore with helicopter. He went with the helicopter and told Dwarakish to utilize the helicopter.

== Cast ==
* Vinod Raj as Raja
* Divya
* Sangeetha Srinath
* Devaraj
* Sundar Raj
* Keerthi Raj

== Soundtrack ==
The music was composed by Vijay Anand with lyrics by Chi. Udaya Shankar and R. N. Jayagopal. Music director A. R. Rahman, who was Dilip then, had assisted Vijay Anand on the keyboards 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Dance Dance Raja Dance
| extra1 = S. P. Balasubramanyam
| lyrics1 = Chi. Udaya Shankar
| length1 = 
| title2 = Elli Neenu Alli Naanu
| extra2 = S. P. Balasubramanyam, S. Janaki
| lyrics2 = R. N. Jayagopal
| length2 = 
| title3 = Ee Namma Baale
| extra3 = S. P. Balasubramanyam, K. S. Chithra
| lyrics3 = Chi. Udaya Shankar
| length3 = 
| title4 =Om Namah Shivaya
| extra4 = S. P. Balasubrahmanyam
| lyrics4 = Chi. Udaya Shankar
| length4 = 
| title5 = Amma Amma 
| extra5 = S. P. Balasubramanyam
| lyrics5 = Chi. Udaya Shankar
| length5 =
| title6 = Naa Adalu
| extra5 = S. P. Balasubramanyam
| lyrics5 = R. N. Jayagopal
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 

 