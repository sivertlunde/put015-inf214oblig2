Sneakers (2011 film)
 
{{Infobox film
| name           = Sneakers
| image          = Sneakers (2011 film) film poster.jpg
| caption        = Film poster
| director       = Valeri Yordanov Ivan Vladimirov
| producer       = Kiril Kirilov Galina Toneva
| writer         = Valeri Yordanov
| starring       = Ivo Arakov
| music          = 
| cinematography = Rali Raltschev
| editing        = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Ivo Arakov as Wee
* Phillip Avramov as The Bird
* Ivan Barnev as Ivo
* Vasil Draganov as Fatso
* Iva Gocheva as The Barmaid
* Ina Nikolova as Emi
* Yana Titova as Polly
* Marian Valev as H
* Valeri Yordanov as Gray

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Bulgarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 