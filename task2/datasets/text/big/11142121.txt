Madanolsavam
{{Infobox film
| name           = Madanolsavam
| image          =
| image_size     =
| caption        =
| director       = N. Shankaran Nair
| producer       = RM Sundaram for RMS FILMS
| writer         = N. Shankaran Nair
| narrator       =
| starring       = Kamal Haasan,  Zarina Wahab,  Thikkurisi Sukumaran Nair
| music          = Original Score & Songs:  
| cinematography = J. Williams  
| distributor    = Vijaya Movies
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Madanolsavam is a Malayalam film written and directed by N. Shankaran Nair starring Kamal Haasan and Zarina Wahab in lead roles. Dialogues are penned by Thoppil Bhasi and comedy scenes by Adoor Bhasi.  The film was dubbed into Tamil as Paruva Mazhai, Telugu as Amara Prema and Hindi as Dil Ka Saath Dil.

This movie has got evergreen songs like Mada prave vaa.., Sandye kanneerithende sandya..., Mele Poomala..., Ee Malarkanyakal..., Sagarame santhamaka née..
 Love Story, written by Erich Segal.

==Cast==
* Kamal Haasan as Raju
* Zarina Wahab as Elizabeth
* Jayan as Dr. Sukumar
* Thikkurissy Sukumaran Nair as Ambadi Rajasekharan Thampi
* Sankaradi as De Cruz
*Meenakumari as Mariyamma
* Adoor Bhasi as Frederick March
* Prathapachandran as Dr. Radhakrishnan
* Mallika Sukumaran as Rajasekharan Thampis wife
* Sreelatha Namboothiri
* Kottayam Santha
* P. K. Abraham
* Pattom Sadan

==Soundtrack==
The soundtrack features six songs composed by Salil Choudhary, with lyrics penned by ONV Kurup. 
;
{| class="wikitable"
|-
! Title !! Singer(s)
|-
| Mada prave vaa|| K J Yesudas 
|- S Janaki
|-
| Mele Poomala|| K J Yesudas, Sabita Chowdhury
|- S Janaki
|-
| Nee Mayum nilavo || K J Yesudas
|-
| Sagarame santhamaka née || K J Yesudas
|}

==External links==
 

 
 


 