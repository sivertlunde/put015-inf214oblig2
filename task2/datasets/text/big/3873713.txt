Lee Dae-ro Can't Die
{{Infobox film
| name         = Lee Dae-ro Cant Die
| image        = Lee Dae-ro Cant Die.jpg
| caption      = Poster to Lee Dae-ro Cant Die (2005)
| film name     = {{Film name
| hangul       =  ,  
| rr           = Lee Dae-ro, juneul sun eobda
| mr           = Yi Tae-ro, chuŭl sun ŏpta}}
| writer       = Hwang Jo-yun
| starring     = Lee Beom-soo Byeon Ju-yeon Choi Seong-guk Lee Young-eun
| producer     = Kim Dong-ju Kim Young-wun Kim Sun-min
| cinematography = Hong Jong-gyeong
| distributor  = ShowEast
| released     =  
| runtime      = 108 minutes
| country      = South Korea
| language     = Korean
| music        = Han Jae-kwon
| budget       =
| gross        =   
}}
Lee Dae-ro Cant Die ( ; also known as Short Time) is a 2005 South Korean action film about a corrupt police officer who is told he has only three months to live, and plots his own death so his wife can collect his insurance policy. The film was released to South Korean cinemas on August 18 and received a total of 838,419 admissions nationwide. 

The film is similar to that of the 1990 American dark comedy film, Short Time.

==Plot==
An officer in the violent crimes division, Dae-ro is a hero in his daughter Hyun-jis eyes, but in fact hes a corrupt cop, interested only in bribe money and pretty women. He is totally selfish and takes great pains to keep himself out of harms way, avoiding the danger inherent in his job. One day, while in pursuit of a suspect, Dae-ro faints and is taken to the hospital. There he is told that he has a brain tumor and has about three months to live at most. To provide for his daughters financial security, Dae-ro plots his own death that will appear accidental so that she will collect a sizable insurance premium.

== References ==
 

== External links ==
*  
*  

 
 
 
 


 