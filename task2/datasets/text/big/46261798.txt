La Belle saison
{{Infobox film
| name           = La Belle saison
| image          = 
| caption        = 
| director       = Catherine Corsini
| producer       = Elisabeth Perez
| writer         = Catherine Corsini Laurette Polmanss 
| starring       = Cécile de France   Izïa Higelin
| music          = Grégoire Hetzel 
| cinematography = Jeanne Lapoirie
| editing        = 
| studio         = Chaz Productions France 3 Cinéma Artémis Productions Solaire Production
| distributor    = Pyramide Distribution
| released       =   
| runtime        = 100 minutes
| country        = France Belgium
| language       = French
| budget         = 
| gross          = 
}}

La Belle saison is an upcoming French-Belgian drama film written and directed by Catherine Corsini, starring Cécile de France and Izïa Higelin.   

== Cast ==
* Cécile de France as Carole  
* Izïa Higelin as Delphine  
* Noémie Lvovsky 
* Laetitia Dosch
* Kévin Azaïs
* Sarah Suco 
* Patrice Tepasso

== Production ==
Principal photography took place in Limousin and in Paris from 22 July 2014 to 16 September. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 