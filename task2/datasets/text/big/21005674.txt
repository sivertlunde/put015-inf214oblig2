Jack Rio (film)
{{Infobox Film
 | name = Jack Rio
 | image = Jack Rio Poster.jpg
 | caption = Dying to meet him? Be careful what you wish for...
 | director = Gregori J. Martin
 | writer = Gregori J. Martin Matthew Borlenghi
 | starring = Matthew Borlenghi, Mary Kate Schellhardt Sean Kanan Nadia Bjorlin
 | distributor = Black Hat Productions
 | budget =
 | released = 13 June 2008 ( USA )
 | runtime =
 | language = English
  | }}

Jack Rio is a 2008 film directed by Gregori J. Martin. The film is based on a short film written and directed by the films lead actor Matt Borlenghi.

==Plot synopsis==
Tommy Jamison the lead character is a famous actor, who is the star of a T.V. soap opera called "The Incredible Jack Rio". He has played the lead for 4 years, however he is sick of playing the role and how people never even call him by his real name instead use the name of his character. The pressure from fans, the loss of his identity and the pressure to be Jack, has become too much for him. Jamison wants to leave the show that has made him famous. When a string of murders seem to follow in his wake, the question is asked does Tommy have an obsessed stalker, killing in his honour?

==External links==
*  

 
 
 


 