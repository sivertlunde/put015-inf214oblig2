The Death of Poor Joe
 
{{Infobox film
| name           = The Death of Poor Joe
| image          = Earliest Dickens film - The Death of Poor Joe (1901).webm
| caption        = Full film George Albert Smith
| producer       = 
| writer         = 
| starring       = Laura Bayley Tom Green
| cinematography = 
| editing        = 
| distributor    = Warwick Trading Company
| released       =  
| runtime        = One minute   
| country        = United Kingdom
| language       = Silent
| budget         = 
}}
 short silent silent drama George Albert Smith, which features the directors wife Laura Bayley as Joe, a child street-sweeper who dies of disease on the street in the arms of a policeman.    The film, which went on release in March 1901, takes its name from a famous photograph posed by Oscar Rejlander after an episode in Charles Dickens Bleak House and is the oldest known surviving film featuring a Dickens character.     The film was discovered in 2012 by British Film Institute curator Bryony Dixon, after it was believed to have been lost since 1954.     Until the discovery, the previous oldest known Dickens film was Scrooge, or, Marleys Ghost, released in November 1901.   

==Cast==
* Laura Bayley as Joe
* Tom Green as the policeman

==See also==
* List of rediscovered films

==References==
 

==External links==
*  
*   in the BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 
 
 


 
 