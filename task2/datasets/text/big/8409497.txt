Asterix & Obelix: Mission Cleopatra
 
{{Infobox film
| name=Asterix & Obelix: Mission Cleopatra  (Astérix & Obélix : Mission Cléopâtre)
| image=Mission_cleopatre_affiche.jpg
| caption= 
| director=  Alain Chabat
| producer= Claude Berri
| writer= Alain Chabat
| based on =  
| starring= Gérard Depardieu Christian Clavier Jamel Debbouze Édouard Baer Monica Bellucci Alain Chabat
| released= 2002
| music= Philippe Chany
| cinematography= Laurent Dailland
| distributor= Miramax Films (USA) Pathé (France/UK)
| runtime= 107 min.
| country = France, Italy
| language= French
| budget         = $50.3 million
| gross          = $110,989,615  
}}

Asterix & Obelix: Mission Cleopatra ( ) is a 2002 French film based on the comic book Asterix and Cleopatra|Astérix et Cléopâtre by René Goscinny and Albert Uderzo and a sequel for the 1999 movie Asterix & Obelix Take On Caesar. It was the most expensive French movie at that time. It is the second adaption of Asterix and Cleopatra, following the 1968 animated movie.

The film was directed by Alain Chabat, written by Chabat, and starred Gérard Depardieu, Christian Clavier, Jamel Debbouze, Édouard Baer, Monica Bellucci, and Chabat. It was co-produced by French companies Katharina / Renn Productions, TF1 Film Productions and German companies KC Medien and CP Medien AG. It sold 14,557,020 tickets in France.

Unlike other live action adaptions of Asterix books, up to now, this movie is the only Asterix live action movie where the plot adheres closely to the original comic book Asterix and Cleopatra|Astérix et Cléopatre without incorporating elements from other books.

He received positive reviews, holding a 86% of positive reviews on Rotten Tomatoes, based on 7 reviews.

== Translations == George and Ringo Starr|Ringo) just before a cry of "lets rock and roll!"

The English language subtitles for other markets are a straight translation of the French dialogue, with only the names changed to match the original English translation of the album. The version released on DVD in the Australia by Madman Entertainment contains two discs, one the original French version (with multiple language subtitles) and the other the Miramax edit.


== Plot ==
Queen Cleopatra has had enough with Caesars insults over Egypt and makes a deal with him: if the Egyptians manage to build a palace larger than Caesars palace in Rome in three months, he would aknowledge that Egypt was the greatest people. To perform this task, Cleopatra hires the architect Numerobis to finish the palace; finishing the palace in time would earn him gold, failure would have him thrown to the crocodiles. Much to the dismay of Pyradonis, Cleoptaras architect, Numerobis is appointed architect of the work on the palace. Seeing that only creating the basics of the palace, he and his secretary Papyris discuss a wizard who makes a special potion, granting those who drink it unimaginable powers and abilities. With that in mind, Numerobis begins his journey far north.

Far up in north, in the little village of Gaul, two Roman legionnares ambush a Gaul an tells him to "Defend himself". The Gaul simply smiles and takes a sip from a little bottle he carries on his belt. As the Romans ask him what that was, the Gaul informs the new Romans that it was a magic potion, which gives him special powers. As another Gaul arrives with a dog, the Gaul punches the Roman, sending him flying away. The other Gaul tells the remaining Roman that he does not need a magic potion, because he fell into the cauldron when he was a little boy, meaning he would be a little bit stronger than the others, before simply giving him a little snap with his fingers, sending the other Roman flying.

At the same time, Numerobis finds the two Gauls and asks questions about the wizard. The Gauls are revealed to be Asterix and Obelix, and the dog is Dogmatix. As they show Numerobis their village, he encounters Getafix, the druid who is responsible for the magic potion. Refusing to sell some of the magic potion, Getafix instead says that he will accompany Numerobis back to Alexandria, as he has always wanted to see the Egyptian city. Asterix and Obelix (along with Dogmatix who Obelix smuggled with) journey to Alexandria as well, encountering the pirates along the way.

When they arrive, Numerobis introduces the Gauls to Papyris, some of the rules the Egyptian has and to Queen Cleopatra herself. While they greet her, one of the maids (Guimieukis) arrive and Asterix falls in love with her. As they observe the workers, Pyradonis is successful in starting a riot. In response, Panoramix makes his magic potion, effectively increasing the building speed on the palace. Angered about this, Pyradonis hires Torturis to lead and trap the Gauls in the pyramid. As the Gauls struggle to get out, Getafix decides to finally let Obelix have a taste of the magic potion, allowing him to breach the wall. However, they are unable to find their way out until Dogmatix arrives and leads them out in a cartoon-similar way (due to the cartoon eyes seen in the dark). As they prepare themselves for the journey back to Alexandria, Caesar is informed of the Gauls and their aid in building the palace. Refusing to lose the bet, Caesar leads an assault against the unfinished palace, planning to destroy it.

As the Romans charge, Asterix and Obelix fend them off for as long as they can before Panoramix decides to send Asterix to Alexandria to deliver the letter to Cleopatra. While Asterix runs and plows his way through a Roman garrison, he drops his vial of potion, and Criminalis drinks it. Meanwhile, Panoramix gives Numerobis some potion as well in order to help Obelix against the Romans, but he instead meets Criminalis who intends to kill him. As Asterix resumes the journey to Alexandria on a horse-carriage, Numerobis and Criminalis battle in a comic-brutal style, with Numerobis emerging victorius after kicking Criminalis into a wall, making him look like one of the painted figures on the wall.

At the same time, Asterix arrives at the palace of Cleopatra and she receives the letter, immediately traveling to the palace in order to stop Caesar from destroying the palace. As she arrives, Caesar tries to explain the situation, but Cleopatra cuts him off. As Numerobis is allowed to continue the work on the palace along the Gauls, they are able to finish it in the three months they had been given, and Caesar aknowledges the Egyptians as the mightiest of all people. While Numerobis and Papyris take the elevator down, Caesar and Cleopatra witness the view before they kiss.

In the epilogue, all the characters (exept Criminalis, whos still stuck in the wall) go to a bar, where they celebrate their work on the palace.

== Cast ==
{| class="wikitable"
|-
! Character !! Actor name
|- Asterix
| Christian Clavier
|-
! Obelix
| Gérard Depardieu
|-
! Numerobis / Edifis
| Jamel Debbouze
|-
! Otis
| Édouard Baer
|-
! Cleopatra
| Monica Bellucci
|-
! Julius Caesar
| Alain Chabat
|-
! Panoramix
| Claude Rich
|-
! Amonbofis
| Gérard Darmon
|-
! Idea
| Isabelle Nanty
|-
! Chamandra
| Noémie Lenoir
|-
! The taster
| Dominique Besnehard
|-
! Narrator
| Pierre Tchernia
|-
|}

== Voice cast ==
{| class="wikitable"
|-
! Character !! English cast
|-
! Asterix
| David Cobourn
|-
! Obelix
| Dominic Fumusa
|-
! Numerobis
| Jamel Debbouze
|-
! Cleopatra
| Diane Neal
|-
! Julius Caesar
| T. Scott Cunningham
|-
! Panoramix
| Phillip Proctor
|-
! Amonbofis
| Tom Weiner
|-
! Idea
| Isabelle Nanty
|-
! Narrator
| Erik Bergmann
|-
|}

==Soundtrack== Mission Cleopatra" - Snoop Dogg and Jamel Debbouze
* "Asterix and Cleopatra" - Philippe Chany
* "I Got You (I Feel Good)" - James Brown
* "Yakety Sax" - Boots Randolph The Imperial March (Darth Vaders Theme)" - John Williams
* "Ti amo" - Umberto Tozzi Chi mai"- Ennio Morricone

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 