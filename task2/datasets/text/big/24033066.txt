Arizona Days
 
{{Infobox film
| name           = Arizona Days
| image          =Arizona Days (1928) - McGowan & Custer.jpg
| image_size     =200px
| caption        =McGowan and Custer in the film
| director       = J.P. McGowan
| producer       = J. Charles Davis II
| writer         = Brysis Coleman (story) Mack V. Wright (writer)
| narrator       =
| starring       =
| music          =
| cinematography = Paul H. Allen
| studio         = El Dorado Productions
| distributor    =
| released       =  
| runtime        = 44 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Arizona Days is a 1928 American silent western film directed by J.P. McGowan, who also portrayed the protagonist.

== Cast ==
* Bob Custer as Chuck Drexel 
* Peggy Montgomery as Dolly Martin 
* John Lowell as John Martin
* J.P. McGowan as Ed Hicks
* Mack V. Wright as Black Bailey 
* Jack Ponder as Reginald Van Wiley

== External links ==
 
* 
* 

 
 
 
 
 
 
 
 


 
 