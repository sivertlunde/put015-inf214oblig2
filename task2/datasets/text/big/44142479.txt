Johnny Holiday (film)
{{Infobox film
| name           = Johnny Holiday
| image          = Johnny Holiday poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Willis Goldbeck
| producer       = R.W. Alcorn 
| screenplay     = Jack Andrews Willis Goldbeck Frederick Stephani
| story          = R.W. Alcorn 
| starring       = William Bendix Stanley Clements Hoagy Carmichael Allen Martin Jr. Greta Granstedt Herbert Newcomb
| music          = Franz Waxman
| cinematography = Hal Mohr
| editing        = Richard Fritch 
| studio         = Alcorn Productions
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Johnny Holiday is a 1949 American crime film directed by Willis Goldbeck and written by Jack Andrews, Willis Goldbeck and Frederick Stephani. The film stars William Bendix, Stanley Clements, Hoagy Carmichael, Allen Martin Jr., Greta Granstedt and Herbert Newcomb. The film was released on November 18, 1949, by United Artists.  

==Plot==
 

==Cast== 
*William Bendix as Sgt. Walker
*Stanley Clements as Eddie Duggan
*Hoagy Carmichael as Hoagy Carmichael
*Allen Martin Jr. as Johnny Holiday
*Greta Granstedt as Mrs. Holiday
*Herbert Newcomb as Dr. Piper
*Donald Gallagher as Supt. Lang
*Jack Hagen as Jackson George Cisar as Barney Duggan
*Henry F. Schricker as Himself 
*Leo Cleary as Trimble / Spenser
*Alma Platt as Miss Kelly
*Jean Juvelier as Mrs. Bellini  Buddy Cole as Buddy Cole
*Staff and Boys of the Indiana Boys School as Themselves 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 