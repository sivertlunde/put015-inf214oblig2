Pardon My Terror
{{Infobox film
| name           = Pardon My Terror
| image          = Pardonmyterror 1sht.jpg
| caption        =
| director       = Edward Bernds
| writer         = Edward Bernds Richard Lane Kenneth McDonald Dick Wessel Vernon Dent Emil Sitka Dudley Dickerson
| cinematography =
| editing        = Paul Borofsky
| producer       = Hugh McCollum
| distributor    = Columbia Pictures
| released       =  
| runtime        = 17 17"
| country        = United States English
}}
 Richard Lane Kenneth McDonald, Dick Wessel, Vernon Dent, Emil Sitka and Dudley Dickerson.

==Plot==
Private detectives Dick and Gus are asked to investigate the disappearance and possible murder of her wealthy grandfather Jonas Morton. The duo encounter houseguest Mr. Grooch and his two assistants, who are behind the goings-on, and plotting to steal the Morton fortune. Creepy butler Jarvis also seems to have an ulterior motive. Dick and Gus presence is not appreciated, and they find themselves the targets of poison, gunfire, and an electrifying death trap.

==Production==
Pardon My Terror was originally meant to star The Three Stooges. Director Edward Bernds had completed the script in 1946 and was ready to shoot the film after the Stooges Half-Wits Holiday, Curly Howards last starring film with the Stooges. Howards untimely stroke rendered him unable to continue with the act, so Bernds jettisoned his original script and hastily rewrote it for Schilling & Lane. Schillings part was written as a combined Curly/Larry Fine role, while Lanes was as Moe Howard. 
 Who Done It?. Who Done It? would become one of the finest comedies the team ever produced. Stooge expert Jon Solomon, author of The Complete Three Stooges: The Official Filmography and Three Stooges Companion commented that "this well-balanced mixture of physical abuse, verbal banter, and emotional surprise is particularly vibrant even for a Stooge film." {{cite book | last = Solomon| first = Jon| authorlink = Jon Solomon| title = The Complete Three Stooges: The Official Filmography and Three Stooges Companion
| publisher = Comedy III Productions, Inc| date = 2002| location = | pages = 346–347 
| url = http://www.amazon.com/Complete-Three-Stooges-Filmography-Companion/dp/0971186804/ref=sr_1_1?ie=UTF8&s=books&qid=1201570359&sr=1-1 | doi =| id = | isbn = 0-9711868-0-4}} 

Coincidentally, Columbia Pictures actors Emil Sitka, Dudley Dickerson and Christine McIntyre appeared in both films. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 