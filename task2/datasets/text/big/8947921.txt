The Male Animal
{{Infobox film
| name           = The Male Animal
| image          = Male animalmp.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Elliott Nugent
| producer       = Hal B. Wallis
| based on       =  
| screenplay     = Stephen Morehouse Avery Julius J. Epstein Philip G. Epstein
| narrator       = 
| starring       = Henry Fonda Olivia de Havilland Joan Leslie
| music          = Heinz Roemheld
| cinematography = Arthur Edeson Thomas Richards
| distributor    = Warner Bros.
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2.4 million
}}

The Male Animal (1942 in film|1942) is a Warner Brothers film starring Henry Fonda, Olivia de Havilland and Joan Leslie.  

The film was based on a hit 1940 Broadway play of the same name written by James Thurber and Elliott Nugent. The screenplay was written by Stephen Morehouse Avery, Julius J. Epstein, and Philip G. Epstein, based on Nugent and Thurbers play. The film was also directed by Elliott Nugent.

==Plot== Bartolomeo Vanzettis sentencing statement to his class as an example of eloquent composition, even in broken English composed by a non-professional.

The schools conservative trustees, led by Ed Keller (Eugene Pallette) threaten to fire Tommy if he doesnt withdraw the reading from his lecture. The subject of free speech and Tommys dilemma of conscience anchor the dramatic subplots social significance. The lighter comic triangle plot concerns a return visit to attend the big football game by Joe Ferguson (Jack Carson), a former football hero and onetime love interest of Turners wife Ellen (Olivia de Havilland). Joe is recently divorced and he rekindles Ellens romantic notions at the very moment when her marriage to Tommy is being tested by the events on campus.

== Cast ==
* Henry Fonda as Tommy Turner
* Olivia de Havilland as Ellen Turner
* Joan Leslie as Patricia Stanley
* Jack Carson as Joe Ferguson
* Eugene Pallette as Ed Keller
* Herbert Anderson as Michael Barnes
* Hattie McDaniel as Cleota Ivan F. Simpson as Dean Frederick Damon
* Don DeFore as Wally Myers
* Gig Young as a student (uncredited)

== Production == Broadway production, Tobacco Road. Don DeFore, another member of the Broadway cast, repeated his role in the film. Co-writer Elliott Nugent played the lead role on the stage before coming to Hollywood to direct Henry Fonda in the film version.

==Remake== musical called Shes Working Her Way Through College (1952), starring Virginia Mayo and Ronald Reagan. In this adaptation, the characters names are changed. Also, the political theme is discarded in favor of a conflict surrounding the professors attempt to mount a musical play featuring a student who is discovered to be a former burlesque dancer.

The film earned an estimated $2.4 million at the North American box office in 1952. 

The film features Gene Nelson and Phyllis Thaxter in the cast, as well as Don DeFore who had also been in The Male Animal.

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  
*   at Internet Archive

 

 
 
 
 
 
 
 
 
 
 
 
 