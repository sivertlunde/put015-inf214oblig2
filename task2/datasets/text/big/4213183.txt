Lovers and Liars
{{Infobox film

 | name = Lovers and Liars
 | caption = 
 | director = Mario Monicelli
 | producer = Alberto Grimaldi
 | writer = Tullio Pinelli
 | starring = Goldie Hawn Giancarlo Giannini 
 | music = Ennio Morricone
 | cinematography = Tonino Delli Colli
 | editing = Ruggero Mastroianni
 | distributor = Levitt-Pickman
 | released = April 25, 1979
 | runtime = 120 min.
 | country = Italy France
 | awards =  Italian
 | budget = 
 | preceded_by = 
 | followed_by = 
 | image = Lovers and Liars VideoCover.jpeg
}} Italian feature directed by Mario Monicelli and starring Goldie Hawn and Giancarlo Giannini. It is Hawns only foreign film.   It was released in the United States in February 1981.

==Plot==
Anita (Hawn) is an American actress who decides to vacation in Rome. There, she becomes involved in a romance with her friends married lover Guido (Giannini).

==Cast==
*Goldie Hawn as Anita
*Giancarlo Giannini as Guido Massacesi
*Claudine Auger as Elisa Massacesi
*Aurore Clément as Cora
*Laura Betti as Laura
*Andréa Ferréol as Noemi
* Renzo Montagnani as Teo 
* Gino Santercole as the truck driver
== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 


 