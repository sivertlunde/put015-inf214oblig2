Krishna Guruvayoorappa
{{Infobox film 
| name           = Krishna Guruvayoorappa
| image          =
| caption        =
| director       = NP Suresh
| producer       = Purushan Alappuzha
| writer         = Purushan Alappuzha Alappuzha Karthikeyan (dialogues)
| screenplay     = Alappuzha Karthikeyan Menaka Unnimary
| music          = V. Dakshinamoorthy
| cinematography = PN Sundaram
| editing        = NP Suresh
| studio         = Sreedevi Arts Movies
| distributor    = Sreedevi Arts Movies
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Menaka and Unnimary in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Prem Nazir
*Srividya Menaka
*Unnimary Baby Shalini
*Balan K Nair
*MG Soman
*Sathyakala

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by  and Koorkkancheri Sugathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anjana sreedhara || P Susheela ||  || 
|-
| 2 || Ennunnikkanna || Ambili || Koorkkancheri Sugathan || 
|-
| 3 || Jaya Jagadeesa   ||  ||  || 
|-
| 4 || Kanna Kaarmukilolivarnna   || Kalyani Menon || Koorkkancheri Sugathan || 
|-
| 5 || Karaaravinda   || P Jayachandran ||  || 
|-
| 6 || Kasthoori Thilakam   || K. J. Yesudas ||  || 
|-
| 7 || Krishna (Bhooloka Vaikuntavaasa) || K. J. Yesudas ||  || 
|-
| 8 || Krishna Krishna Mukunda || K. J. Yesudas ||  || 
|-
| 9 || Minnum Ponnin Kireedam || P Susheela ||  || 
|-
| 10 || Mookane Gaayakanaakkunna || K. J. Yesudas || Koorkkancheri Sugathan || 
|-
| 11 || Naarayana krishna || Kalyani Menon || Koorkkancheri Sugathan || 
|-
| 12 || Raajaputhri   || K. J. Yesudas ||  || 
|-
| 13 || Sankadaapahaa   || Kalyani Menon ||  || 
|-
| 14 || Thrikkaal Randum || K. J. Yesudas || Koorkkancheri Sugathan || 
|-
| 15 || Yogeendraanaam || K. J. Yesudas ||  || 
|}

==References==
 

==External links==
*  

 
 
 

 