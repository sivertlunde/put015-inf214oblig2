The Narrow Trail
{{Infobox film
| name           = The Narrow Trail
| image          = 
| alt            = 
| caption        = 
| director       = William S. Hart Lambert Hillyer
| producer       = Thomas H. Ince
| screenplay     = William S. Hart Harvey F. Thew 
| starring       = William S. Hart Sylvia Breamer Milton Ross Bob Kortman
| music          = 
| cinematography = Joseph H. August
| editing        = 
| studio         = Artcraft Pictures Corporation William S. Hart Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western silent film directed by William S. Hart and Lambert Hillyer and written by William S. Hart and Harvey F. Thew. The film stars William S. Hart, Sylvia Breamer, Milton Ross and Bob Kortman. The film was released on December 30, 1917, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*William S. Hart as Ice Harding 
*Sylvia Breamer as Betty Werdin 
*Milton Ross as Admiral Bates
*Bob Kortman as Moose Holleran 
*Fritz as King

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 