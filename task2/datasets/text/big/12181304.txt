Back Street (1961 film)
{{Infobox film
| name           = Back Street
| image          = BackStreet1961.jpg
| caption        = David Miller
| producer       = Ross Hunter
| writer         = William Ludwig Eleanore Griffin Fannie Hurst (novel)
| starring       = Susan Hayward John Gavin Vera Miles Frank Skinner
| cinematography = Stanley Cortez
| editing        = Milton Carruth
| distributor    = Universal Pictures
| released       =  
| runtime        = 107 min 
| country        = United States
| awards         = English
| budget         =
}}
 David Miller, novel by Frank Skinner. The film stars Susan Hayward, John Gavin and Vera Miles.

The story follows two lovers who have limited opportunities to get together because one of them is married.

Hedda Hopper claims Hunter was considering Gregory Peck and William Holden for the lead until she suggested John Gavin. Laurence Olivier Shuns $300,000 to Play Caesar
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   15 July 1960: b12.  
 Imitation of Madame X.
 1932 and 1941 screen versions in many ways - changing the names of several characters and updating the story to what was then the present day. Good examples of how the plotline was sensationalized in this third version are the attempted suicide and the fatal car crash.

==Plot==
Wealthy department-store heir Paul Saxon has a romantic fling with a Nebraska dress-shop owner, Rae Smith, who breaks it off when she discovers he is married.

Rae moves to New York to become a fashion designer, then on to Rome to become the famed Dalians partner in a salon. Paul continues to woo her, explaining that his alcoholic wife Liz wont grant him a divorce and is unstable, having tried to commit suicide.

Her resistance lowered, Rae becomes the lover of Paul, meeting secretly with him at a house near Paris that he buys. Pauls son learns of the affair and demands that Rae stop seeing his father. Liz makes a public scene humiliating Rae at a charity fashion show featuring her designs purchasing the closing creation, a wedding gown, for $10,000.

As a drunkened Liz leaves the house to attend a party Paul confronts her.  He gets into the car with her as the two argue they  fight over the keys in the ignition.  The car crashes instantly killing Liz and leaving Paul critically paralyzed in the hospital.  Paul dies from his injuries, but not before insisting his son call Rae so he can tell her he loves her.  Rae, Paul Jr. and his sister Caroline are left alone with their grief.

==Cast==
* Susan Hayward as  Rae Smith
* John Gavin as  Paul Saxon 
* Vera Miles as  Liz Saxon 
* Charles Drake as  Curt Stanton 
* Virginia Grey as  Janey née Smith 
* Reginald Gardiner as  Dalian 
* Tammy Marihugh as  Caroline 
* Robert Eyer as  Paul Saxon Jr. 
* Natalie Schafer as  Mrs. Evans 
* Doreen McLean as  Miss Hatfield 
* Alex Gerry as  Mr. Venner 
* Karen Norris as  Mrs. Penworth 
* Hayden Rorke as  Charley Claypole  Mary Lawrence as  Marge Claypole 
* Joseph Cronin as  Airport Clerk
==References==
 
==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 

 