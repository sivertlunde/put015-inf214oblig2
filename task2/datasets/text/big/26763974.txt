Trauma (1962 film)
{{Infobox film
| name           = Trauma
| image_size     =
| image	=	Trauma FilmPoster.jpeg
| caption        = Robert M. Young
| producer       = Joseph Cranston (producer) Sal Mungo (associate producer) Robert M. Young (writer)
| narrator       =
| starring       = See below
| music          = Buddy Collette
| cinematography = Jacques R. Marquette
| editing        = Hal Dennis
| distributor    =
| released       = 1962
| runtime        = 93 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Robert M. Young.

== Plot summary ==
 

== Cast ==
*John Conte as Warren Clyner
*Lynn Bari as Helen Garrison
*Lorrie Richards as Emmaline Garrison
*David Garner as Craig Schoonover
*Warren J. Kemmerling as Luther
*William Bissell as Thaddeus Hall
*Bond Blackman as Robert
*William Justine as Treasury Agent
*Ray Lennert as Treasury Agent
*Renee Mason as Carla
*Robert Totten as Gas Station Attendant
*Alfred Chafe as Police Officer
*Ruby Borner as Maid

== Soundtrack ==
* "Emmalines Theme" (Written by Buddy Collette and Minette Allton)

== External links ==
* 
* 

 
 
 
 
 
 


 