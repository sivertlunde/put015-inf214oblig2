Iron Ladies of Liberia
{{Infobox film| name =Iron Ladies of Liberia
  | image =
  | caption = 
  | director =Daniel Junge, Siatta Scott Johnson
  | producer =Henry Ansbacher Micah Schaffer
  | executive producer =Mette Hoffman Meyer, Jonathan Stack
  | writer =
  | starring =Ellen Johnson-Sirleaf, Beatrice Munah Sieh, Antoinette Sayeh 
  | music =Gunnard Doboze	 	
  | cinematography =Daniel Junge	 	
  | editing =
  | distributor = Women Make Movies
  | released =7 September 2007
  | runtime =77 minutes
  | language =English
  | budget =
}}

The Iron Ladies of Liberia (2007) is an independently produced documentary film that gives behind-the-scenes access to President Ellen Johnson-Sirleafs first year in government. Johnson-Sirleaf is Africas first female president.   

==Synopsis==

The film opens with journalist Siatta Scott Johnsons narration over Johnson-Sirleafs inauguration on 16 January 2006. Among the distinguished guests in attendance are the then first lady of the USA Laura Bush, secretary of state Condoleezza Rice, and South African president Thabo Mbeki. Ellen Johnson Sirleaf is the first ever freely elected female head of state in Africa. 

Momentarily archive footage is shown of events in Liberia during the 14 year civil war—scenes of public executions, child soldiers and maimed victims–scenes reoccurring briefly throughout the documentary. Then the presidents cabinet is shown, Sirleaf appoints women in high administrative posts dubbed “the Iron Ladies”; Beatrice Munah Sieh is appointed the national chief of police, Antoinette Sayeh is appointed the Minister of Finance, the Minister of Justice and the Minister of Commerce too are women.  
 Charles Taylor’s civil war is shown.  Johnson-Sirleaf laments that Taylor loyalists are a constant threat to the stability of Liberia. 

Siatta Scott Johnson investigates public opinion with regard to the two governments and finds a divided sentiment between the people. In her efforts to make inclusive decisions, Johnson-Sirleaf meets with former rebel leaders and other opposition leaders at state of the nation meetings. In spite of her efforts towards inclusiveness Edwin Snowe, who was elected spokesperson for the House of Representatives on the same day Johnson-Sirleaf took office, is seen as an outspoken critic of the president, relentlessly seeking political allies to call for the president’s impeachment. 

Antoinette Sayeh travels to Washington DC to speak at the World Bank Headquarters on Liberian’s outstanding debt, $3.7 billion owned to creditors such as the IMF, The World Bank and the USA, the latter being the oldest bilateral partner due to their historical ties. The debt is a deterrent to the country’s ability to raise new financing from multi-lateral institutions. 

Back in Liberia Johnson-Sirleaf personally responds to angry mobs of workers and reformed soldiers, including Armed Forces of Liberia soldiers who were forced out of work due to the peace agreement, demanding higher salaries and pensions respectively by speaking candidly on the government’s limitations, instead of making the typical empty political promises. Towards the end of her first year as president and influx of Chinese investment is made explicit, the film shows a montage of infra-structures built by the Chinese, the new partnership is marked by an official visit form president Hu Jintao. 

Meanwhile Condoleezza Rice representing the USA at the Liberia partners’ forum 2007, announces a cancellation of the $391 million debt. This becomes the most important achievement of Johnson-Sirleaf’s first year in office. The film concludes on a positive note, with Johnson-Sirleaf’s voice commentary over images of her waving at the crowd during open presidencies, juxtaposed with images of women chanting and cheering for the president.

==Historical context==

Historically, in the 1980s Johnson-Sirleaf challenged the military regime of dictator Samuel Doe. She sent $10 000 to Charles Taylor to support the infamous warlord’s rebellion, the National Patriotic Front of Liberia (NPFL).  Her support of Taylor was tactic, Taylor claimed his aim was to invade Monrovia to remove Samuel Doe from office and avenge the death of hundreds of people from the Gio and Mano ethnic groups.  

In a turn of events it become apparent that Taylor wanted to take over Liberia, consequently this split the NPFL into three different factions; forces supporting Taylor, forces supporting Price Yormi and forces loyal to Doe. Charles Taylor was responsible for a civil war more destructive than Samuel Doe’s regime ever was. Although Mrs. Johnson Sirleaf has since apologized for her “misjudgment” and said that “when the true nature of Mr. Taylor’s intentions became known, there was no more impassioned critic or strong opponent to him in a democratic process”.  

Doe was later killed on September 9, 1990 by Johnson’s forces. Afterwards, no particular rebel group was strong enough to fill the political vacuum. The bloody conflict raged until 1996, when the Economic Community of West African States (ECOWAS) brokered a peace accord and set elections for 1997. Taylor won the elections with 75% of the vote. Despite this temporary peace, civil war broke out again as opposition groups began to fight to oust Taylor from government. Liberia’s second civil war ended in 2003 with Taylor’s indictment by a UN-backed court and subsequent exile to Nigeria. At the end of 14 years of civil war, roughly three quarters of the population lives on less than $1 a day, and as much as 85% of the Liberian population is unemployed, this is the situation that Johson-Sirleaf’s government inherited. 

==Reviews==

The film received generally positive reviews, with the review aggregator website Rotten Tomatoes showing 56% positive and a rating of 3.5/5 from 232 reviews.  Richard Propes of the Independent Critic felt that “Iron Ladies of Liberia is an insiders portrait of the president...that is both its strength and its greatest weakness. While Iron Ladies of Liberia was made with Sirleaf Johnsons cooperation, it often feels like an intentionally drawn portrait meant to show her in the most positive light possible.” Probes concludes that “Iron Ladies of Liberia does feel, at times, out of balance. While the filmmaker, an acknowledged acquaintance of the president, essentially plays observer for Sirleaf Johnsons first year of leadership, what it lacks is a more holistic look at Sirleaf Johnson and the nation shes leading.”  

Angus Wolfe Murray of Eye for Film offers a review with different point of view: 
It’s OK that it doesn’t cover all the problems, because it shows the ones Ellen has to deal with: endemic corruption, a demobilized army on strike, chaos and disorder in the streets, the manipulations of the old political elite, still hanging on to power with their embezzled riches. It doesn’t show the fishermen in dugout canoes, paddling over high waves to brave the ocean, or the plight of over half a million returning refugees   it doesn’t need to. Liberia is too much for most people to grasp…. It tells you enough about the problems to get you thinking. The consensus at whydemocracy.net parallels this sentiment in that “rebuilding a nation after 14 years of civil war is not easy. The people are longing to for change but the government can only deliver after going through the hurdles of the international bureaucracy . The movie teaches the viewer to appreciate not only the hard work of Ellen Johnson-Sirleaf but of new governments everywhere”. 

Scholar and activist Obioma Nneaemeka identifies an emerging feminist engagement particular to African countries that is evident in this film. http://www.seeingblack.com/article_353.shtml  Nneameka names this specific form of feminism as “nego-feminism” where the prefix, nego, signifies negotiation and the elimination of unyielding self-righteousness, or “no ego.” In this respect, this feminist engagement is deeply rooted in humility without seeming docile. Nnaemeka explains that this feminist practice “knows when, where, and how to detonate patriarchal land mines; it also knows when, where, and how to go around negotiate with or negotiate around patriarchy in different contexts.” To that end, nego-feminism is fundamental to Johnson-Sirleaf political manoeuvres. 

Film critic Astride Charles commends the documentary for “rejuvenat discussion about international monetary policy and the way in which Africa’s societal and economic problems are framed. Instead of myopically focusing on abject poverty within Africa, the film probes some reasons why African nations do not prosper from their own natural resources.” Further Charles notes that the film “also gives an extensive insight into leadership and gender dynamics within African countries. Through the tangibility of a documentary, it conveys promise for African countries under neo-colonial conditions.” 

==Accolades==

*The Iron Ladies of Liberia was named the best feature film by a woman director at the 15th annual African Diaspora Film Festival. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 