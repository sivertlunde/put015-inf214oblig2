The Tillman Story
{{Infobox film
| name           = The Tillman Story
| image          = The Tillman Story.jpg
| caption        = 
| director       = Amir Bar-Lev
| producer       = 
| writer         = 
| starring       = Pat Tillman
| narrator       = Josh Brolin
| music          = 
| cinematography = 
| editing        = 
| distributor    = Passion Pictures 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $798,940 
}} war in Afghanistan, the coverup of the true circumstances of his death, and his familys struggle to unearth the truth. It was nominated for the Grand Jury Prize at the 2010 Sundance Film Festival. The film is narrated by Josh Brolin. 

==Reception==
French journalist Marjolaine Gout gave the film 4 stars out of 5. She wrote about it as a “stunning” documentary exposing the social practice in the United States using history and media to federate a nation. She explains that the movie shows the manipulation of the mass with a constant staging of their life. She talks also about the concept of heroism present in this film. She makes a link between Daniel Boone and Patrick Tillman in the use of their identity as legend and symbol. 

The film currently has a 93% fresh rating on Rotten Tomatoes based on 82 reviews. 

==References==
 

==External links==
* 
* 
* 
* 
* , by Bob Mondello. NPR.org. September 7, 2010.
* 

 
 
 
 
 
 
 
 
 
 

 