Peter and Vandy
{{Infobox film
| name           = Peter and Vandy
| image          = Peter&Vandy Poster.jpg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = Jay DiPietro
| producer       = Bingo Gubelmann Benji Kohn Austin Stark Peter Sterling
| screenplay     = Jay DiPietro
| based on       =  
| starring       = Jason Ritter Jess Weixler
| music          = Jason Lifton
| cinematography = Frank G. DeMarco
| editing        = Geoffrey Richman
| studio         = Cook Street Productions Paper Street Films
| distributor    = Strand Releasing
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} romantic independent independent drama film starring Jason Ritter and Jess Weixler. The film was written and directed by Jay DiPietro, adapted from his own play of the same name which opened in 2002 in New York.  

==Plot==
Peter and Vandy is a love story told out of order. Set in Manhattan,  the story shifts back and forth in time, juxtaposing Peter and Vandys romantic beginnings with the twisted, manipulative, regular couple they become.

==Cast==
* Jason Ritter as Peter
* Jess Weixler as Vandy
* Jesse L. Martin as Paul
* Tracie Thoms as Marissa
* Noah Bean as Andrew
* Bruce Altman as Dad
* Dana Eskelson as Emma
* Kristina Klebe as Michelle
* Zak Orth as Keith
* David Rasche as Alan
* Maryann Plunkett as Mom

==Release== AFI Dallas Film Festival, the Waterfront Film Festival, the Provincetown International Film Festival and the Tallgrass Film Festival.   
 Region 1. 

==Reception==

===Critical response===
When the film was released,   and Animal Collective, Peter and Vandy is more a designer frame for actors than nourishing entertainment." 

Film critics Frederic and Mary Ann Brussat of the web based Spirituality & Practice, liked the film, writing, "Writer and director Jay DiPietro has adapted this enticing romantic story from his 2002 play. Using the now familiar techniques of nonlinear storytelling, jump cuts, and masterful editing, the filmmaker challenges us to understand and appreciate that intimate relationships stand or fall on the basis of how couples handle trifles and everyday routines." 

Metromix film critic Geoff Berkshire also liked the film and gave the film a positive review, writing, "DiPietro’s simple story flirts with indie-filmmaking-by-the-numbers but still manages a refreshingly clear-eyed look at relationships. It’s closer (if not quite close enough) to Eternal Sunshine of the Spotless Mind than to any number of dreary Hollywood factory rom-coms. The non-linear approach relies on both subtle and obvious visual clues to keep the audience properly oriented, and enhances the narrative without the self-conscious raison d’etre gimmickry of Steven Soderberghs The Girlfriend Experience. DiPietro’s dialogue is sharp and Ritter and Weixler credibly flesh out characters who aren’t always likeable, they’re just two regular people trying their best at love." 

Yet, critic Todd McCarthy of Variety (magazine)|Variety magazine panned the film when it opened in at the Sundance Film Festival, calling the film, "A time-jumbling love story about a couple who never inspire rooting interest, Peter and Vandy is an aggravating romance that runs only 78 minutes but ends not a moment too soon...Some of the linear deck-shuffling creates small frissons, but theres no underlying tension or subtext to shore up the banal talk. The accompanying songs are innocuous." 

The review aggregator Rotten Tomatoes reported that 63% of critics gave the film a positive review, based on nineteen reviews." 

===Awards===
Nominated
* Sundance Film Festival: Grand Jury Prize, Dramatic, Jay DiPietro; 2009.

==Soundtrack==

===Track listing=== The National 
* "Wet and Rusting" by Menomena
* "Brace Yourself" by Les Savy Fav
* "Air Better Come" by The Shaky Hands
* "Enticing Luxuries" by Chris DiPetrio
* "Overture" by Patrick Wolf
* "Good Arms vs. Bad Arms" by Frightened Rabbit
* "Fireworks" by Animal Collective
* "For Money or Love" by The Like Young
* "The Same Old Song" by 33 To Nothing The National

==References==
 

==External links==
*   (Strand Releasing)
*  
*   Strand Releasing Channel
*   interview at indieWire

 
 
 
 
 
 
 
 
 
 