The Man Who Knew Too Much (1934 film)
 
 
{{Infobox film
| name           = The Man Who Knew Too Much
| image          = The man who knew too much 1934 poster.jpg
| director       = Alfred Hitchcock Charles Bennett D. B. Wyndham-Lewis Edwin Greenwood (scenario) A.R. Rawlinson (scenario)
| starring       = Leslie Banks Edna Best Peter Lorre Nova Pilbeam Frank Vosper
| producer       = Michael Balcon (uncredited)
| music          = Arthur Benjamin
| cinematography = Curt Courant
| editor         = Hugh Stewart
| distributor    = Gaumont British|Gaumont-British Picture Corporation
| released       =  
| runtime        = 75 minutes
| language       = English
| budget         = £40,000 (estimated)
| country        = United Kingdom
}}
 Gaumont British. It was one of the most successful and critically acclaimed films of Hitchcocks British period.
 remade the film with James Stewart and Doris Day in 1956 for Paramount Pictures. The two films are, however, very different in tone, in setting, and in many plot details.

The film has nothing except the title in common with G. K. Chestertons 1922 book of detective stories of the same name. Hitchcock decided to use the title as he had the rights for some of the stories in the novel. 

==Synopsis== clay pigeon shooting contest. They befriend a foreigner, Louis Bernard (Pierre Fresnay), who is staying in their hotel. One evening, as Jill dances with Louis, she witnesses him being killed. Before dying, Louis passes onto them some vital information to be delivered to the British consul.

To ensure their silence, the assassins, led by Abbott (Peter Lorre), kidnap the Lawrences daughter. Unable to seek help from the police, the couple return to England and, after following a series of leads, discover that the group intends to assassinate the head of state of an unidentified European country during a concert at the Royal Albert Hall. Jill attends the concert and distracts the gunman with a scream.
 Wapping in London, near the docks, where they have their hide-out in the temple of a sun cult|sun-worshipping cult. Bob enters and is held prisoner, but manages to escape. The police surround the building and a gunfight ensues. The assassins hold out until their ammunition runs low and most of them have been killed. Betty, who has been held there, and one of the criminals are seen on the roof, and it is Jills sharpshooting skills that dispatch the man, who is revealed as the man who beat Jill in the shooting contest in Switzerland.

Abbott seems to commit suicide rather than be captured, and Betty is returned to her parents.

==Production== Road House Charles Bennett The Man Who Knew Too Much (1956) DVD 

The story is credited to Bennett and D.B. Wyndham Lewis; Bennett claims Lewis had been hired to write some dialogue which was never used and provided none of the story. 

Peter Lorre was unable to speak English at the time of filming (he had only recently fled from Nazi Germany) and learned his lines phonetically. 
 1956 remake. 

Hitchcock hired Australian composer Arthur Benjamin to write a piece of music especially for the climactic scene at Royal Albert Hall. The music, known as the Storm Clouds Cantata, is used in both the 1934 version and the 1956 remake.
 Alfred Hitchcocks cameo appears 33 minutes into the film. He can be seen crossing the street from right to left in a black trench coat before they enter the Chapel.

==Production crew==
* Directed by Alfred Hitchcock Charles Bennett and D. B. Wyndham-Lewis
* Associate Producer: Ivor Montagu
* Photography: Curt Courant
* Editor: H St. C.J. Stewart
* Music: Arthur Benjamin

==Cast==
* Leslie Banks as Bob
* Edna Best as Jill
* Peter Lorre as Abbott
* Frank Vosper as Ramon
* Hugh Wakefield as Clive
* Nova Pilbeam as Betty Lawrence
* Pierre Fresnay as Louis
* Cicely Oates as Nurse Agnes
* B. A. Clarke Smith as Binstead George Curzon as Gibson

==References==
 

==Bibliography==
* Ryall, Tom. Alfred Hitchcock and the British Cinema. Athlone Press, 1996.
*   – Contains interviews with Alfred Hitchcock and a discussion on the making of The Man Who Knew Too Much (1934).

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 