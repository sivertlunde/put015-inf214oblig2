Futtocks End
{{Infobox film
  | name = Futtocks End
  | image =
  | director = Bob Kellett
  | producer = Bob Kellett
  | music   = Robert Sharples  Richard OSullivan   Mary Merrall   Hilary Pritchard   Jennifer Cox   Ernest C. Jennings 
  | released = February 1970
  | runtime  = 45 minutes
}} British comedy film released in 1970 in film|1970, directed by Bob Kellett. Entirely silent, with a musical score, sound effects and incoherent mutterings, the story revolves around a weekend gathering at the decaying country home of the eccentric and lewd General Futtock (Ronnie Barker) and the series of saucy mishaps between the staff (Michael Hordern plays the lecherous butler) and his guests. It was filmed at Grims Dyke, the former home of W. S. Gilbert.

Futtocks End was released on DVD in June 2006 and was shown in Trafalgar Square as part of the 2007 St Georges Day celebrations.

The film can be seen as a precursor to some of the later Two Ronnies sketches, and the style was revisited in The Picnic.

==Cast==
* Ronnie Barker ...  Gen. Futtock 
* Roger Livesey ...  The Artist 
* Julian Orchard ...  The Twit 
* Kika Markham ...  The Niece 
* Mary Merrall ...  The Aunt 
* Hilary Pritchard ...  The Bird 
* Peggy Ann Clifford ...  The Cook  Richard OSullivan ...  The Boots 
* Jennifer Cox ...  The Maid 
* Suzanne Togni ...  Tweenie 
* Sammie Winmill ...  Tweenie 
* Kim Kee Lim ...  The Japanese Businessman 

== External links ==
*  at Digital Classics online shop
* 

 
 
 
 
 
 

 