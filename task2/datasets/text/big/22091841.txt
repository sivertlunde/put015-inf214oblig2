Navy Blue and Gold (film)
{{Infobox film
| name           = Navy Blue and Gold
| image          = Navy Blue and Gold - 1937- Poster.png
| caption        = 1937 Theatrical poster
| director       = Sam Wood
| producer       = Sam Zimbalist George Bruce
| narrator       = Robert Young James Stewart Lionel Barrymore
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =
| runtime        =
| country        = United States English
| budget         = $458,000  . 
| gross          = $1,168,000 
| preceded_by    =
| followed_by    =
}} Robert Young, James Stewart and Lionel Barrymore. 

==Synopsis==
After each is accepted to the Naval Academy, three young men, Dick Gates, Roger Ash and "Truck" Cross, decide to become roommates.

Dick is tricked into committing a rules violation toward Harnett, an upperclassman. Roger gets even by challenging Harnett to a boxing match and winning it.

Truck becomes attracted to Dicks sister, Pat. He also faces expulsion for not properly identifying himself upon admission to the academy, which is revealed when Truck tries to clear the sullied reputation of his father.

Roger also runs into trouble when he is found drunk in a bar, but the schools former football coach covers for him. Truck, a gifted football player, is cleared of all charges in time to race to the stadium and help win the Army-Navy game for his side.

==Cast== Robert Young as Roger Ash
*James Stewart as "Truck" Cross
*Lionel Barrymore as Captain "Skinny" Dawes
*Florence Rice as Patricia Gates
*Billie Burke as Mrs. Gates Tom Brown as Richard Gates Jr
*Samuel S. Hinds as Richard Gates Sr Paul Kelly as Tommy Milton
*Barnett Parker as Graves

==Production notes==
Production Dates:  7 Sep—early Nov 1937.  Portions of the picture were filmed at the Naval Academy in Annapolis, MD. The Los Angeles Memorial Coliseum was used as the Southern Institute stadium in the early portion of the film and later as the Naval Academy stadium.
==Reception==
The film was very popular: according to MGM records it made $884,000 in the US and Canada and $284,000 in other countries, recording a profit of $297,000. 
==See also==
*Lionel Barrymore filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 