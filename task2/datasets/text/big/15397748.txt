Boys to Board
{{Infobox film
| name           = Boys to Board
| image          =
| caption        = Tom McNamara
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker Tom McNamara Jack Davis Allen Hoskins
| cinematography =
| editing        =
| distributor    = Pathé
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
 short subject comedy released. The Our Gang series (later known as "The Little Rascals") was created by Hal Roach in 1922, and continued production until 1944.

==Plot==
A kindly old school teacher helps the gang escape from his wife’s miserable boarding school.  While escaping, they run afoul of a bootlegger, who captures them and ties them up until the old school teacher rescues them just before the sheriff gets there.  The school teacher returns to the boarding school and demands better treatment for the boys.
|
==Notes==
Mary Kornman does not appear in this film.

When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into TV syndication as Mischief Makers in 1960 under the title "Boarding School". About two-thirds of the original film was included. All of the original inter-titles were cut.

==Cast==

===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickey Jack Davis as Jack
* Allen Hoskins as Farina
* Ernie Morrison as Ernie Sunshine Sammy
* Andy Samuel as Andy

===Additional cast===
* Richard Daniels as Pop Malone
* Helen Gilmore as Mother Malone
* Clara Guiol as Household Helper
* Wallace Howe as Sheriff Charles Stevenson as Moonshine Mose

==See also==
* Our Gang filmography

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 