Madras Cafe
 
 
{{Infobox film
| name           = Madras Cafe
| image          = Madras Cafe Poster.jpg
| alt            =
| caption        = Theatrical poster
| director       = Shoojit Sircar John Abraham  Shoojit Sircar Ronnie Lahiri   
| writer         = Somnath Dey Shubendu Bhattacharya Juhi Chaturvedi  (dialogue)  John Abraham Nargis Fakhri Rashi Khanna Siddharth Basu Prakash Belawadi
| music          = Shantanu Moitra
| cinematography = Kamaljeet Negi
| editing        = Chandrashekhar Prajapati JA Entertainment Rising Sun Films JA Entertainment
| released       =  
| runtime        = 130 minutes   
| country        = India
| language       = Hindi
| budget         =   
| gross          =     
}} political Spy espionage thriller John Abraham, Indian intervention assassination of former Indian prime minister Rajiv Gandhi.  

Madras Cafe was released on 23 August 2013. Box Office India stated the film did average business.  The film won the National Film Award for Best Audiography for Nihar Ranjan Samal (location sound recording) and Bishwadeep Chatterjee (sound design) at the 61st National Film Awards. 

==Background== Indian peace-keeping force was forced to withdraw.      As he journeys to Sri Lanka with the intention of disrupting the LTTE rebels, he becomes entangled in rebel and military politics.    There he meets a British journalist Jaya Sahni (Nargis Fakhri)  who wants to reveal the truth about the civil war, and in the process he uncovers a conspiracy to assassinate "a former Indian prime minister", Rajiv Gandhi, through the use of plastic explosives. Although Vikram tries, at 10:10&nbsp;pm, a LTTE suicide bomber kills Gandhi while bowing down to put a wreath on his neck. 

==Plot==

The plot opens in Jaffna, where a bus full of passengers is stopped by armed men. They take all of them out and kill them mercilessly. A little girl tries to escape, but is shot and killed immediately.

The movie then moves to Kasauli, where a bearded man, Vikram Singh (John Abraham) wakes up after having visions of men getting killed. He then finds on TV that the Sri Lankan President was killed by a human bomb. He purchases a bottle of liquor and goes to a nearby church. The priest of that church, who seems to know him for the past three years, asks him about his "conspiracy" when he says that "Our Prime Minister could have been saved from the conspiracy". The bearded man then starts narrating his story.

The film then goes five years back, when the man tells that the continuous battle between the majority in Sri Lanka Sinhalese and minorities Tamils had reached a dangerous level. In that battle thousands of Tamils lost their lives, causing their youths to take weapons in their hands and join the LTF leader Anna Bhaskaran. Due to the plight of Tamils there, the Indian Prime Minister (Sanjay Gurubaxani) decided to sign a peace accord with Sri Lankan Government and said that the elections should happen there peacefully before Diwali. However, Anna refused to accept the accord and the Indian Peace Keeping Forces were forced to withdraw from the island. A heated meeting in New Delhi between the officials leads Robin Dutt aka RD (Siddharth Basu) to call upon his best man, Maj. Vikram Singh  to find out the solution.

After meeting and discussing the strategy with RD and his deputy, Swaroop (Avijit Dutt) Vikram travels to Sri lanka and meets a war correspondent Jaya Sahni (Nargis Fakhri) and tries to find out a way to stop the rebels. After reporting to his senior Bala (Prakash Belwadi) he tries to find some that may help to find Shri, the only man who can withstand and oppose Anna. After meeting with an informer Narayanan, Vikram manages to visit Shri. Vikram promises Shri to help him rise against Anna by providing him with arms. The arms deal date is decided at 6 July. However, the deal goes terribly wrong and one of Vikrams associates is killed in a surprise attack by LTF. Bala scolds Vikram of his working style and tells him to go to Colombo Safehouse. Vikram meets Jaya to seek her help. Jaya tells him that they know his next step even before he implement it. However, she tells him that "Before I came here, a foreign agency guy met with an Indian official." Next day, Vasu, Balas associate, meets a man, gives him a photo of Vikram, telling him "to keep him alive".

Meanwhile, RD and his team is shocked to learn on TV that Vikram is kidnapped by LTF. However, he is left alive by LTF, but badly wounded. Bala visits him in the hospital and tells him to leave Sri Lanka as he is on the hit-list of both the camps. Vikram feels suspicious about Bala. He calls SP, one of his associates, and tells him to report all activites of Bala to him. Vikram, posing as a war correspondent, manages to reach Malaya, second in-command of Anna and RD urges him to tell about Annas hideout. Vikram and Indian forces then launch a massive attack on the LTF base camp. A devastating gunbattle begins, and Vikram escapes from the island after being told by RD. Everyone believes Anna got killed in the fight. Unfortunately, he survies and kills Malaya and Shri. SP later tracks some discussion of Anna over the phone and tells Bala about this, but Bala tells him to ignore them, causing SP to believe that Bala might be a mole. He escapes with the intercepts and files of the case. Bala finds about it and burns some papers, later telling someone over the phone that SP and Vikram are in Kochi and he should send some men there. Vikram later receives a call from SP who tells him to meet him. After meeting with SP, Vikram comes home to find Ruby (Rashi Khanna), his wife, murdered. Vikrams associate in Kochi, Kush tells him that Vasu has been tracked. He nabs Vasu from a theatre and asks him what he knows. Vasu tells him that indeed Bala was a leak and he was helping him alongwith a person named Reed from Singapore. Vikram calls Jaya and requests her to use her sources. She agrees to help and later consoles Vikram about his wife.

As told by Jaya, Vikram reaches Bangkok and picks a taxi. he reaches Happyland Fish Market, where a source of Jaya (Dibang) tells him that he has a tape. Vikram is shocked to see that Bala was honeytrapped, forcing him to divulge all the information about their movements. Bala is later confronted by Vikram on phone, he later commits suicide by shooting himself. RD realises that this might be a Code Red, to assasinate the Prime Minister. He asks Vikram to take care of this and tells his team to seal the coastline. A massive manhunt begins and hundreds of LTF associates are nabbed by Indian security forces and local cops. In the Madras RAW office, Arjun, an officer tracks down the conversation of Vijayan Joseph, a bombmaker and Anna and tells this to Rishi (Tarun Bali). Rishi tells this to Vikram and further says that Kannan Kanan, an associate of Annas man Kanda, is in Madurai Jail and might be helpful. Kannan reveals that some suspicious refugees came from the island to Tamil Nadu. After a short but important meeting with Jaya, Vikram sees X on a clock at the Airport and deduces that the LTF is going to assassinate PM on the same day at X PM (10 PM). RD then calls the PM to cancel his rally. PM says that he will be fine and not to worry about him. Vikram then manages to catch Vijayan from his hideout who tells that the refugees are going to assassinate the PM by plastic explosives which are untraceable to metal detectors.

Vikram rushes to the place where the PM is taking part in the rally. He reaches there nearly on time, but the suicide bomber manages to put the wreath on the neck of the PM and while bowing down, she pulls the trigger and kills the PM along with herself and many others. Vikram recovers but sits there dejected and defeated. Later, Vikram submits his report on the assassination to the investigation committee who considers his report. A few days later, RD too resigns, and Vikram, after taking a voluntary retirement, comes in Kasauli.

The movie comes to present where the priest asks Vikram who won the battle. Vikram says he doesnt know, but in this battle, Indians lost their Prime Minister and Lankan Tamils lost their future. He later walks away, reciting the linings told by Rubys father on their wedding day. He completes another report and sends it to Jaya in Singapore. She starts her work on the report. In Kasauli, Vikram moves of the house he was living in.

An epilogue tells that the civil war continued for 27 years, killing more than 40 thousand Sri Lankan subjects, 30 thousand Tamil rebels, 21 Sri Lankan forces and 1200 Indian forces, and still thousands of Lankan Tamils are homeless. In 2009, Sri Lankan forces launched a brutal aerial and land attack, finishing the rebels, and the credits roll.

==Cast== John Abraham Agent Vinod, James Bond template. I want to show that intelligence officers are ordinary people who live amongst us. It is only that they have to solve issues where national security is at stake," says Sircar. Sircar says he needed an actor who can easily get lost in the crowd but with John Abraham it seems next to impossible. "The role also requires a certain level of physicality and John Abraham has worked for the role. I agree this is a new territory for him but I think he has pitched it right. Lets see how the audiences take him."   
* Nargis Fakhri as Jaya Sahni (a British war correspondent in Sri Lanka, inspired by many war correspondents, including Anita Pratap ) As for Fakhri, Sircar says her voice has not been dubbed. "Nargis Fakhri is playing a foreign war correspondent. I needed a girl who looks Indian journalist but has an accent so there is no chance that audience will remember her Rockstar (2011 film)|Rockstar performance while watching Madras Café. She will converse in English and she is familiar with the language," says Sircar. 
* Rashi Khanna as Ruby Singh,    wife of Major Vikram Singh 
* Siddhartha Basu as Robin Dutt (RD), chief of Research and Analysis Wing.  He is Major Singhs mentor and appoints him to take sole responsibility of executing the covert operations in Sri Lanka
* Ajay Rathnam as Anna Bhaskaran, the leader of the fictional LTF rebels. The character closely resembles the Liberation Tigers of Tamil Eelam leader Velupillai Prabhakaran.      
* Prakash Belawadi    as Bala, Major Vikram Singhs superior in Jaffna. Due to his long stint in Sri Lanka, Bala is the only one who has first-hand information on the reality of the situation. As Major Singh arrives to execute his covert operation, Bala and his team helps him to get access to locations and people who are crucial to make the operation successful. Bala was inspired by a real life R&AW mole known as KV Unnikrishnan. KV Unnikrishnan, the agencys station chief in Chennai in 1987 was honey trapped by the CIA. The US spy service threatened to reveal Unnikrishnans compromising photographs with an air hostess to force him to co-operate. 
* Tinu Menachery  as a Tamil rebel
* Agnello Dias  as a Sri Lankan minister Cabinet Secretary of India 
* Dibang  as a former intelligence officer
* Leena Maria Paul as a Tamil rebel      

==Production and development==

“If I would have gone with this script to anyone else, they would have rejected it because of the kind of sensitivity the subject has. I dont want to name them, but three of them have already done it. Nobody was ready to produce the film... its very daring of John Abraham and Viacom 18 Motion Pictures to back up this project," director Sircar told the press after the release of the film.    John Abraham said that director Shoojit Sircar narrated the script of Madras Cafe to him in 2006 but could not get around to begin it. "After our last film together, we decided to get back to doing where we started off from. Thats the story behind Madras Cafe," he said. 

Sircar stated, "The film is a work of fiction, but it is based on research into real events, it has a resemblance to actual political events, dealing with civil war and the ideology of a rebel group." 

===Title===
The film was initially titled Jaffna after the northern Sri Lankan city.  It was renamed Madras Cafe, as the plot to kill Gandhi was hatched at the café.  The original location of the café is not specified in the film.   

===Casting===
John Abraham, the lead actor and one of the producers of the film, plays Vikram Singh, a military officer who is sent to Jaffna to head RAWs covert operations. "I had to lose a lot of muscle because these officers look like regular people. When they are in a crowd, they are completely inconspicuous," says Abraham.  Commenting on remarks comparing his look to Tom Hanks in Cast Away, he said: "Deciding on my look for the movie was quite challenging. It took lot of brain storming and we finalized this messy look, which apparently you think is inspired by Tom Hanks, but actually its not." 

American model-turned-actress Nargis Fakhri was cast to play Jaya Sahni, a British journalist in Jaffna.  For the role of foreign war correspondent, Fakhri was chosen because the director required "a girl who looked Indian but had an   accent". Thus this was the first film where her voice was not dubbed.   Initially, American-based actress Freida Pinto was chosen to portray Jaya Sahni, but due to prior commitments she withdrew from the project. 

Shoojit Sircar contacted model Sheetal Mallar for the film,  but as things did not work out, newcomer Rashi Khanna was signed for the role, marking her debut.  The cast also included a number of non-professional actors, such as quiz master Siddharth Basu, filmmaker Prakash Belawadi and journalist Dibang.   

===Location and sets=== M60s were used, for which special permission was obtained from the local authorities. 

The trailer was released on 12 July 2013.  The film was also dubbed in Tamil-language|Tamil. 

==Release==
The film was released on 23 August in India, the United States and United Arab Emirates.  However, it was not released in the United Kingdom and Canada as planned owing to the protests by Tamil diaspora in regards to its depiction of the Tamil rebels,  nor in Tamil Nadu where exhibitors feared its release was not worth the risk in regards to the controversy.   

===Critical reception=== Agent Vinod also hinted at, but Sriram Raghavan got carried away with the demands of the box office. Sircar chooses to keep it closer to reality."  Baradwaj Rangan later wrote in The Hindu, "Madras Café is not a comforting fantasy. It is the journey of any Indian operative who got wind of the fact that Rajiv Gandhi was going to be assassinated and did his damnedest to prevent it. The journalist in this film, for instance, is not the kind of cardboard cut-out we find in Madhur Bhandarkar and Prakash Jha films, but someone who has to decide between naming a source (and going against the ethics of her profession) and aiding an investigation."   

Rajeev Masand of CNN-IBN also praised Sircar, opining "Unlike in the West, its hard to make films on real-life historical events in India. Political pressures and sensitive groups invariably throw a spanner in the works. Which is why its commendable what director Shoojit Sircar has undertaken with Madras Café." 

The Pakistani newspaper Dawn (newspaper)|Dawn gave the film a positive review by saying "Shoojit Sircars human-drama of politics, rebellion, genocide and spy-games adapts Indian prime minister Rajiv Gandhis assassination plot, and the Sri Lankan civil war with sweaty palms and a gawky breakneck pace. And yet, for all its clumsy footing, at times, half-intelligent writing, it is engaging". The daily concludes that "For all its speed and embedded seriousness about global conflict, the nature of war, consequences and international trade, Madras Cafés lack of braves turns it into mellow spy-thriller. And trust me, the words "mellow" and "spy-thriller" do not gel." 
 Sri Lankan Tamil but the language you hear on the streets of Chennai – an odd gaffe for a film filled with so much research." 

===Awards===
In September 2013, Abrahams role as a RAW agent won him the "Pride of the Nation" award, given by the Anti-Terrorist Front, for "his attempt to raise the sensitive issue of former Prime Minister Rajiv Gandhis assassination".  The film was given Ramnath Goenka Memorial Award at 20th Screen Awards in January 2014.

==Box office==
Madras Cafe finished its theatrical run at the box office with average numbers. The film, which ruled the box office in its first week, saw a fall in business in its second week due to the release of Satyagraha (film)|Satyagraha. However, the film still fetched   in its second week, taking the grand total to  . 

Madras Cafe has not fared well in terms of overseas box office collections. In the USA and Australia, it has grossed around 56&nbsp;million INR.  . Box Office India. Retrieved 29 August 2013.   

==Soundtrack==
{{Infobox album  
| Name = Madras Cafe
| Longtype = to Madras Cafe
| Type = Soundtrack
| Artist = Shantanu Moitra
| Border = yes
| Alt =
| Caption =
| Released = August 2013
| Recorded = 2013 Feature film soundtrack Hindi
| Label = T-Series
| Producer = Shantanu Moitra
| Last album = Inkaar (2013 film)|Inkaar (2013)
| This album = Madras Cafe (2013) Ente (2013)
}}
The films soundtrack is composed by Shantanu Moitra and lyrics are written by Ali hayat, Zebunissa Bangash and Manoj Tapadia

{{Track listing
| headline = Madras Cafe extra_column = Singer(s) music_credits = no lyrics_credits = yes total_length = 30:46

| title1 = Sun Le Re Papon
| lyrics1 = Ali hayat
| length1 = 5:11

| title2 = Ajnabi Zebunissa Bangash
| lyrics2 = Zebunissa Bangash
| length2 = 5:17

| title3 = Khud Se
| extra3 = Papon
| lyrics3 = Manoj Tapadia
| length3 = 4:49

| title4 = Sun Le Re (Reprise)
| extra4 = Papon
| lyrics4 = Ali hayat
| length4 = 3:58

| title5 = Madras Cafe Theme
| extra5 = Instrumental
| lyrics5 =
| length5 = 4:04

| title6 = Conspiracy
| extra6 = Instrumental
| lyrics6 =
| length6 = 3:07

| title7 = Entry to Jaffna
| extra7 = Instrumental
| lyrics7 =
| length7 = 1:06

| title8 = Title Theme
| extra8 = Instrumental
| lyrics8 =
| length8 = 3:16

}}

==Controversy==
  Naam Tamilar Seeman stated MDMK party DMK party M Karunanidhi BJP president Ashish Shelar said "the film is an effort to glorify a particular political party and its leaders by demeaning  other sect of people. This cannot be permitted", and threatened to stall the release of the film in Mumbai. 
 human rights violations during the final stages of the war. Upon hearing the arguments, the court refused to grant an interim injunction to prevent the release of the Hindi version, while noting the Tamil version should not be released without the CBFCs clearance, which was later obtained.  It also issued notices to DGP, producer to give a detailed reply on charges by 3 September.  John Abraham has already refuted claims about Rajapaksa financing the film earlier during a promotional event. 
 Tamil prejudices, several theatres in the state chose not to screen the film. 

==See also==
* Assassination of Rajiv Gandhi
* 2009 Tamil diaspora protests
* Kuttrapathirikai The Terrorist
* Kannathil Muthamittal

==References==
   {{cite news|title=Jaffna Story, Preview & Synopsis
|url=http://www.cinebasti.com/movie/Jaffna/3540/story|accessdate=25 November 2012}} 
 
}}

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 