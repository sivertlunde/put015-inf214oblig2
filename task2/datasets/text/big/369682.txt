Lost in Translation (film)
{{Infobox film
| name           = Lost in Translation
| image          = Lost in Translation poster.jpg
| caption        = Theatrical release poster
| alt            =  
| director       = Sofia Coppola
| producer       = Sofia Coppola Ross Katz
| writer         = Sofia Coppola
| starring       = Bill Murray Scarlett Johansson Giovanni Ribisi Anna Faris Fumihiro Hayashi  Air
| cinematography = Lance Acord
| editing        = Sarah Flack
| studio         = American Zoetrope Tohokushinsha Film
| distributor    = Focus Features (US) Pathé (France) Constantin Film (Germany) Momentum Pictures (UK)
| released       =  
| runtime        = 101 minutes 
| country        = United States 
| language       = English Japanese
| budget         = $4 million   
| gross          = $119.7 million 
}} The Virgin Suicides (1999). It stars Bill Murray, Scarlett Johansson, Giovanni Ribisi, Anna Faris, and Fumihiro Hayashi.

The film revolves around an aging actor named Bob Harris (Murray) and a recent college graduate named Charlotte (Johansson) who develop a rapport after a chance meeting in a Tokyo Shinjuku Park Tower|hotel.
 Best Picture, Best Actor Best Director Best Original Best Actor Best Actress in a Leading Role respectively. The film was also a commercial success, grossing $119 million from a budget of only $4 million.

==Plot== Suntory whisky, for which he will receive $2 million. Charlotte (Johansson), a young college graduate, is left behind in her hotel room by her husband, John (Ribisi), a celebrity photographer on assignment in Tokyo. Charlotte is unsure of her future with John as she believes he takes more interest in his celebrity models, most notably a young and popular American actress named Kelly (Faris), than he does in her. At the same time, Bobs own 25-year marriage is tired and lacking in romance as he goes through a midlife crisis.

One night, after a long photo shoot, Bob retreats to the hotel bar. Charlotte, sitting at a table with John and friends, notices Bob and has a waiter take him a cup of peanuts from her table. Later, Bob and Charlotte have brief encounters each night at the hotel bar, until Charlotte invites Bob to meet up with some local friends of hers. Bob accepts and arrives later at her hotel room dressed in clothes that appear to be designed for a younger generation. The two begin a friendship and bond through their adventures in Tokyo together while experiencing the differences between Japanese and American culture, and between their own generations.

On the penultimate night of his stay, Bob attracts the attention of the resident vocalist. The next morning, Bob awakens to find the woman in his room, having apparently slept with her. Charlotte arrives at his room to go out for breakfast only to find the woman in his room, leading to conflict and tension over a subsequent lunch. Later that night, during a fire alarm at the hotel, Bob and Charlotte reconcile and express how they will miss each other as they make one more trip back to the hotel bar.

On the following morning, Bob is set for his departure back to the United States. He tells Charlotte goodbye at the hotel lobby shortly before checking out and sadly watches her retreat back to an elevator. While riding in a taxi to the airport, Bob sees Charlotte on a crowded street and gets out and goes to her. Bob embraces Charlotte and whispers something (substantially inaudible to the audience) in the tearful Charlottes ear. The two share a kiss, say goodbye and Bob departs.

==Cast==
* Bill Murray as Bob Harris
* Scarlett Johansson as Charlotte
* Giovanni Ribisi as John
* Anna Faris as Kelly
* Fumihiro Hayashi as Charlie Brown
* Akiko Takeshita as Ms. Kawasaki
* François Du Bois as the Pianist
* Takashi Fujii as TV host
* Hiromix as herself

==Analysis== Yutaka Tadokoro), and an interpreter (Takeshita) are on a set, filming a commercial for Suntory whisky (specifically, 17-year-old Hibiki (whisky)|Hibiki). In several exchanges, the director gives lengthy, impassioned directives in Japanese. These are invariably followed by brief, incomplete translations from the interpreter.

  Director  : The translation is very important, O.K.? The translation.
Interpreter  : Yes, of course. I understand. Bogie in Casablanca (film)|Casablanca, saying, "Heres looking at you, kid,"—Suntory time!
Interpreter  : He wants you to turn, look in camera. O.K.?
Bob: ...Is that all he said?   

In addition to the meaning and detail lost in the translation of the directors words, the two central characters in the film—Bob and Charlotte—are also lost in other ways. On a basic level, they are lost in the alien Japanese culture. But in addition, they are lost in their own lives and relationships, a feeling, amplified by their displaced location, that leads to their blossoming friendship and growing connection with one another.   

By her own admission, Coppola wanted to create a romantic movie about two characters that have a moment of connection. The storys timeline was intentionally shortened to emphasize this moment.  Additionally, Coppola has said that since "theres not much happening in the story besides  ", the filmmakers tried to keep an ongoing tension.   

  }}
Murray has described his biggest challenge in portraying Bob as managing the characters conflicted feelings. On one hand, Murray said, Bob knows that it could be dangerous to become too close to Charlotte, but on the other, he is lonely and knows that having an affair would be easy. Murray worked to portray a balance between being affectionate and being "respectable". 

The academic Marco Abel lists Lost in Translation as one of many films that belong to the category of "postromance" cinema, which he says offers a negative perspective of love, sex, romance, and dating. According to Abel, the characters in such films reject the idealized notion of lifelong monogamy. 
 floating world" polyglot film that challenges the film industrys "more usual tendency to ignore or deny issues of language difference" by highlighting Bob and Charlottes difficult contact with the Japanese language. 

===Aesthetics===
The author and lecturer Maria San Filippo contends that the films setting, Tokyo, is an audiovisual metaphor for Bob and Charlottes world views. She explains that the calm ambience of the citys hotel represents Bobs desire to be secure and undisturbed, while the energetic atmosphere of the city streets represents Charlottes willingness to engage with the world.  Coppola and Acord, the films cinematographer, agreed that Lost in Translation needed to rely heavily on visual expression to support the characters romance.

Robert Hahn, an essayist writing for The Southern Review, suggested that the filmmakers deliberately used chiaroscuro, the art of using strong contrasts between light and dark to support the story. He wrote that the films dominant light tones symbolize feelings of humor and romance, and they are contrasted with dark tones that symbolize underlying feelings of despondency. He compared this to the technique of the painter John Singer Sargent. 

The films opening shot, which features a close shot of Charlotte resting in transparent pink underpants, has interested various commentators. In particular, it has been compared to the portraitures of the painter John Kacere and the image of Brigitte Bardot in the opening scene of the 1963 film Contempt (film)|Contempt. Dwyer wrote that when the two shots are compared, they reveal the importance of language difference, as both films highlight the complexities involved with characters speaking multiple languages.    Filippo wrote that while the image in Contempt is used to remark on sexual objectification, Coppola "doesnt seem to be making a statement at all beyond a sort of endorsement of beauty for beautys sake".   
 mainstream film, and its subtle ones, which are typified by Independent film#The "Indie Film" Movement|"indie" film. 

==Production==

===Development===
  }} neon lights Park Hyatt Tokyo, where most of the films interior sequences take place, as one of her "favorite places in the world".  Particularly, she was attracted to its quietness, design, and "combination of different cultures", which include a New York bar and French restaurant. 
 The Virgin Suicides, and she spent little time planning or rewriting it.   Coppola has called the film a "valentine" to Tokyo,  in which she has displayed the city in the way that it is meaningful to her.

Coppola wrote the film with Murray in mind and said she would not have made it without him.  She said that she had always wanted to work with Murray and that she was attracted to his "sweet, lovable side".  She pursued him for five months to a year, relentlessly sending telephone messages and letters.       She enlisted help from Wes Anderson, who had directed Murray in two films, and screenwriter Mitch Glazer, who was a mutual friend.   In July 2002, Coppola and Murray finally met in a restaurant, and he agreed to participate because he "couldnt let her down". 
 The Big Sleep.  Murray and Johansson did not do any auditions before filming. 

===Filming===
Lance Acord, the films director of photography, has written that the cinematographic style of Lost in Translation is largely based on "daily experiences, memories and impressions" of his time in Japan.  He worked closely with Coppola to visualize the film, relying on multiple experiences he shared with her in Tokyo before production took place. Location scouting was carried out by Coppola, Acord, and Katz; and Coppola created 40 pages of photographs for the crew so that they would understand her visual intentions. 
 artificial lights Shibuya Crossing; they avoided police by keeping a minimal crew.  Acord avoided grandiose camera movements in favor of still shots to avoid taking away from the loneliness of the characters. 
 35 mm Kodak Vision 500T 5263 stock for nightlight exteriors and Kodak Vision 320T 5277 stock in daylight. A smaller Moviecam Compact was used in confined locations. Coppola said that her father, Francis Ford Coppola, tried to convince her to shoot on video, but she ultimately decided on film, describing its “fragmented, dislocated, melancholic, romantic feeling", in contrast with video, which is "more immediate, in the present".  In interviews, she said she wanted to shoot Tokyo with a spontaneous "informality", similar to the "way a snapshot looks", and she chose to shoot on high-speed film stocks to evoke a "homemade intimacy".    Some scenes were shot wholly for mood and were captured without sound. 

Lost in Translation was shot six days per week in September and October 2002, over the course of 27 days.  During this time, videotape footage was mailed to editor Sarah Flack in New York City, where she began editing the film in a Red Car office.    The scenes with Bob and Charlotte together were largely shot in order.  Many of the interior scenes were shot overnight, because the hotel did not allow the crew to shoot in much of its public space until after 1 a.m. 

Various locations were used during production; in particular, the bar featured prominently in the film is the New York Bar, which is situated on the 52nd floor of the Shinjuku Park Tower and part of the Park Hyatt in Shinjuku, Tokyo. Other locations include the Heian Jingu shrine in Kyoto and the steps of the San-mon gate at Nanzen-ji, as well as the club Air in the Daikanyama district of Tokyo. All of the locations mentioned in the film are the names of actual places that existed in Tokyo at the time of filming. Murray described the first few weeks of the shoot as like "being held prisoner", since he was affected by jet lag, and Johansson said the shoot made her "busy, vulnerable and tired".  

Coppola spoke of the challenges of directing the movie with a Japanese crew, since she had to rely on her assistant director to make translations.  Much of the performances were improvised, and Coppola openly allowed modifications to dialogue during shooting. For example, the dialogue in the scene with Harris and the still photographer was unrehearsed.  Coppola has said that she was attracted to the idea of Bob and Charlotte going through stages of a romantic relationship all in one week — in which they have met, courted, hurt each other, and discussed intimate life. To conclude this relationship, Coppola wanted a special ending even though she thought the concluding scene in the script was mundane. Coppola instructed Murray to perform the kiss in that scene without telling Johansson, to which she reacted without preparation. The whisper was also unscripted, but too quiet to be recorded. While Coppola initially considered having audible dialogue dubbed into the moment, she later decided that it was better to keep it "between the two of 
them." 

After filming, Coppola and Flack spent approximately 10 weeks editing the film.  In the bonus features of the films DVD, Murray called Lost in Translation his favorite film that he has worked on,  and Coppola described the film as her most "personal", since much of the story is based on her own experiences.  For example, Charlottes relationship with her husband is based on Coppolas relationship with her husband when she first married, and the "Suntory" commercial is based on a commercial Coppolas father, Francis Ford Coppola, shot with Akira Kurosawa. 

===Music===
  My Bloody Valentine. Coppola said much of the soundtrack consisted of songs that she "liked and had been listening to", and she worked with Reitzell to make Tokyo dream pop mixes.  Allmusic gave the soundtrack four out of five stars, saying "Coppolas impressionistic romance Lost in Translation features an equally impressionistic and romantic soundtrack that plays almost as big a role in the film as Bill Murray and Scarlett Johansson do." 
 More Than This" during the shoot itself because they liked the band and thought the lyrics fit the story. 

==Reception==

===Box office===
Lost in Translation was screened at the 2003 Telluride Film Festival.    It was given a limited release on September 12, 2003 in 23 theaters where it grossed $925,087 on its opening weekend with an average of $40,221 per theater and ranking 15th at the box office.   It was given a wider release on October 3, 2003 in 864 theaters where it grossed $4,163,333 with an average of $4,818 per theater and ranking 7th. The film went on to make $44,585,453 in North America and $75,138,403 in the rest of the world for a worldwide total of $119,723,856. 

===Critical response===
Lost in Translation opened to wide acclaim from western critics, particularly for Coppolas direction and the performances of Murray and Johansson. It has a "Certified Fresh" score of 95% on Rotten Tomatoes, with an average rating of 8.4 out of 10, based on 222 reviews. The critical consensus states "Effectively balancing humor and subtle pathos, Sofia Coppola crafts a moving, melancholy story that serves as a showcase for both Bill Murray and Scarlett Johansson.  The film also holds a score of 89 out of 100 based on 44 reviews on Metacritic indicating "universal acclaim".    Film critic Roger Ebert gave the film four out of four stars and rated it the second best film of the year, describing it as "sweet and sad at the same time as it is sardonic and funny", while also praising Bill Murray and Scarlett Johansson.   

In his review for The New York Times, Elvis Mitchell wrote, "At 18, the actress gets away with playing a 25-year-old woman by using her husky voice to test the level of acidity in the air ... Ms. Johansson is not nearly as accomplished a performer as Mr. Murray, but Ms. Coppola gets around this by using Charlottes simplicity and curiosity as keys to her character".    Entertainment Weekly gave the film an "A" rating and Lisa Schwarzbaum wrote, "working opposite the embracing, restful serenity of Johansson, Murray reveals something more commanding in his repose than we have ever seen before. Trimmed to a newly muscular, rangy handsomeness and in complete rapport with his characters hard-earned acceptance of lifes limitations, Murray turns in a great performance".   

In his review for  .  
 Antonioniesque flavour, its also a wispy romantic comedy with little plot and some well-observed comic moments".     In his review for The Guardian, Joe Queenan praised Coppolas film for being "one of the few Hollywood films I have seen this year that has a brain; but more than that, it has a soul."   

Rolling Stone magazines Peter Travers gave the film four out of four stars and wrote, "Before saying goodbye, they whisper something to each other that the audience cant hear. Coppola keeps her film as hushed and intimate as that whisper. Lost in Translation is found gold. Funny how a wisp of a movie from a wisp of a girl can wipe you out."     J. Hoberman, in his review for the Village Voice, wrote, "Lost in Translation is as bittersweet a brief encounter as any in American movies since Richard Linklaters equally romantic Before Sunrise. But Lost in Translation is the more poignant reverie. Coppola evokes the emotional intensity of a one-night stand far from home—but what she really gets is the magic of movies".     Entertainment Weekly put it on its end-of-the-decade, "best-of" list, saying, "Six years later, we still have no clue what Bill Murray whispered into Scarlett Johanssons ear. And we dont want to. Why spoil a perfect film?" 

The Los Angeles Film Critics Association and National Society of Film Critics voted Bill Murray best actor of the year.       The New York Film Critics Circle also voted Murray best actor and Sofia Coppola best director.    In addition, Coppola received an award for special filmmaking achievement from the National Board of Review.    Lost in Translation also appeared on several critics top ten lists for 2003. 

Roger Ebert added it to his "great movies" list on his website.  Paste Magazine named it one of the 50 Best Movies of the Decade (2000-2009), ranking it at #7. 
 flower arrangement; meanwhile she portrays the contemporary Japanese as ridiculous people who have lost contact with their own culture." 

Hawaii filmmaker and author E. Koohan Paik commented that "The Japanese are presented not as people, but as clowns." and "Lost in Translation relies wholly on the "otherness" of the Japanese to give meaning to its protagonists, shape to it plot, and color to its scenery. The inaccessibility of Japan functions as an extension of the alienation and loneliness Bob and Charlotte feel in their personal lives, thus laying the perfect conditions for romance to germinate: theyre the only ones who understand each other. Take away the cartooniness of the Japanese and the humor falls flat, the main characters intense yearning is neutralized and the plot evaporates"  

Japanese TV critic Osugi of Osugi and Piko fame said ""The core story is cute and not bad; however, the depiction of Japanese people is terrible!"  

In another Guardian article, journalist David Stubbs described Lost in Translation as "mopey, self-pitying drivel", and its characters as "spoiled, bored, rich, utterly unsympathetic Americans".  In another article published in a 2004 issue of Macleans, Steve Burgess criticized the film as an ethnocentric "compendium of unpleasant stereotypes ... indicative of the way visitors and foreign workers often view Japan."  Robin Antepara, an educator writing for an issue of Commonweal, opined that while the story embodies stereotypes, "Charlotte is the personification of Japanese watchfulness," an often neglected virtue.  Respected critic Calen Cole also lamented the films particularly meandering quality, noting that "it has a habit of lulling the viewer into a prescribed, choreographed sense of languor." Nevertheless, he applauded the film as being "classic," and said that he still "liked it." Another opinion was written by the academic Maria San Fillipino, in which she stated that the "retreat into America-centrism would be disappointing if, again, it were not so truthful ... Coppola knows firsthand that American tourists rarely get to know any Japanese well enough to discover their depth as sympathetic human beings."  Katz responded to some of the criticism by saying "Sofias love of Japan and love of the people that shes met there is incredibly evident in the film...Literally, we recounted experiences that I think all of us had gone through," and that none of the scenes were "any slight to Japanese people." 

===Accolades===
  Best Actor, Mystic River.
 Golden Globes Best Musical Best Screenplay, Best Musical Best Director, Best Musical or Comedy Actress.   
 BAFTA film Best Actor Best Actress awards. It was also nominated for best film, director, original screenplay, music and cinematography. It won four IFP Independent Spirit Awards, for Best Feature, Director, Male Lead, and Screenplay.    The film was honored with the original screenplay award from the Writers Guild of America.   

==Release==
Lost in Translation was released on DVD on February 3, 2004.   Entertainment Weekly gave it an "A" rating and criticized "the discs slim bonus features", but praised the film for standing "on its own as a valentine to the mysteries of attraction".   

Lost in Translation was also released in high definition on the now defunct HD DVD format.  A Blu-ray edition was released on January 4, 2011. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
 

 
 
{{succession box |
  | before = Chicago (2002 film)|Chicago
  | after = Sideways
  | title =  
  | years = 2004
|}}
 

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 