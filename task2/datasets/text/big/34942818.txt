My Kontrabida Girl
{{Infobox film
| name           = My Kontrabida Girl
| image          = Mykontrabidagirlposter.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Jade Castro
| producer       = Annette Gozon-Abrogar Jose Mari Abacan Katia Montenegro
| writer         = Jade Castro
| starring       = Rhian Ramos Aljur Abrenica Bea Binene Jake Vargas
| music          = 
| cinematography = 
| editing        = 
| studio         = GMA Films
| distributor    = GMA Films
| released       =  
| runtime        = 
| country        = Philippines Filipino English English
| budget         = 
| gross          = P13,092,496 
}} Filipino romantic comedy film directed by Jade Castro. It stars Rhian Ramos, Aljur Abrenica, Bea Binene and Jake Vargas. 

The films story centers on the life of a famous soap opera antagonist and her funny and heart-warming quest in finding her one true love.

==Plot==

Isabel Reyes (Rhian Ramos) is an infamous soap opera villain in the country. Everyone simply loathes Isabel which makes her the most condemned personality in the world of television.

But by some weird twist of fate, Isabel’s life turns upside down when she encounters a near-death accident. The wicked antagonist experiences a sudden change of heart and being the top kontrabida of her soap opera, Isabel couldn’t just deliver her heartlessness and cruelty in front of the camera. Her over-the-top villainous persona disappears and she finds herself powerless to kick and slap the lead star.

Isabel’s show hits a major ratings slump and the network advises her to take a break from work with hopes of reigniting her passion. To solve her dilemma, Isabel seeks the help of the biggest kontrabida icon in the country, no less than Ms. Bella Flores. She finds her on YouTube and says that for Isabel to get herself back, she needs to find the person who hurt her the most. And that person for Isabel is Chris Bernal (Aljur Abrenica).

Armed with pure wits and sheer determination, Isabel sets off to meet Chris who is still living in the same town where she left him many years back when she was a child. As they navigate their way through romance and heartbreak, will it further sabotage Isabel’s career and their chances for a relationship?

As they rediscover their past, Isabel begins to see Chris in a new light and she unexpectedly finds herself falling in love all over again with the very same guy who broke her heart. Will they ever get the right timing to be together? Will the Kontrabida girl find the perfect and genuine love she has been wishing for in the arms of Chris? 

==Cast Characters==

===Main Cast===
* Rhian Ramos as Isabel Reyes
* Aljur Abrenica as Chris Bernal
* Bea Binene as Joyce Bernal
* Jake Vargas as Poy

===Supporting Cast===
* Chariz Solomon as Charity
* Gwen Zamora as Cameo Appearance
* Bela Padilla as Evelyn
* Enzo Pineda as Enzo Ken Chan as JC
* Kevin Santos as Kevin
* Sef Cadayona as Dex
* Odette Khan as Aunt Marot
* Sabina Santiago

===Special Participation===
* Jeanie Rose L. Tolin as crazy girl
* Bianca Umali as young Isabel
* Julie Anne San Jose 
* Bella Flores 
* Celia Rodriguez
* Gladys Reyes 
* Louise delos Reyes
* Lolit Solis
* Tiya Pusit
* Jason Abalos 
* Jessa Mae T. Tejada
* Nardelyn C. Aguilar

==Theme Song== Derrick Monasterio. 

==Release==

===Local Release===
The film was released in Philippine theaters nationwide on March 14, 2012 and had a total of 3 weeks of screen time in these theaters.

===International Release=== The Road, although no confirmations have been made by the management of GMA Network nor its distribution partner, Freestyle Releasing.

===Critical Response===
The Cinema Evaluation Board gave My Kontrabida Girl a grade of B.  It received mixed to negative reviews from local movie critics. Rito Asilo of Philippine Daily Inquirer mentions the movies flaws but admits audiences can "have a hearty laugh."  Abby Mendoza of Philippine Entertainment Portal commented that My Kontrabida Girl is a better comedy than it is a romantic film but its really confused when it comes to another scenes. 

===Box office===
According to Box Office Mojo, My Kontrabida Girls opening week gross amounted to P6.7 million nationwide.  At the end of its third week, the movies total gross amounted to P13.1 million nationwide.

==Trivia==
* Aljur Abrenicas role is named after Kris Bernal, his on-screen loveteam partner.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 