Fair Weather Fiends
{{Infobox Hollywood cartoon|
| cartoon_name     = Fair Weather Fiends Woody Woodpecker 
| image            = Fiends-title.jpg
| caption          =  James Culhane
| story_artist     = Ben Hardaway Milt Schaffer
| animator         = LaVerne Harding Sidney Pillet Emery Hawkins Grim Natwick  
| background_artist  = Terry Lind
| voice_actor      = Ben Hardaway Lionel Stander
| musician         = Darrell Calker   
| producer         = Walter Lantz
| studio           = Walter Lantz Productions
| distributor      = Universal Studios
| release_date     =  
| color_process    = Technicolor
| runtime          =  6 41"
| movie_language   = English
| preceded_by      = The Reckless Driver
| followed_by      = Smoked Hams
}} Universal Pictures.

==Notes== James "Shamus" Dick Lundy would direct the series until the 1948 studio shutdown.
*The title is a parody of "fair weather friends", which means those who are only friends when circumstances are easy and pleasant.

==References==
*Cooke, Jon, Komorowski, Thad, Shakarian, Pietro, and Tatay, Jack. " ". The Walter Lantz Cartune Encyclopedia.

 
 
 
 
 
 


 