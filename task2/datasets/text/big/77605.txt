One Foot in Heaven
{{Infobox film
| name           = One Foot in Heaven
| image          = Original movie poster for the film One Foot in Heaven.jpg
| caption        = original promo poster
| director       = Irving Rapper
| producer       = Irving Rapper
| writer         = Hartzell Spence (book)
| based on       = One Foot in Heaven (1940 book)  
| screenplay     = Casey Robinson
| starring       = Fredric March Martha Scott Beulah Bondi Gene Lockhart
| music          = Max Steiner
| cinematography = Charles Rosher
| editing        = Warren Low
| country        = United States
| budget         =
| gross          = 
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 108 minutes
| language       = English
}}

One Foot in Heaven is a 1941 American biographical film starring Fredric March, Martha Scott, Beulah Bondi, Gene Lockhart and Elisabeth Fraser.

The movie was adapted by Casey Robinson from the autobiography by Hartzell Spence.  It was directed by Irving Rapper.

It was nominated for the Academy Award for Best Picture. 

==Plot== The Call" Methodist church ministry with circuit minister congregations to be flexible and change with the times.
  western but rather than finding it sinful, Will is impressed by its moral. The following Sunday he preaches a sermon advising his congregation that young people may have something to teach their elders.
 Harry Davenport), and another, influential banker Preston Thurston (Gene Lockhart), after organizing a childrens choir to replace the off-key church choir, run for years by Mrs. Thurston (Laura Hope Crews), her family and social circle. Soon after, Hartzell (Frankie Thomas) is expelled from school because of a gossip campaign falsely accusing him of making a young girl pregnant and forcing her family to move to San Francisco.
 Skinner organ, and carillon that as luxuries are being cut from the plans to finance a recreation center for the church. A year later, their dream church and parsonage finished, Will accepts the challenge of returning to Iowa to aid a small church in trouble, confident that he leaves behind a revitalized church when its members, including all those with whom he locked horns, gather spontaneously on a weekday morning to sing The Churchs One Foundation as he plays it on the new carillon.

==Cast==
* Fredric March as William Spence 
* Martha Scott as Hope Morris Spence 
* Beulah Bondi as Mrs. Lydia Sandow 
* Gene Lockhart as Preston Thurston 
* Elisabeth Fraser as Eileen Spence at Age 17  Harry Davenport as Elias Samson 
* Laura Hope Crews as Mrs. Preston Thurston  Grant Mitchell as Clayton Potter 
* Moroni Olsen as Dr. John Romer 
* Frankie Thomas as Hartzell Spence at Age 18 
* Jerome Cowan as Dr. Horrigan 
* Ernest Cossart as Mr. John E. Morris 
* Nana Bryant as Mrs. Morris 
* Carlotta Jelm as Eileen Spence at Age 11 
* Peter Caldwell as Hartzell Spence at Age 10

==Reception==

The film was received quite warmly both theatrically and critically, the New York Times calling it "a Fine and Human Story of a Ministers Life".  

==Notes==

Olivia de Havilland was originally scheduled for the role of Hope Spence, but was moved over to star opposite Errol Flynn in They Died With Their Boots On. 

Raymond Massey was Hartzell Spences choice to play his father,  while his mother preferred Fredric March.  The studios decision to cast March was made independently of the familys wishes.

The Reverend Norman Vincent Peale was hired as technical advisor on the film. 

The film which Spence goes to see with Hartzell is William S. Harts 1917 Western, "The Silent Man".   At the Hollywood premiere of the film, Hart was a guest of honor.

In an uncredited role, Gig Young has one of his first on-screen performances as a Groom asking for a dog license. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 