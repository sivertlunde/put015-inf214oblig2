Closet Cases of the Nerd Kind
{{Infobox Film |
  name     = Closet Cases of the Nerd Kind |
  image          = |
  director       = |
  producer       = Rick Harper Bob Rogers|
  writer         = Rick Harper Bob Rogers|
  starring       = Stan Greiwe Corey Burton Sandy Stotzer |
  distributor    = Pyramid Films |
  released   = 1980 |
  runtime        = 12 min |
  language = English |
    budget         = |
}} spoof of the classic science fiction film Close Encounters of the Third Kind.

==Plot==

In the film, sewage worker Roy Dreary and an assortment of odd characters meet up with some cute and cuddly extraterrestrials traveling to earth in a giant pie in the sky. Dreary develops an obsession with mashed potatoes, whipped cream and maraschino cherries. He encounters singing mailboxes, truck radios that spout bubbles and bubble music, and one pie in the face after another, before finally finding himself at the Sara Loo pie factory-and his close encounter of the nerd kind.  At one point, he even encounters Darth Vader on a motorcycle complaining that he is blocking the road.

Other characters include a wide-eyed cherubic child, a famous French scientist, a bewildered wife, a mysterious code which turns out to be the notation for the Greek letter Pi, and an oversized xylophone on which Dreary signals to the aliens. All of the character voices are over-dubbed by voice artists Corey Burton and Sandy Stotzer.

==Awards==
 Houston International Film Festival, and the Golden Eagle from CINE. While the film is not currently available on home video, it has been issued in the past on the compilation VHS release Hardware Wars, and Other Film Farces from Warner Home Video.

==External links==
* 

 
 
 
 
 
 

 