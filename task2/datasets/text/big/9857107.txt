Notte prima degli esami
{{Infobox Film  |
  name     = Notte prima degli esami |
  caption        = Original movie poster|
  image          =  Notte_prima_degli_esami_poster.jpg|
  producer         = Federica Lucisano Fulvio Lucisano Giannandrea Pecorelli|
  director       = Fausto Brizzi |
  writer         = Fausto Brizzi Massimiliano Bruno Marco Martani Giannandrea Pecorelli	  |
  starring       = Giorgio Faletti Cristiana Capotondi Nicolas Vaporidis Sarah Maestri Chiara Mastalli Andrea De Rosa Eros Galbiati Elena Bouryka Valeria Fabrizi |
  music         = Bruno Zambrini  |
  cinematography = Marcello Montarsi |
  editing         = Luciana Pandolfelli |
  distributor    = 01 Distribuzione |
  released   = February 17, 2006 (Italy) |
  runtime        = 100 min. | Italian |
  budget =  |
}}
 
 Italian comedy teen film,  written and directed by Fausto Brizzi. It describes the lives of two groups of Italian teenagers during the preparation of the esame di maturità (the final exam of Italian high school). It is set in Rome during the year 1989.

The inspiration for the title was a famous 1980s song by Antonello Venditti. 

==Plot==

The film follows two teenagers, Luca (Nicolas Vaporidis) and Claudia (Cristiana Capotondi), and their friends as they all prepare for the dreaded maturità (high school graduation) exams during the summer of 1989. At a party, Luca meets and immediately falls for Claudia. The film then follows both teenagers and their friends through their various personal experiences and adventures during the summer. In addition, throughout the film, Luca is desperately trying to get back in favour with his literature teacher (Giorgio Faletti), who will be the teacher sitting in on his oral exams. 
 The Wild Boys"), and Cecchetto ("Gioca Jouer"). 

==Sequels and remakes==

A sequel, Notte prima degli esami - Oggi was released in 2007.  The sequel features many of the same actors playing the same characters they played in the original, but it is set in 2006, during Italys World Cup winning summer. And in 2011, Italian television station Rai Uno aired Notte prima degli esami 82, a two-part miniseries involving many of the same characters as the first two movies, but using mostly different actors and setting the story in the summer of 1982.

A French remake of the original movie, titled Nos 18 ans, was released in 2008.

==References==
 

==External links==
*    (Official site)
*  

 
 
 
 


 