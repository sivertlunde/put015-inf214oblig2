Dreamscape (1984 film)
{{Infobox film
| name           = Dreamscape
| image          = Dreamscapeposter.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph Ruben
| producer       = Chuck Russell Bruce Cohn Curtis
| screenplay     = David Loughery Chuck Russell Joseph Ruben
| story          = David Loughery Roger Zelazny
| starring = {{Plainlist|
* Dennis Quaid
* Max von Sydow
* Christopher Plummer
* Eddie Albert
* Kate Capshaw
* David Patrick Kelly
}}
| music          = Maurice Jarre
| cinematography = Brian Tufano
| editing        = Lorenzo DeStefano Richard Halsey
| studio         = Zupnik-Curtis Enterprises
| distributor    = 20th Century Fox
| runtime        = 99 minutes
| released       =  
| country        = United States
| language       = English
| budget = $6 million 
| gross          = $12,145,169 
}} science fiction horror film directed by Joseph Ruben and written by David Loughery, with Chuck Russell and Ruben co-writing.

==Plot== REM sleep (i.e., while they are dreaming).  Novotny equates the original idea for the dreamscape project to the practice of the Senoi, who believe the dream world is just as real as reality. 

Alex is blackmailed into joining Novotny’s project that he (Novotny) intended to use for a benevolent purpose as a clinic to diagnose and treat sleep disorders, particularly in the form of nightmares, but the project has been hijacked by Bob Blair (Christopher Plummer), a powerful government agent with possible CIA ties, though it is never clearly revealed in the film. Alex eventually discovers that he is actually involved in a U.S. government-funded project to use this dream-linking technique for assassination. Before the plot is revealed, Alex gains experience helping a man worried about his wife’s infidelity and taking over the case of a young boy named Buddy (Cory Yothers) who is plagued with nightmares so terrible that a previous psychic lost his mind in an attempt to help Buddy. Buddys nightmare bogeyman involves a large snakeman which later becomes a weakness for Alex. 

 
A subplot involving Alex and Jane’s growing infatuation culminates with him sneaking into Janes dream without the use of the machine that is a part of the process, a point Jane does not realize at first because she is too angry that Alex was able to have sex with her in her dream. With the help of a novelist named Charlie Prince (George Wendt), who has been covertly investigating the project for the basis of a new book, Alex learns of Blair’s sinister intentions. 

 
Prince and Novotny are both murdered to silence them; things get worse when the President of the United States (Eddie Albert) is admitted as a patient and Alex’s colleague Tommy Ray Glatman (David Patrick Kelly), a psychopath who (as Alex discovers) shot and killed his own father, is sent into the Presidents nightmare by Blair in an attempt to assassinate the President. Blair considers the President a threat to national security due to the Presidents nightmares of a post-apocalyptic world, which represent his fears and becomes cause for his wishing to enter unfavorable negotiations for nuclear disarmament.

Alex and Jane manage to get close enough to the President’s room for Alex to project himself into the Presidents dream and save him: after a fight in which Glatman rips out a police officers heart, attempts to incite a mob of nuclear attack victims to attack the President, and battles Alex in the form of the snake-monster from Buddys dream, Alex assumes the appearance of Glatmans murdered father (Eric Gold) in order to distract him, allowing the President to ram a spear into Glatmans back, killing him. The President is grateful to Alex but reluctant to confront Blair, who apparently holds a truly powerful position in the government. To protect himself and Jane, Alex enters Blair’s dream and murders him before Blair can bring about any sort of retribution.

The film ends with Jane and Alex boarding a train to Louisville, Kentucky, intent on making their previous dream encounter a reality. Encountering the ticket conductor from Janes dream gives them a moment of pause.

==Cast==
* Dennis Quaid as Alex Gardner
* Max von Sydow as Dr. Paul Novotny
* Christopher Plummer as Bob Blair
* David Patrick Kelly as Tommy Ray Glatman
* Kate Capshaw as Jane DeVries
* George Wendt as Charlie Prince
* Eddie Albert as The President
* Chris Mulkey as Gary Finch
* Larry Gelman as Mr. Webber
* Cory “Bumper” Yothers as Buddy
* Redmond Gleeson as Snead
* Eric Gold as Tommy Rays father
* Peter Jason as Roy Babcock

==Production==
According to author Roger Zelazny, the film developed from an initial outline that he wrote in 1981, based in part upon his novella, "He Who Shapes", and novel, The Dream Master. He was not involved in the project after 20th Century Fox bought his outline. Because he did not write the film treatment or the script, his name does not appear in the credits; assertions that he removed his name from the credits are unfounded.  

==Release and reception==
Dreamscape was released on August 15, 1984. This was the second film released to movie theaters that was rated PG-13 under then new MPAA ratings guidelines following Red Dawn, which had come out five days prior. The Flamingo Kid was the first film to receive the rating, but was not released until December 1984. 

Dreamscape has a 75% Fresh rating on review aggregator Rotten Tomatoes from 28 critics. The RT consensus is "Dreamscape mixes several genres – horror, sci-fi, action – and always maintains a sense of adventure and humor."  The film is ranked #93 on Rotten Tomatoes Journey Through Sci-Fi (100 Best-Reviewed Sci-Fi Movies). 

==See also==
* The Dream Master Open Your Eyes
* Vanilla Sky
* Inception
* List of American films of 1984

==References==
 

==External links==
*   
*  
*  
*  
*   on YouTube
*   on YouTube

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 