China Behind
 
 
{{Infobox film
| name           = China Behind
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 再見中國
| director       = Tang Shu Shuen
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = Chang Chao-tang
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 110 min
| country        = Hong Kong
| language       = Mandarin
| budget         = 
| gross          = HK$ 1.939 M.
}} 1974 Cinema Hong Kong British colonial authorities,  that was lifted in 1980.   

The film was shot secretly in Taiwan without official authorization, and the cinematography was done by renowned Taiwanese photographer Chang Chao-tang (張照堂).

==Synopsis==
The plot concerns four college students and a middle-aged doctor who try to escape the devastation brought by the Chinese Cultural Revolution. 

==Reception== Best 100 Chinese Motion Pictures" during the 24th Hong Kong Film Awards ceremony on 27 March 2005. It was also was selected for inclusion in Hong Kong Film Archives "100 must-see Hong Kong movies" in 2011. 

==References==
 

==External links==
*  
*  
*  

 
 
 