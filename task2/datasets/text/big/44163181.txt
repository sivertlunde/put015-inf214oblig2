Mamalakalkkappurathu
{{Infobox film
| name           = Mamalakalkkappurathu
| image          =
| caption        =
| director       = Ali Akbar
| producer       =
| writer         = Ali Akbar
| screenplay     = Ali Akbar
| starring       = Manoj K Jayan Anil Elias Kilimol
| music          = Mohan Sithara
| cinematography = MJ Radhakrishnan
| editing        = Venugopal
| studio         = Prathikarana Films
| distributor    = Prathikarana Films
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by Ali Akbar. The film stars Manoj K Jayan, Anil, Elias and Kilimol in lead roles. The film had musical score by Mohan Sithara.   

==Cast==
*Manoj K Jayan
*Anil
*Elias
*Kilimol
*Manakkad Usha
*Master Hari
*Nazer
*Rathna Purushothaman
*Xavier
*Sheela

==Soundtrack==
The music was composed by Mohan Sithara and lyrics was written by Ali Akbar and TC John. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Nidra Veenudayum Raavil || K. J. Yesudas, Sindhudevi || Ali Akbar || 
|-
| 2 || Uchalu thira malavan || K. J. Yesudas, Sindhudevi || TC John || 
|-
| 3 || Vala Nalla Kuppivala  ] || K. J. Yesudas, Chorus || Ali Akbar || 
|-
| 4 || Vala Nalla Kuppivala     || Chorus, Sindhudevi || Ali Akbar || 
|}

==References==
 

==External links==
*  

 
 
 

 