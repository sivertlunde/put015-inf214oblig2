Soul Diaspora
 
{{Infobox film
| name           = Soul Diaspora
| image          = soul-diaspora-poster.jpg
| alt            = Soul Diaspora Poster
| caption        = Where is home?
| director       = Odera Ozoka
| producer       = Clotilde Delavennat, Odera Ozoka, James Peterson, Jonathan S. Abrams, Uju Uchendu
| writer         = Odera Ozoka
| starring       = Sadiq Abu Kristian Steel Mimi Vasser Brendan Jackson Maggie Maki Serge Eustache Donald Ajluni
| music          = 
| cinematography = Edwin Kim
| editing        = Richard Schachter
| studio         = 
| distributor    = 
| released       = 2009
| runtime        = 
| country        = United States
| language       = English/Fulani
| budget         = US$20,000   
| gross          = 
}}
Soul Diaspora is a 2009 film written and directed by Odera Ozoka. The film received 3 nominations at the 6th African Movie Academy Awards in 2010 for Best Actor in a Leading Role, Best film by an African Filmmaker in Diaspora, and AMAA Achievement in Sound, winning the award for Best film by an African Filmmaker in Diaspora. In addition, the film won the Audience Favorite Award at the 2010 Pan African Film Festival in Los Angeles, USA.

==Plot==
The story depicts Saidu (Sadiq Abu), a Nigerian immigrant living in Los Angeles who is forced to overcome sleepless nights of his tormented past in Africa. The audience finds him alone in this modern world, often hearing voices in his head, sometimes not even his, as the film interweaves color and black and white to illustrate this protagonists conflicted behavior and tortured mental state.

Saidus "lifes path" brings him to working less than minimum wage at a mechanic shop, forging little by little a friendship with a sixteen-year-old Afghani-American, Reza (Kristian Steel), the son of the shops friendly owner. Yet, even then, the mysterious Saidu is unable to fully overcome his alienation and loneliness. He soon meets and bonds with a stripper, Latisha (Mimi Vasser), after frequenting the same bar.

During the film, the audience meets an African-American named Tyrone (Serge Eustache) who may not share Saidus morality, but is confronted with internal troubles. He is embroiled in a seamy love triangle with Saidus encounter, Latisha and another woman, Lori (Maggie Maki), whom he has no feelings for but seems unable-or rather unwilling to rid himself of.

All the characters souls are stripped to the core by one searing (and national) event, which give them all fresh perspective. 

==Cast==
*Sadiq Abu
*Kristian Steel
*Mimi Vasser
*Clotilde Delavennat
*Brendan Jackson
*Maggie Maki
*Serge Eustache
*Donald Ajluni

==References==
 

==External links==
*  
*  

 
 
 
 
 


 