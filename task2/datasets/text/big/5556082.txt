Nasu: Summer in Andalusia
{{Infobox film
| name           = Nasu: Summer in Andalusia
| image          =
| alt            = 
| caption        = 
| director       = Kitarō Kōsaka Masao Maruyama
| writer         = Kitarō Kōsaka
| based on       =  
| starring       = Yo Oizumi
| music          = Toshiyuki Honda
| cinematography = 
| editing        = Takeshi Seyama Madhouse
| distributor    = Asmik Ace Entertainment
| released       =  
| runtime        = 47 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Afternoon manga magazine.

Kōsaka became interested in adapting the work after his long-time collaborator, Hayao Miyazaki, a fan of cycling, himself recommended the manga to Kōsaka.       The film soon went on to become the first Japanese anime film ever to be selected for the Cannes Film Festival.     
 English by German version was released as Nasu – Sommer in Andalusien.

A sequel,  , set in Japan on the  s, held at the 2008 Tokyo International Anime Fair.   

==Plot== Spanish cyclist, Iberian region of Andalusia. As the story progresses, Pepe is faced with frustrating consequences, with him facing pressure from his sponsors and the wedding of his former girlfriend, Carmen, to his elder brother, Angel, coinciding on the same day of the penultimate stage of the bicycle race. Originally is meant to be a backup racer (domestique) whose job is to assist his more prominent team-mate to win the race. However he accidentally overhears a conversation in his sponsors van through his radio com-link which had been negligently left open, in which he finds out, to his shock, that his sponsor intends to fire him after the race. Realizing that he may not find another team for the next season and that he has nothing left to lose, he disregards his instructions and sets out to win the race for himself.

==Influences== Telekom team of Erik Zabel, who Bazel is named after.  Zabel was one of the most prominent sprinters of all time, winning many stages in the Grand Tours.

==Credits== directors of photography were Katsuyoshi Kishi and Hisao Shirai, its art director was Naoya Tanaka and its editing was done by Takeshi Seyama. Its producer was Masao Maruyama and its executive producer was Tamotsu Shiina.

===Cast===
*Yo Oizumi – Pepe
*Eiko Koike – Carmen
*Hiroaki Hirata – Frankie
*Masahiko Tanaka – Ciocci
*Minoru Hirano – Hernandez
*Mitsumasa Daigo – Zulbaran
*Rintarou Nishi – Pizzaro
*Seiji Sasaki – Zamenhoff
*Toshio Kakei – Angel
*Yuko Kaida – Woman A
*Yuzo Sato – Gilmore

==Staff==
*Original Work: Iō Kurodas Nasu (manga)|Nasu
*Director, Screenplay, Animation Director: Kitarō Kōsaka
*Episode Direction: Atsushi Takahashi Masao Maruyama
*Ending Theme: Kiyoshirō Imawano
*Music: Toshiyuki Honda Madhouse
*Special Assistance: Nihon Jitensha Shinkōkai
*Production: Nasu: Summer in Andalusia Production Committee

==References==
 

==External links==
*    
*    
*  
*  

 

 
 
 
 
 
 
 
 
 
 