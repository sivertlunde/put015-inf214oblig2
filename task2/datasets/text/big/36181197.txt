China Bound
{{infobox film
| name           = China Bound
| image          =File:China Bound lobby card.jpg
| imagesize      =
| caption        =Lobby card
| director       = Charles Reisner
| producer       = Metro-Goldwyn-Mayer Frank Butler(story) Sylvia Thalberg(story) Peggy Kelly(writer) Robert E. Hopkins (intertitles)
| starring       = Karl Dane George K. Arthur
| music          =
| cinematography = Reggie Lanning
| editing        = George Hively
| distributor    = MGM
| released       =   reels
| country        = United States
| language       = Silent (English intertitles)
}}
China Bound is a 1929 silent film comedy produced and distributed by MGM. A rarely revived comedy due to some of its Asian racial content, a selected scene was shown in Robert Youngsons 1965 MGMs Big Parade of Comedy.  

Prints held at George Eastman House and Filmoteca Espanola (Madrid). 

==Cast==
*Karl Dane - Sharkey Nye
*George K. Arthur - Eustis
*Josephine Dunn - Joan
*Polly Moran - Sarah
*Carl Stockdale - McAllister
*Harry Woods - Officer

==References==
 

==External links==
* 
* 
* ... 

 

 
 
 
 
 
 
 
 


 