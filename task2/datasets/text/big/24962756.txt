Kiki (1926 film)
{{Infobox film
 | name = Kiki
 | image = File:Kiki 1926 film.jpg
 | caption = Lobby card
 | director = Clarence Brown 
 | writer = André Picard (novel) Hanns Kräly
 | starring = Norma Talmadge Ronald Colman
 | producer = Norma Talmadge
 | cinematography = Oliver T. Marsh 
 | distributor = First National Pictures
 | budget = 
 | released =  
 | country = United States
 | language = Silent film English intertitles
 | runtime = 108 minutes
}} silent romantic comedy film directed by Clarence Brown and starring Norma Talmadge and Ronald Colman. The film is based upon a 1920 novel of the same name by André Picard, which was later adapted by David Belasco and performed on Broadway to great success in 1921 by his muse Lenore Ulric.  

The film was restored from the only three "rather incomplete" surviving copies, one each in English, French and Czech. As noted in the prologue to the restored film, the English and French story lines differ.

==Plot==
Kiki (Norma Talmadge) ekes out a living selling newspapers on the streets of Paris. When she learns that a chorus girl has been fired from the Folies Barbes revue managed by Victor Renal (Ronald Colman), she sets out to fulfill her dream and apply for the job. Poverty stricken, she spends her rent money to buy suitable clothes. She gets kicked out the first time, as she was not sent by the Agency, but manages to sneak back in. While waiting in the reception area, she is mistaken for the secretary by an Agency applicant, who gives Kiki her letter of recommendation to present to Renal. He mistakes it for Kikis, and gives her an audition. Her singing talent gets her the job.

Her debut, however, is a disaster. She repeatedly gets in the way of the shows star and Renals fiancee, Paulette Mascar (Gertrude Astor). Paulette finally pushes her, sending her crashing into a harp in the orchestra pit. When Renal tries to separate the battling women, Paulette slaps him.

Renal also sends Kiki a letter of dismissal. When she comes to see him, he feels sorry for her and gives her back her job. He tries to hustle her out of his office, before Paulette enters, but Kiki refuses to leave. As a result, Paulette and Renal have a falling out. 

Renal decides to take Kiki to dinner. Paulette goes out with Renals financial backer, Baron Rapp (Marc McDermott), and ends up at the same restaurant.  Determined to humiliate her rival, Paulette invites herself to Renals table and taunts Kiki into drinking several glasses of champagne. Kiki becomes drunk, embarrassing Renal. He deposits her in his limousine and asks where she lives. As she has been evicted for not paying the rent, she confesses she has no place to go, so he takes her home.

He kisses her, but when he tries for more, she locks herself in his bedroom. He is forced to sleep (uncomfortably) in another room. Each day, Renal decides to get rid of her, but each night he relents. Meanwhile, Kiki intercepts Paulettes daily letters to him to prevent a reconciliation.

Renal finally learns about the letters from Rapp. Rapp recommends he get back together with Paulette and offers to take Kiki off his hands. Kiki mistakenly believes that Renal wants Paulette back, while Renal thinks in error that Kiki welcomes Rapps attentions and greater wealth. Kiki decides to go, taking only what she came with, but then changes her mind and decides to fight for her love. After she threatens Paulette with a knife, Renal orders her to leave. Thinking quickly, Kiki pretends to fall into a coma, convincing a doctor that she is a victim of catalepsy, which the doctor states might last up to two years. Upon hearing this, Rapp makes a hasty departure. Despite Paulettes urging to leave for a performance, Renal decides he cannot leave Kiki alone. Once Paulette is gone, Kiki kisses Renal and confesses she loves him. He embraces her.

==Cast==
* Norma Talmadge as Kiki
* Ronald Colman as Victor Renal
* Gertrude Astor as Paulette
* Marc McDermott as Baron Rapp
* George K. Arthur as Adolphe, Renals valet who becomes Kikis enemy
* William Orlamond as Brule
* Erwin Connelly as Joly
* Frankie Darro as Pierre
* Mack Swain as Pastryman

==Reception==
Kiki was one of the few comedy films Norma Talmadge made. Like her other films, Kiki was praised for its quality of productions, first-rate cast, clothes and settings.  Talmadge was praised by critics, but the film became a commercial failure. Her fans demanded for her only to do drama films, and not comedy.  
 remade in 1931, starring Mary Pickford. 

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 