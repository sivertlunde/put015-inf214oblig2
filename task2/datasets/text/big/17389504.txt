Red Lights (2004 film)
 
{{Infobox film
| name           = Lights
| image          = Feux rouges 127614.jpg
| caption        = Theatrical Poster
| director       = Cédric Kahn
| producer       = Patrick Godeau
| based on       =  
| starring       = Jean-Pierre Darroussin Carole Bouquet Vincent Deniard
| music          = 
| cinematography = Patrick Blossier
| editing        = Yan Dedet
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
}}
Red Lights ( ) is a 2004 French thriller film directed by Cédric Kahn. The film is adapted from a Georges Simenon Red Lights (novel)|novel,  written in 1955 and set in the northeastern United States. The film is set in modern-day France.

==Synopsis==
The film stars Jean-Pierre Darroussin and Carole Bouquet as married couple Antoine and Hélène Dunan on a road trip to pick up their children and begin a vacation.  After a number of arguments the two are separated and they both encounter a horror on the road.

==Plot==
One summer mid-afternoon, Antoine (Darroussin) leaves his insurance firm job to meet up with his wife Hélène (Bouquet), as they are to fetch their kids (somewhere distant, but it is not explained as to exactly where they are or who they’re with), and then head on to his in-laws in the ‘Basque Country’ for two weeks vacation, and he wants to beat the traffic (and the two million cars that will be on the road). They arrange to meet at a local bar, but she is late arriving and it gets on his nerves. When she does arrive, it’s clear their relations have been strained for some time.

She wants to go home to shower and have a quick cold dinner, so they do, further frustrating him. They finally get on the road, but after a while the horribly jammed traffic gets to him, and he decides to get off the highway and take ‘the back way’, at least to Tours anyway. He’s tired, but won’t let her drive, and they spat. He stops several times along the way to grab a drink or two at local bars, while she sits in the car. He becomes slightly inebriated, further causing tension and argument among them. Her breaking point is reached when he stops again, and she threatens to drive on without him, telling him he can take the train and meet up with her later. He takes the car keys and goes into the bar, where, as he drinks, he sees a news item on the TV about a man who has escaped from a nearby prison. When he eventually leaves the bar and returns to his car, Hélène is not there, having left him a note: “I’m taking the train.”

Somewhat frantic, he drives to the train station nearby to look for her, but the last train of the night has left. He drives on to the next train stop, but is delayed along the way at a police check-point, where they’re looking for the escaped convict. After arriving after midnight and twenty-five minutes too late at the train station, he yet again goes into a bar to drown his sorrows. He tries to strike up a conversation with a large, quiet man, who is not particularly interested in such, and whom leaves while Antoine is not looking.

Eventually leaving the bar, Antoine finds the large man outside, asking to hitch a ride on to Bordeaux, which Antoine allows. After being on the road awhile, Antoine realizes the man (Vincent Deniard) is the escaped convict, but the rebel in himself thrills him more than causing any fear. Antoine even openly admires the thug for his independent spirit, and they even manage to get through another check-point.

Antoine stops to buy gasoline, and manages to get a small bottle of whiskey too, downing it almost completely. The convict is annoyed at his drinking, but soon falls asleep, when Antoine, now drunk, runs off the road causing the car’s tire to go flat. The convict demands he change it, but Antoine passes out, so the convict does. The convict slaps Antoine awake, and they get back on the road to Bordeaux. Not much later the convict turns off the road, causing them to argue, and the convict beats Antoine pretty good. When he comes to, he finds the convict dragging him from the car out in the woods, likely to be killed and left there. Antoine says, "Make it fast and clean; I dont want to suffer." While the convict retrieves the tire jack from the car to use as a weapon, Antoine manages to hide. The convict finds him and they struggle, Antoine getting hold of the jack, which he uses to beat the convict to death.

He tries to leave, but manages to disable the car, and eventually hitches a ride into town, After making arrangements to get the car fixed, he makes more than a dozen phone calls attempting to locate his wife.  Finally succeeding, he discovers she is in hospital after being wounded in an ‘incident’ at the train station in Poitiers. He goes to the hospital where she is, whereupon police lieutenant Levet (Jean-Pierre Gos) notifies him his wife was beaten, robbed, and raped by the dangerous convict, whose body has now been discovered. Antoine explains to the lieutenant what had happened between himself and Hélène, but leaves out his subsequent ‘adventure’, of course. The lieutenant may suspect Antoine, but the issue is not pushed any further.
 flashback nightmare as Hélène is recovering, realizing the terrible things he has done. Antoine finally gets in to see Hélène, and apologies for everything. They reconcile and express their love for each other. After another day, leaving the hospital to fetch their kids, the last scene is the two of them driving happily and quietly down a country road.

==Cast==
*Jean-Pierre Darroussin as Antoine Dunan
*Carole Bouquet as Hélène Dunan
*Vincent Deniard as L homme en cavale (man on the run/the convict)
*Jean-Pierre Gos as L‘ Inspecteur (police lieutenant Levet)

==Reception==
Critical reaction to the film was positive when released. The New York Times reviewer Stephen Holden called the film a "brilliant, sinister French thriller". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 