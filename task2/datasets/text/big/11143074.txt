Naanum Oru Thozhilali
{{Infobox film
| name = Naanum Oru Thozhilali
| image =
| image_size =
| caption =
| director = C. V. Sridhar
| producer = C. V. Sridhar
| writer = C. V. Sridhar
| narrator = Ambika Jaishankar
| music = Ilaiyaraaja
| cinematography = P. Bhaskar Rao
| editing = M. Umanath
| studio = Chitralaya Films
| distributor = Chitralaya Films
| released = 1 May 1986
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}}
 1986 Tamil Ambika in the lead roles while Jaishankar, V.S. Raghavan|V. S. Raghavan and Devika play supporting roles. The film released in May 1986.

==Cast==
* Kamal Haasan Ambika
* Jaishankar
* V.S. Raghavan|V. S. Raghavan
* Veera Raghavan
* Devika
* Shetty Deepa in a special appearance
* Rajiv in a special appearance

==Production== MGR in the lead titled Naanum Oru Thozhilali in the mid-1970s, which had been shelved. 
 Deepa and Rajiv make special appearances in a Qawali song in the film. 

==Release==
Naanum Oru Thozhilali released on May 1, 1986 alongside Sridhars other film Yaro Ezhuthia Kavithai and it became a rare incident when two films of the same director were released simultaneously. But only this film emerged successful at the box office.  Hoever, critics point out that the difference of quality between the Cinemascope footage shot in 1981 and 1:1.33 footage shot in 1986 were visible throughout the film including the color quality and the appearance of the lead actors.

==Soundtrack==
{{Infobox album|  
  Name        = Naanum Oru Thozhilali
|  Type        = Soundtrack
|  Artist      = Ilaiyaraaja
|  Cover       =
|  Released    = 1986
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Naanum Oru Thozhilali (1986)
|  Next album  = 
}}

The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 04:14
|-
| 2 || Angel Aadum Angel || P.Suseela || 04:29
|-
| 3 || Mandaveli || S. P. Balasubrahmanyam || 03:15
|-
| 4 || Naan Pooveduthu || S. P. Balasubrahmanyam, S. Janaki || 04:37
|-
| 5 || Oru Nilavum Malarum || S. P. Balasubrahmanyam, S. Janaki || 04:32
|-
| 6 || Pattu Poove || S. P. Balasubrahmanyam || 04:30
|-
| 7 || Sembaruthi Poove || S. P. Balasubrahmanyam, S. Janaki || 04:41
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 