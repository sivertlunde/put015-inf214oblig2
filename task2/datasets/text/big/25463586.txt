List of Telugu films of 1993
 
  Tollywood (Telugu Hyderabad in 1993 in film|1993.

==1993==
{| class="wikitable"
|-
! Title !! Director !! Cast !! Music director !! Sources
|- Amala || ||
|- Vijayakumar || ||
|-
|Abbai Garu|| E. V. V. Satyanarayana || Daggubati Venkatesh, Meena Durairaj || M. M. Keeravani ||
|-
|Allari Alludu|| A. Kodandarami Reddy || Akkineni Nagarjuna, Naghma, Meena Durairaj, Vanisri ||M. M. Keeravani ||
|-
|Allari Priyudu || K. Raghavendra Rao || Rajasekhar (actor)|Rajasekhar, Ramya Krishna, Madhoo || M. M. Keeravani ||
|-
|Amma Koduku || Vadde Ramesh || Rajasekhar (actor)|Rajasekhar, Sukanya, Brahmanandam || Ilayaraja ||
|-
|Bangaru Bullodu|| Raviraja Pinisetty || Nandamuri Balakrishna, Ramya Krishna, Raveena Tandon || Raj-Koti ||
|-
|Bava Bavamaridi ||  Sarath || Suman (actor)|Suman, Krishnam Raju, Malashri, Jayasudha,  Silk Smitha || Raj-Koti ||
|-
|Chitemma Mogudu || Kodi Ramakrishna || Mohan Babu, Divya Bharati, Pooja Bedi, Brahmanandam || M. M. Keeravani ||
|- Vamshi || Mohan Babu, Nirosha, Mohini || Ilayaraja ||
|-
|Donga Donga || Mani Ratnam || Prashanth Thiagarajan, Heera Rajagopal, Anu Agarwal, Anand (actor)|Anand, S. P. Balasubramaniam || A. R. Rahman || 
|-
|Gaayam || Ram Gopal Varma || Jagapathi Babu, Revathi Menon, Urmila Matondkar, Shiva Krishna, Kota Srinivasa Rao || Ilayaraja ||
|-
|Gentleman || S. Shankar || Arjun Sarja, Madhoo, Shuba Sri, Goundamani, Senthil || A. R. Rahman || 
|-
|Jamba Lakadi Bamba || E. V. V. Satyanarayana || Naresh (actor)|Naresh, Aamani, Brahmanandam || Raj-Koti ||
|- Vamshi || Rajendra Prasad, Vani Viswanath, Shamili || Vamsy|Vamshi||
|- Suman || M. M. Keeravani ||
|-
|Kongu Chatu Krishnudu || K. Ajay Kumar || Naresh (actor)|Naresh, Meena Durairaj, Brahmanandam || Sri ||
|-
|Kunti Putrudu || Dasari Narayana Rao || Mohan Babu, Vijayashanti || Ilayaraja ||
|-
|Mahanadi (film)|Mahanadi || Santhana Bharathi || Kamal Hassan, Sukanya, Shobana, Poornam Vishwanathan || Ilayaraja ||  
|- Major Chandrakanth ||K. Raghavendra Rao ||N.T. Rama Rao, Mohan Babu, Naghma, Ramya Krishna || M. M. Keeravani ||
|- Vidyasagar ||
|-
|Mayalodu || S. V. Krishna Reddy || Rajendra Prasad, Soundarya || S. V. Krishna Reddy ||
|-
|Mechanic Alludu|| B. Gopal || Chiranjeevi, Akkineni Nageswara Rao, Vijayashanti|| Raj-Koti ||
|-
|Money (1993 film)|Money|| Shiva Nageswara Rao || J. D. Chakravarthy, Chinna, Renuka Shahane, Jayasudha, Paresh Rawal || Sri ||
|- Bapu || Rajendra Prasad, Aamani || M. M. Keeravani ||
|-
|Muta Mesthri|| A. Kodandarami Reddy || Chiranjeevi, Meena Durairaj, Roja Selvamani || Raj-Koti ||
|-
|Nippu Ravva|| A. Kodandarami Reddy || Balakrishna Nandamuri, Vijayashanti, Shobana || Bappi Lahiri ||
|-
|Parugo Parugu || Relangi Narasimha Rao || Rajendra Prasad, Shruti || Raj-Koti ||
|- Koti ||
|-
|Rajendrudu Gajendrudu || S. V. Krishna Reddy || Rajendra Prasad, Soundarya || S. V. Krishna Reddy ||
|-
|Rakshana|| Uppalapati Narayana Rao || Akkineni Nagarjuna, Shobana, Roja Selvamani, Nasser || M. M. Keeravani ||
|-
|Rendilla Poojari || T Prabhakar || Suman (actor)|Suman, Naghma, Shobana || M. M. Keeravani ||
|-
|Topi Raja Sweety Roja || N Shiva Prasad || Rajendra Prasad, Roja Selvamani || Rajendra Prasad ||
|- Srikanth || M. M. Keeravani ||
|-
|}
==Reference==
 
 
 
 

 
 
 