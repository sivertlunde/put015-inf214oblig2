Rosarigasinos
{{Infobox film
| name           = Rosarigasinos
| image          = Rosarigasinos-poster.jpg
| caption        = DVD poster
| director       = Rodrigo Grande
| producer       = Adolfo Aristarain José A. Martínez Suárez José Martínez
| writer         = Rodrigo Grande
| narrator       = 
| starring       = Federico Luppi Ulises Dumont
| music          = Ruy Folguera
| cinematography = Félix Monti
| editing        = Miguel Pérez
| distributor    = INCAA
| released       =    
| runtime        = 90 minutes
| country        = Argentina Spanish
| budget         = 
}}
Rosarigasinos ( ) is a 2001 Argentine film, written and directed by Rodrigo Grande and starring Federico Luppi and Ulises Dumont.  The film is also known as Presos del Olvido in Spain. 

The film was produced by Adolfo Aristarain, José Martínez, and José A. Martínez Suárez; the associate producer was Alfredo Suaya and was partly funded by the INCAA.

==Plot==
The picture tells of two prison friends who cope with life outside jail after being paroled.

Tito (Federico Luppi) and Castor (Ulises Dumont) are two robbers whose failed scheme landed them in the Rosario prison for 30 years. Before being jailed, however, the duo stashed a whole lot of cash near the Paraná River. They plan on getting back to it as soon as theyre released.

Thirty years later, however, their insecurities and the pressures of being re-adjusted to society, threaten to ruin their perfect crime.

==Cast==
* Federico Luppi as Tito
* Ulises Dumont as Castor
* María José Demare as Morocha
* Francisco Puente as El Gordo
* Gustavo Luppi as Young Tito
* Enrique Dumont as Young Castor
* Saul Jarlip as Ramoncito Fernández

==Distribution==
The film had its premiere in Argentina on June 19, 2001.

The film was shown at various film festivals, including: the Mill Valley Film Festival, USA; the Chicago Latino Film Festival, USA; the Lleida Latin-American Film Festival, Spain; the Los Angeles Latino Film Festival, USA; the Bogota Film Festival, Colombia; and others.

==Awards==
Wins
* Huelva Latin American Film Festival: Silver Colon; Best Actor, Ulises Dumont; Special Jury Award, Rodrigo Grande; 2001.
* Mar del Plata Film Festival: AMUCI Jury - Special Mention, Ruy Folguera; Best Actor Ulises Dumont and Federico Luppi; 2001.
* Argentine Film Critics Association Awards: Silver Condor; Best Music, Ruy Folguera; 2002.
* Lleida Latin-American Film Festival: Best First Work, Rodrigo Grande; 2002.

Nominations
* Argentine Film Critics Association Awards: Silver Condor; Best Actor, Ulises Dumont; Best Actor, Federico Luppi; Best New Actor, Francisco Puente; Best Original Screenplay, Rodrigo Grande; Best Supporting Actress, María José Demare; 2002.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*    

 
 
 
 
 
 