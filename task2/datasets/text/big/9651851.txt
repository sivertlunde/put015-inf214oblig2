California Straight Ahead!
{{Infobox film
| name           = California Straight Ahead!
| image_size     =
| image	         = California Straight Ahead! FilmPoster.jpeg
| caption        =
| director       = Arthur Lubin
| producer       = Trem Carr
| writer         = Herman Boxer Scott Darling
| narrator       =
| starring       = John Wayne
| music          =
| cinematography = Harry Neumann
| editing        = Charles Craft Erma Horsley
| distributor    =
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
}}
California Straight Ahead! is a 1937 American film about truck drivers starring John Wayne and directed by Arthur Lubin.  The action movie features a memorable cross-country race between trucks and trains.

== Cast ==

* John Wayne as Biff Smith Louise Latimer as Mary Porter
* Robert McWade as "Corrigan"
* Theodore von Eltz as James Gifford
* Tully Marshall as "Harrison"
* Emerson Treacy as Charlie Porter Harry Allen as "Fish" McCorkle
* LeRoy Mason as "Padula"
* Grace Goodall as Mrs. Porter
* Olaf Hytten as "Huggins"
* Monte Vandergrift as "Clancy"
* Lorin Raker as a Secretary

==See also==
* List of American films of 1937
* John Wayne filmography

==External links==
* 
*  

 

 
 
 
 
 
 
 
 

 