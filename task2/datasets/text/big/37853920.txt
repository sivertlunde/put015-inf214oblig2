Yaaruda Mahesh
 
 

{{Infobox film
| name           = Yaaruda Mahesh
| image          = Yaaruda Mahesh poster.jpg
| caption        = Movie poster
| director       = R. Madhan Kumar
| producer       = Antony Nawaz Sathya Narayanan R. Madhan Kumar
| writer         = R. Madhan Kumar
| story          = R. Madhan Kumar
| screenplay     = R. Madhan Kumar
| starring       = Sundeep Kishan Dimple Chopade
| music          = Gopi Sunder
| cinematography = Raana
| editing        = 
| studio         = Colour Frames Red Studios Anbu Pictures
| released       =  
| runtime        = 
| country        = 
| language       = Tamil
| gross =  
}}

Yaaruda Mahesh (English: Who is Mahesh) is a 2013 Tamil Adult comedy film directed by R. Madhan Kumar, starring Sundeep Kishan and Dimple Chopade. It also has the likes of Jagan, Srinath, Livingston, Uma Padmanaban, Swaminathan, Singamuthu and Nellai Siva supporting the lead.  The film has been shot around Chennai and Kerala.  The film produced by Red Studios and Colour Frames. The films soundtrack and background score were composed by Gopi Sunder, while Raana handled the cinematography.  The film released on 26 April to below average reviews.  The film did average business at the box office.  The film is also dubbed in to Telugu in the name of Mahesh. The film is co-produced by ee-rojullo fame maruthi. 

==Plot==
The film opens with Shiva going to college for testing. While testing, Shivas pen runs out of ink and he asks for a new one. He receives a new one from the girl next to him, Sindhya. He immediately falls for her and starts following her. He attends an educational tour with Sindhya, Priya, (Vasanths lover), Vasanth himself, and Shiva. Eventually, Sindhya falls for Shiva too. He then finds out that Sindhya got the top rank in the test and a scholarship to the US. Before she leaves, Sindhya invites Shiva to breakfast at her house while her family is out. At her house, Shiva strips her and the two end up having sex. Sindhya then leaves to the US. She comes back 3 months later, saying that she has some private information to tell him. When she gets back, she announces to Shiva that she is pregnant and thinks that no one is in the house. However, Shivas mom and dad overhear and they scold the two for having sex.

The film skips ahead a few years where it shows Shiva and Sindhya, husband and wife, having a child. Sindhya is working while Shiva is an
unemployed husband, looking after the child at home. Sindhyas brother, Randy, a psychiatrist tries to help by setting up a trick for Shiva to discover his interests. He sets up a plot in which Shiva overhears Sindhya talking to a person named Mahesh who is really Randy, making Shiva think that the child is not his, but someone elses. Sindhya and Randy want Shiva to overhear them talking. Shiva gets suspicious on who Mahesh is and starts getting data from the library, helped by Vasanth. The climax is in the people he meets and he finally figures who it is. He accuses Sindhya and tries to kill the child. His family and Randy then explain what happened. He then rewrites the test and passes, it ends with Shiva getting a job and Randy still on his case on whether he is back to normal.

==Cast==
* Sundeep Kishan as Shiva
* Dimple Chopade as Sindhya
* Jagan as Vasanth
* Sneha Ramesh
* Vijay Srinath as Randy Livingston
* Uma Padmanabhan
* Swaminathan
* Robo Shankar
* Singamuthu
* Nellai Siva
* Venkat Sundar as Military
* Sana Oberoi (Special appearance)

==Release== Sun TV. The film was given an "A" certificate by the Indian Censor Board. The film released in 72 screens on 26 April 2013 with another movie Naan Rajavaga Pogiren. 

==Box office==
The film opened with 90 shows on its first weekend in Chennai box office. It was removed after one week. It sold 6,600 tickets in Chennai during its lifetime.The film final gross over more than   worldwide.The film declared average grosser. 

==Critical reception==
The film received Below Average reviews from critics.
TOI wrote:"If double-meaning, adult jokes are your cup of tea, you will enjoy this movie".  Indianexpress wrote:"With a wafer-thin plot, the debutant director weaves together a string of incidents. At times, it seems more like a theater of the absurd, with quirky characters and scenes added on just for its bizarre humor quotient, rather than for its relevance to the plot".  Behindwoods wrote:"To sum it up, Yaaruda Mahesh is a unidirectional product that targets youth with liberal dose of adult humor but is not powerful enough to sustain the interests through the entire length of the film".  

==Soundtrack==
Soundtrack is composed by Gopi Sundar.

* Yaaruda Andha Mahesh&nbsp;— Gopisundar, Anna Katherina
* Pudhu Paarvai&nbsp;— Haricharan, Priya Himesh
* Vayadhai Keduthu&nbsp;— Anna Katherina, Suchith Suresan
* Odum Unakkithu&nbsp;— Anna Katherina, Suchith Suresan
* Yemaathita&nbsp;— Mukesh, Gopisundar
* Uyire Uyiril&nbsp;— Gopisundar

==References==
 

==External links==
*  

 
 
 
 
 