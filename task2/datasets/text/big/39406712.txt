Wrong Cops
{{Infobox film
| name           = Wrong Cops
| image          = Wrong Cops.jpg
| alt            =
| caption        = 
| director       = Quentin Dupieux
| producer       = Gregory Bernard Diane Jassem Josef Lieck
| writer         = Quentin Dupieux
| starring       = Mark Burnham Éric Judor Marilyn Manson Quentin Dupieux
| cinematography = 
| editing        = Quentin Dupieux	 	
| studio         = Realitism Films
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = France United States 
| language       = English
| budget         = 
| gross          = 
}} independent comedy film written and directed by Quentin Dupieux. The ensemble film premiered at the 2013 Sundance Film Festival.

==Production==
The first trailer was launched on November 22, 2013. 

==Synopsis== crooked cops look to dispose of a body that one of them accidentally shot.

==Cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Mark Burnham || Duke 
|-
| Éric Judor || Rough
|-
| Marilyn Manson || David Delores Frank 
|- Steve Little || Sunshine
|-
| Grace Zabriskie || Donna 
|-
| Eric Roberts || Bob
|-
| Arden Myrin || Shirley
|- 
| Eric Wareheim || Officer de Luca
|-
| Isabella Palmieri || Rose
|-
| Jennifer Blanc || Ruth
|-
| Jack Plotnick || Dolph Springer
|- Steve Howey || Sandy/Michael
|-
| Kurt Fuller || Music Producer
|- Daniel Quinn || Neighbor
|-
| Ray Wise || Captain Andy
|}

==Critical reception==
From Christie Ko of ScreenCrave:
 

From Alex Koehne of Twitch:
 

From Scott Menzel of We Live Film did not care for the film:
 

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 