La Antena
{{Infobox Film
| name           = La antena
| image          = Laantena poster.png
| image_size     = 
| caption        = Theatrical release poster
| director       = Esteban Sapir
| producer       = Federico Rotstein	
| writer         = Esteban Sapir
| narrator       = 
| starring       = Alejandro Urdapilleta Julieta Cardinali Rafael Ferro Valeria Bertuccelli
| music          = Leo Sujatovich
| cinematography = Cristian Cottet
| editing        = Pablo Barbieri Carrera
| distributor    = Pachamama Cine
| released       =    
| runtime        = 90 minutes
| country        = Argentina Spanish
| budget         = 
| gross          = 
}} Argentine drama film, written and directed by acclaimed film director Esteban Sapir. The film features Alejandro Urdapilleta, Rafael Ferro, Florencia Raggi, and others. 

==Plot==
The movie begins with a pair of hands typing on a typewriter. The denizens of a nameless city "in the year XX" have lost their voices. People communicate by mouthing out words that are spelled mid-air. The only person who has kept the use of her voice is La Voz ("the voice"), a singer working for the sole TV channel broadcast in the city, run by Mr. TV, who desires La Voz. La Voz wears a hood over her head that hides away her face. She has a son called Tomás, an eyeless little kid who nonetheless also has a voice (although this is kept a secret). Tomás lives next door to Ana, whom he one day befriends after a letter addressed to his house is erroneously delivered to hers.

Anas parents are estranged - he works for Mr. TV as a TV repairman, she is a nurse at a hospital. When Ana loses a "balloon man" owned by the channel, her father and grandfather are fired from the studio. Soon enough, Anas father stumbles upon evidence that La Voz has been kidnapped, and, together with Mr. TVs vengeful son, they set out to spy on Mr. TV. Anas father pays his ex-wife to let them into the hospital, where Mr. TV and his henchman Dr. Y (a scientist whose lower head has been replaced with a TV screen showing a mouth) subject La Voz to a series of experiments of dubious nature. They plan to use La Vozs unique power to finally subdue the denizens of the city. However, Dr. Y theorizes that a second voice might counter the effect of La Vozs. Mr. TVs outraged son comes out of hiding, is overpowered, and then put away, by his fathers henchmen, whereas Anas father manages to escape with the aid of his wife.

The reconciled couple manage to rescue Ana and Tomás from Mr. TVs henchmen (led by a masked, malformed man referred to as "the Rat Man") and meet with the grandfather. Since Mr. TV is going to broadcast La Vozs voice and thus subdue all citizens, they have to broadcast a second voice to counter the effect. The grandfather suggests using an old station, The Aerial, abandoned in the outskirts of the city, in the snowy mountains. Tomás, Ana and her parents don inflatable suits (equal to those donned by "balloon men") which send them floating up in the sky. Just as the grandfather finishes elevating them, the Rat Man and his henchmen arrive and shoot him. The family are then propelled away into the mountains.

Meanwhile, Mr. TV and Dr. Y initiate the broadcast during a boxing match. The citizens become hypnotized and subsequently fall asleep. Words then start oozing out of their bodies - the machine that sucked out their voice now takes their words out of them. In The Aerial, the Rat Man and his henchmen storm into the station, stopping short Tomáss transmission. Anas father and the Rat Man fight over a gun and stumble into a secret room in the station that reveals The Aerials director, a young girl fitted inside a glass orb that oversees the production of the drugged food that keeps citizens under Mr. TVs control.

The gun goes off and kills The Aerials director, who turns into an old woman after dying, and Ana knocks down the Rat Man. Back at the lab, Tomáss transmission sends Dr. Y into a choking fit and is finished off by Mr. TV. The transmissions counter each other and the citizens wake up, now able to use their voice (albeit without being able to speak). In the end, the family comes out of The Aerial, trying their new voices.

==Cast==
* Alejandro Urdapilleta as Mr TV
* Rafael Ferro as The Inventor
* Florencia Raggi as The Voice
* Julieta Cardinali as Nurse
* Valeria Bertuccelli as Son of Mr TV
* Ricardo Merkin as The Grandfather

==Deleted scenes==
The DVD contains a number of deleted scenes that expand on the movie. Among these are featured a scene presenting the Rat Man looking at a family portrait of anthropomorphic mice, a scene leading to Anas father and grandfather being fired (as well as a shorter episode where the grandfather laments the dismissal), a scene depicting La Voz stripping in front of Mr. TV (followed by her drugging and kidnapping), an altogether different introduction for Dr. Y (as well as a presentation of the transmission device) and both an alternate beginning and ending. The alternate beginning differs from the one in the final cut in that it explains the existence of "balloon men". The alternate ending does not contradict the final cut ending but rather expands on it: in the ending, after Mr. TV has finished Dr. Y off, La Voz frees herself from her bondage and realizes much to her rage that she now has a face but apparently cannot talk anymore. She then takes hold of a remote control and "deletes" Mr. TV. It also shows Mr. TVs son freeing himself from a chained television and proceeding to write on a typewriter (the shots matching those of two hands writing on a typewriter at the beginning of the film).

==Production==
The movies script consisted of a mere 60 pages and a story-board of over 3,000 shots that took 5 months to draw. Principal shooting took 11 weeks and the post-production took more than a year for completion.

==Exhibition==
The film premièred at the Rotterdam Film Festival on January 24, 2007. It was the first time in 36 years that a film was chosen for both the official competition and opening of the Rotterdam Film Festival.

It was released to cinemas in the United Kingdom on 16 May 2008 by Dogwoof Pictures, with a DVD-Video release following on 18 August 2008.

==Critical reception==
Film critics liked the film, with one writing, "This was the most original film that I have seen since last years Pans Labyrinth. What was even more amazing was that the budget was estimated at $1.5 million, quite a bit of money in Argentina, but not for Hollywood. It just proves that you do not need $60 million dollars to do a film, especially one of quality. On a critical note, some viewers will be overwhelmed by the fast pace of the screenplay. Plus, with so many metaphors, one might have difficulty keeping up with what is actually going on. The production crew, headed by Daniel Gimelberg, brings out Esteban Sepirs imagination in full throttle." 

==Awards==
Wins
* Clarin Entertainment Awards: Clarin Award, Best Film Director (Esteban Sapir); Best Original Film Music, (Leo Sujatovich); 2007.
* Argentinean Film Critics Association Awards: Best Director (Esteban Sapir); Best Editing (Pablo Barbieri Carrera); Best Sound (José Luis Díaz); 2008.
* A Night of Horror International Film Festival: Best Foreign Language Film; 2008.
* Fant-Asia Film Festival: Fantasia Ground-Breaker Award (third place); 2008.

==References==
 

==External links==
*  
*   at cinenacional.com   La Nación by Diego Batlle  
*    

 
 
 
 
 
 