Legend of a Fighter
 
 
{{Infobox film name = Legend of a Fighter image = Legend of a Fighter.jpg caption = DVD cover art traditional = 霍元甲 simplified = 霍元甲 pinyin = Huò Yuánjiǎ jyutping = Fok3 Jyun4 Gaap3}} director = Yuen Woo-ping producer = Ng See-yuen writer = Ng See-yuen Leung Lap-yan Wong Jing starring = Bryan Leung Yuen Yat-choh Yasuaki Kurata Phillip Ko Brandy Yuen Lau Hok-nin Charlie Chan music = Stanley Chow cinematography = Ma Goon-wah editing =  studio = Seasonal Film Corporation distributor = released =   runtime = 89 minutes country = Hong Kong language = Cantonese budget =  gross = HK$1,875,117.00
}}
Legend of a Fighter is a 1982 Hong Kong martial arts film based on the story of Chinese martial artist  , the film starred Bryan Leung as the lead character.

==Plot== Fok Yuen-gap in this action movie. As the fourth of Huo Endis children Fok Yuen-gap was born weak and susceptible to illness. Fok Yuen had asthma at an early age and contracted jaundice; his father discouraged him from learning martial arts. 

His Father hires Kong Ho-san (Yasuaki Kurata), a tutor from Japan, to teach his son academics and moral values. But Kong secretly learns the Huo familys style of martial arts, mizongyi. Fok Yuen wants to learn martial arts against his fathers wishes.  He observes his father teaching his students martial arts during the day and secretly practices at night with Kong.  After some time, Kong leaves Fuk Yuen and advices him to practice Martial Arts. 

Fok Yuen grows up and is challenged by a Japanese Fighter Sanaka. Fok Yuen defeats Sanaka and his Japanese swordsman. Sanaka commits suicide because of the defeat and his father sends the Top fighter from Japan to challenge Fok Yuen in China. 

It is revealed that Kong Ho-san is the Japanese Fighter with whom Fok Yuen is supposed to fight. Kong willing his student (Fok Yuen) to do his best, imposters as that he hates Fok Yuen and all the Chinese people. At the end it is revealed after Fok Yuen defeats Kong Ho-san.

The movie Fearless (2006 film)|Fearless starring Jet Li is remake of this film in 2006.

==Cast== Fok Yuen-gap
**Yuen Yat-choh as young Fok Yuen-gap
*Yasuaki Kurata as Kong Ho-san Fok Yan-tai
*Brandy Yuen as Bucktooth
*Lau Hok-nin as Foks assistant
*Charlie Chan as Eagle Claw school representative
*Lee Ka-ting as Sanaka Jr.
*Fong Yau as Sanaka Sr.
*Huang Ha as master defeated by Sanaka
*Fung Fung as boxing promoter
*Fung Hak-on as Bow Tie
*San Kuai as Shantung Yellow Tiger
*Yuen Cheung-yan as pipe smoker
*Lee Fat-yuen as Japanese swordsman
*Tai San
*Lau Fong-sai
*Hoh Tin-shing

==External links==
* 
* 
* 
*   

 
 

 
 
 
 
 
 
 
 
 
 
 

 
 