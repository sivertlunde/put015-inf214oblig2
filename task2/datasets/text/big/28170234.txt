Information Received
{{Infobox film
| name           = Information Received
| image          = "Information_Received"_(1961).jpg
| image_size     = 
| caption        =  Robert Lynn
| producer       = Harry Alan Towers Oliver A. Unger
| writer         =  Paul Ryder Berkely Mather (story)
| narrator       = 
| starring       =  Sabine Sesselmann  William Sylvester 
| music          = Martin Slavin
| cinematography = Nicolas Roeg
| editing        =  Lee Doig
| studio         = United Co-Productions
| distributor    = J. Arthur Rank Film Distributors  (UK)
| released       = August 1961  (UK)
| runtime        = 98 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Robert Lynn and starring Sabine Sesselmann, William Sylvester and Hermione Baddeley.  In the film, a police detective goes undercover to infiltrate a safe-breaking outfit. 

==Cast==
* Sabine Sesselmann - Sabina Farlow
* William Sylvester - Rick Hogan
* Hermione Baddeley - Maudie
* Edward Underdown - Drake
* Robert Raglan - Supt. Jeffcote Frank Hawkins - Sgt. Jarvie David Courtney - Mark
* Peter Allenby - Patterson Walter Brown - Vic Farlow Bill Dancy - Johnny Stevens
* Dan Meaden - Country Policeman
* Ted Bushell - Prison Trustee
* Tim Brinton - TV announcer Johnny Briggs - Willis David Cargill - Librarian Larry Taylor - Darnell Douglas Cameron - Warder Benham David Ensor - Judge Tony Shepherd - Squad Car Policeman

==Critical reception==
TV Guide gave the film 2 out of 4 stars, and wrote, "the film is stylish and witty at times, but its pace just isnt as fast as it should be."  

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 