Swarnna Malsyam
{{Infobox film
| name = Swarnna Malsyam
| image =
| caption =
| director = BK Pottekkad
| producer = PM Sreenivasan
| writer = Sreekumari Mankombu Gopalakrishnan (dialogues)
| screenplay = Madhu Jayabharathi Adoor Bhasi Thikkurissi Sukumaran Nair
| music = MS Baburaj
| cinematography = N Karthikeyan
| editing = NR Natarajan
| studio = Manappuram Movies
| distributor = Manappuram Movies
| released =  
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed by BK Pottekkad and produced by PM Sreenivasan. The film stars Madhu (actor)|Madhu, Jayabharathi, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by MS Baburaj.   

==Cast==
   Madhu 
*Jayabharathi 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*Sreelatha Namboothiri 
*Adoor Pankajam 
*Girish Kumar  Khadeeja 
*Rani Chandra 
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Mankombu Gopalakrishnan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aashakalerinjadangi || P Susheela || Mankombu Gopalakrishnan || 
|- 
| 2 || Maanikyappoomuthu || K. J. Yesudas || Mankombu Gopalakrishnan || 
|- 
| 3 || Njaattuvelakkaaru || KP Brahmanandan, MS Baburaj, P. Susheeladevi, Radha || Mankombu Gopalakrishnan || 
|- 
| 4 || Paalapookkumee Raavil || K. J. Yesudas || Mankombu Gopalakrishnan || 
|- 
| 5 || Thulaavarsha Meghamoru || K. J. Yesudas || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 


 