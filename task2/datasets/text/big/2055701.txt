Three Seasons
 
{{Infobox film
| name           = Three Seasons
| image          = Three_Seasons_Korean_cover.jpg
| caption        = Three Seasons Korean DVD Cover
| director       = Tony Bui
| producer       = Tony Bui
| writer         = Tony Bui, Timothy Linh Bui Don Duong, Nguyen Huu Duoc, Zoe Bui, Tran Manh Cuong, Harvey Keitel, Hoang Phat Trieu
| distributor    = October Films (USA), Arthaus Filmverleih (Germany), Mars Distribution (France), Versátil Home Vídeo (Brazil)
| released       = April 30, 1999 (USA)
| runtime        = 113 min. (theatrical) 104 min. (USA)
| language       = Vietnamese
| music          = Richard Horowitz (Original Score)
| awards         = Audience Award (Sundance Film Festival), Grand Jury Prize (Sundance Film Festival, Best Cinematography (Independent Spirit Award)
| budget         = $2 million  
| gross          = $2,021,698 (USA) 
}}

Three Seasons (  film filmed in Vietnam about the past, present, and future of Ho Chi Minh City in the early days of Doi Moi.  It is a poetic film that tries to paint a picture of the urban culture undergoing westernization.  The movie takes place in Ho Chi Minh City, formerly Saigon.  As the characters try to come to terms with the invasion of capitalism, neon signs, grand 5-star hotels, and Coca-Cola signs, their paths begin to merge.
 the embargo. The filmmakers were followed by Vietnamese inspectors throughout filming.

==Plot== lotuses from American tourists Vietnamese alike.  To pass the time, the girls sing rich folk songs that touch the heart of a poet (Teacher Dao) who lives in an old temple overlooking the pond.  Teacher Dao (Manh Cuong Tran) suffered leprosy at the age of 26 and had consequently lost his fingers.
 cents USD). Don Duong) cyclo driver who hangs out with his buddies near a grand hotel.

Through a chance meeting, Hai eventually falls in love with Lan (Zoe Bui) who works as a prostitute in big hotels.  Even seemingly happy after receiving every American dollars from her clients, she carries a silent resentment of herself and her clients.  She tells Hai that she wont be doing this job for long and dreams of sleeping in an air-conditioned room, with no one to bother her.  Lan tries to embrace the capitalist invasion by re-inventing herself and though she resents the lifestyle, she promises to one day live like them.  Hai respects her and sees through her pain.  After winning $200 USD in a cyclo race, he treats her to her dream.  Lan feels guilty and rejects Hais advances.  She feels that she is incapable and doesnt deserve Hais special treatment.  During one of the last scenes of the film, Lan finally comes to terms with herself and Hai as he takes her to a place streaming with red phượng vĩ (Royal Poinciana) blossoms.  Lan, dressed in a beautiful white áo dài (traditional Vietnamese dress), marvels at her surroundings.

Hais encounter with Kien An is a ridicule of the western embrace of "convenience".  Kien Ans flowers, hand-picked and real, are being driven out by mass-produced plastic flowers.  Hai comments that they are even sprayed with perfume to imitate the smell.  The only difference is that the plastic flowers never wilt and die.  Hai wants no part in this and asks Kien An for two.  Kien An respects him and gives him the lotus for free.

Kien Ans tale involves various personal tragedies and how poetry can triumph and provide respite for the human soul.  Teacher Dao is particularly interested in Kien Ans song because it reminds him of his days as a small boy (when he was "light and pure") along the river markets.  Teacher Dao also tells Kien An of a recurring dream.  He dreams of being able to visit the river markets and drop white lotuses, letting them float downriver.

Kien An remarks that Teacher Daos pain involves his inability to leave the seemingly abandoned temple.  Teacher Dao corrects her and says that even though he never leaves the place, in spirit, he yearns for the songs of the birds, the scent of the lotus, and the freedom of the clouds that lazily float in the sky.  In his prime, he was a successful poet.  After losing his fingers to leprosy, Teacher Dao had given up hope of ever writing again.  Kien An wants to help him and so she promises to lend him her fingers.  From time to time, Kien An would visit his home in the temple to copy whatever the poet recites.

Soon, ill health as the result of old age and leprosy takes away the poets life.  Huy (Hoang Phat Trieu), Teacher Daos headman, carries out one of his final wishes and gives Kien An Daos poetry book, which contains a never-before seen picture of Dao.  The two remarks that he was very handsome before being overtaken by leprosy.  Kien An asks Huy for help to make the late Daos dream come true.  She visits the river market and drops lotus flowers, just like in Daos dream.

James Hager is an American GI (military)|G.I. who returns to Vietnam to look for his daughter, in hope of "coming to peace with this place".  Hai and his buddies jokingly say that Hager probably lost a few screws in his head.  They witness him sitting in front of a hotel for weeks, smoking,  staring at a restaurant across the street.  They watch him with curiosity but never approach him.  His story ends with him meeting his daughter.  He gives her a bundle of lotus buds he has bought from Kien An and tries to talk with her.

Through these intertwining tales, Tony Bui is able to portray the struggles of a vanishing culture.    Kien An represents the countrys old ways, living as if untouched by time.  Lan represents the countrys present, re-inventing herself and hoping to embrace the capitalist invasion.  Hai (the cyclo driver) acts as a bridge between the past and the present, living care-free yet observing the "improvements" of westernization with a silent resentment.  Woody, the young peddler, acts as the countrys future, naïve, innocent, and easily fooled.  Woodys story ends with him playing soccer with his friends in the rain.

==Cast==
In order of appearance (major characters):
*Nguyen Ngoc Hiep - Kien An
*Hoang Phat Trieu - Huy, Teacher Daos headman Don Duong - Hai
*Nguyen Huu Duoc - Woody
*Harvey Keitel - James Hager
*Zoe Bui - Lan
*Tran Manh Cuong - Dao
*Gideon Barnett - Extra asking Woody for chewing gum in Vietnamese dubbed from British accent to American accent.

==Critical reception==
The film was met with critical acclaim. The film has a score of 81% with a certified "Fresh" rating on Rotten Tomatoes based on 31 reviews and scored a 90% audience approval rating as well 

Roger Ebert of the Chicago Sun-Times awarded the film 3 out of 4 stars and wrote "We require Asia to be ancient, traditional and mysterious. It fills a need. We dont want to know that Hong Kong is a trade capital and Japan is an economic giant. Were looking for Shangri-La, for the sentimental fantasies of generations of Western writers who fell for the romantic idea of the East -- and centuries of Eastern writers who did, too. "Three Seasons" is so languorously beautiful, because it has the sentiment of a Chaplin film, because exotic customs and settings are so seductive, we change the rules. What is wrong in Chicago becomes colorful, even enchanting, in the former Saigon. Taken as a fable, its enchanting. Art often offers us such bargains; it is better to attend "La Boheme" than to freeze in a garret." 

==Awards and nominations== Berlin International Film Festival
**Golden Berlin Bear (1999), nominated   
*Golden Satellite Awards
**Best Motion Picture, Foreign Language (2000), won
*Golden Trailer Awards
**Best Foreign (1999), won
*Independent Spirit Awards
**Best Cinematography (2000), won (Lisa Rinzler)
**Best First Feature (2000), nominated
*Political Film Society, USA
**PFS Award (2000), nominated Portland International Film Festival
**Best First Film (1999), won
*Stockholm Film Festival
**Bronze Horse (1999), nominated
*Sundance Film Festival
**Audience Award (1999), won
**Cinematography Award (1999), won
**Grand Jury Prize (1999), won
Three Seasons was the first in Sundance Film Festival history to ever receive both the Grand Jury Award and Audience Award.

==References==
 

==External links==
* 
* 

 
 
{{succession box
| title= 
| years=1999
| before=Slam (film)|Slam
| after=Girlfight tied with You Can Count on Me}}
 

 
 
 
 
 
 
 
 