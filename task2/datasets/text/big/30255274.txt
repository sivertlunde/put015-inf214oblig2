The Decemberists: A Practical Handbook
{{Infobox film
| name           = The Decemberists: A Practical Handbook
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jeff Feller
| producer       = Aaron Stewart-Ahn
| writer         = 
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Decemberists: A Practical Handbook is a 2007 film featuring indie rock band The Decemberists. The film includes music videos, a live concert, and documentary footage.

==Content== Portland with Tarkio and 5 Songs, Meloy says that he wrote it to amuse his girlfriend Carson Ellis.   
 The Chimbley Sweep," "The Sporting Life," and "I Was Meant for the Stage." Scott McCaughey guested; Petra Haden, who by that point had left the band, also appeared.
 The Tain," Here I Dreamt I Was an Architect."

==Reception==
Critical reviews were mixed. The documentary section was described as "dry"  and unengaging,  and as chronicling "perhaps the least interesting chapter in the bands history";  but also as "cute",    "nothing short of endearing" and "rewarding.".  Some viewed the concert segment favorably,   though others noted that the first half dragged  or that the "awkward" and "disappointing" performance was "not, by far, the Decemberists’ best."  The music videos, however, were mostly praised. CMJ New Music Monthly criticized the poor-quality editing of the DVD. 

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 