Dr. M (film)
{{Infobox film
| name           = Dr. M
| image          = Dr. M film.jpg
| image_size     = 
| alt            = 
| caption        = French theatrical release poster
| director       = Claude Chabrol
| producer       = Hans Brockmann François Duplat Christoph Holch 
| screenplay     = Claude Chabrol Sollace Mitchell
| based on       =  
| story          = Thomas Bauermeister 
| narrator       = 
| starring       = Alan Bates Jennifer Beals Jan Niklas Mekong Delta Paul Hindemith  
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| studio         = N.E.F. Filmproduktion und Vertriebs Ellepi Films Italian International Film Cléa Productions Solyfic ZDF Telefilm Saar GmbH La Sept 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = West Germany France Italy
| language       = English
| budget         = 
| gross          = 
}} Mabuse der Spieler by Norbert Jacques. 

==Plot== people taking mass hypnosis.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Alan Bates || Dr. Marsfeldt / Guru
|-
| Jennifer Beals || Sonja Vogler    
|-
| Jan Niklas || Lt. Claus Hartman  
|-
| Andrew McCarthy || Assassin  
|-
| Hanns Zischler || Moser  
|-
| Benoît Régent ||  Stieglitz  
|-
| Alexander Radszun || Engler 
|-
| Daniela Poggi || Kathi
|- William Berger || Penck
|-
| Wolfgang Preiss || Kessler 
|-
| Jean Benguigui || Rolf
|-
| Isolde Barth || Mrs. Sehr  
|-
| Béatrice Macola || Anna 
|}

==Critical reception==
Steve Simels of Entertainment Weekly gave the film a C-:
 

Jackson Adler of TV Guide gave the film 3 out of 4 stars:
 

==Availability==
 
The film was released in the United States as Club Extinction on VHS. 

==See also==
*Dr. Mabuse the Gambler

==References==
 

== External links ==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 