Video Girl Ai
{{Infobox animanga/Header
| image           =  
| caption         = Cover of volume 1 of the English version of the manga
| ja_kanji        = 電影少女
| ja_romaji       = Denei Shōjo
| genre           = Romantic comedy, Science fiction
}}
{{Infobox animanga/Print
| type            = manga
| author          = Masakazu Katsura
| publisher       = Shueisha
| publisher_en    =     Viz Media Star Comics   Waneko   Planeta DeAgostini
| demographic     = Shōnen manga|Shōnen
| magazine        = Weekly Shōnen Jump
| first           = 1989
| last            = 1992
| volumes         = 15
| volume_list     = #Volumes
}}
{{Infobox animanga/Video
| type            = live film
| director        = Ryū Kaneda
| producer        = 
| writer          = 
| music           = 
| studio          = Toho
| released        = June 29, 1991
| runtime         = 95 minutes
}}
{{Infobox animanga/Video
| type            = ova
| director        = Mizuho Nishikubo
| producer        =
| writer          = 
| music           = 
| licensee        =     Viz Media
| studio          = Production I.G
| first           = March 27, 1992
| last            = August 28, 1992
| runtime         = 
| episodes        = 6
| episode_list    = 
}}
{{Infobox animanga/Print
| type            = light novel
| author          = Sukehiro Tomita
| publisher       = 
| label           = 
| published       = 1993
}}
 
 English by Viz Communications. It was formerly published in the anthology Animerica Extra by Viz.

It was started in 1989 and continued until 1992, and fifteen manga volumes were produced.

A live-action movie of Video Girl Ai was released in 1991.  The plot starts much like the first volume of the manga, but differs later, and the ending is quite different from the OVA and manga. An English dubbed version was released in December 2001.

The Video Girl Ai anime is a six-part OVA series which was produced by Production I.G|I.G. Tatsunoko. The series was released in 1992 by Jump Video. It roughly covers most of the material found in volumes 1 and 3 of the manga (and some of Volume 2). The character designs for the anime remained faithful to the manga style.

Anime writer  . September 11, 2014. Retrieved on September 15, 2014. 

It is commonly speculated that the author, Masakazu Katsura, used this series as what could be described as a pilot; although he wanted to write a straight romantic comedy, he included sci-fi and action elements, so that the series would guarantee to be a success with both his publishers and (teenage male) audience.

Video Girl Ai was followed in 1993 with another sci-fi/romantic comedy manga DNA² and by the straight romantic comedy I"s in 1997.

==History==
It was started in 1989 and continued until 1992, and fifteen manga volumes were produced. The first 13 volumes tell a story about a video girl named Ai Amano. The last two volumes, which came years later, focus on a video girl named Len, hence the new name for these two volumes – Video Girl Len. In fact, a pun is present here which is lost in translation; the two video girls names, "Ren" and "Ai", combine to form renai – a Japanese word used to describe the type of romantic comedy that Video Girl Ai is. Although they have different protagonists, the "Ai" and "Len" sub-stories are not entirely unrelated; they take place in the same setting, with a similar premise.  Two characters from the first 13 volumes also appear in volumes 14 & 15.  Volume 15 concludes with a bonus chapter about Video Girl Haruno.  Her story was written before Video Girl Ai, and is almost totally separate from the stories of Ai and Len, being alluded to in only one line of dialogue in the other chapters.

==Story==
The story starts when Yota Moteuchi finds out that the girl he likes, Moemi Hayakawa, is in love with his best friend, Takashi Niimai. Disappointed by this fact, he decides to rent a video from a mysterious video store that appeared in front of him on his way home. The video store was called "Gokuraku" ("Paradise"). The unique thing about this video store was that the videos in the store contained "video girls", girls which literally come to life and out of the users television when the video tape is played to cheer the renter up. Not knowing about the video girls, Yota chooses to rent the video Ill Cheer You Up!, starring Ai Amano. Ai comes to life with the purpose to brighten up Yotas life and encourage him to pursue his love.

However, Yota plays the video on a broken video recorder, which causes Ai to come out "broken"; among other effects she has the ability to feel emotions. This additional feature of Ai causes her to eventually fall in love with Yota; a feeling which, after giving up on Moemi, Yota begins to return. However, a mysterious man related to Gokuraku known as Rolex enters the story and tries to recall Ai as she is faulty, and the fact that Ais tape is nearing the end of its playing time makes matters even worse.

From this point on, the story changes focus slightly and concentrates on Yota and Ai attempting to overcome the difficulties presented by Gokuraku. Various other complications come into the story; for example Yotas continuing love for Moemi, and his relationship with a new character, Nobuko Nizaki.

Initially, Ai spends some of her time teasing Yota mercilessly in various sexual manners i.e. pretending to initiate intercourse, or joining Yota "innocently" in the bath "to help him wash". Yotas resulting embarrassment and attempt to extricate himself from the situation results, as always, in some slapstick humor and more resulting sexual tension.

==Characters==
*  Beautiful, full of boundless energy. Hard to say what she is really like, versus what she was intended to be. Video girls are generally supposed to be comforting, nubile, excellent cooks and socially graceful, but the malfunction of Yotas VCR has made her tomboyish, at times rude, prone to violence, a terrible cook (actually, she learns to cook all by herself), but full of heart and able to feel human emotion. Her chest endowment has also shrunk considerably due to said VCR malfunction. Voiced by: Megumi Hayashibara (OAV), Minami Takayama (CD-drama), and by Maggie Blue OHara in the English dub.

*  Yota is the stereotypical loser, unable to declare his feelings to his unrequited love, Moemi, socially awkward, with a tendency to get nervous and clumsy around women. Voiced by: Takeshi Kusao (OAV), Nozomu Sasaki (CD-drama), and Brad Swaile in the English dub.

*  An attractive girl, though almost hopelessly moon-eyed over Takashi, who is too popular to really appreciate Moemis affectations. Voiced by: Yuri Amano (OVA), Kotono Mitsuishi (CD-drama), and Jennifer Copping in the English dub.

*  Your typical "tall, dark, and handsome" popular guy. He is one of Yotas best friends, and rejects Moemis advances because he knows Yotas feelings for her. Voiced by: Koji Tsujitani (OAV), Kazuhiko Inoue (CD-drama), and Samuel Vincent in the English dub.

*  A girl, one year behind Yota, who developed a crush on him in art class two years earlier and now, with Moemi and Ai temporarily sidelined, can pursue romantic ties with Yota. She first appears midway through volume 3 and only appears in the manga.

*  A girl, an orphan and a runaway, who had played with Yota in kindergarten.  Her family then moved away.  Her theme is misfortune.  Her attribute is a hand extended to help one up. She first appears in volume 6 and only appears in the manga. She seems to have a weak heart.Later on in the series she dies in the hospital, and becomes Ais role model in love.

 *  This nickname is based on a pun with the Japanese verb 持てる (moteru), which means to be well liked or popular (or to have something). A second way to read Moteuchi would be "Motenai," which is the negative conjugation for moteru (in other words, to not have something). An attempt to get the joke across to English speaking audiences was made by Ai, who reads his name and declares, "Motenai?!? As in LOSER?" Yota corrects her, but the image has already been planted into the audiences mind.

===Len story characters===
*Len Momono  Star of Lets Fall in Love. A new and untested video girl. She was created by the nameless "Old Man" who once worked in the Gokuraku store. Unlike Ai and the original Video Girls, shes allowed to feel emotions of her own.
*Hiromu Taguchi and Toshiki Karukawa  The boys who rent the tape. Hiromu is the center character of this new story arch, and is pretty much as shy as Yota used to be. He has a keen interest on Ayumi, but there are some problems in their relationship because of his shyness and because of the bad reputation she has. Later, they engage, but Hiromu becomes too happy to pay attention to Ayumi, and they break up temporarily until he can "find her again" in his memories. Toshiki, on the other hand, is more emotionally expressive and prone to teenage-typical reactions, like spying on Len (which makes her angry).
*Ayumi Shirakawa  The girl Hiromu wants to love. However, their relationship is made difficult because of a rumor spread out by her ex-boyfriend, which gave her a bad reputation in her school and beyond (Hiromu and Ayumi attend different schools). Len then devised a plan to reapproach them, just to make Ayumi see who she was dealing with all along. She breaks up with her ex-boyfriend for good and starts dating Hiromu.
*Yota Moteuchi  Now eight years older than he appeared at the end of "Video Girl Ai", Yota now teaches at an art school in the afternoon, which Hiromu and Ayumi attend. He is Hiromus mentor and they talk often about Len. Yota tells Ayumi that Len went through the same experiences that she is going through. He does mention at one point in the series that Ai is doing well.

==Volumes==
ISBNs are for the 2nd edition by VIZ Media
* Volume 1 – American Edition: ISBN 1-59116-074-X
* Volume 2 – American Edition: ISBN 1-59116-075-8
* Volume 3 – American Edition: ISBN 1-59116-075-8
* Volume 4 – American Edition: ISBN 1-59116-104-5
* Volume 5 – American Edition: ISBN 1-59116-146-0
* Volume 6 – American Edition: ISBN 1-59116-607-1
* Volume 7 – American Edition: ISBN 1-59116-748-5
* Volume 8 – American Edition: ISBN 1-59116-303-X
* Volume 9 – American Edition: ISBN 1-59116-304-8
* Volume 10 – American Edition: ISBN 1-59116-305-6
* Volume 11 – American Edition: ISBN 1-59116-306-4
* Volume 12 – American Edition: ISBN 1-59116-307-2
* Volume 13 – American Edition: ISBN 1-59116-308-0
* Volume 14 – American Edition: ISBN 1-59116-309-9
* Volume 15 – American Edition: ISBN 1-4215-0295-X

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 