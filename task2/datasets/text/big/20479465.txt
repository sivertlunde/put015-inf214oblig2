Oye Lucky! Lucky Oye!
 
 
{{Infobox film
| name           = Oye Lucky! Lucky Oye!
| image          = oyelucky.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Dibakar Banerjee
| producer       = Ronnie Screwvala
| writer         = Urmi Juvekar Dibakar Banerjee
| starring       = Abhay Deol Paresh Rawal Neetu Chandra
| music          = Sneha Khanwalkar
| cinematography = Kartik Vijay
| editing        = Shyamal Karmakar Namrata Rao
| distributor    = UTV Motion Pictures
| released       = 28 November 2008
| runtime        =
| country        = India
| language       = Hindi
}}
 Bollywood black National Film Best Popular Film. The film is inspired by the real life shenanigans of Devinder Singh  alias Bunty, a real-life “super-chor”, originally from Vikaspuri, Delhi. 

==Story==
A boy from a poor, dysfunctional family from suburban   and a business partner. Meanwhile the media speculates on how he got away with stealing 140 TV sets, 212 Video cassette recorders, 475 shirts, 90 music systems, 50 jewellery boxes, 2 dogs, and a greeting card – in a spree of burglaries that included households in Bangalore, Chandigarh, Mumbai, and other cities in India.

==Cast==
* Abhay Deol as Lucky
* Paresh Rawal as Luckys Father / Gogi Arora / Dr. B. D. Handa
* Neetu Chandra as Sonal
* Archana Puran Singh as Kamlesh (Mrs. Handa)
* Manu Rishi as Bangali
* Richa Chadda as Dolly
* Anurag Arora as Inspector Devender Singh
* Manjot Singh as the Young Lucky / Luckys Younger Brother
* Rajinder Sethi as a Criminal Reporter
* Amandeep Singh Bakshi
* Chandan Anand as Young Bangali
* Rajeev Ailabadi as Babul Awasthi
* Rajiv Gaur as Waiter at New Amar
* Karmaditya bagga as one of the friends
* Gajendra Rathi as a Cop

==Reception==
===Critical reception===
Oye Lucky! Lucky Oye! received positive critical acclaim. Raja Sen of Rediff gave the movie 4.5/5 stars, saying that "All I can say is – after very gratefully handing it four and a half stars, in case you asked – that this is a movie to love. And one that makes the audience feel just like the hero: really, really lucky."  Naresh K. Deoshi of Apun Ka Choice gave the movie 3.5/5 stars, concluding that "Grab a ticket. If youre broke, steal it."  Nikhat Kazmi of Times of India gave the movie 3.5/5 stars, commenting that "Oye Lucky! Lucky Oye! works perfectly as a simple story of a young boy (Abhay Deol) who is driven to crime not because he is hungry, poor, starving."  Syed Firdaus Ashraf of Rediff gave the movie 3/5 stars, concluding that "The film only fails in the music department, by Sneha Khanwalkar. The dhols and drums get too loud from time to time, and get very annoying. If you can overlook this minor discomfort, go for it!"  Rajeev Masand of CNN-IBN gave the movie 3/5 stars, stating that "Watch it because its a film that respects your intelligence. And films like that are hard to find."  Sonia Chopra of Sify gave the movie 2.5/5 stars, saying that "Writer-director Dipankar Banerjee cannot live down the expectations Khosla Ka Ghosla brings with it. Banerjee does meet those expectations, however Oye Lucky! is a different product altogether. More than a beginning-middle-end story, Oye Lucky! is more a peek into Delhis belly, into the characters lives, and into complex bitter-sweet relationships."  Martin DSouza of Glamsham gave the movie 2.5/5 stars, concluding that "A film with a feel of the eighties, OLLO will identify well with the viewers from the North. But yes, if you want a quiet, funny outing to lighten your mood, watching OLLO is not a bad option."  Taran Adarsh of Bollywood Hungama gave the movie 2/5 stars, saying that "On the whole, OYE LUCKY! LUCKY OYE! is a well-executed enterprise, which has its share of limitations. At the box-office, the film caters to the Northern audience mainly – Delhi and Punjab specifically. Besides North, the plexes in Mumbai should fare slightly better." 

===Box office===
Oye Lucky! Lucky Oye! was a below average grosser, grossing   nett in its lifetime , having released 2 days after the 26/11 Mumbai attacks.

==Awards== National Film Award Best Popular Film 
* 2009: Filmfare Award
**  
**   Best Costumes: Rushi Sharma / Manoshi Nath
* 2009: IIFA Award
**  
* 2009: Star Screen Award Best Story: Dibaker Banerjee: Nominated

==References==
 

==External links==
*  

 


 
 
 
 
 
 
 
 
 