Estilo Hip Hop
{{Infobox film
| name           = Estilo Hip Hop
| image          = 
| alt            =  
| caption        = 
| director       = Loira Limbal Vee Bravo
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = IndiePix Films
| released       =   
| runtime        = 57 minutes
| country        = United States Cuba Brazil Chile
| language       = English Portuguese
| budget         = 
| gross          = 
}}
 PBS series Amazon and iTunes. 

== Reception ==

Reception to Estilo Hip-Hop has been generally positive. XXL Magazine described the film as "an antidote to the get-rich-quick schemes favored by many in modern hip-hop."  PopMatters awarded the documentary a 7/10 score and called it "sharp (and) engaging." 

== References ==
{{Reflist |refs=
 http://www.estilohiphop.net/ 
 http://www.estilohiphop.net/category/screenings-and-news/ 
 http://www.xxlmag.com/online/?p=50073 
 http://www.popmatters.com/pm/review/107566-global-voices-estilo-hip-hop/ 
 http://www.pbs.org/itvs/globalvoices/estilohiphop.html 
}}

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 