Mariem Hassan, la voz del Sáhara
{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = Mariem Hassan, la voz del Sáhara
| image          = Marieum Hassan, la voz del Sahara.jpg
| caption        = 
| director       = Manuel Domínguez
| producer       = Canalmicro S. L., Manuel Domínguez
| writer         = 
| starring       = Mariem Hassan
| distributor    = 
| released       = 2007
| runtime        = 55 minutes
| country        = Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Manuel Domínguez
| cinematography = 
| editing        = Sonia Marques, Manuel Domínguez
| music          = Mariem Hassan
}}
Mariem Hassan, la voz del Sáhara (Mariem Hassan: the Voice of the Sahara) is a 2007 documentary film directed by Manuel Domínguez. 

== Synopsis ==

Mariem Hassan is the voice of the Sahara, the voice of the desert. Worshipped by the Saharaui in exile, Mariem Hassan inspires hope to those who still live in the territories occupied by Morocco. An intelligent woman with an awe-inspiring voice, she has managed to take traditional Saharaui music to the 21st century. {{cite web
 |url=http://www.fcat.es/FCAT/index.php?option=com_zoo&task=item&item_id=531&Itemid=37
 |title=Mariem Hassan, the voice of the Sahara
 |work=FCAT
 |accessdate=2012-03-10}} 
The documentary records the many ups and downs of Mariems life. {{cite web
 |url=http://www.acmi.net.au/filmoteca_voice_sahara.aspx
 |publisher=Australian Center for the Moving Image
 |title=Mariem Hassan: the Voice of the Sahara
 |accessdate=2012-03-10}} 
It reveals her as a "courageous and enduring character who has become one of the most charismatic and respected singers of the world music scene". {{cite web
 |url=http://awsa.org.au/wp-content/uploads/2010/03/mariemmelb30mar2010.pdf
 |title=Mariem Hassan: the Voice of the Sahara 
 |work=Melbourne Filmoteca
 |accessdate=March 2012}} 

To make the documentary, director Manuel Dominguez accompanied Mariem Hassan on her travels with a camera, selecting from all of her concerts and recordings. 
His narrative captures the excitement of each "agarit" (cry of the desert). We see her at the ETNOSOI Festival in Helsinki in 2004, in Les Escales, France, where the concert was transformed into a party with the Tuareg groups that joined the performance, in a theater in Liège singing "Do not abandon me," two days before enduring an operation for cancer, and in recordings made in the desert in Saharawi camps. {{cite web
 |url=http://poemariosahara.blogspot.com/2008/04/el-documental-mariem-hassan-la-voz-del.html
 |date=April 5, 2008
 |title=El documental Mariem Hassan la voz del Sahara se estrena en FISAHARA
 |work=POEMARIO POR UN SAHARA LIBRE
 |accessdate=2012-03-10}} 

==Director==

The Director, Manuel Dominguez, founded the Nubenegra record company in Madrid in 1994.
Starting with Spanish and Cuban music, the company soon extended its range to new types of music from places such as Equatorial Guinea, Guinea Buissau, Western Sahara, Brazil and the Sudan. {{cite web
 |url=http://www.last.fm/label/NUBENEGRA
 |work=last.fm
 |title=NUBENEGRA
 |accessdate=2012-03-10}} 
In 1998 Nubenegra published Mariem Hassans album "Polisario Vencerá", first released in 1982. Since then Nubenegra has released several of her albums, starting with "Mariem Hassan con Leyoad" (2002). {{cite web
 |url=http://www.nubenegra.com/artistas/index_ampli.php3?id=36
 |title=MARIEM HASSAN
 |publisher=Nubenegra
 |accessdate=2012-03-10}} 
As well as directing the documentary Mariem Hassan, la voz del Sáhara, Manuel Dominguez supplied archival footage of Mariems career and assisted in the production of the TV show Mariem Hassan: Voice of the Saharwis. {{cite web
 |url=http://www.linktv.org/newmuslimmusic/mariem
 |work=LinkTV
 |title=Mariem Hassan: Voice of the Saharwis
 |accessdate=2012-03-10}} 

== References ==
 

 

==External links==
*{{cite web 
 |url=http://www.archive.org/details/La_Voz_del_Sahara
 |title=Mariem Hassan - La voz del Sáhara (2008)
 |work=Internet Archive
 |title=Mariem Hassan: the Voice of the Sahara 
 |date= 30 March 2010
 |accessdate=2012-03-10}}

 
 
 
 
 
 
 
 
 


 
 