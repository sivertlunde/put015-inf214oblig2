Felix and Meira
 
{{Infobox film
| name           = Felix and Meira
| image          = 
| caption        = 
| director       = Maxime Giroux
| producer       = 
| writer         = Maxime Giroux Alexandre Laferrière
| starring       = Martin Dubreuil Luzer Twersky Hadas Yaron
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Canada
| language       = French, Yiddish, English
| budget         = 
}}

Felix and Meira ( ) is a 2014 Canadian drama film directed by Maxime Giroux. It was screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival,    in competition at the San Sebastian Film Festival and at the Torino Film Festival.

The film won the award for Best Canadian Feature Film at the Toronto festival. 

The film was included in the list of "Canadas Top Ten" feature films of 2014, selected by a panel of filmmakers and industry professionals organized by TIFF.  

==Cast==
* Martin Dubreuil as Félix
* Hadas Yaron as Meira
* Luzer Twersky as Shulem
* Anne-Élisabeth Bossé as Caroline
* Benoît Girard as Théodore
* Melissa Weisz as Ruth

==References==
 

==External links==
*  
*   at Cinoche.com

 
 
 
 
 
 
 
 
 