The Matchmaker (1958 film)
{{Infobox film
| name           = The Matchmaker
| image          = Matchmaker 1958 poster.jpg
| caption        = One of theatrical release posters
| director       = Joseph Anthony
| producer       = Don Hartman
| writer         = Thornton Wilder (play) John Michael Hayes(screenplay)
| starring       = Shirley Booth Anthony Perkins Shirley MacLaine Paul Ford Robert Morse
| music          = Adolph Deutsch
| cinematography = Charles Lang
| editing        = Howard A. Smith
| distributor    = Paramount Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| preceded by    = 
| followed by    = 
}}
  1958 United American comedy 1955 play of the same name by Thornton Wilder. Costumes by Edith Head.

==Plot== milliner Irene Molloy, Dolly shows him the photograph of a woman she calls Miss Ernestina Simple and tells him the buxom beauty would be a far better choice for him. Horace agrees to have dinner with Ernestina at the Harmonia Gardens after visiting Irene.

Meanwhile, Horaces clerk head Cornelius Hackl convinces his sidekick Barnaby Tucker that they, too, deserve an outing to New York. The two cause cans of tomatoes to explode, spewing their contents about the store, which justifies their closing it for the day and heading to the city.  While there, they come across Irenes hat shop and Cornelius is instantly taken to her. The pair are forced to hide however, when Mr. Vandergelder and Dolly arrive. Though Dolly and Irene cover up for them, Mr. Vandergelder still realizes that Irene is hiding people in her shop (though he doesnt know who) and leaves in disgust. Irene furiously demands that Cornelius and Barnaby repay her by taking her and the shop assistant Minnie out to a fancy restaurant for dinner (Dolly had led her to believe that the men were secretly members of high society).

By total coincidence, Cornelius, Barnaby, Irene, Minnie, Horace, and Dolly all dine at the same restaurant. Horace realizes that Dolly tricked him and that there is no such person as Ernestina Simple. Cornelius worries over how to pay for the meal until a well-meaning diner gives him Mr. Vandergelders wallet (which the diner believes Cornelius dropped). Over the course of the evening, Irene and Cornelius fall in love as Barnaby falls for Minnie. The two men escape being caught by Mr. Vandergelder by disguising themselves as women and dancing towards the door. Before going, they leave the two women a note confessing who they really are and that they love them.

The next day, Irene and Minnie help the two shopkeepers pretend to be setting up a feed store of their own across the street from Mr. Vandergelders. Frightened by the competition, Horace gives them better working hours and wages. Realizing how foolishly hes been acting, he agrees to marry Dolly as well.
==Hello, Dolly!== Hello Dolly! movie of the same name with Barbra Streisand and Walter Matthau were both based upon Wilders play.

==See also==
* Hello, Dolly! (film)|Hello, Dolly!
* List of American films of 1958
* Matchmaking

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 