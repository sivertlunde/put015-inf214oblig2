Pakarnnattam
{{Infobox film
| name           = Pakarnnattam
| image          = Pakarnnattam_film.jpg
| image size     =
| border         =
| alt            =
| caption        =
| director       = Jayaraj
| producer       = Ravi Kottarakkara
| writer         = C. P. Udaybhanu
| screenplay     = C. P. Udaybhanu
| story          =
| based on       =  
| narrator       =
| starring       = Jayaram  Sabitha Jayaraj
| music          = Kailas Menon
| cinematography =Sinu Sidharth
| editing        = Sobin K Soman
| studio         = Ganesh Pictures
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Pakarnnattam  ( ) is a 2012 Malayalam film directed by Jayaraj, starring Jayaram and Jayarajs wife Sabitha Jayaraj in the lead roles.  The film is based on political violences and fight against Endosulfan menace in Kasargod, Kerala.    

==Plot==
The film explores the practice that makes martyrs of the local political activists who are willing to sacrifice their life for ideologies.    The film is also a poignant love story of a Meera (Sabitha Jayaraj) and  her relentless fight against her own family and society for the man she love. Meera falls in love with Thomas (Jayaram), who becomes an accused in a political murder and is sent to jail. Although Thomas is innocent, he accepts the verdict against him for his political party. Along with Meeras battle to save Thomas, the film also makes a statement on the tragedy caused by the pesticide Endosulfan, in Kasaragod district. Thomas is an activist who fights for the rights of the victims of Endosulfan.   

==Accolades==
Pakarnnattam was screened in the Malayalam Cinema Today section in the 16th International Film Festival of Kerala (IFFK), 2011.      

==Cast==
* Jayaram as Thomas
* Sabitha Jayaraj as Meera
* Bhasi Thiruvalla as Adv. Kaimal
* Vijay Victor as Sudhi
* Satish Poduval as Raghavan
* Nedumbram Gopi as Meeras father
* Madampu Kunjukuttan as Meeras uncle
* Vijayan Peringode as Meeras uncle
* Eliamma as Thomass mother

==References==
 

 
 
 
 
 

 