Plush (film)
{{Infobox film
| name           = Plush
| image          = File:PlushFilmPoster2013.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Catherine Hardwicke
| producer       = Jason Blum   Catherine Hardwicke   Sherryl Clark 
| writer         = Arty Nelson   Catherine Hardwicke
| starring       = Emily Browning   Cam Gigandet   Frances Fisher   Xavier Samuel   Brandon Jay McLaren   Dawn Olivieri
| music          = Nick Launay
| cinematography = Daniel Moder Julia Wong
| studio         = Blumhouse Productions IM Global Octane
| distributor    = Millennium Entertainment
| released       = September 13, 2013
| runtime        = 
| country        = United States
| language       = English
| budget         = $2 million
| gross          = $3,080
}}
 Thomas Dekker, and Frances Fisher.

==Plot==
After losing her band mate and brother to a drug overdose, rising rock star Hayley finds herself in a downward spiral. The second album from her band Plush is received as a critical and commercial disaster. She finds new hope and friendship in Enzo, the replacement guitarist who inspires her to reach new creative heights. But soon their collaboration crosses the line sexually and Hayley, who is married with two children, retreats from Enzo’s advances. As Hayley slowly discovers Enzo’s dark and troubled history, she realizes she may have let a madman into her home and that her mistake may cost the lives of people closest to her. 

== Cast ==
* Emily Browning as Hayley
* Xavier Samuel as Enzo
* Cam Gigandet as Carter
* Dawn Olivieri as Annie Thomas Dekker as Jack
* Frances Fisher as Camila
* Elizabeth Peña as Dr. Lopez
* Brandon Jay McLaren as Butch Hopkins/Writer
* Marlene Forte as Dr. Ortiz
* Bradley Metcalf, Jack Metcalf, and Travis Metcalf as The Twins
* Kennedy Waite as Lila
* Steve Asbury as Donnie/Drummer
* Marcus AK Andersson as Diego/Bass Player
* James Kyson as Coat & Tie Fan
* Indira G. Wilson as Limo Driver
* Caitlin Bray as Enzos Sister

== Production ==
In 2012 Hardwicke announced her intentions to film Plush based on a script she wrote with Artie Nelson.  IM Global was named as the financier for the film,  which would star Emily Browning as the lead character.     Principal photography began on August 27, 2012 in Los Angeles.   

Teaser photos and posters were released on to the official website in August 2013. 

==Novel==
A tie-in novel based upon the film was released on July 27, 2013. The book, which is also entitled Plush, was written by Kate Crash, who also contributed several songs to Plushs soundtrack. The novel tells the story of the film and the nine years prior to the start of the film.

==Soundtrack== Thomas Dekker supplied vocal performances for their respective tracks.

==Reception==
Critical reception for Plush has been predominantly negative and the film currently holds a rating of 33% "rotten" on Rotten Tomatoes based upon six reviews.  Christy Lemire gave the film a half a star, criticizing it as "inauthentic at every turn".  In contrast IndieWire gave the film a "B+", commenting that although Plush wasnt "high art", it did "commit fully and follow through with the courage of its convictions". 

== References ==
 

== External links ==
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 