Earth and Ashes
{{Infobox film
| name           = Earth and Ashes
| image          = 
| image size     = 
| caption        = 
| director       = Atiq Rahimi
| producer       = Dimitri de Clercq
| writer         = Kambuzia Partovi Atiq Rahimi
| narrator       = 
| starring       = Abdul Ghani
| music          = 
| cinematography = Éric Guichard
| editing        = Urszula Lesiak
| distributor    = 
| released       = 13 May 2004
| runtime        = 102 minutes
| country        = Afghanistan Dari
| budget         = 
| preceded by    = 
| followed by    = 
}} Afghan film directed by Atiq Rahimi. It was Afghanistans submission to the 77th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.         It was also screened in the Un Certain Regard section at the 2004 Cannes Film Festival.    It won the Golden Dhow award at the 2005 Zanzibar International Film Festival.  
 

==Cast==
* Abdul Ghani - Dastaguir
* Jawan Mard Homayoun - Yassin
* Kader Arefi - Fateh
* Guilda Chahverdi - Zaynab
* Walli Tallosh - Mirza Qadir

==See also==
*Cinema of Afghanistan
*List of submissions to the 77th Academy Awards for Best Foreign Language Film
*16 Days in Afghanistan

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 