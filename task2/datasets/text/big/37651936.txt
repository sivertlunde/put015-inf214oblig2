The Little Minister (1921 film)
{{infobox film
| name           = The Little Minister
| image          =
| imagesize      =
| caption        =
| director       = Penrhyn Stanlaws
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = James M. Barrie (novel) Edfrim Bingham (scenario)                                                                      
| starring       = Betty Compson
| music          =
| cinematography = Paul Perry
| editing        =
| distributor   = Paramount Pictures
| released       = December 25, 1921
| runtime        = 6 reels (6.031 feet)
| country        = USA
| language       = Silent (English intertitles)

}} version by Vitagraph with Alice Calhoun.  
 Cinematheque Royale BFI London, and Filmmuseum Nederlands (EYE Institute). 

==Cast==
*Betty Compson - Babbie
*George Hackathorne - Gavin
*Edwin Stevens - Lord Rintoul
*Nigel Barrie - Captain Halliwell
*Will R. Walling - Doctor McQueen
*Guy Oliver - Thomas Whammond
*Fred Huntley - Peter Tosh
*Robert Brower - Hendry Munn
*Joseph Hazelton - John Spens
*Mary Wilkinson - Nancy Webster

==See also==
*The House That Shadows Built (1931) a promotional film for Paramount with a clip of this movie

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 