Yiddle with His Fiddle
{{Infobox film
| name = Yidl Mitn Fidl
| image = Yiddlemitnfid.jpg
| caption = Advertisement in Davar, 1937. Joseph Green Jan Nowina-Przybylski
| producer = Jozef Frankfurt Edward Hantowitz
| writer = Joseph Green Konrad Tom
| starring = Molly Picon
| music = Abe Ellstein
| cinematography = Jakob Jonilowicz
| editing = Jack Kemp
| studio = Green Films
| distributor = Sphinx Films Corp.
| released =  
| runtime = 92 minutes
| country = Poland
| language = Yiddish
| budget = $50,000
| gross =
}} 1936 musical film.

==Plot==
Arye and his daughter Itke are musicians, or Klezmer|klezmorim, who became impoverished and were evicted from their home in Kazimierz Dolny|Kazimierz. Arie sees no choice but to embark on a career of a travelling band, but fears for the safety of his daughter on the dangerous roads. Itke solves the problem by disguising herself as a boy and adopts the persona of "Yidl", ostensibly Aries son. During their voyages, they meet another pair of merrymakers, the father-and-son duo Isaac and Ephraim Kalamutker, with whom they form a quartet and roam through the Polish countryside seeking engagements. "Yidl" falls in love with Ephraim, who is utterly oblivious to the true sex of his companion. The four are hired to perform in the wedding of young Teibele to the old, rich man Zalman Gold. The bride had to cancel her prior engagement with her true love, Yosl Fedlman, for her late father left many unpaid debts. Yidl takes pity on Teibele and the quartet smuggle her out of the party and have her join them as vocalist. To Yidls dismay, Ephraim is enamored with the young woman. Itke reveals her true self to Isaac, who determines to assist her and leaves to locate Yosl. When arriving in Warsaw, the group become a success and are hired to perform in a concert. However, personal tensions between the members run high. Efraim signs a contract with a local orchestra. Teibeles lost match finally arrives, and they run off together before the show. Yidl, quite by accident, takes her place and recounts her entire story and love for Efraim in song form. She is applauded and signed on a contract for a career in the United States. Having learned the truth, Efraim abandons his commitments and joins her on the ship to New York.

==Cast==
* Molly Picon as Itke/Yidl
* Simcha Fostel as Arie
* Leon Liebgold as Efraim Kalamutker
* Max Bozyk as Isaac Kalamutker
* Dora Fakiel as Teibele
* Barbara Liebgold as Teibeles mother
* Samuel Landau as Zalman Gold
* Chana Lewin as Widow Flaumbaum        

==Production== Joseph in Joseph Green, met with success, he decided to create an entirely Yiddish film, and returned to his native Poland to do so. Yidl Mitn Fidl was the most successful Yiddish film of all time and the most popular of Greens films as well.

The film was shot on location in Kazimierz Dolny, Poland, with local inhabitants as extras.    Based on a novella by Konrad Tom, the screenplay was written by Green. Its score was composed by Abraham Ellstein, and the lyrics to the songs were written by Itzik Manger. Jakob Jonilowicz was the photo director of the film.

It was filmed in Poland to minimize costs: the total budget was $50,000. Picon was contemplating about entering English-language entertainment and had to be paid an astronomical fee in terms of Yiddish cinema, $10,000 or a fifth of the entire expenditure, to star in the main role. Beside her, all actors were Polish. The picture turned into a resounding commercial success and covered the producers expenses even before opening in the United States. When it premiered in the   with Hebrew dubbing.  In Britain, it opened at Academy Cinema,  ."  Several copies were sent to Nazi Germany, where Jews were not allowed to attend regular cinemas, and viewing was restricted to "members of the Jewish Race." Premiere in the hall of the Jüdischer Kulturbund took place on 2 May 1938, and it then ran in communities across Germany.  

In 1956, a remastered version, fully dubbed into English, was released in New York for a short run, bearing the title Castles in the Sky. 

==References==
 

==External links==
* 
* 

 
 
 
 
 

 
 