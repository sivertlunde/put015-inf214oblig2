The Gambler (2014 film)
 
{{Infobox film
| name           = The Gambler
| image          = The Gambler poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Rupert Wyatt
| producer       = {{plain list|
*Irwin Winkler
*Robert Chartoff
*Mark Wahlberg
*Stephen Levinson
*David Winkler
}}
| screenplay     = William Monahan
| based on       =  
| starring       = {{plain list|
*Mark Wahlberg
*John Goodman
*Brie Larson
*Michael K. Williams
*Jessica Lange
}}
| music          = {{plain list|
*Jon Brion 
*Theo Green
}}
| cinematography = Greig Fraser
| editing        = Pete Beaudreau
| production companies = Chartoff/Winkler Productions Closest to the Hole Productions Leverage Entertainment
| distributor    = Paramount Pictures
| released       =  
| runtime        = 111 minutes  
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $39.2 million 
}} 1974 film, AFI Fest,  and was theatrically released in the United States on December 25, 2014.

==Plot== win one of his college basketball games by a margin of 7 points or less, he will murder Amy. Bennett goes to Frank, who advises him to adopt a "fuck you" attitude towards life by getting enough money to be completely free, and lends him $260,000 to pay his debt to Lee. Bennett convinces Lee to stake him $150,000, as the only way he can pay his full $410,000 debt to Lee and also pay back the money he borrowed from Frank is to gamble and win. He uses the money from Lee to bribe Lamar into keeping the point spread to under 8 in the basketball game. Bennett sends Dexter to Vegas to bet on the game with the $260,000 he got from Frank. Lamar succeeds in the scheme, and Bennett uses his winnings to pay his debt to Baraka, denying he knows anything about the large bet made in Vegas. He then convinces Lee and Frank to meet him in a neutral gambling den and wagers enough money to pay both Lee and Frank off—if he wins—on a roulette spin. Successful, he walks out, stating he was playing for Frank and Lee. The payment to Frank is more than he owed, but he refuses to take the "cream" when Frank offers to give back the overpayment. Bennett runs through the city to Amys apartment, a broke but free man.

==Cast==
 
* Mark Wahlberg as Jim Bennett
* John Goodman as Frank
* Brie Larson as Amy Phillips
* Michael K. Williams as Neville Baraka
* Jessica Lange as Roberta
* Anthony Kelley as Lamar Allen
* Alvin Ing as Lee
* Domenick Lombardozzi as Ernie
* Emory Cohen as Dexter Steve Park as Two
* Leland Orser as Larry
* George Kennedy as Ed

 

==Production==
===Development=== The Gambler with the original producers, Irwin Winkler and Robert Chartoff. Intended as a directorial project for Martin Scorsese, it was reported that Leonardo DiCaprio was attached as the star and William Monahan would write the screenplay.  

In a 2011 interview, screenwriter James Toback gave the autobiographical story of the original films background and development, and criticized the idea of his film being remade. 

Scorsese left the project and filmmaker Todd Phillips was in talks to take over as of August 2012.  In September 2013, actor Mark Wahlberg and director Rupert Wyatt expressed interest in making the film. 

===Casting===
As of October 17, 2013, Brie Larson was in talks to play the female lead role, alongside Wahlberg.    On January 15, 2014, Emory Cohen joined the cast of the film, playing one of the professors students.   

===Filming=== Shooting began on January 20, 2014.  Wahlberg was spotted during the filming of The Gambler on January 21, in Los Angeles.  On February 3, 2014, Wahlberg was spotted on The Gamblers set in Downtown Los Angeles.  On February 13, Jessica Lange and Wahlberg were spotted again during filming.  On March 13, there was a basketball scene filmed in Los Angeles.   

===Music=== Universal Music released a soundtrack album for the film on December 15, which features songs from various artists. 

==Release==
The Gambler had its world premiere during the 2014 AFI Fest at the Dolby Theatre in Los Angeles on November 10.  Paramount previously set the film for a limited release in the United States on December 19, 2014, for an Oscar-qualifying run strategy, and planned to expand the film on January 1, 2015.  But on December 5, Paramount announced the film would be released wide in cinemas on December 25, 2014, instead of the previous platform release plans. 

On October 22, 2014, the first teaser poster and red band trailer were released.  On November 5, 2014, the green band trailer was released. 

==Reception==
The Gambler has received mixed responses from critics. On Rotten Tomatoes, the film holds a rating of 46%, based on 123 reviews, with an average rating of 5.6/10. The sites consensus reads, "Well-paced and reasonably entertaining in its own right, The Gambler still suffers from comparisons to the James Caan classic that inspired it."  On Metacritic, the film has a score of 55 out of 100, based on 40 critics, indicating "mixed or average reviews". 

Jessica Langes performance has received critical acclaim. TheWrap wrote that Lange had one of her "meatiest film roles in ages."  The Huffington Post described her performance as "ferocious" and capable of "knocking down William Monahans profanity laced dialogue with gleeful abandon"  Also, the Boston Herald described her work as "strikingly memorable",  which Newsday, The Philadelphia Inquirer, and Indiewire have agreed with, terming her "affecting", "stirring", and "terrific".    James Berardinelli from ReelViews described her as "heartbreaking as the cold, rich widow who blames herself on some level for her sons failure."  Chris Nashawaty from Entertainment Weekly lauded her acting as effortless by saying "  can do icy in her sleep..."  Furthermore, Rex Reed from the The New York Observer described her performance as "hard" and "venomous".  Peter Travers from Rolling Stone described her performance as "reliably superb".  Jeff Baker from the The Oregonian stated that her acting is "fierce".  Indiewire suggested Lange as a contender for the Academy Award for Best Supporting Actress. 

==See also==
*The Gambler (1974 film)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 