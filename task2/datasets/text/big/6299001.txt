Track Down
 
 
{{Infobox film name           =Track Down image          =Takedown 2000.jpg caption        =Finnish DVD cover director       =Joe Chappelle producer       = writer  David Newman Leslie Newman John Danza Howard A. Rodman starring       =Skeet Ulrich Russell Wong Angela Featherstone Donal Logue Christopher McDonald Master P Amanda Peet with Jeremy Sisto Tom Berenger Patrick Holland James Pocock music  Chris Holmes James Kole cinematography =Dermott Downs editing        =Joe Rabig studio         =Millennium Films distributor    =Dimension Films released       =  runtime        =92 minutes country        =United States language  English
|budget         =
}} 2000 film about computer hacker Kevin Mitnick, based on the book Takedown by John Markoff and Tsutomu Shimomura. The film was directed by Joe Chappelle and stars Skeet Ulrich and Russell Wong.     

==Plot==
For years Kevin Mitnick had eluded federal agents while using the latest electronic gadgetry to break into countless computers and gain access to sensitive and valuable information. But when he breaches the system of leading computer crimes expert Tsutomu Shimomura, it sets off an epic chase through cyberspace between a pair of hard-driven geniuses operating on different sides of the law. 

== Release ==
The film was released to theaters in France as Cybertraque in 2000, then on DVD in Europe Kevin Poulsen (2004-09-09).  , The Register. (SecurityFocus).  as Takedown later, such as in Germany in May 2003. {{cite web |title=Takedown |date= 2004 |first=Patrick |last=Joseph |publisher=Filmbesprechungen.de |
|url=http://www.filmbesprechungen.de/php/ausgabe.php4?FilmID=1277 |archiveurl=http://web.archive.org/web/20040512042437/http://www.filmbesprechungen.de/php/ausgabe.php4?FilmID=1277 |archivedate=2004-05-12}}  It was released on DVD in the U.S. as Track Down in late 2004. Leyden, Joe (2004-12-07)  . Variety (magazine)| Variety. 

==Criticism==
===Factual inaccuracies===
In Kevin Mitnicks The Art of Deception, Mitnick states that both book and movie are "extremely inaccurate" and based on media hype.  In the film, Mitnick and Shimomura meet twice, one of these meetings prompts Kevin to flee to Seattle. This meeting did not actually take place.  

The film depicts Mitnick hacking into Shimomuras computers and stealing/deleting his files and software. Though Mitnick admits hacking Shimomuras computers using IP spoofing, he claims he never caused any damage to anyone by deleting files or data, merely copying source code of some software, out of curiosity.

The 2001 documentary "Freedom Downtime" tries to get behind some of the false rumors about Kevin Mitnick that ended up being presented as facts in the movie Track Down.

===Lawsuit regarding alleged copyright violation===
In 1997, California author Jonathan Littman wrote The Fugitive Game: Online with Kevin Mitnick, in which he presented Mitnicks side of the story.     Littman alleged that portions of the film were taken from his book without permission. {{cite news
 |title=Movie About Notorious Hacker Inspires a Tangle of Suits and Subplots
 |last=Fost |first=Dan
 |date=May 4, 2000
 |url=http://www.sfgate.com/cgi-bin/article.cgi?file=/chronicle/archive/2000/05/04/BU71498.DTL
 |work=San Francisco Chronicle
 |accessdate=2007-04-24
 |archiveurl=http://www.webcitation.org/query?url=http%3A%2F%2Fwww.sfgate.com%2Fbusiness%2Farticle%2FMovie-About-Notorious-Hacker-Inspires-a-Tangle-of-2782693.php&date=2012-08-19 
 |archivedate=August 19, 2012
}} 

As a result, Littman sued The Walt Disney Company and Miramax. {{cite news
 |title=Author sues Disney for hacker movie
 |author=ZDNet staff
 |date=2 May 2000
 |url=http://www.zdnet.com/news/author-sues-disney-for-hacker-movie/107271
 |work=ZDNet
 |accessdate=2013-09-07
}} 

Another copyright issue. The circuit displayed was copied without authority from this website.
http://www.techlib.com/electronics/audiooscillators.htm
The creator of the stolen circuit appears amused.

== Cameo ==
In the "CMAD - Computer Misuse & Anomaly Detection Conference" scene of the movie the real Tsutomu Shimomura appears in the audience next to a heckler (Donal Logue).

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 