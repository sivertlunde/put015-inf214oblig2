Demon Wind
{{Infobox film
| name               = Demon Wind
| image              = Demon_wind_cover.jpg
| imagesize          = 250px 
| caption            = VHS Cover
| director           = Charles Philip Moore
| producer           = Michael Bennett Peter Collins Sandy Horowitz Paul Hunt
| writer             = Charles Philip Moore
| starring           = Eric Larson Francine Lapensée Rufus Norris Jack Forcinito Stephen Quadros Mark David Fritsche Sherry Leigh
| music              = Bruce Wallenstein
| cinematography     = Thomas L. Callaway
| edited             = Christopher Roth
| distributor        = Demon Wind Productions Ltd. United Filmmakers
| released           = July 20, 1990
| runtime            = 96 mins
| country            = United States
| language           = English
}}
Demon Wind is an American 1990 horror film directed by Charles Philip Moore.  The film concerns a group of friends who travel to an old farm, and soon find they cant leave as a mysterious fog sets in. 

==Synopsis==
We are introduced to 1931. A body is being burned on a cross, a woman named Regina is at a farm, and attempting to barricade a door, from where beyond, demons try to enter.  Her husband George transforms into a demon instead and kills her.

Sixty years later, after the suicide of his father, a young man named Cory, the grandson of Regina and George, and his girlfriend Elaine, along with a group of their friends, travel up to the farm, where the events took place sixty years before, so that Cory can figure out what happened to his grandparents.  They soon come under attack by a band of vicious demons and when the kids try to escape a mysterious fog brings them back to the farm.  The only good thing is that they are protected by a shield that prevents the demons from entering the house.  One-by-one the kids soon become possessed by the demons, they manage to fight them off with a pair of daggers they find, which is the only thing that will kill them.  But when the demons master arrives, the kids realize they will need something stronger to put an end to the master and his demons.  

==Cast==
* Eric Larson - Cory
* Francine Lapensée - Elaine
* Rufus Nirris - Harcourt
* Jack Forcinito - Stacey (credited as Jack Vogel)
* Stephen Quadros - Chuck
* Mark David Fritsche - Jack
* Sherry Leigh - Bonnie (credited as Sherry Bendorf)
* Bobby Johnston - Dell
* Lynn Clark - Terri
* Richard Gabai - Willy
* Mia M. Ruiz - Reena (credited as Mia Ruiz)
* Kym Santelle - Harriet
* Stella Kastner - Grandmother Regina
* Axel Toowey - George
* Jake Jacobson - Corys Father
* C.D.J. Koko - Grand Demon (credited as D. Koko)
* Jeffrey Urbach - Demon
* Elizabeth Ince - Grandmother Regina (uncredited)
* Joyce Lasley - Demon (uncredited)
* Sandra Margot - Beautiful Demon (uncredited)
* Mindy McEnnan - Demon Girl (uncredited)
* Lou Diamond Phillips (uncredited)
* Chuck Williams - Demon (uncredited)

==Reception==
Demon Wind has received a negative critical reaction.  On Rotten Tomatoes, the film has a "rotten" score of 28%  and it holds a rating of 4.8/10 at the Internet Movie Database 

==References==
 

==External links==
* 

 
 
 
 