Minna Agechau
{{Infobox animanga/Header
| image           = 
| caption         = 
| ja_kanji        = みんなあげちゃう
| genre           = Erotic comedy
}}
{{Infobox animanga/Print
| type            = manga
| author          = Hikaru Yuzuki
| publisher       = Shueisha
| demographic     = seinen manga|Seinen
| magazine        = Weekly Young Jump
| magazine_en     = 
| published       = 
| first           = 1982
| last            = 1987
| volumes         = 19
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Shusuke Kaneko
| producer        = 
| writer          = Toshiki Inoue
| music           = 
| studio          = Nikkatsu
| licensee        = 
| released        = April 20, 1985
| runtime         = 90 minutes
}}
{{Infobox animanga/Video
| type            = film
| director        = Osamu Uemura
| producer        = 
| writer          = 
| music           = 
| studio          = 
| licensee        = 
| released        = March 27, 1987
| runtime         = 38 minutes
}}
{{Infobox animanga/Video
| type            = special
| title           = 
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = 
| licensee        = 
| network         = Fuji Television
| network_en      = 
| released        = June 22, 1987
| runtime         = 
}}

 
  is an erotic manga series by Hikaru Yuzuki. It was serialized in seinen magazine Weekly Young Jump from 1982 to 1987.

It was adapted into a 1985 film directed by Shusuke Kaneko. There are also a 1987 anime adaptation  and a TV special. 

==Awards==
7th Yokohama Film Festival 
*9th Best Film

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 