College Girl (1974 film)
{{Infobox film 
| name           = College Girl
| image          =
| caption        = Hariharan
| producer       = Dr Balakrishnan
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan
| starring       = Prem Nazir Vidhubala Adoor Bhasi Balakrishnan
| music          = A. T. Ummer
| cinematography = TN Krishnankutty Nair
| editing        = G Venkittaraman
| studio         = Rekha Cine Arts
| distributor    = Rekha Cine Arts
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, Hariharan and produced by Dr Balakrishnan. The film stars Prem Nazir, Vidhubala, Adoor Bhasi and Balakrishnan in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Prem Nazir as Rajan
*Vidhubala as Radha
*Adoor Bhasi as Sukumaran
*Balakrishnan
*Jose Prakash as Nanu
*Manavalan Joseph
*Pattom Sadan as Hyder Prema as Vichaminia
*Sankaradi as Parakulam Raman Nair
*Sebastian
*Unni
*Cochin Haneefa as College Student
*Ramdas
*T. S. Muthaiah as Leelas Father
*Premachandran
*Paul Vengola as Govindan
*Anjana Raja
*Bahadoor as Damu
*Balan Kovil
*Devnath
*Geetha (Old)
*K. P. Ummer as Kunjahammadali Hajiyar
*Kanjangad Balakrishnan
*Karunan Khadeeja as College Principal
*Kumaran Nair Meena as Meenakshi
*Muraleedharan
*PC Thomas
*Paravoor Bharathan as Kittunni Ammavan
*Philomina as Professor / Principal Parukkuttyamma
*Prajatha
*Pushpa
*Rajamma Sadhana as Leela
*Saraswathi
*TS Radhamani
*Vimala Junior Sudheer as College Student
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Dr. Balakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amrithaprabhaatham Virinju || Chandrabhanu, Devi Chandran || Dr. Balakrishnan || 
|-
| 2 || Anjanamizhikal || K. J. Yesudas, S Janaki || Dr. Balakrishnan || 
|-
| 3 || Arikathu Njammalu Bannotte || Sreedevi || Dr. Balakrishnan || 
|-
| 4 || Chandanakkuriyitta || K. J. Yesudas || Dr. Balakrishnan || 
|-
| 5 || Kingini Ketti || K. J. Yesudas || Dr. Balakrishnan || 
|-
| 6 || Muthiyamma Pole Vannu || P Jayachandran, P. Madhuri, Chorus || Dr. Balakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 