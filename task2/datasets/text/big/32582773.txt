All Together (2011 film)
 
{{Infobox film
| name           = All Together
| image          = AndIfWeAllLivedTogether2011Poster.jpg
| caption        = French film poster
| director       = Stéphane Robelin
| producer       = Christophe Bruncher Frédérique Dumas Aurélia Grossmann
| writer         = Stéphane Robelin
| starring       = Jane Fonda Daniel Brühl Geraldine Chaplin Pierre Richard Claude Rich Guy Bedos
| music          = 
| cinematography = Dominique Colin
| editing        = Patrick Wilfert Kino Lorber (US)
| released       =  
| runtime        = 96 min
| country        = France Germany
| language       = French
| budget         =
| gross          =
}}
All Together  ( , literally "And If We All Lived Together?"), is a 2011 French-German comedy film written and directed by Stéphane Robelin, and starring Jane Fonda and Geraldine Chaplin as participants of an alternate living experiment, that is observed by a graduate student played by Daniel Brühl. The film marks Fondas first French-speaking role since starring in Jean-Luc Godards  film Tout Va Bien (1972). {{Cite news
| last = Young
| first = Neil
| title = And If We All Lived Together? (Et si on vivait tous ensemble?): Film Review
| work = The Hollywood Reporter
| accessdate =3 April 2012
| date = 13 August 2011
| url = http://www.hollywoodreporter.com/review/we-all-lived-together-si-222847
}} 

Filming took place over two months in Paris in summer 2010.   The film premiered on the closing night of the Locarno International Film Festival on 13 August 2011.  It was released on DVD on March 19, 2013. 

==Plot==
Jean (Bedos) is a romantic revolutionary, yet enjoys the spoils of a bourgeois lifestyle with his wife, Annie (Chaplin). Annie is a retired psychologist, who complains about not being able to see enough of her children and assorted grandchildren. Albert (Richard) is showing increasing signs of dementia; his energetic American wife Jeanne (Fonda) is a former university lecturer who is suffering from cancer but who assures her husband that she is cured, yet shops for a brightly-colored coffin. 

Widower Claude (Rich) is an aging womanizer with an appetite for pursuits with prostitutes. Knowing how lonely he is at home alone after a previous heart attack, Jean suggests that the five friends should live together in their house, an idea that appalls Annie. Claude suffers another heart attack from walking up too many flights of stairs, on the way to visiting one of his lady friends. Albert is also hospitalized after his beloved dog knocks him down during a walk, though he claims he slipped on the sidewalk. Unable to see his dog be given away, Jeanne and Albert hire Dirk (Brühl), a German ethnology student, to walk him instead.

After seeing the sad conditions of Claudes retirement home, the friends decide to move in together on Jeans suggestion. Dirk, who has changed his thesis to reflect the condition of Frances aging population, moves in with them as a caregiver. Meanwhile, Annie prepares to build a pool on her property in the hopes that it will attract her grandchildren. Jeanne becomes frustrated at Alberts worsening condition as he begins to forget that he is living with his friends instead of at his own home. Albert also speaks with Jeannes doctor, who informs him that Jeannes latest tests indicate that her cancer is not cured, but is worsening. 

Jeanne strikes up a friendship with Dirk, giving him advice on troubles with his girlfriend and telling him that life is short and that he should be with someone who is more his type. She also reveals that she had a lover in the past, but is still good friends with him. One day she tells Albert that she is going out to walk the dog with him. Albert acknowledges this, yet forgets and goes off to find her, leaving the bath running. He finds them in a park together and accuses her of starting a relationship with him, angering her; he also forgets who Dirk is and why he hired him. The water overflows from the tub, ruining Annies precious furniture, yet Albert does not know why shes upset. 

Later, Albert brings Dirk along to help him with opening some old trunks. Accidentally finding Claudes things instead, Albert discovers that Claude had been having an affair with both Annie and Jeanne forty years earlier. He reveals this to Jean, who doesnt believe him until he accuses Annie and she confesses. Jean confronts Claude in the unfinished pool during dinner and threatens him with a knife, and Jeanne faints when she hears that Claude had also been having an affair with Annie. The friends make up that night while Jeanne is bedridden.

The next morning, the friends drink champagne and Jeanne succumbs to her illness shortly after. She is buried in a bright pink coffin, and, as per her requests, the surviving friends leave their champagne glasses on it. The pool is filled shortly after and Annies grandchildren are finally spending time with her and Jean. Claude finds Dirk having sex with a new part-time caregiver, a girl Jeanne hired who is more of Dirks type. Before they can toast to the new assistant, a confused Albert wanders in, asking after Jeanne; he still believes her to be alive. The film ends on a melancholy note as the friends and Dirk wander through the streets with Albert, calling Jeannes name.

   Screen Daily. 14 August 2011  

==Cast==
*Jane Fonda as Jeanne
*Daniel Brühl as Dirk
*Geraldine Chaplin as Annie
*Pierre Richard as Albert
*Claude Rich as Claude
*Guy Bedos as Jean
*Camino Texeira as Maria

==References==
 
<!--
 
 
 
 
 
-->

==External links==
*  

 
 
 
 
 
 
 
 