Franz + Polina
{{Infobox Film name = Franz + Polina image = FranzPolina.jpg producer =Oleg Urushev
| director = Mikhail Segal cinematography = Maksim Trapo editing = Rinat Khalilullin writer = Ales Adamovich starring = Adrian Topol Svetlana Ivanova released = November 10, 2006 (US) runtime = 119 minutes language = German
|music = Andjei Petras
}} 2006 Russian occupied Belarusia. Belarusian woman whose village is massacred.
 FIPA DOr Grand Prize. 

== Plot == partisan brother returns he whispers to Polina to get Franz away and she passes Franz off as her deaf-mute brother. Franz impregnates Polina after briefly settling in a log cabin in the woods.
 collaborator but saved by Franz as they join a band of refugees. Franz kills a German officer for his uniform and infiltrates a nearby village to obtain an antibiotic. When he returns he is already delirious with typhoid fever and accidentally reveals his national identity by slurring in German language|German. The refugees are sympathetic with the exception of a small boy, the sole survivor of a family that was lured back to the village by the seemingly humane behavior of the SS. He has a gun, bought with his fathers watch, and his adoptive mother and sister are unable to dissuade him from using it on Franz. They warn Polina and soon the pair are on the run again.

Their escape is interrupted by a German patrol which in turn is ambushed by partisans. After this rescue Franz is at the river bank retrieving water for pregnant Polina when the boy appears with the gun. The movie closes with the boy returning with the water and comforting a screaming Polina.

==DVD release==
The notes on the DVD case compare the lovers to Romeo and Juliet. Their differing nationalities are stressed by printing the title in two alphabets, namely as Franz + Полина.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 