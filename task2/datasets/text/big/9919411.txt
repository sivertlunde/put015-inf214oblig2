Il Gaucho
{{Infobox film
 | name = Il Gaucho
 | image = Il Gaucho.jpg
 | caption =
 | director = Dino Risi
 | writer =    Ruggero Maccari Ettore Scola Tullio Pinelli  Dino Risi
 | starring =  Vittorio Gassman
 | music = Chico Novarro Armando Trovajoli
 | cinematography =  Alfio Contini
 | editing =
 | producer =Mario Cecchi Gori
 | distributor =
 | released =   1965
 | runtime =
 | awards =
 | country =
 | language =  Italian
 | budget =
 }} 1965 Cinema Italian comedy film directed by Dino Risi.  It was co-produced by Argentine, where it was released as Un italiano en la Argentina.  For his role in this film Nino Manfredi won a Grolla doro for best actor. 

== Cast ==
*Vittorio Gassman: Marco Ravicchio
*Nino Manfredi: Stefano
*Amedeo Nazzari: Ingegnere Maruchelli
*Silvana Pampanini: Luciana
*Maria Grazia Buccella: Mara
*Nando Angelini: Aldo
*Maria Fiore: Maria
*Annie Gorassini: Lorella
*Umberto DOrsi: Pertini, the producer
*Francesco Mulè: Fiorini
*Jorgelina Aranda: Italia Marucchelli

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 