Price of Glory
{{Infobox Film
| name           = Price of Glory
| image          = Price of glory.jpg
| caption        = Theatrical Poster for the Film
| director       = Carlos Ávila
| producer       = Arthur Friedman Moctesuma Esparza
| writer         = Phil Berger Maria del Mar Jon Seda Clifton Collins Jr. Ernesto Hernandez Ron Perlman Louis Mandylor Joseph Julián González
| cinematography = Affonso Beato
| editing        = Gary Karr
| studio         = Shoreline Entertainment
| distributor    = New Line Cinema
| released       = March 31, 2000
| runtime        = 118 min.
| country        = United States English
| budget         = $10,000,000
| gross          = $3,548,556   
}} Huntington Park, California, Los Angeles, California, and Nogales, Arizona|Nogales, Arizona.  The film was released by New Line Cinema on March 31, 2000.

==Plot==
Arturo Ortega, a man with enduring aspirations of being a competitor in professional boxing. While Arturo had the intellect, ambition, and agility to be a professional, his career proved to be a short one, and, after a living out his fifteen minutes of fame, he’s washed up. However, Arturo has instilled his passion for boxing in his three sons, who have grown up learning all about the world of prizefighting. The three boys begin competing in the ring, with Arturo as their manager and coach, but Johnny swiftly displays so much promise that other managers and promoters want to take over his contract and put make him the next boxing champion. Arturo feels let down when sonny decides that he wants to work with another manager, while his other two sons rail against sonny for turning his back on his father and hope Arturo has the same conviction in their talents in the ring. 

==Cast==
*Jimmy Smits as Arturo Ortega Maria del Mar as Rita Ortega
*Jon Seda as Sonny Ortega
*Clifton Collins Jr. as Jimmy Ortega
*Ernesto Hernandez as Johnny Ortega
*Ron Perlman as Nick Everson
*Louis Mandylor as Davey Lane
*Sal Lopez as Hector Salmon
*Danielle Camastra as Mariella Cruz
*Paul Rodriguez as Pepe
*Ulises Cuadra as Young Sonny Ortega
*Mario Esquivel as Young Jimmy Ortega
*Jeff Langton as Referee

==Box office==
The film grossed $3,440,228 in the United States and $108,328 in the foreign markets. 

==Critical reception==
The film was not well received by critics when it was released in March 2000.  Film critic Roger Ebert gave the film a rating of two stars out of a possible four saying, "The film made me feel like I was sitting in McDonalds watching some guy shout at his kids."  Marc Savlov of the Austin Chronicle said of the film "It is a TKO before it even had a chance to get off a decent hook."   Price of Glory currently has a rating of 32 out of 100 on the popular website, Metacritic.   The film has a 30% "Fresh" rating on Rotten Tomatoes. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 