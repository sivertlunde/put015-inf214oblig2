The Last Dymaxion
{{Infobox film 
| name           = The Last Dymaxion
| image          = The Last Dymaxion.jpg
| caption        = Film poster Noel Murphy
| producer       = Noel Murphy
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| gross          = 
}} Noel Murphy.    about Buckminster Fullers 1933 Dymaxion car as well as Fuller himself. 
 British architect Norman Foster, who called the car “an incredibly beautiful aesthetic object” and rebuilt the fourth Dymaxion car from drawings and study of the first three cars. In the film, noted automobile collector Jay Leno calls Buckminster Fuller a genius.   Buckminster Fullers advocacy for humanity permeates the film. 

Director Noel Murphy said describing "Buckminster Fuller as a car designer would be like describing Jimi Hendrix as a guitar tech”. Fuller was a visionary environmentalist; his lightweight teardrop-shaped car spun on three wheels, held a dozen people and was originally meant to run on alcohol fuel. The documentary explores his other green innovations, including his geodesic domes and philosophical approach to architecture.   

==Story==
 
Exploring Fullers invention of the Geodesic Dome and its innovative structural system, the documentory also examines Fullers early heavy drinking, expulsion from Harvard University and suicide attempt. In 1927, after Fuller had lost his 4 year daughter due to Polio and had lost his job, he contemplated drowning himself in the freezing waters of Lake Michigan. Fuller reported an “intense mystical experience” realizing he belonged to the universe and could not take his own life. 

==Production==
Filming took place over two years and tells the story of Buckminster Fuller. The director Noel Murphy (comedian)  traveled to England, Spain and all over the United States, researching, filming and interviewing Jay Leno and Norman Foster.    
Fuller designed and built three car prototypes shaped like zeppelin and called them Dymaxion, short for Dynamic Maximum Tension. The filmmaker, Noel Murphy, a descendant of a General Motors founder, has spent about $100,000 so far, traveling to Fuller archives and New England factory sites and interviewing those interested in Fuller.     
Mr. Murphy has acquired Fuller artifacts during his research, including a 1941 photo of a Dymaxion prototype taken just before an unsealed gas cap caused it to catch fire and be destroyed. One Dymaxion prototype survives, and was recently returned to the collection of the National Automobile Museum in Reno, Nevada. Murphy personally took part in the  restoration at Crosthwaite & Gardiner, car restorers in East Sussex, England.  It had spent years rusting on a farm before ending up at the museum. “Part  of the car was destroyed but Bucky’s true legacy was his thinking,” the historian Jay Baldwin explains in the documentary. 

==Screening==
The film has been screened at McPherson Museum of Art and History, Yale University and Earth Day Cabrillo College.    

==References==
 

 
 
 
 
 
 