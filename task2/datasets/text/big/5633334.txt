Olly Olly Oxen Free (film)
 
{{Infobox Film
| name           = Olly Olly Oxen Free
| image          = OllyOllyOxenFree.jpg
| caption        = DVD Cover
| director       = Richard A. Colla
| producer       = Richard A. Colla
| writer         = Richard A. Colla Maria L. De Ossio Eugene Poinc
| narrator       = 
| starring       = Katharine Hepburn Kevin McKenzie Dennis Dimster
| music          = Bob Alcivar
| cinematography = Gayne Rescher
| editing        = Lee Burch
| distributor    = Sanrio
| released       = August 25, 1978  
| runtime        = 83 minutes United States English
| budget         = 
| preceded_by    = 
| followed_by    =  1978 United American comedy film directed by Richard A. Colla and starring Katharine Hepburn. The screenplay by Eugene Poinc is based on a story by Poinc, Colla, and Maria L. de Ossio. The title is derived from "Olly olly oxen free" a phrase used in childrens games to indicate that those in hiding came out into the open safely and freely. Indeed, that is the character arc premise in the film. Noticeably,  the film has more of a T.V. movie feel,  complete with the types of camera shots, blue screen shots, budget special effects, and background music one would have heard on a made for T.V movie in the mid 1970s. 

==Plot summary==
Eccentric Miss Pudd (Hepburn) is the owner of a store she considers an antique museum, a sort of Noahs Ark of the past,  but most would call a junkyard. She frequently refuses to part with her merchandise because of its sentimental or historical value, but she does agree to loan some items to two boys, Alby (Kevin McKenzie) and Chris (Dennis Dimster), who befriend her, and who explain that they are repairing an antique hot air balloon that belonged to the older boy, Kevins grandfather, an adventurous stunt man named The Great Sandusky. In honor and memory of Kevins grandfather, who died one year earlier and is sorely missed by Kevin who apparently has no father, they plan to take the balloon aloft to celebrate the mans birthday. A curious element of the film is that the boys mothers are talked about, and one is heard from the kitchen, but never seen. Other than the postman, Kate Hepburns Miss Pudd is the primary adult in the film, a sort of wobbly Auntie Mame.  By todays standards, it would be preposterous to imagine that this much shenanigans could take place, including the noisy demolition of a backyard greenhouse, tower, and workshop, with no other adult, let alone Kevins mother, noticing or caring.  But then, perhaps, even in the 70s, this seemingly benign film may have carried that ironic undercurrent of social commentary.   

In a scene reminiscent of the Wizard of Oz, the balloon is accidentally fully inflated, and launched, with the two boys and their English sheepdog in the basket.  Miss Pudd chases after them in her antique car, and manages to wind up riding the anchor dangling from the bottom of the basket. After she is pulled up, the balloon continues to drift south from Napa Valley, to San Francisco, and gets lost in a fog.  Allowing for suspension of belief is key to viewing this film, but there is a fine sequence where Kate Hepburn reveals her own inner fears and disappointments,  and struggles to come to terms with them.  Later that night, they drift finally to Los Angeles, where a police helicopter demands that they land.  The balloon lands on the stage of the elegant art-deco Hollywood Bowl,  at an outdoor concert where the orchestra is playing Tchaikovskys 1812 Overture. Amidst fireworks,  the audience thinks the balloon landing is part of the show and is enthralled as the ragamuffin crew disembarks, and is sent home. 

Back north, in what must be an endless summer, the boys and Miss Pudd come across the Grandfathers wood crate of mysterious contents, and the adventures continue. 

==Production== Calistoga and St. Helena, California in the summer of 1976. The interiors are well dressed, including the breakfast room of Kevins house which is smartly turned out with white enameled chairs and lime green fern patterned wallpaper. The Victorian house is lovingly shot from many angles, distracting the viewer from remembering that this is a firm for kids. The film failed to find a distributor until 1978, when it was given a limited release in the Midwestern United States. In 1981, the film was released in New York City to indifferent reviews and vanished quickly from theaters.

==References==
*  , 1985, pp.&nbsp;392–93. ISBN 0-688-04528-6
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 