Kalyana Samayal Saadham
{{Infobox film
| name           = Kalyana Samayal Saadham
| image          = kalyana samayal saadham.jpg
| alt            = 
| caption        = First look poster
| director       = RS Prasanna
| producer       = Arun Vaidyanathan Ananth Govindan
| story          = RS Prasanna
| screenplay     = RS Prasanna Prasanna Lekha Washington
| music          = Arrora
| cinematography = Krishnan Vasant
| editing        = Sathyaraj
| studio         = 
| distributor    = Abi TCS Studios  Thirukumaran Entertainment   RPP Film Factory
| runtime        = 
| released   = 6 December 2013
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Prasanna and Lekha Washington in the lead roles.   Produced by Ananth Govindan and Arun Vaidyanathan under the Everest Entertainment banner, it was distributed by ABITCS Studios, Thirukumaran Entertainment and RPP Film Factory. Krishnan Vasant was the cinematographer and Arrora the music composer.  The film released on 6 December 2013 and received positive reviews from critics.  

==Cast== Prasanna as Raghu Viswanathan
* Lekha Washington as Meera Chandrasekaran
* Delhi Ganesh as Chandrasekaran
* Uma Padmanabhan as Meeras mother
* Raaghav as Gautham
* Natarajan (Nat Narayanasamy) as Viswanathan (Raghus Father)
* Geetha Ravishankar as Raghus mother
* Kathadi Ramamurthy as Raghus grandfather
* Crazy Mohan as Dr Kamlesh Neelu as Meeras uncle / Card Mama
* Vatsala Rajagopal as Meeras grandmother
* Dr. Sharmila as Raghus aunty
* Srilekha Parthasarathy as Raghus aunty
* Sreekanth Dass as Dhina

==Filming==
The filming started on 3 January 2013.  Kalyana Samayal Saadham (KSS), which is about a big fat Indian wedding will also feature in Emmy-nominated American television series Born to Explore with Richard Wiese. 

The innovative Social Media campaign of the movie is hugely successful, and has been reported by National Media.  

==Music==
The films original soundtrack was composed by debutant Arrora alias Naveen. The audio was launched on 2 August 2013.   A single track "Mella Sirithai" and its video was released in around May which got positive response for its innovative making. Srikanth Varadhan has made his debut as a lyricist with this film and actor Prasanna had sung the song "Pallu Pona Raja" with lyrics penned by Srikanth. Behindwoods wrote:"A marriage of imaginative melodies and ideas. R.S.Prasanna and Arrora start on the right track". 

* Modern Kalyanam - Megha (singer)|Megha, Nikita
* Mella Sirithai - Haricharan, Chinmayi
* Kalyanam 2.0 - Arrora, Keerthana
* Pallu Pona Raja - Prasanna, Sangeetha Rajeshwaran
* Kadhal Marandhaayada - Arrora, Keerthana
* Pallu Pona Raja - Prasanna, Sangeetha

==Release==
The film released in 260 screens across TN on 6 December 2013. It had been certified a U/A from the censor board. 

===Critical reception===
Times of India gave 4 stars out of 5 and wrote, "The first 15 minutes play out like a romantic fantasy and director Prasanna deftly changes track, turning it into a comedy about erectile dysfunction. Prasanna treats these segments quite cheekily but never resorts to crassness. And gently (and deftly), he nudges this lighthearted material into serious territory".  Cinemalead rated the film with 3 out of 5 stars and wrote "Kalayana Samayal Sadham is feel good feast which everyone can enjoy."  Rediff gave the film 3 stars out of 5 and wrote, "Kalyana Samayal Saadham is not just about a big fat Indian wedding but also highlights the emotional and sensitive topic of stress-related erectile dysfunction that is becoming an increasingly common problem among the modern youth".   Baradwaj Rangan wrote, "It wants to be little more than an innocuous, pleasant, crowd-pleasing entertainer, so you can’t fault it for what it isn’t — but there’s a lingering sense of what-could-have-been".  Deccan Herald wrote, "Kalyana Samayal Saadham turns out to be an interesting rom-com providing food for thought. Despite its savoury title, the film is not a big fat wedding fest. But it touches upon a sensitive topic of erectile dysfunction, a modern-day problem among urban youth given the stressful life they lead".  The New Indian Express wrote, "To craft an adult comedy with a sensitive theme, with maturity and style, is harder still. But debutant director R S Prasanna has pulled it off with finesse. Kalyana Samayal Sadham entertains even as it takes up an issue that is becoming more prevalent in modern stressful times".  Sify wrote, "Kalyana Samayal Saadham is a well-made adult romcom, that keeps you smiling. It is take on a big fat Brahmin wedding and its pitfalls and rituals is great fun".  Behindwoods gave 3 stars out of 5 and wrote, "Kalyana Samayal Saadham, is a practical take on a very serious issue but laced with lighthearted approach" and called it "A pleasant feel good urban flick". 

Sudhish Kamath picked it as one of five films that have redefined Tamil cinema in 2013, writing, "this is a surprisingly subtle and truly modern Tamil romantic comedy by writer-director R.S. Prasanna, which has pushed the boundaries of the unexplored characters and situations further". 

==References==
 

==External links==
*  

 
 
 
 
 
 