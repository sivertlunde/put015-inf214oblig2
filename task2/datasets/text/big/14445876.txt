The Last Breath (2009 film)
 
{{Infobox film
| name           = The Last Breath
| image          = 
| alt            =  
| caption        = 
| director       = David Jackson
| producer       = Scott Horsfield
| writer         = Jamie Shearing
| starring       = Francis Magee
| music          = Mark Rutherford
| cinematography = Simon Poulter
| editing        = Tom Canning
| studio         = 
| distributor    = 
| released       =   
| runtime        = 11 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Last Breath is a 2009 British live-action short film starring Francis Magee. The film was directed by David Jackson and was funded through the Digital Shorts (UK Film Council funding scheme). It received distribution support from the British Council 

The film was written by Jamie Shearing and produced by Scott Horsfield.

== Plot ==
While on a scuba diving trip the Calvin family surface to find that the world’s oxygen supply has suddenly dissipated. With their tanks running low they embark on a race against time to reach the nearby dive hut...

== Cast ==
*Francis Magee - Paul
*Sara Markland - Mary
*Sarah Savage - Claire
*Carole Weyers - Anna

== Production ==
The Last Breath was filmed on location at Hastings Fly Fishing Club in August 2008. The film was shot using the Red One camera (Red Digital Cinema Camera Company)

It was produced by Crazy Horse Entertainment and VBM Productions.

==References==
 

== External links ==
* 
* 
* 
* 

 
 
 
 
 
 

 