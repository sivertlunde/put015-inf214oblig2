Planeta Bur
 
{{Infobox film
| name           = Planeta Bur
| image          = PlanetaBurDVDRelease.jpg
| image_size     =
| caption        = Cover of the Lenfilm DVD Release
| studio    = Lennauchfilm
| director       = Pavel Klushantsev
| producer       = L. Presnyakova <!--
                  -->Vladimir Yemelyanov Aleksandr Kazantsev <!--
                  -->Pavel Klushantsev
| narrator       =
| starring       = See below Iogann Admoni <!--
                  -->Aleksandr Chernov
| cinematography = Arkadi Klimov
| editing        = Volt Suslov
| distributor    =
| released       = 1962
| runtime        = 72 min
| country        = Soviet Union
| language       = Russian
| budget         =
| gross          =
}}

Planeta Bur ( ) is a 1962 Soviet science fiction film directed by Pavel Klushantsev.

The film is also known as Planet of the Storms, Planet of Storms, Planet of Tempests, Planeta Burg (American bootleg title) and Storm Planet.

==Synopsis== Soviet spaceships, Sirius, Vega, and Capella, are on their way to the planet Venus. The Capella is struck by a meteorite and destroyed. The remaining two ships, Sirius and Vega, continue on, despite the fact that the planned mission required three ships. A replacement spaceship, the Arktur, will be sent from Earth, but will not arrive for two months.

The cosmonauts aboard Sirius and Vega decide that some sort of landing and exploration is better than waiting. Ivan and Kern go down from Vega in the glider, leaving Masha in orbit. All contact is lost after they land in a swamp. The Sirius lands nearby and the three-man crew set out in their hovercar to find them.

During their travels they hear an eerie womans song in the distance and encounter prehistoric beasts both benign and threatening. Ivan and Kern, meanwhile, must fight off some man-sized T-rex beasts as they head to meet the men of Sirius. The two fall ill with a fever. Their robot, John, stands watch.

The Sirius crew must submerge their hovercar to escape a pterodactyl. In doing so, they discover what might have been an ancient city. Alexy finds a strange triangular rock and a statue of a pterodactyl with rubies for eyes.

Once on dry land, the Sirius crew contact the robot John and tell him to administer an anti-fever drug. Ivan and Kern recover just as a volcano sends down rivers of lava. They order John to carry them across, but he malfunctions half way there. The hovercar shows up just in time to rescue them while John is lost to the lava.

All five return to Sirius, but worry that Masha had landed the Vega somewhere, stranding them all. An earthquake and flood from rain threaten to strand the Sirius, so they must take off immediately. Alexey discovers that his odd triangular rock is really a sculpture of a womans face, proving that there might still be intelligent life on Venus. They blast off and find that Masha remained in orbit and together they head home.

== Cast ==
*Vladimir Yemelyanov as Ilya Vershinin
*Georgiy Zhzhonov as Bobrov
*Gennadi Vernov as Alyosha
*Yuri Sarantsev as Scherba
*Georgi Tejkh as Kern
*Kyunna Ignatova as Masha

== Adaptations ==
In 1965 Curtis Harrington added several American-made scenes (starring Basil Rathbone and Faith Domergue) and released the dubbed result under name Voyage to the Prehistoric Planet. Since all credits were removed, soviet actors were "renamed" with non-Russian names (Gennadi Vernov as Robert Chantal, Georgiy Zhzhonov as Kurt Boden) or left completely uncredited. 

In 1968 Peter Bogdanovich (under the name Derek Thomas) added several different new scenes involving Mamie Van Doren and several other attractive women in shell brassieres, and released the film  as Voyage to the Planet of Prehistoric Women; the "new" scenes also included footage from another Russian SF film, Mikhail Karyukovs Nebo Zovyot (which was curiously itself edited in a same way by the young Francis Ford Coppola into Battle Beyond the Sun).
This version is essentially the first film retold, with the parallel viewpoint of the telepathic women whose god (a pterodactyl) is killed by the men from Earth. There is an ironic twist at the end when the women find a new god.

== External links ==
* 
* 
 
 
 
 
 
 
 
 
 
 
 