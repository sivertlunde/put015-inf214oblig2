Freestyle: The Art of Rhyme
{{Infobox film
| image          = Freestyle The Art of Rhyme US release poster (low res).jpg
| alt            = U.S. release poster for "Freestyle: The Art of Rhyme"
| caption        = U.S. release poster Kevin Fitzgerald
| producer       = Henry Alex Rubin
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = Todd Hickey
| editing        = Paul Devlin
| studio         =  VH1 Television (USA TV)
| released       =      
| runtime        = 60 mins / 90 mins (directors cut)
| country        = United States
| language       = 
| budget         = 
| gross          = $11,255 (USA)
}}
 Kevin Fitzgerald about the art of freestyle rap|freestyle, improvisational hip-hop. Taking more than seven years to make, the documentary includes performances and commentary by artists such as Supernatural (rapper)|Supernatural, Mos Def, The Roots, Notorious B.I.G., Jurassic 5, and Pharoahe Monch.  

== Content ==
Directed by Kevin Fitzgerald (also known as DJ Organic), the history of freestyle rap is explored in the film, with a mix of performance and commentary from a number of artists. Using archive footage, the film traces the origins of improvised hip-hop to sources including African-American preachers, Jamaican Deejay (Jamaican)#Toasting|toasts, improvised jazz, and spoken-word poets such as The Last Poets.

== Reception ==
=== Box office ===
Following its 7 July 2004 release, the film grossed $4,842 in its opening weekend, and made $12,600 domestically in two weeks.   

=== Critical reception === average score of 63 from 11 reviews on MetaCritic, and an average score of 89% from 18 reviews on Rotten Tomatoes.       A. O. Scott of the New York Times described it as "a heady, rousing education in an art form that is too often misunderstood",     and Fred Camper of the Chicago Reader called it " n engaged and knowing look at the underground world of improvised rap, concentrating on artists less interested in commercial success and cutting records than in the spontaneous right now of nonconceptual rhyme."   

=== Awards ===
The film performed well at festivals, receiving awards for both the soundtrack and the documentary overall.      

{| class="wikitable center"
|-
! Year
! Festival
! Category
! Award
! Result
! Notes
|-
| rowspan="3"| 2000
| Woodstock Film Festival
| rowspan="2"| Best Documentary
| rowspan="2"| Jury Prize
|  
|
|-
| Urbanworld Film Festival
|  
| Tied with  .
|-
| Los Angeles Independent Film Festival
| Best Soundtrack
| Audience Award	
|  
|
|-
| 2002
| Florida Film Festival
| colspan="2" | Special Jury Award	
|  
| For documentary filmmaking.
|-
| 2006
| Karachi Film Festival
| Best Documentary
| colspan="2" | Special mention
| " or its innovative camerawork and editing technique."   
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 