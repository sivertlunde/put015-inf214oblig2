The Gypsy of Athens
{{Infobox film
| name = The Gypsy of Athens
| image =
| image_size =
| caption =
| director = Ahilleas Madras 
| producer = 
| writer =  Ahilleas Madras 
| narrator =
| starring = Ahilleas Madras    Frida Poupelina
| music = 
| cinematography = Joseph Hepp
| editing =
| studio =   Agax Film 
 | distributor = 
| released = 1922
| runtime = 
| country = Greece Greek intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} silent drama film directed by  Ahilleas Madras.  A gypsy falls in love with an American woman to the fury of his former lover.

==Partial cast==
* Ahilleas Madras as Yor  
* Frida Poupelina as Smale  

== References ==
 

== Bibliography ==
* Marcel Cornis-Pope & John Neubauer. History of the Literary Cultures of East-Central Europe: Junctures and disjunctures in the 19th and 20th centuries. Volume IV: Types and stereotypes. John Benjamins Publishing, 2010.
 
== External links ==
*  

 
 
 
 
 
 
 
 
 

 