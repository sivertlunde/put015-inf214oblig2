Sangharsh (2014 film)
{{Infobox film
| name           = Sangharsh
| image          = Sangharsh.jpeg
| caption        = Theatrical release poster
| director       = Saisparsh
| producer       = Vidnyan Siddhi Films Ltd
| writer         = 
| starring       = Rajesh Shringarpure Sangeeta Kapure Prajakta Mali Nakul Ghanekar Anshuman Vichare
| music          = Nishikant Sadaphule Bapi –Tutul
| cinematography = Sandy
| editing        = Nilesh Gawand
| studio         =
| distributor    = 
| released       =  
| runtime        =
| country        = India Marathi
| budget         =
| gross          = 
}}
Sangharsh (   film produced by Vidnyan Siddhi Films Ltd. It features an ensemble star cast and revolves around the lives of the residents of Gunaji Chawl and how they get caught in the politician-underworld nexus. The film was released on 7 February 2014.          

==Plot==
Best friends Bhau, Manya and Tavlya are the soul of Gunaji Chawl. Bhau is a karate black belt champion; Manya is a fabulous dancer and dreams of winning a dance reality show and Tavlya has no mission in life but to play practical jokes on people around him and imitate them.

The trio’s life takes an unexpected turn when they get into a brawl with a few goons. They find themselves caught in a conflict which pitches them against a politician, an underworld don and some corrupt police officers.

In the due course of time, they find themselves slipping deeper into murky waters and soon realize of a devious conspiracy against the common man.

Will the trio triumph? Let us find out in this marathon journey of love, courage and endurance - Sangharsh.

==Cast==
*Ravi Shinde alias Bhau – Rajesh Shringarpure
*Sakshi – Sangeeta Kapure
*Bijli – Prajakta Mali
*Manya – Nakul Ghanekar
*Tavlya – Anshuman Vichare
*Radhika – Madhavi Nimkar
*Avinash Waghmare – Devdutta Nage
*Mangala Tai – Sulabha Arya
*Gurav Kaka – Arun Nalawade
*Mai – Amita Khopkar
*Raghubhai – Sushant Shelar
*Prataprao – Dr. Vilas Ujawane
*Inspector Kalokhe – Ajay Purkar
*Bala – Sunil Deo
*Bavlya – PankajKhamkar
*Inspector Bhosale – Rajesh Kamble
*PratapraoPA – Nilesh Suryavanshi
*Ingale – Sudhir Pednekar
*Baban - Ashruba
*Hero - Akshay veer

==Crew==
*Producer - Vidnyan Siddhi Films Ltd.
*Director–Saisparsh
*Executive Director–R. Viraj
*Executive Producer–Mangesh Jagtap
*Screenplay & Dialogues–R. Viraj
*Story–Saisparsh
*Cinematographer –Sandy
*Music–Nishikant Sadaphule, Bapi –Tutul
*Lyrics – Saisparsh, Ashwini Shende, Nishikant Sadaphule 
*Choreographers –Saroj Khan, Rajeev Surti, Raju Khan, Ranju Varghese
*Editor–Nilesh Gawand
*Action - Javed – Aejaz
*Art Director–Jaywant Waingankar

== References ==
 

==External links==
* http://divyamarathi.bhaskar.com/article/BOL-sangharsh-first-look-launched-4449075-PHO.html

 
 
 


 