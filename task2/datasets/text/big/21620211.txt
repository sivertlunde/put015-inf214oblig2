Against the Dark
 
{{Infobox film
| name           = Against the Dark
| image          = Against the Dark movie poster.jpg
| caption        = Official movie poster
| director       = Richard Crudo
| producer       = Phillip B. Goldfine
| writer         = Mathew Klickstein
| starring       = {{Plain list |
* Steven Seagal
* Tanoai Reed
* Jenna Harrison
* Linden Ashby
* Emma Catherwood
* Skye Bennett
* Keith David
}}
| music          = Philip White
| cinematography = William Trautvetter
| editing        = Tim Silano
| distributor    = Sony Pictures Home Entertainment 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $7,000,000
}} action horror film starring Steven Seagal and directed by Richard Crudo.

In a post-apocalyptic world, destroyed by a disease, which turns humans into infected strongly resembling vampires, Seagal plays Tao, the leader of a squad of ex-military vigilantes who are attempting to find and rescue a group of survivors trapped in a hospital. This is Steven Seagals first horror film.

The film was released direct-to-video on February 10, 2009 and was Seagals first release of 2009. 

==Plot==
An epidemic disease has overwhelmed humanity, turning nearly everyone into blood-thirsty infected who resemble vampires and zombies. There is no known cure and very few survivors left fighting "against the dark".

Tao (Seagal) a tough guy in charge of a group of vampire/zombie hunters. The survivors in the hospital are shown to be scared, isolated and untrusting. As the characters work their way through the hospital, they drift apart numerous times, often getting lost and attacked. To make matters worse for the survivors, a massive army base is planning to "sterilize" (bomb) the area at dawn.

The survivors explain that there is only one functioning exit from the hospital (even though there seem to be numerous entrances through which they entered) and they have to get to this exit before the emergency power runs out and they are shut inside. To make matters worse many of the stairways and elevators are blocked off, forcing them to make their way level by level, finding a way to the next one down to gain the exit.

Meanwhile, Cross (Linden Ashby) tries to prevent Lt. Waters (Keith David) from unleashing a bombing raid on the hospital. A number of survivors and a hunter are killed and one (hunter) is infected as they make their way out of the hospital. A survivor is captured by a human to be fed on by his infected daughter, but is rescued by Tao. As the humans make their way out a horde of infected chase them. As they exit the hospital just in time before the power fails the vampires become trapped inside. The surviving humans then run as jet planes bomb the forsaken hospital.

==Cast==
* Steven Seagal as Tao
* Tanoai Reed as Tagart
* Jenna Harrison as Dorothy
* Danny Midwinter as Morgan
* Emma Catherwood as Amelia
* Stephen Hagan as Ricky
* Daniel Percival as Dylan
* Skye Bennett as Charlotte
* Linden Ashby as Cross
* Keith David as Lt. Waters

==Reception==
 

Rob Hunter of FilmSchoolRejects.com says that although the film could have been entertaining, "the idea and the execution are worlds apart" and questions the strange decisions of the survivors. Hunter praises Tanoai Reed (a cousin and stunt double of Dwayne Johnson) as the one bright spot of the film, delivering top-notch fight scenes, even if his acting could do with improvement. 

==See also==
* Vampire film

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 