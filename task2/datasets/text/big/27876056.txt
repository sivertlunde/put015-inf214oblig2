25 Hill
{{Infobox film
| name           = 25 Hill
| image          = 25_Hill_Movie_Poster_2011.jpg
| caption        = Theatrical poster
| director       = Corbin Bernsen
| producer       = Corbin Bernsen Chris Aronoff
| writer         = Corbin Bernsen
| starring       = Corbin Bernsen Nathan Gamble Maureen Flannigan Ralph Waite Bailee Madison
| music          = Lee Strauss
| cinematography = T.J. Hellmuth
| editing        = Bradley R. Golowin
| studio         = Team Cherokee Productions
| distributor    = Echolight
| released       =   
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
25 Hill is a 2011 drama film written and directed by Corbin Bernsen about the All-American Soap Box Derby, the championships of which are held each August in Akron, OH. It stars Nathan Gamble, Corbin Bernsen, Maureen Flannigan, and Ralph Waite.

==Plot==
12 year-old Trey Caldwell (Nathan Gamble) is shattered when his soldier father is killed in Afghanistan, leaving a hole in the boy’s life – and an unfinished Soap Box Derby car in the garage. A final gift from his dad, the car is a constant reminder of all that could have been. But when Trey meets Roy Gibbs (Corbin Bernsen), a grizzled Fire Chief devastated by the loss of his firefighter son on 9/11, a new relationship forms and old wounds finally begin to heal. As the unlikely team works to complete the Soap Box car and train for the upcoming Derby, they’ll learn that life isn’t about the starting line or checkered flag – it’s about having the courage to make the incredible journey of faith in between.

==Cast==
*Nathan Gamble as Trey Caldwell, a boy who has no interest in derby racing. 
*Corbin Bernsen as Roy Gibbs, a man who helps Trey build his derby cart.
*Maureen Flannigan as Maggie Caldwell, Treys mom, who supports him.
*Ralph Waite as Ed, a man who helps Trey.
*Timothy Omundson as Thomas Caldwell, Treys dad.
*Bailee Madison as Kate Slater, Treys nemesis.
*Brad Raider as Derek Slater, Kates father.
*Andrew Nellise, Todd Hinton, Scott Sherwood as "Team Slater"
*Jim Clark, Western Regional Track Announcer

==Production==

===Filming===
Filming started in Akron, Ohio on April 7, 2010 and went into post-production on August 13.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 