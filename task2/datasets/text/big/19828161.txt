Benjamin Franklin, Jr.
{{Infobox film
| name           = Benjamin Franklin Jr.
| image          =
| image_size     =
| caption        =
| director       = Herbert Glazer
| producer       = MGM
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Charles Shoenbaum
| editing        = Leon Borgeau MGM
| released       =  
| runtime        = 10 53"
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by Herbert Glazer.  It was the 211th Our Gang short (212th episode, 123rd talking short, 124th talking episode, and 43rd MGM produced episode) that was released.

==Plot==
The gang kids are upset that World War II is causing them deprivations and inconveniences. Organizing a fact-finding committee, Gang members Mickey, Froggy, Buckwheat, and Janet try to determine what to do about the present national crisis. With the help of a convenient copy of Benjamin Franklins Poor Richards Almanac, the kids stage a play in which they cathartically come to grips with the sacrifices indigenous to the war effort, and provide patriotic solutions to the situation.

==Notes==
Billie Thomas is the last remaining cast member from the Hal Roach Studios days.   This is just one more teach the children/morality lesson/conservation lesson/World War patriotism film short that is anything but entertaining or funny.   

==Cast==
===The Gang=== Robert Blake as Mickey
* Janet Burston as Janet
* Billy Laughlin as Froggy
* Billie Thomas as Buckwheat
* Mickey Laughlin as Happy

===Additional cast===
* Ernie Alexander as John, Mickeys father Barbara Bedford as Janets mother
* Margaret Bert as Martha, Mickeys mother
* Vincent Graeff as Club member put in his place Jimmy Stewart
* Frank Ward as First club member to gripe
* Dickie Hall as Club member
* Barry Downing as Extra club member
* Elana Savona as Extra club member

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 