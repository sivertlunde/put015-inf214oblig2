A Night Like This (film)
 
  same title. 

==Synopsis==
Police Constable Mahoney, with the help of the affable Clifford Tope, outwits a criminal gang that operates from a gambling club. Mahoney and Tope restore a stolen necklace to its owner. 

==Cast==
*Clifford Tope – Ralph Lynn*
*PC Michael Mahoney  –  Tom Walls*
*Cora Mellish  –  Winifred Shotter*
*Mrs Decent  – Mary Brough*
*Miles Tuckett  – Robertson Hare*
*Aubrey Slott  –  Claude Hulbert
*Micky the Mailer  –  C. V. France
*Molly Dean  –  Joan Brierley
*Koski  –  Boris Ranevsky
*Waiter –   Reginald Purdell
*Mrs Tuckett  –  Norma Varden*
*Mimi, cocktail shaker  –  Kay Hammond
*Taxi driver  –  Hal Gordon
*Night club band  –  Roy Foxs Band
*Singer  –  Al Bowlly
*Pianist  –  Lew Stone

::Source :  British Film Institute  , British Film Institute, accessed 28 February 2013 
Cast members marked * were the creators of the roles in the original stage production; Michael Mahoney was called Michael Marsden in the stage play. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 