Cello (film)
{{Infobox film
| name           = Cello
| image          = Cello film poster.jpg
| director       = Lee Woo-cheol
| producer       = Yun Hyo-seok Kim Sang-chan Jang Yong-seok
| writer         = Jeong Woo-cheol
| starring       = Sung Hyun-ah Park Da-an Jeong Ho-bin
| music          = Lee Han-na
| cinematography = Gwon Yeong-cheol
| editing        = Kim Yong-su
| distributor    = Tube Entertainment
| released       = August 18, 2005  , Koreanfilm.org. Retrieved on March 19, 2008. 
| runtime        = 94 minutes
| country        = South Korea
| language       = Korean
}} South Korean horror film.

== Plot ==
 
The viewer first sees a young woman playing the Ave Maria on a cello, the scene shifts to show another woman bleeding on an operating table.

Another scene shift shows Hong Mi-ju (Sung Hyun-ah) watching students playing their cellos while their professor grades them. The professor tried to coax her into going to a welcome home concert for the little sister of Kim Tae-yeon. Mi-ju declines both the concert and a job offer to become more than just an associate teacher. In the teachers lounge, Mi-ju is confronted by a student who tells Mi-ju that all because of Mi-ju, all of her work for music is for nothing. The student is smug and cocky, promising revenge. Mi-ju leaves, shaken, and starts to drive home. On her way back home, she nearly avoids getting into an accident with a truck.  When she gets home, Mi-ju receives a message on her cell phone: "Are you happy? ...You should be."

Startled, Mi-ju looks around the house while turning on lights, but they snap off when she reaches the attic. There, she sees her elder, autistic daughter, Yoon-jin (Choi Ji-eun). The lights come on, revealing her husband Jun-ki (Jeong Ho-bin), her sister-in-law Kyeong-ran (Wang Bit-na), and her younger daughter Yoon-hye. Everyone but Yoon-jin, who appears mute and emotionless, sings "Happy Birthday" to Mi-ju. Mi-ju opens her presents. Later on, as they bathe, Mi-ju tells Yoon-jin how happy she is, how much she loves Yoon-jin. Mi-ju seems to share a special affection for Yoon-jin.

The next day, they pass a music store. Yoon-jin stops to stare at a cello, and Mi-ju buys it for her. Mi-ju meets the new, silent housekeeper, Ji-sook, and begins to teach Yoon-jin how to play the cello. 

More strange things happen; Yoon-hye plays in Kyeong-rans room until the latters fiance, Hyeon-woo, calls. Having been shooed out, Yoon-hye goes to her sisters room to ask to try the cello, but the normally calm and emotionless Yoon-jin bites her little sister. 

The same night, Kyeong-ran has a breakdown, as her fiance has seemingly broken up with her. Eventually the family has to just leave Kyeong-ran alone. In Yoon-jins room, Mi-ju watches the sleeping Yoon-jin, but her daughters sleeping face suddenly becomes ghastly, and eerie voices can be heard. 

In Kyeon-rans room, a crack appears on a picture of Kyeong-ran and her fiance, and through the wall, a ghost emerges and throws her through the glass balcony door. The rest of the family does not seem to hear a thing. However, Yoon-jin gets out of bed and pulls back the curtain, and sees Kyeong-ran strangled and dangling at her window.

Sensing his wifes inner turmoil, Jun-ki asks whats going on, and presents her with her old college yearbook, asking why Kim Tae-yeons pictures are cut out. Mi-ju tells her husband the reason why she quit playing the cello is because of her former friend, Kim Tae-yeon. The flash back shows a plain girl, Kim Tae-yeon, struggling to play as good as her friend, and struggling to pretend to be happy for her friend as Mi-ju rises far and above her friend. Mi-ju tells her husband that she doesnt think Tae-yeon was trying to kill her out of jelousy, but the night Mi-ju was chosen above Tae-yeon, there was a car accident and Tae-yeon was killed while Mi-ju was merely injured.

Mi-ju attends the cello concert she had previously declined. However, with a blink of an eye, Mi-ju finds herself alone in the room. She sees the same ghost who emerged through Kyeong-rans wall on stage playing the cello. But then, it is over and the other people and the cello player reappear, but Mi-ju leaves, terrified, hearing the voices from the night before.

At home, Yoon-hye begs again to play the cello, but becomes frightened when the cello starts to appear and disappear without anyone touching it. She tries to leave Yoon-jins room, but the door is locked. They are suddenly transported to the balcony, with Yoon-hye hanging from the side. Mi-ju returns to see Yoon-jin pulling Yoon-hyes fingers back one by one. Yoon-hye falls to her death, and it begins to rain. Mi-ju gathers her now dead little daughter, and places her body in the basement. When Mi-jus husband, Jun-ki, returns home from work, he asks where Yoon-hye is, but Mi-ju replies she sent her daughter to camp. Wanting to talk to his little girl, Jun-ki calls her cell phone, but gets no answer. However, he starts to hear the ringing of the phone in the basement where Yoon-hyes body is... 

Confronting his wife, he demands to know whats going on, accusing Mi-ju of killing their little girl. As Mi-ju explains it was an accident, starting to get hysterical. In the struggle, Mi-ju pushes her husband back, only to find he has been stabbed by a sharp pipe and is dead. Mi-ju turns to face the stairway slowly and sees the ghost who looks like the disgruntled student and whispers, "...Kim Tae-yeon..." 

Through the ghost of Kim Tae-yeons eyes, we see a flashback of what really happened. Tae-yeon, who looks exactly like the student from the beginning of the movie; pretty, talented, and confident (indeed, the audience is led to believe there really wasnt a student to begin with, but rather Mi-ju hallucinating events), is the more talented cello player. It is Tae-yeon, not Mi-ju, who was the more talented, sweet, and "better" student. After the final humiliation of Tae-yeon being chosen over her, Mi-ju swerves while driving them home and crashes the car by a steep incline off the road. Tae-yeon is thrown from the car and is barely holding on from falling off the side of the cliff. Mi-ju grabs her friends hand but eventually lets Tae-yeon fall to her death.

Brought back to the present, Mi-ju tries to stab Tae-yeons ghost with a knife to stop her from going to Yoon-jin, begging "Not my Yoon-jin, not my Yoon-jin!". She then sees that she has stabbed the housekeeper. Believing the cello holds the power to the ghost of Tae-yeon, Mi-ju grabs a golf club and rushes to Yoon-jins room. Mi-ju grabs the cello, throws it out against the wall, and smashes it as Yoon-jin screams in her room. When the noise stops, Mi-ju goes in. The room is empty except for the cello, which is unharmed. She looks back into the hallway to see the beaten and bloody body of her daughter. As Mi-ju kneels by the weltering Yoon-jin, she feels Tae-yeon forcing her hand to stab Yoon-jins beaten body. Mi-ju resists and stabs herself in the chest.

Mi-ju wakes up in the hospital to find that her earlier car accident was not imaginary, and that the previous events have been part of her coma—the voices she heard were actually those of Jun-ki and Yoon-hye, whispering for her to wake up. Mi-ju finds her family members are all safe and sound, around her and hugging her tightly. Outside the hospital room, Jun-ki asks the doctor why his wife thought they were all dead. The doctor replies it was probably part of the coma—Mi-jus family is all that is important to Mi-ju now, and often anxiety will pertain to what is most important to a person. 

When Mi-ju returns home, she receives the same message again: "Are you happy? ...You should be." She walks through the house turning on lights, which go out when she reaches the attic. Again, she finds her family there, in the same way as she did in the beginning of the movie. They sing "Happy Birthday" to her, and Kyeong-ran gives Mi-ju the same album. Inside, she finds a scribbled inscription: "This is only the beginning," before the ghostly hands of Kim Tae-yeon reach through Mi-jus hair and slowly grasp her old friends face. 

==Cast==
* Sung Hyun-ah ... Hong Mi-ju
* Park Da-an ... Kim Tae-yeon
* Jeong Ho-bin ... Jun-ki
* Jin Woo ... Kyung-ran
* Kim Na-woon ... Sun-ae
* Jin Ji-hye ... Yoon-hye

== References ==
 

== External links ==
*  
*  
*   at HanCinema

 
 
 
 
 