O Homem do Futuro
{{Infobox film
| name           = O Homem do Futuro
| image          = Man from the Future.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Cláudio Torres
| producer       = Tatiana Quintella Cláudio Torres
| writer         = Cláudio Torres
| starring       = Wagner Moura Alinne Moraes Gabriel Braga Nunes Maria Luisa Mendonça
| music          = Luca Raele Mauricio Tagliari
| cinematography = Ricardo Della Rosa
| editing        = Sérgio Mekler
| studio         = Conspiração Filmes Globo Filmes
| distributor    = Paramount Pictures 
| released       =  
| runtime        = 106 minutes
| country        = Brazil
| language       = Portuguese
| budget         = R$8 million 
| gross          = R$11,498,810 
}}
O Homem do Futuro (lit. "The Man from the Future") is a 2011 Brazilian romantic comedy film directed by Cláudio Torres.  The film stars Wagner Moura and Alinne Moraes. It was shot in Paulínia and Campinas in the state of São Paulo, and in Rio de Janeiro. 

== Plot ==
In 2011, João "Zero" (Moura), a bitter but brilliant physicist, spends his days brooding over a fateful night 20 years before when he was publicly betrayed and humiliated at a college party by his girlfriend. He now heads one of the largest scientific projects in Brazil, but his eccentricities and tantrums have brought him to the verge of being fired by his former college roommate and current sponsor, Sandra. 
 bridge to the past, leading him to the year 1991 and the traumatic night when the beautiful Helena (Moraes) left him for a popular playboy, Ricardo (Nunes). 
 time paradoxes caused by the presence of three versions of himself in 1991.

== Cast ==
* Wagner Moura as João "Zero" Henrique
* Alinne Moraes as Helena
* Maria Luiza Mendonça as Sandra
* Gabriel Braga Nunes as Ricardo
* Fernando Ceylão as Otávio

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 