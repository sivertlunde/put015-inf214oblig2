Brown of Harvard (1918 film)
 
{{Infobox film
| name           = Brown of Harvard
| image          = BrownofHarvard1918newspaperad.jpg
| image_size     =
| caption        = Newspaper advertisement.
| director       = Harry Beaumont
| producer       = William N. Selig
| writer         = Gilbert Colman (novel) Rida Johnson Young (play and novel) Harry Beaumont
| narrator       =
| starring       = Tom Moore Hazel Daly
| music          =
| cinematography =
| editing        =
| distributor    = Perfection Pictures (George Kleine System)
| released       =  
| runtime        = 6 reels
| country        = United States Silent (English intertitles)
| budget         =
}} 1918 film Broadway play Brown of Harvard  by Rida Johnson Young and the novel by Young and Gilbert Colman. The Washington State University football team and its coach, William "Lone Star" Dietz, participated in filming while in Southern California for the 1916 Rose Bowl. 

==Cast== Tom Moore as Tom Brown
*Hazel Daly as Evelyn Ames
*Sidney Ainsworth as Victor Colton (as Sydney Ainsworth)
*Warner Richmond as Claxton Madden
*Walter McGrail as Gerald Thorne
*Nancy Winston as Marian Thorne
*Alice Gordon as Mrs. Ames
*Kempton Greene as Wilton Ames
*Francis Joyner as Cart Wright (as Frank Joyner) Frank Joyner as Cart Wright Robert Ellis as Bud Hall
*Lydia Dalzell as Edith Sinclair
*Walter Hiers as Tubby
*Arthur Housman as Happy (as Arthur Hausman) Johnnie Walker as Jean William "Lone Star" Dietz Football Player (uncredited)

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 