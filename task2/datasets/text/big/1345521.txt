High Plains Drifter
 
 

{{Infobox film
| name           = High Plains Drifter
| image          =  High Plains Drifter poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Clint Eastwood Robert Daley
| writer         = Ernest Tidyman
| starring       = Clint Eastwood Verna Bloom Marianna Hill
| music          = Dee Barton
| cinematography = Bruce Surtees
| editing        = Ferris Webster The Malpaso Company
| distributor    = Universal Studios
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $5.5 million 
| gross          = $15,700,000   Box Office Mojo. Retrieved April 4, 2013. 
}} novelized it), Malpaso Company frontier mining Signet Books, 1975. ISBN 978-0-451-06159-1 

The film was shot on location on the shores of Mono Lake, California. Dee Barton provided the eerie film score. The film was critically acclaimed at the time of its initial release and remains popular, holding a score of 96% on Rotten Tomatoes.

==Plot==
A stranger on horseback rides into the mining town of Lago. Three gun-toting men follow him into the saloon, taunting him. When they follow him to the barbershop and threaten him, the Stranger shoots and kills all three. Impressed with this performance, a dwarf named Mordecai, who works in the barbershop, befriends the Stranger. A woman named Callie Travers arranges to bump into the Stranger in the street, making it seem his fault. When she slaps his cigar from his mouth, he drags her into the livery stable and rapes her. Next, the Stranger rents a room at the hotel, where Callie Travers shoots at him in his bath (inexplicably, he remains uninjured). That night, he dreams about a man being brutally whipped.

Sheriff Sam Shaw tells the Stranger he will not be charged for killing the three men. Meanwhile, the townsmen discuss a trio of feared gunfighters, Stacey Bridges and the brothers Dan and Cole Carlin, who are due to be released from prison that day.

(It is later revealed that some time before that, the town Marshal, Jim Duncan -- the man who appears to the Stranger in his dream -- had been whipped to death by Bridges and the Carlin brothers, while the people of Lago looked on. Only Sarah Belding, wife of hotelier Lewis Belding, made attempts to rescue him. A corrupt faction in Lago wanted Duncan dead because the Marshal discovered that the towns mine is on government ground -- the townsfolk fearing that this news, if reported, would result in the mines closure, which would threaten the towns livelihood). The townspeople double-crossed the three gunfighters after they killed Duncan, leading to the trios imprisonment, and the men are expected to seek vengeance.

Since the men slain by the Stranger at the barber shop were the mining companys new protectors, the townsmen decide to hire the Stranger as their replacement. The Stranger declines the job until Shaw tells him he can have anything he wants. Accepting these terms, the Stranger indulges in the towns goods and services, including giving away goods to a law-abiding Native American and his children who have been verbally abused in a racist manner by the town elders. He then makes Mordecai both sheriff and mayor. He also has Beldings clients moved out of the hotel, dismantles Beldings barn in order to make picnic benches, has the entire town painted red, and paints the word "HELL" on the "LAGO" sign just outside town.

While the Stranger trains the townspeople to defend themselves, Bridges and the Carlin brothers are released from prison and make their way to Lago. They kill three men and take their horses.

A group of townsfolk tries to ambush the Stranger in the hotel, but he kills all but one. After Belding inadvertently divulges his complicity in the attack (which left the hotel destroyed), the Stranger drags Sarah Belding into their room. Believing that he intends to rape her, she retreats into a corner, defending herself with a pair of scissors. He mocks her, implying she is the one who wants to have sex with him. Outraged, she attacks him with the scissors but he overpowers her and the struggle transforms into a mutual passionate embrace. The next morning, Sarah tells the Stranger about Duncans murder and that he is buried in an unmarked grave. She remarks that the dead dont rest without a marker.

The Stranger rides out the next morning, finds the gunfighters, and has a shootout with them before returning to Lago. With the town painted red, townsmen with rifles stationed on rooftops, and a picnic and welcoming banner set up for the gunfighters, the Stranger mounts his horse and rides away. However, when the gunfighters arrive, they easily overcome the inept resistance of the townspeople, killing several corrupt civic leaders. By nightfall, they have the townspeople collected in the saloon while other buildings burn. The Stranger returns, killing the gunfighters one by one, whipping Cole Carlin to death, hanging Dan Carlin with another whip, and shooting Stacey Bridges. Belding attempts to shoot the Stranger in the back, but Mordecai shoots Belding first. 

The next day, the Stranger departs, slowly riding through the ruined town in the same manner he arrived. At the cemetery, he passes Mordecai carving a fresh grave marker. Mordecai comments to the departing Stranger that he never did know his name, to which the Stranger answers cryptically, "Yes, you do." A look of astonishment crosses the little mans face, and he enigmatically replies "yes, sir, captain" and salutes. The camera pulls back to reveal that the wooden marker carved by Mordecai reads, "MARSHAL JIM DUNCAN. REST IN PEACE." The Stranger rides out, vanishing into the haze.

==Cast==
 
* Clint Eastwood as The Stranger
* Verna Bloom as Sarah Belding Mariana Hill as Callie Travers
* Billy Curtis as Mordecai Mitchell Ryan as Dave Drake
* Jack Ging as Morgan Allen
* Stefan Gierasch as Mayor Jason Hobart
* Ted Hartley as Lewis Belding Geoffrey Lewis as Stacey Bridges, outlaw
* Dan Vadis as Dan Carlin, outlaw Anthony James as Cole Carlin, outlaw Walter Barnes as Sheriff Sam Shaw
* Paul Brinegar as Lutie Naylor Richard Bull as Asa Goodwin
* Robert Donner as Preacher
* John Hillerman as Bootmaker
* John Quade as Freight Wagon Operator
* Buddy Van Horn as Marshal Jim Duncan
* William OConnell as the Barber
* Scott Walker as Bill Borders, outlaw
 

==Production==
 )]] The French Connection. McGilligan (1999), p. 221  Tidymans screenplay was inspired by reports of the real-life murder of Kitty Genovese, where people ignored the killing of a young woman in Queens in 1964. Holes in the plot were filled in with black humor and allegory, influenced by Sergio Leone.  An uncredited rewrite of the script was provided by Dean Riesner, screenwriter of other Eastwood projects.

Universal wanted Eastwood to shoot the feature on their own back lot, but Eastwood opted instead to film on location. Eastwood scouted locations for filming in a pickup truck while driving alone through Oregon, Nevada and California.  300 miles from Hollywood, Eastwood had an entire town built on the shores of Mono Lake for the project, as he considered the area "highly photogenic". Hughes, p. 28  Over 40 technicians and 10 construction workers built the town in 18 days using 150,000 feet of timber.  The town of Lago comprised fourteen houses and one two-story hotel. Complete buildings, rather than facades, were built, so that Eastwood could shoot interior scenes on the site. Additional scenes were filmed at Reno, Nevadas Winnemucca Lake and Californias Inyo National Forest.  Eastwood filmed High Plains Drifter in sequence. Eliot (2009), p. 144  Filming was completed in only six weeks, two days ahead of schedule, and under budget.
 French and German Dubbing dubbings restore it.

==Reception== 20th highest Arthur Knight Saturday Review remarked that Eastwood had "absorbed the approaches of Siegel and Leone and fused them with his own paranoid vision of society". McGilligan, p. 223  Jon Landau of Rolling Stone concurred, remarking that it is his thematic shallowness and verbal archness which is where the film fell apart, yet he expressed approval of the dramatic scenery and cinematography. 

Eastwood reflected on the films meaning, indicating "its just an allegory&nbsp;... a speculation on what happens when they go ahead and kill the sheriff and somebody comes back and calls the towns conscience to bear. Theres always retribution for your deeds."  Hughes, pp. 30–31 

==See also==
* List of American films of 1973
* Pale Rider
* Weird West

==References==
 

Bibliography
 
*  
*  
*  
*  
 

==Further reading==
* Guérif, François (1986). Clint Eastwood, p.&nbsp;94. St Martins Pr. ISB

==External links==
  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 