Ji Hujur
 
{{Infobox film
| name           = Ji Hujur
| image          =
| alt            =  
| director       = Jakir Hossain Raju
| producer       = Md. AbbasUllah Sikdar
| writer         = Jakir hossain raju
| starring       = Symon Sadik Sara Zerin Jemi Anan kazi Hayat AbbasUllah Sikdar Nasrin Onamika
| music          = Ali Akram Shuvo  Azad Mintu
| cinematography = hasan Ahmed
| editing        = Towhid Hossain Chowdhury
| studio         = Anandamela Cholocchitro Ltd.
| distributor    = Anandamela Cholocchitro Ltd.
| released       =  April 2012
| country        = Bangladesh Bengali
| budget         =
| gross          =
}}
Ji Hujur also ( ) is a Bangladeshi Bengali language|Bengali-language film. Directed by Jakir Hossain Raju. starring   Symon Sadik, Sara Zerin, Jemi, Anan, kazi Hayat, AbbasUllah Sikdar, Nasrin, Onamika.  

==Story Line==

 

==Review==
The film received positive reviews from the critics. The role of Symon Sadik was highly praised.

==Cast==
{{columns-list|2|
* Symon Sadik as Abdur Rahman
* Sara Zerin as bunty 
* Jemi as S I faruq ( Police Officer )  
* Anan as Runa ( S I faruqs Girl Friend )
* Kazi Hayat as police commissioner
* Doly johur as Mrs. Haque (Buntys Mother)
* AbbasUllah Sikdar as haque Saheb (Buntys father)
* Lina Ferdousy as Abdur Rahmans mother
* Nasrin as Item Girl
* Onamika as bably (Buntys sister)
* Roton as Bodor (Police)
* Chikon Ali as Kader (Police)
* Mahtab as Kana Manik
* Joy as Child Artist
* Pina
}}

==Music==
{{Infobox album
| Name = Ji Hujur
| Type = soundtrack
| Cover = 
| Artist = Andru Kishor Konok Chapa Monir Khan Mehrin Roma
| Released = 2012 (Bangladesh)
| Recorded = 2011
| Genre = Films Sound Track
| Length = 22 Minute
| Label = AnandaMela Cholocchitro Ltd.
| Producer = Md. AbbasUllah Sikdar
}}

{{track listing	
| extra_column = Singer(s)
| title1 = Dont disturb me
| extra1 = Mehrin
| length1 = 4:13

| title2 = chintaa koro (item Song)
| extra2 = Roma
| length2 = 4:15

| title3 = Chand hoye Toke ami dakbo
| extra3 = Monir Khan, Konok Chapa
| length3 = 4:00

| title4 = Morechi Morechi Ami 
| extra4 = Konok Chapa,  Andru Kishor
| length4 = 4:50

| title5 = Keno Elo Eto Prem
| extra5 = Konok Chapa
| length5 = 4:48
}}

==Reference==
 

 
 
 
 
 
 


 
 