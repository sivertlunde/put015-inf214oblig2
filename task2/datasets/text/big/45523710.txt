Med dej i mina armar
{{Infobox film
| name=Med dej i mina armar
| image= 
| caption= 
| director=Hasse Ekman
| producer=Lorens Marmstedt, Terrafilm
| writer=Hasse Ekman
| starring=Edvin Adolphson Karin Ekelund Thor Modéen
| music=Kai Gullmar, Gunnar Malmström
| released=1940
| runtime=83 min
| country=Sweden Swedish
}}
 Swedish comedy comedy film directed by Hasse Ekman. 

==Plot summary==
Krister Dahl loses his memory when he is hit in the head by a golf ball one day. He later goes to a party and meets his ex-wife and immediately fall in love with her again, not knowing who she is. She does not know what to think about this sudden change in his behaviour. He is like new man, only question is, who is he? 

==Cast==
*Edvin Adolphson as Krister Dahl
*Karin Ekelund as Barbro Brandt
*Thor Modéen as Vårby, Kristers valet
*Stig Järrel as Felix Tallgren
*Katie Rolfsen as Hilda, Barbros maid
*Marianne Aminoff as Britt Lambert 
*Carl-Gunnar Wingård as Svanberg, Lawyer 
*Anna-Lisa Baude as Mrs. Svensson 
*John Botvid as Alexander Danielsson, taxi driver
*Leif Amble-Næss as Sardini
*Mimi Pollak as Miss Carlander
*Eivor Engelbrektsson as Miss Svensson
*Nils Jacobsson as Klas Höglund 
*Åke Johanson|Åke Johansson as caddie
*Julia Cæsar as Kristers Second Secretary 
*Emil Fjellström as Coachman
*Anna-Lisa Ryding as Connie Löfberg
*Ilse-Nore Tromm as Ester
*Sven-Olof Sandberg as Courtyard Singer 

==External links==
* 

 
 

 
 
 
 
 