Ananthasayanam
{{Infobox film 
| name           = Ananthasayanam
| image          =
| caption        =
| director       = K Suku
| producer       = K Suku
| writer         =
| screenplay     = Jagathy NK Achari Prema
| music          = K. Raghavan
| cinematography = Melli Irani
| editing        = G Venkittaraman
| studio         = Bhavanachithra
| distributor    = Bhavanachithra
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, Prema in lead roles. The film had musical score by K. Raghavan.   

==Cast==
 
*Sheela
*Jayabharathi
*Adoor Bhasi Prema
*T. R. Omana
*Mancheri Chandran
*Bahadoor
*K. P. Ummer
*Kottarakkara Sreedharan Nair Meena
*N. Govindankutty
*Nellikode Bhaskaran
*Radhamani
*Raghava Menon Sujatha
*Thodupuzha Radhakrishnan
*Chandramohan 
 

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dukhathin Gaagulthaamalayil || B Vasantha || Sreekumaran Thampi || 
|-
| 2 || Maanava Hridayam || P Jayachandran || Sreekumaran Thampi || 
|-
| 3 || Maarivil Gopuravaathil || KP Brahmanandan || Sreekumaran Thampi || 
|-
| 4 || Sandhyaa Megham || S Janaki || Sreekumaran Thampi || 
|-
| 5 || Udayachandrike || S Janaki || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 