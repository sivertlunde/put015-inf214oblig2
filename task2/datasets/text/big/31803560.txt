The Flirting Widow
{{Infobox film
| name           = The Flirting Widow
| image          = The_Flirting_Widow_1930_LC.jpg
| caption        = Theatrical poster
| director       = William A. Seiter
| producer       = William A. Seiter
| story          = A.E.W. Mason
| screenplay     = John F. Goodrich William Austin Claude Gillingwater
| music          = Alois Reiser
| cinematography = Sidney Hickox
| editing        = John F. Goodrich
| distributor    = First National Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
}}
The Flirting Widow is a 1930 talkie|all-talking Pre-Code Hollywood|pre-code American comedy film directed by William A. Seiter and produced and released by First National Pictures, a subsidiary of Warner Bros. The film stars Dorothy Mackaill, Basil Rathbone, Leila Hyams and Claude Gillingwater.  It is based on a story, entitled Green Stockings, by A.E.W. Mason. The film was remade in 1933 as Her Imaginary Lover at the Teddington Studios, the British branch of Warner Brothers-First National Productions.

==Synopsis==
Claude Gillingwater refuses to let his youngest daughter (Leila Hyams) get married before her older sister, played by Dorothy Mackaill, finds a husband. Gillingwater is afraid that people will call Mackaill an old maid and be made the butt of jokes. Mackaill, who has no interest in getting married, attempts to fool her father by pretending to be engaged. After a short vacation away from  home she makes the announcement to her family and informs everyone that he has gone to Arabia but that they will be married upon his return to England. When Mackaills father receives this news, he finally consents to Hyams marriage. At Hyams insistence, Mackaill writes a love letter to her fiancé. After finishing her letter, Mackaill attempts to throw it away when no one is looking. A servant, however, finding the letter posts it. The letter is received by a real Colonel Smith, who happens to then be in Arabia, played by Basil Rathbone. He is amused by the love letter. After her sisters marriage, Mackaill publishes an obituary in the paper for her Colonel Smith. Rathbone reads the obituary and decides to pay a visit to Mackaill, pretending to be a close friend of Colonel Smith. Rathbone pretends to bring some mementos of Smiths and when he gives them to Mackaill she is made uncomfortable. Eventually Rathbone reveals his identity and in the course of time they fall in love with each other.

==Cast==
* Dorothy Mackaill - Celia Faraday
* Basil Rathbone - Colonel John Vaughn-Smith
* Leila Hyams - Lady Evelyn Faraday Trenchant William Austin - James Raleigh Raleigh
* Claude Gillingwater - William Faraday
* Emily Fitzroy - Aunt Ida
* Flora Bramley - Phyllis Faraday
* Anthony Bushell - Robert  Tarbor
* Wilfred Noy - Martin, the Butler

==Preservation==
The film survives intact and has been broadcast on both television and cable.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 