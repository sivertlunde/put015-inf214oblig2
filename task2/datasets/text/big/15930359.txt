Mission Istaanbul
 
 
{{Infobox film
| name           = Mission Istaanbul
| image          = Mission istabl.PNG
| caption        = Theatrical release poster
| director       = Apoorva Lakhia
| producer       = Ekta Kapoor Shobha Kapoor Sunil Shetty Shabbir Boxwala
| story          = Suresh Nair
| screenplay     = Suresh Nair Apoorva Lakhia
| starring       = Vivek Oberoi Shriya Saran Zayed Khan
| music          = Shamir Tandon Chirantan Bhatt Anu Malik Mika Singh
| cinematography = Gururaj R J
| editing        = Rajesh Singh
| distributor    = Balaji Motion Pictures Popcorn Motion Pictures
| released       =  
| runtime        = 125 mins Turkish
| country        = India
| budget         =
| gross          =
}}
 action thriller film starring Vivek Oberoi, Shriya Saran and Zayed Khan.  The film, directed by Apoorva Lakhia, features Abhishek Bachchan and Peter Handley in a special appearance.

==Plot==
The story is based on the life of Indias top news reporter Vikas Sagar (Zayed Khan). He would do anything for his news report, even risk his life. Ambitious, popular and professional, Vikas is considered one of the most promising journalists in the business. Due to this reason, Uwais Hussain (Sunil Shetty), a senior producer at the controversial news channel "Al Johara" offers him a job as the head of the channels India bureau. Vikas is going through a divorce with his wife Anjali (Shriya Saran), and to get his mind of things, Vikas accepts the offer and flies to Istanbul to start work for "Al Johara". 

On the flight, Vikas finds himself seated next to the stunningly beautiful Lisa Lobo (Shweta Bhardwaj), who is headed to Istanbul to attend a medical convention or so she claims. The two begin a conversation and get on well, until the flight lands, and Lisa disappears at the airport.

Once in Istanbul, Vikas meets with the head of Al Johara, Ghazni (Nikitin Dheer), who has business interests all over the world but whose obsession these days is Al Johara as an instrument to shape world events. There is only one word of caution to Vikas and thats never ever to venture onto the 13th floor known as the Catacomb. He once again meets Uwais, who tells him that he is looking forward to quitting his job and settling down with his Irish girlfriend. Ghazni sends Vikas and Uwais on an assignment to cover kidnapped journalists in one of terrorist Abu Nazirs terrorist-camps in Afghanistan. 

The two meet Abu Nazirs brother, Khalil (Shabbir Ahluwalia), who tries to scare Vikas by killing one of the kidnapped journalists. Vikas, shocked, takes action and fights Khalils thugs. Khalil then shoots Uwais multiple times leading to his death. Vikas jumps on a helicopter and rides away, seeing Uwais get killed. Reeling at this close encounter with terrorism and watching a brutal killing, Vikas arrives in Istanbul in a daze. Ghazni finds out about Uwais death and holds a funeral. At the funeral, Vikas is approached by a former Turkish commando, Rizwan Khan (Vivek Oberoi), who hints that no senior employee has ever quit Al Johara and rattles off a list of Al Johara employees who had been killed in terrorist attacks within days of there being of rumours of their plans to quit or their resumes circulating in the job market.

Vikas remembers Uwais telling him that he wants to quit his job. This makes Vikas suspicious about Al Johara being connected to terrorism. Before asking any more questions, Rizwan Khan disappears. Vikas discreetly runs a check on the names rattled off by Rizwan and discovers he was indeed telling the truth. Five Al Johara reports before Uwais had either died in a car bomb explosion, abducted and killed or simply found dead. He slowly notices that not everything is as simple as it looks in the offices of Al Johara, especially when he seems to bump into strangers whose faces he soon recognizes as suicide bombers in a couple of terrorist attacks.

Vikas, confused, accidentally goes to the forbidden 13th floor of the building, and is tortured by Al Joharas staff. Lisa arrives and saves him by taking him away. She sends him to a van for rescue, and it turns out that Ghazni is inside the van waiting for him. Lisa is informing Al Johara terrorists about Vikas. After escaping from Ghazni, Vikas finds Rizwan and asks for help. Rizwan shows Vikas that Abu Nazir is actually dead but being kept alive by Al Johara through videotapes by using digital images of the terrorist and doctoring old footage. 

Vikas hacks into Al Joharas computers and puts a virus in them. Rizwan breaks the hands of a man in the Al Johara building whom Vikas had seen entering the authorised lift to the 13th floor. Using these hands, Rizwan and Vikas break in to the 13th floor where Vikas finds out the truth about how Al Johara helps terrorism increase and what they do and saves it on a PEN drive.

Lisa helps them escape the Al Johara building and tells them that she informed Gazni about Vikas activities so that he could trust her but actually she is working for RAW. Soon enough Gazni frames Lisa, Vikas and Rizwan as terrorists.

Vikas calls Anjali who tells him that she is coming to Istanbul to pick him up. This call allows Gazni to track Vikas and soon he finds out about Anjalis arrival and picks her up from the airport unbeknownst to both Gazni and Anjali that Vikas and Rizwan are also present.

Anjali is tortured by Gazni and his men and so is Vikas especially when he refuses to give the PEN drive. Eventually he gives it to them because Khalil was about to shoot Anjali. After the PEN drive download was complete, Khalil attempts to kill Vikas and Anjali but an explosive fitted by Rizwan blows up allowing Vikas to free himself. Vikas and Gazni engage in a lengthy fight as well as Rizwan and Khalil. Vikas kills Gazni and Rizwan kills Khalil

In the end, Vikas and Anjali are reunited and Rizwan unexpectedly turns up at their place

==Cast==
* Zayed Khan as Vikas sagar
* Vivek Oberoi as Rizwan khan
* Shriya Saran as Anjali Sagar
* Nikitin Dheer as Al Gazini
* Shabbir Ahluwalia as Khalil Nazir
* Sunil Shetty as Owais Hussain
* Shweta Bhardwaj as Dr. Lisa Lobo
* Abhishek Bachchan in a Special Appearance
* Pia Trivedi in a Special Appearance
* Brent Mendenhall as President George W. Bush
* Omar Abdullah in a Special Appearance.
* Khalil Ahmed, as Osama Bin Laden

== Production ==
Zayed Khans role was initially meant to be played by Bobby Deol. Tusshar Kapoor was offered a villain role but turned that role down which then went to Nikitin Dheer. 

== Trivia ==

* The name of the fictional network in the film, Al Johara, is Arabic for "The Jewel". Johara is not a Turkish word, and Turkish does not use the prefix Al. Similarly, the name Abu Nazir, used by the films bin Laden-esque terrorist, is also Arabic, not Turkish. Unlike in many Arab cultures, Turkish men do not take the name Abu   or, indeed, have any similar honorific or nickname based on the name of their son(s). The names of many of the Turkish characters are also not Turkish names. The correct spelling in Turkish for "Rizwan" is Rızvan, whereas Owais is most definitely not a Turkish name, since the Turkish alphabet does not include the letter "W".

* For the climactic battle, the reed-thin Zayed Khan put on 11&nbsp;kg and weighed 75&nbsp;kg for the first time in his life. All because he had to take on the movies bad man Niketan Dheer, who was 6 feet 4&nbsp;inches tall and weighed 86&nbsp;kg.
* Vivek Oberoi grew his hair for his role as a Turkish commando in the film.

* Jammu and Kashmir politician Omar Abdullah appears in the film as himself.  

* Brent Mendenhall, a look-alike of U.S President George W. Bush, appears in the film as Bush.

* Khalil Ahmed, a double for Osama Bin Laden, appears.

* The organisation Al Johara in the movie and its alleged role as a mouthpiece for terrorists, i.e. having tapes delivered to the news organisation by terrorists, bears a resemblance to Qatar-based news channel Al Jazeera and the fact that it too was criticised by some White House officials as being an outlet for terrorists. In this respect the movie helps to re-inforce the Western worlds take on the War on Terror, which is a bit ironic since the country that is home to the films fictional network, Turkey, is a NATO country with troops engaged in the War on Terror in Afghanistan.

== Soundtrack ==

A soundtrack for Mission Istaanbul was released prior to the film and is available for retail. The soundtrack closely follows the theme of the film spanning a wide variety of music director such as Mika Singh, Hamza Farooqui, Chirantan Bhatt, Shamir Tandon and Anu Malik. The album consists 9 songs including 3 remix version.

===Track listing===
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! Title !! Singer(s)!! Music !! Lyrics !! Length
|-
| "World Hold On"  Raaj
| Shamir Tandon
| Shabbir Ahmed
| 05:12
|-
| "Mission Mission" 
| Hamza Farooqui
| Chirantan Bhatt
| Hamza Farooqui
| 03:57
|-
| "Jo Gumshuda"  Ege
| Anu Malik
| Sameer
| 05:37
|-
| "Nobody Like You" 
| Neeraj Shridhar, Anoushka Shankar|Anoushka, Ishq Bector
| Chirantan Bhatt
| Hamza Farooqui
| 03:45
|-
| "Yaar Mera Dildaara" 
| Zubeen Garg, Sunidhi Chauhan
| Anu Malik
| Sameer
| 05:21
|- 
| "Apun Ke Saath" 
| Mika Singh, Priya Nayar, Vikas Kohli
| Mika Singh
| Mika Singh & Virag Mishra
| 04:06
|- 
| "Jo Gumshuda (Remix)" 
| Shaan, Mahalakshmi Iyer, Ege
| Anu Malik
| Sameer
| 03:51
|- 
| "Nobody Like You (Remix)" 
| Neeraj Shridhar, Anoushka, Ishq Bector
| Chirantan Bhatt
| Hamza Farooqui
| 03:26
|- 
| "World Hold On (Remix)" 
| Kunal Ganjawala, Gayatri Ganjawala, Raaj
| Shamir Tandon
| Shabbir Ahmed
| 05:11
|}

==Criticial Reception==
Mission Instaanbul received negatived reviews from critics.Bollywood Hungama critic Taran Adarsh gave it 1.5 out of 5 stars.  The Hindu stated that Mission Istaanbul has no sting.  Rediff.com gave it 1 star.  

==Boxoffice==
Mission Istaanbul net. grossed   in India with a distributor share of  .  The film flopped at the domestic boxoffice.  

==References==
 

==External links==
*  

 

 
 
 