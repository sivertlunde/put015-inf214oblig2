Bad Milo!
  
{{Infobox film
| name           = Bad Milo!
| image          = Bad Milo! Theatrical Poster.jpg
| caption        = 
| director       = Jacob Vaughan
| producer       = Gabriel Cowan Adele Romanski John Suits
| writer         = Benjamin Hayes Jacob Vaughan
| starring       = Ken Marino Peter Stormare Gillian Jacobs Stephen Root Mary Kay Place  Patrick Warburton 
| music          = Ted Masur
| cinematography = James Laxton
| editing        = David Nordstrom
| studio         = New Artists Alliance Floren Shieh Productions Duplass Brothers Productions
| distributor    = Magnet Releasing
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
}}
Bad Milo! is a 2013 horror-comedy film written by Jacob Vaughan and Benjamin Hayes and directed by Jacob Vaughan. The film stars Ken Marino and was released in theaters October 4, 2013. 

==Cast==
* Ken Marino as Duncan
* Gillian Jacobs as Sarah
* Stephen Root as Roger
* Peter Stormare as Highsmith
* Mary Kay Place as Beatrice
* Patrick Warburton as Phil

==Reception==
The film currently holding a 62% "fresh" rating on Rotten Tomatoes; the consensus states: "Bad Milo! sets some deliriously low expectations with its gross premise – and then manages to match most of them in sick, entertaining style." 

==References==
 

==External links==
*  
* 

 
 
 
 


 