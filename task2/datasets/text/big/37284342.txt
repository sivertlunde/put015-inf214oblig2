Tracks (2013 film)
 
{{Infobox film
| name           = Tracks
| image          = Tracks 2013.jpg
| alt            = 
| caption        = Theatrical release poster John Curran
| producer       = {{Plainlist|
* Iain Canning
* Emile Sherman
|}}
| screenplay     = Marion Nelson
| based on       =  
| starring       = {{Plainlist|
* Mia Wasikowska
* Adam Driver
|}}
| music          = Garth Stevenson
| cinematography = Mandy Walker
| editing        = Alexandre de Franceschi
| studio         = See-Saw Films
| distributor    = Transmission Films
| released       =  
| runtime        = 113 minutes  
| country        = Australia
| language       = English
| budget         = $12 million 
| gross          = $4.9 million 
}} John Curran Adelaide Film Festival on 10 October 2013. This was the Australian premiere.    The film has also been shown at several other film festivals, including London, Vancouver, Telluride, Dubai, Sydney OpenAir, Dublin and Glasgow.

==Plot== Australian deserts National Geographic photographer Rick Smolan documents her journey.

==Cast==
* Mia Wasikowska as Robyn Davidson
* Adam Driver as Rick Smolan
* Rolley Mintuma as Mr. Eddie
* Brendan Maclean as Peter
* Rainer Bock as Kurt
* Jessica Tovey as Jenny Emma Booth as Marg

==Production==
There were five attempts in the early 1980s and 1990s to turn the book into a film.  In 1993, Julia Roberts was attached to star in a planned Caravan Pictures adaptation.  The project eventually fell apart. 

In May 2012, it was reported that Mia Wasikowska would play Robyn Davidson in a John Curran–directed adaptation.  In August, Adam Driver was cast as Rick Smolan.  The screenplay was written by Marion Nelson.

Finance was sourced from Screen Australia, the South Australian Film Corporation, Adelaide Film Festival, Deluxe Australia, pre-sales, and Canadas Aver Media Finance.

With a US$12 million budget,  the film began shooting on 8 October 2012.    It was filmed in South Australia and the Northern Territory.  Julie Ryan was a co-producer.

Tracks was released to DVD and Blu-ray on 25 June 2014.

==Reception==
===Box office=== limited theatrical release in North America opening in 4 theaters and grossing $21,544, with an average of $5,386 per theater and ranking #62 at the box office. The widest release for the film was 67 theaters and it ultimately earned $510,007 domestically and $4,368,235 internationally for a total of $4,878,242, below its estimated budget of $12 million. 

===Critical response===
Tracks received mainly positive reviews from critics. Review aggregator website Rotten Tomatoes gives the film has a score of 81% based on 111 reviews with an average score of 7 out of 10. The critical consensus states " What Tracks lacks in excitement, it more than makes up with gorgeous cinematography and Mia Wasikowskas outstanding performance."  The film also has a score of 78 out of 100 on Metacritic based on 34 critics, indicating "generally favorable reviews".  

Leonard Maltin gave Tracks 3.5 out of 4 stars, and has stated that it is the last movie (by wide release date) that will be included in the final edition of Leonard Maltins Movie Guide. 

Word and Film rated Tracks as the best movie adaptation of the year for 2014, Lisa Rosenman. Word and Film 18 December 2014
www.wordandfilm.com/2014/12/the-best-movie-adaptations-2014/  and Sydney Arts Guide awarded Marion Nelson the Best Adapted Screenplay Award 2014. 

Mandy Walker won 2014 Best Cinematography awards for Tracks from both the Australian Cinematographers Society and the Film Critics Circle of Australia  

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (4th AACTA Awards|4th)  AACTA Award Best Film Iain Canning
| 
|- Emile Sherman
| 
|- AACTA Award Best Actress Mia Wasikowska
| 
|- AACTA Award Best Cinematography Mandy Walker
| 
|- AACTA Award Best Costume Design Mariot Kerr
| 
|-
|}

==References==
{{Reflist|2|refs=

   

   

   

   

   

   

   

   

}}

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 