Singularity Principle
{{Infobox film
| name           = Singularity Principle
| image          = Singularity Principle.jpg
| caption        = 
| director       = David Robert Deranian   Austin Robert Hines  
| producer       = 
| writer         = David Robert Deranian   Austin Robert Hines  
| starring       = William B. Davis   Michael Patrick Denis   John Diehl 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Big Screen Entertainment   Epcott
| released       =  
| runtime        = 87 minutes
| country        = United States Canada Bahamas Australia
| language       = English
}}
Singularity Principle, is a 2013 science fiction film starring William B. Davis, Michael Patrick Denis and John Diehl. The film was produced by Double A Pictures and Salient Clear.  Epcott purchased rights to distribute the file in Japan  {{cite web
| title      = Big Screen Entertainment Signs Deal for New Sci-Fi Film SINGULARITY PRINCIPLE
| work       =broadwayworld.com
| publisher  = Wisdom Digital Media
| date       = February 6, 2013
| url        = http://www.broadwayworld.com/bwwmovies/article/Big-Screen-Entertainment-Signs-Deal-for-New-Sci-Fi-Film-SINGULARITY-PRINCIPLE-20130206#
| accessdate = 2013-12-07 }}   The film was originally released in Japan cinemas in March, 2013 and in the United States in October 2013.  The film was shot at the Canadian Light Source synchrotron in Saskatoon  {{cite web
| title      =  LS stars in Singularity Principle 
| work       = The StarPhoenix 
| publisher  = Postmedia Network Inc.
| date       = September 28, 2012
| url        =http://www2.canada.com/saskatoonstarphoenix/news/arts/story.html?id=d259f5cb-45e3-4ab4-b8b9-90b0647345cd
| accessdate = 2013-12-07 }}  {{cite web
| title      =  Backstage Pass: Canadian Light Source Synchrotron both set and star in new film
| work       =  Metro
| publisher  = Metro Media
| date       = October 17, 2012 
| url        = http://metronews.ca/news/saskatoon/407198/backstage-pass-canadian-light-source-synchrotron-both-set-and-star-in-new-film/
| accessdate = 2013-12-07 }}  with subsequent segments filmed in the Bahamas {{cite web
| title      =  Tourism Today features the film, "Singularity Principle" while it shoots in Nassau 
| work       =thebahamasweekly.com
| publisher  = 
| date       = 
| url        = http://www.thebahamasweekly.com/publish/ministry_of_tourism_updates/Tourism_Today_features_Singularity_Principle_while_shooting_in_Nassau19960.shtml
| accessdate = 2013-12-07 }}  {{cite web
| title      =  Feature films use scenic Bahamas as backdrop – video 
| work       =The Bahamas Investor
| publisher  = 
| date       = February 6, 2013
| url        = http://www.thebahamasinvestor.com/2012/feature-films-use-scenic-bahamas-as-backdrop-video/
| accessdate = 2013-12-07 }}  and Australia.

== Plot == parallel universes. Jacks protege, Dr. Peter Tanning (Michael Denis), is being questioned by Lawrence Cason (William B. Davis) who works for a clandestine Black operation|black-ops organization to learn how to reproduce and control the physical process.

== Cast ==
*William B. Davis as Lawrence Cason 
*Michael Patrick Denis as Dr. Peter Tanning
*John Diehl as Jack Brenner  
*Amy LoCicero as Dr. Lori Cason 
*Adam Formanek as Tim Sedal 
*Darren Toombs as William Buck Townsend

== References ==
 

== External links ==
* 
* 

 
 
 
 
 


 