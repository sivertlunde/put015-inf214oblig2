Garshom
{{Infobox film 
| name           = Garshom
| image          =
| caption        =
| director       = P. T. Kunju Muhammed
| producer       =
| writer         = P. T. Kunju Muhammed
| screenplay     =  Urvashi Murali Murali Madhu Madhu Siddique Siddique
| music          = Ramesh Narayan
| cinematography = Sunny Joseph
| editing        = Venugopal
| studio         = Janasakthi Films
| distributor    = Janasakthi Films
| released       =  
| country        = India
| language       = Malayalam
}}
 1999 Cinema Indian Malayalam Malayalam film, Madhu and Siddique in lead roles. The film had musical score by Ramesh Narayan.    It was the debut film of noted lyricist Rafeeq Ahammed.

==Cast== Urvashi
*Murali Murali
*Madhu Madhu
*Siddique Siddique
*Baby Hency
*Beena Antony
*Irshad
*VK Sreeraman
*Valsala Menon

==Soundtrack==
The music was composed by Ramesh Narayan and lyrics was written by Rafeeq Ahamed. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ethu Kaalaraathrikalkkum || Hariharan || Rafeeq Ahamed || 
|-
| 2 || Parayaan maranna || Hariharan || Rafeeq Ahamed || 
|-
| 3 || Parayaan maranna   || KS Chithra || Rafeeq Ahamed || 
|}

==References==
 

==External links==
*  

 
 
 

 