PK.COM.CN
 
{{Infobox film
| name           = PK.COM.CN
| image          = PKCOMCNposter.jpg
| caption        = 
| film name = {{Film name| traditional    = 誰說青春不能錯
| simplified     = 谁说青春不能错}}
| pinyin         = Shéi Shuō Qīng Chūn Bù Néng Cuò
| jyutping       = Seoi4 Syut3 Cing1 Ceon1 Bat1 Nang4 Co3
| director       = Xiao Jiang
| producer       = 
| writer         = Xiao Jiang He Changsheng (novel)
| starring       = Jaycee Chan Bolin Chen Niu Mengmeng
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = China
| language       = Mandarin
| budget         = 
}} 2007 Cinema Chinese film directed by Xiao Jiang and starring Jaycee Chan, Bolin Chen, and Niu Mengmeng. The plot was adapted from an online novel, Lost in Time (在时), by He Xiaotian (何小天). in this film: Jaycee Chan and Bolin Chen reunite after starring in the Twin Effects II.

The film has an unusual style, incorporating music, dance, and art.  It was screened at the Hollywood China Film Festival in 2007 and at the 2008 Shanghai International Film Festival.

== Plot ==
PK.COM.CN tells the story of Zhang Wenli (Jaycee Chan), a young doctor with overbearing parents who is invited to a reunion of his medical school class. At the reunion he has flashbacks to his best friends from medical school - his popular and charismatic roommate, Ji Yinchuan (Bolin Chen), and a mysterious and rebellious girl, A Fei (Niu Mengmeng).

== Cast ==
* Jaycee Chan
* Bolin Chen
* Niu Mengmeng
* Zhang Bo
* Law Kar-ying
* Li Qinqin

==External links==
*   - Film Details
*   - Unbound: Rise of the Online Serial Novel 
*  , "The Case Dazzles at Hollywood China Film Festival"

 
 
 
 
 

 