Raising Arizona
{{Infobox film
| name           = Raising Arizona
| image          = Raising-Arizona-Poster.jpg
| alt            =
| caption        = Theatrical release poster Joel Coen Ethan Coen  }}
| producer       = Ethan Coen Joel Coen  }}
| writer         = Joel Coen Ethan Coen
| starring       = Nicolas Cage Holly Hunter
| music          = Carter Burwell
| cinematography = Barry Sonnenfeld
| editing        = Michael R. Miller
| studio         = Circle Films
| distributor    = 20th Century Fox
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $6 million
| gross          = $29,180,280	
}}
 William Forsythe, blockbuster at cult status. flamboyant camera American Film 100 Years...100 Laughs list, and 45th on Bravo (US TV channel)|Bravos "100 Funniest Movies" list.

==Plot==
Criminal Herbert I. "Hi" McDunnough (Nicolas Cage) and policewoman Edwina "Ed" (Holly Hunter) meet after she takes the mugshots of the Recidivism|recidivist. With continued visits, Hi learns that Eds fiancé has left her. Hi proposes to her after his latest release from prison, and the two get married. They move into a desert mobile home, and Hi gets a job in a machine shop. They want to have children but Ed is Infertility|infertile, and they cannot adopt anyone because of His criminal record. The couple learns of the "Arizona multiple birth|Quints," sons of locally famous furniture magnate Nathan Arizona (Trey Wilson); Hi and Ed kidnap one of the five babies, whom they believe to be Nathan Junior.
 William Forsythe), wife swapping and Hi assaults him. That night, Hi decides to steal a package of diapers for the baby, but gets carried away and starts to rob the convenience store. Ed sees this and, furious, drives off without him. Hi is forced to flee on foot from the convenience store chased by police officers, two cashiers with guns, and a pack of guard dogs. Ed eventually picks him up, leading to a tense ride home.

At the McDunnough residence the next day, Glen approaches Hi to fire him, and reveals that he has deduced Juniors identity because of the newspaper article he read about Junior missing, and blackmails Hi, threatening to turn him over to the police unless Glen and Dot get custody of Junior. Gale and Evelle overhear this conversation and turn on Hi, tying him up and taking Junior for themselves. Gale and Evelle leave with plans to rob a "hayseed" bank with Junior in tow. When Ed comes home, she frees Hi and the two arm themselves and set out together to retrieve the child. En route, Ed suggests that they should end their marriage after recovering the boy. Meanwhile, Nathan Arizona Sr. is approached by the bounty hunter Leonard Smalls (Randall "Tex" Cobb) who offers to find the child for $50,000. Nathan Sr. declines the offer, believing that Smalls himself is his sons kidnapper. Smalls decides to recover the child anyway to sell on the black market. He begins tracking Gale and Evelle and learns of their bank robbery plans.
 dye canisters explodes in their loot sack, blocking the cars windows and incapacitating them. At the bank, Smalls arrives for Junior just ahead of Ed and Hi. Ed grabs the baby and flees; Hi is able to fend Smalls off for a while, but soon finds himself at Smalls mercy. As Smalls throws Hi to the ground and prepares to kill him, Hi holds up his hand to reveal that he has pulled the pin from one of the hand grenades on Smalls vest. Smalls attempts to get rid of the grenade, but his fingers get stuck on the clips and he is blown to pieces when the grenade explodes and sets off all the others.

Hi and Ed sneak Junior back into the Arizona home and are confronted by Nathan Sr. After Nathan Sr. learns why they took his son, he understands the couples predicament and decides not to call the authorities. He counsels them: when Hi and Ed say that they are splitting up, he advises them to sleep on it. Hi and Ed go to sleep in the same bed, and Hi has a dream about Gale and Evelle reforming after returning to prison; Glen gets his due from a Polish-American police officer after "telling one Polack joke too many"; and Nathan Jr. gets a football for Christmas from "a kindly couple who wish to remain unknown", later becoming a football star. The dream ends with an elderly couple together enjoying a holiday visit from a large family of children and grandchildren.

==Cast==
 
* Nicolas Cage as Herbert "H.I."/"Hi" McDunnough
* Holly Hunter as Edwina "Ed" McDunnough
* Trey Wilson as Nathan Arizona Sr.
* John Goodman as Gale Snoats William Forsythe as Evelle Snoats
* Sam McMurray as Glen
* Frances McDormand as Dot
* Randall "Tex" Cobb as Leonard Smalls (Lone Biker of the Four Horsemen of the Apocalypse|Apocalypse)
* T.J. Kuhn as Nathan Jr.
* Lynne Dumin Kitei as Florence Arizona
* Warren Keith as Younger FBI Agent
* Henry M. Kendrick as Older FBI Agent
* Charles Lew Smith as Whitey (convenience store clerk)
* M. Emmet Walsh as Machine Shop Ear-Bender
* Patrick McAreavy as Whitetail Ferguson
 
In contrast to Blood Simple, the characters of Raising Arizona were written to be very sympathetic. Coen, Coen, Allen 2006, pp. 32.  The Coens wrote the part of Ed for Holly Hunter. Coen, Coen, Allen 2006, pp. 21. 

Several babies had to be fired on-set due to them taking their first steps rather than crawling. One mother put her babys shoes on backwards to keep the baby crawling rather than walking. Coen, Coen, Allen 2006, pp. 19  The character of Leonard Smalls was created when the Coen Brothers tried to envision an "evil character" not from their imagination, but one that the character Hi would have thought up. Coen, Coen, Allen 2006, pp. 29  Randall "Tex" Cobb gave the Coens difficulty on set, with Joel noting that "hes less an actor than a force of nature ... I dont know if Id rush headlong into employing him for a future film." 

==Production==

===Conception===
The Coen Brothers started working on Raising Arizona with the idea to make it as different as possible from their previous film, Blood Simple, by having it be far more optimistic and upbeat. Coen, Coen, Allen 2006, pp. 17.  Coen, Coen, Allen 2006, pp. 18.  The starting point of scriptwriting came from the idea of the character of Hi, who has the desire to live a regular life within the boundaries of the law. Coen, Coen, Allen 2006, pp. 27.  To create their characters dialect, Joel and Ethan created a hybrid of local dialect and the assumed reading material of the characters, namely, magazines and the Bible. Coen, Coen, Allen 2006, pp. 29.  The script took three and a half months to write. Coen, Coen, Allen 2006, pp. 30 

The film was influenced by the works of director Preston Sturges and writers such as William Faulkner and Flannery OConnor, known for her southern literature. Coen, Coen, Allen 2006, pp. 23.  Coen, Coen, Allen 2006, pp. 24.  Coen, Coen, Allen 2006, pp. 26.  Joel and Ethan showed their completed script to Circle Films, which was their American distributor for Blood Simple. Circle Films agreed to finance the movie.  Coen, Coen, Allen 2006, pp. 34.  The Coens came to the set with a complete script and storyboard.  With a budget of just over five million dollars, Joel Coen noted that "to obtain maximum from that money, the movie has to be meticulously prepared." Coen, Coen, Allen 2006, pp. 28. 

===Filming===
Raising Arizona was shot in ten weeks.  The relationship between actor Nicolas Cage and the Coens was respectful, but turbulent. When he arrived on-set, and at various other points during production, Cage offered suggestions to the Coen brothers, which they ignored. Coen, Coen, Allen 2006, pp. 19.  Cage said that "Joel and Ethan have a very strong vision and Ive learned how difficult it is to accept another artists vision. They have an autocratic nature." Coen, Coen, Allen 2006, pp. 20. 

Many crew members who had worked with Joel and Ethan on Blood Simple returned for Raising Arizona, including cinematographer Barry Sonnenfeld, co-producer Mark Silverman, production designer Jane Musky, associate producer and assistant director Deborah Reinisch, and film composer Carter Burwell. Coen, Coen, Allen 2006, pp. 33  Russell 2001, pp. 27 

==Release==
The film was screened out of competition at the 1987 Cannes Film Festival.   

==Reception== David Denby New York Siskel & Ebert & the Movies, critic Gene Siskel said the film was as "good looking as it is funny" and that "despite some slow patches" he recommended the film, giving it a "thumbs up."    Writing for The New Yorker, Pauline Kael wrote that "Raising Arizona is no big deal, but it has a rambunctious charm."   

Negative reviews focused on a "style over substance" view of the film. Variety (magazine)|Variety wrote, "While   is filled with many splendid touches and plenty of yocks, it often doesnt hold together as a coherent story."    Writing for The New York Times, Vincent Canby wrote, "Like Blood Simple, its full of technical expertise but has no life of its own ... The direction is without decisive style."    Julie Salamon of the Wall Street Journal wrote that the Coen Brothers "have a lot of imagination and sense of fun&mdash;and, most of all, a terrific sense of how to manipulate imagery," but "by the end, the fun feels a little forced."  Dave Kehr of the Chicago Tribune wrote that "the overlooked form peels away from the slight, frail content, and the film starts to look like an episode of Hee Haw directed by an amphetamine-crazed Orson Welles."    Roger Ebert wrote a negative review, stating the film "stretches out every moment for more than its worth, until even the moments of inspiration seem forced. Since the basic idea of the movie is a good one and there are talented people in the cast, what we have here is a film shot down by its own forced and mannered style."   

Later writings about the film have been generally positive. Both the British film magazine Empire (magazine)|Empire and film database Allmovie gave the film five stars, their highest ratings.       Allmovies Lucia Bozzola wrote, "Complete with carefully modulated over-the-top performances from the entire cast, Raising Arizona confirmed the Coens place among the most distinctive filmmakers to emerge from the 1980s independent cinema," while Caroline Westbrook of Empire declared it a "hilarious, madcap comedy from the Coen brothers that demonstrates just why they are the kings of quirk."  The Dutch magazine Vrij Nederland placed Raising Arizonas bank robbery scene second on their list of "the 5 best bank robberies in film history," behind a bank robbery scene from the 1995 thriller Heat (1995 film)|Heat.  In 2000, the film was nominated for the American Film Institutes list of the one hundred best comedy films of the twentieth century. Nominations for the list were submitted by film directors, screenwriters, actors, editors, cinematographers, critics, historians, and film executives.  The film placed at Number 31 on the list. 

==Soundtrack==
{{Infobox album  
| Name        = Original Motion Picture Soundtracks: Raising Arizona and Blood Simple
| Type        = Soundtrack
| Artist      = Carter Burwell
| Cover       = Original Motion Picture Soundtracks - Raising Arizona and Blood Simple.gif
| Alt =
| Released    = 1987
| Recorded    =
| Genre       = Film score
| Length      =  
| Label       = Varèse Sarabande
| Producer    =
| Chronology  = Coen brothers film soundtracks
| Last album  = Blood Simple/Raising Arizona (1987)
| Next album  = Millers Crossing#Soundtrack|Millers Crossing (1990)
}} score to Raising Arizona is written by Carter Burwell, the second of his collaborations with the Coen brothers.  The sounds are a mix of Organ (music)|organ, massed choir, banjo, whistling, and yodeling.
 Ludwig van Symphony No. 9 and "Russian Folk Themes and Yodel." Credited musicians for the film include Ben Freed (banjo), Mieczyslaw Litwinski (Jews harp and guitar), and John R. Crowder (yodeling). Holly Hunter sings a traditional murder ballad, "Down in the Willow Garden," as an incongruous "lullaby" during the film. 

Selections from Burwells score to Raising Arizona were released on an album in 1987, along with selections from the Coens sole previous feature film, Blood Simple.  The tracks from Raising Arizona constitute the first ten tracks on a 17-track CD that also features selections from the Blood Simple#Soundtrack|Blood Simple soundtrack.

# "Introduction&nbsp;– A Hole in the Ground"&nbsp;– (0:38)
# "Way Out There (Main Title)"&nbsp;– (1:55)
# "He Was Horrible"&nbsp;– (1:30)
# "Just Business"&nbsp;– (1:17)
# "The Letter"&nbsp;– (2:27)
# "Hail Lenny"&nbsp;– (2:18)
# "Raising Ukeleles"&nbsp;– (3:41)
# "Dream of the Future"&nbsp;– (2:31)
# "Shopping Arizona"&nbsp;– (2:46)
# "Return to the Nursery"&nbsp;– (1:35)

AllMusic gave the album a rating of   (4.5 out of 5). 

==References==
;Bibliography
* {{cite book
 | last= Coen | first= Joel
 |author2=Ethan Coen |author3=William Rodney Allen
  | title= The Coen brothers: interviews
 |publisher= University Press of Mississippi
 |year= 2006
 |isbn= 1-57806-889-4
 |url=http://books.google.com/?id=4zO2BS4FiE4C
}}
* {{cite book
 | last= Russell | first= Carolyn R.
 | title= The films of Joel and Ethan Coen
 |publisher= McFarland
 |year= 2001
 |isbn= 0-7864-0973-8
 |url=http://books.google.com/?id=nyqHy2sEDKQC
}}
 
;Citations
 

== External links ==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 