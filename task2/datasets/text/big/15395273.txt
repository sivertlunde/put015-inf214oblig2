Jawab (1995 film)
{{Infobox film
| name           = Jawab 
| image          = Jawab95.jpg
| image_size     =
| caption        =
| director       = Ajay Kashyap|
| producer       = 
| writer         = 
| starring       = Raaj Kumar Harish Kumar Karishma Kapoor
| music          = Anu Malik
| cinematography = 
| editing        = 
| distributor    = 
| released       = 27 January 1995
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }}

Jawab is 1995 Hindi movie directed by Ajay Kashyap and starring Raaj Kumar, Harish Kumar, Karishma Kapoor.

Other cast include Prem Chopra, Mukesh Khanna, Annu Kapoor, Farida Jalal.

==Cast==

* Raaj Kumar ... Ashwani Kumar Saxena
* Karishma Kapoor ... Suman
* Harish Kumar ... Ravi A. Saxena 
* Mukesh Khanna ... Rajeshwar
* Farida Jalal ... Rajeshwars maidservant 
* Annu Kapoor ... Ladle (Rajeshwars nephew)
* Prem Chopra ... Sobhraj

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Churaya Neend Churayee"
| Udit Narayan, Kavita Krishnamurthy
|- 
| 2
| "Ek Tak Ant Thak" Poornima
|- 
| 3
| "Kal Hum Jahan Mile The"
| Kumar Sanu, Sadhana Sargam
|-
| 4
| "Duniya Se Masoom" 
| Vinod Rathod
|- 
| 5
| "Ye Dil Mein Rahanewale"
| Sadhana Sargam
|-
| 6
| "Tum Pe Dil Aa Gaya"
| Kumar Sanu, Sadhana Sargam
|- 
| 7
| "Ye Dil Mein Rahanewale"
| Kumar Sanu
|- 
| 8
| "Ye Dil Mein Rahanewale"
| Bela Sulakhe
|-
| 9
| "Ye Dil Mein Rahanewale"
| Mohammad Aziz
|}

== External links ==
*  

 
 
 


 