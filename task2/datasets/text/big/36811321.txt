Angels in Stardust
{{Infobox film
| name           = Angels in Stardust
| image          = Angels in Stardust film poster.jpg
| caption        = Theatrical release poster
| director       = William Robert Carey
| producer       = Scott Dolezal
| screenplay     = William Robert Carey
| based on       =   Billy Burke AJ Michalka
| music          = John Hunter
| cinematography = Alexandre Lehmann
| editing        = Rick Spalla Dennis Virkler
| studio         = HIP Films Inception Film Partners
| distributor    = High-Motor Productions
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
}}
 Billy Burke directorial debut, and is based on the novel Jesus in Cowboy Boots, which Carey also wrote.  It was released in theaters and on video on demand on February 21, 2014.

==Plot==
The film centers around a small-town girl (Michalka) living in a Texas community built on an abandoned drive-in movie lot, who turns to an imaginary friend, The Cowboy (Burke), for solace from her self-absorbed mother (Silverstone) and the dangerous world around her. 

==Cast==
*Alicia Silverstone as Tammy Billy Burke as The Cowboy
*AJ Michalka as Vallie Sue
*Amelia Rose Blaire as Loretta
*Jeannetta Arnette as Jacqueline Windsor
*Chandler Massey as Angelo
*Michael Spears as Tenkill
*Sierra Fisk as Principal
*Dennis Cockrum	as Mr. Sunday
*Darin Heams as Old Ray
*Tyler Riggs as Mickey
*Adam Taylor as Pleasant
*Kelly Ramel as Francine

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 