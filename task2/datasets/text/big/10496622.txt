Pistha
{{Infobox film
| name           = Pistha
| images         =        
| director       = K. S. Ravikumar
| producer       = Pyramid Natarajan
| writer         = M. A. Kennedy
| narrator       = Karthik Nagma Mansoor Ali Khan Jayashree
| music          = S. A. Rajkumar
| cinematography = Ashokrajan
| editing        = K. Thanikachalam
| studio         = Pyramid Films International
| distributor    = Pyramid Films International
| released       = 9 May 1997
| runtime        = 174mins
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Pistha is a 1997 Tamil language film directed by K.S. Ravikumar and starring Karthik Muthuraman and Nagma in the lead roles. The movie completed a 100 day run in theaters and turned out to be a profitable venture for Pyramid Natarjan.

==Story==
Manigandan is a server in a hotel who is a devotee of Lord Ayyappan. He follows a fasting for Ayyappa and is very sincere and honest. One day a businessman Dharmaraj visits the hotel where Manigandan works. He is very admired by Manis sincerity and offers him Manager job in his factory at Ooty when he lost his server job. Dharmaraj takes Mani to his factory and office to introduce him to his employees who does not do any work but play games, sit idle, watch TV etc. Innocent Mani does not understand why employees does not work but gets scared that he must manage all of them which was impossible by 419 managers before him. Mani looks at a girl who breaks everything whatever she sees and Dharmaraj pays for everything she broke. He finds that she is Vanilla daughter of Dharmaraj who is an arrongant, spoilt woman who cannot be controlled even by her father. Mani and Vanilla fights frequently and Mani succeeds without any effort. Vanilla makes her every attempt to throw him away. Vanilla visits home of her sisters lover to give money for their expenses. She finds out that she must inherit all her grandfathers property to continue to give money for them. The only way to inherit the property is to get married as per the will. She decides to marry Manigandan and attempts to win his heart. Manigandan also declares his love for vanilla but Dharmaraj objects to the love as Vanilla is very dangerous girl and she might spoil his life. But unwillingly he accepts for the marriage and feels pity for Mani. Mani and Vanilla get married. On the day of the marriage to everybodys surprise Manigandan is found to be don who is an ex jail convict. Vanilla is shocked by this and it is now Manigandans turn to overpower Vanilla and he begins his game.

The rest of the story follows with the fight between Mani and Vanilla and how Mani surpasses Vanillas arrogancy. In the end, Vasu Vikram reveals that Nagmas sisters lover is a crook who killed her and pretended to be handicapped to steal her property. In a fight ensuing, Manikandan pushes him and makes him really handicapped, Vennila forgives Manikandan for mistakes. The film ends with Manikandan turning into Ayyappan devotee again.

==Cast== Karthik as Manikandan
*Nagma as Vennila Mouli as Dharmaraj
*Manivannan
*Jayashree Sindhu
*Balu Anand
*Vasu Vikram
*Sai Arun Mansoor Ali Khan
*Idichapuli Selvaraj
*Kavignar Kalidasan as Hotel owner
*K. S. Ravikumar in a Special Appearance

==Soundtrack==
* Kozhi Curry
* Saranam Ayyappa
* Kangalile Oru Kadhal Nila
* Siru Siru
* Azhagu

==References==
 

 

 
 
 
 