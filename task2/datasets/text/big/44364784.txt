Najmia
{{Infobox film
| name           = Najmia
| image          = 
| caption        = 
| director       = Cristhian Andrews
| producer       = Ruya Koman Olha Kaly Ranjeet S. Marwa Kimberly Marie Freeman Katie Thomas
| writer         = Cristhian Andrews Ranjeet S. Marwa
| starring       = Nadya Alabsi Suhail Dabbach
| music          = Emily Rice
| cinematography = Adam Volerich
| editing        = Sophie Yaeger
| studio         = Audacity Innovative Empire Motion Pictures
| distributor    = 
| released       =  
| runtime        = 18 minutes
| country        = United Kingdom
| language       = Arabic
}}
Najmia is an upcoming British short film produced by Audacity Innovative, Empire Motion Pictures and Yaeger Productions.   It is inspired by the true story of Fawziya Abdullah Youssef, a pregnant 12-year old Yemeni child bride, who died after severe complications during childbirth in 2009.      

The film is written and directed by Cristhian Andrews   It stars Nadya Alabsi and The Hurt Lockers own Suhail Dabbach.   The film is scheduled to be released in May, 2015.



==References==
 

==External links==
*  website

 