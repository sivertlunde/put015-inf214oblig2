Horn 'Ok' Pleassss

{{Infobox film
| name           = Horn Ok Pleassss
| image          = Horn OK Pleassss Poster.jpg
| caption        = Official Poster
| director       = Rakesh Sarang
| producer       =
| writer         = Mangesh Kulkarni
| starring       = Nana Patekar Muzammil Ibrahim Sharib
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 162 minutes
| country        = India
| language       = Hindi
| budget         =
| gross          =
|}}
Horn Ok Pleassss is a Bollywood romantic comedy film directed by Rakesh Sarang. The film stars Muzammil Ibrahim and Nana Patekar in lead roles. The film was set to be released in 2009, but unfortunately the director wanted it to come out lately so the film did get released, but only in India. Worldwide except India, the film was released in Summer 2010.

== Plot ==

The protagonist of the film, Govinda (Nana Patekar), plays the central theme role. His own lifes theme works only around love. Hes a truck driver, and also a billionaire, who goes out of his way to spread and support his hobby for love. Ajay (Muzammil Ibrahim) loves Sia (Rimi Sen), the twin sister of Govindas wife Ria (also played by Rimi Sen). Govinda does not know this.
Ajays unplanned surprise is his meeting with Govinda. And him seeking his help leads to a chain of incidents.

This chain of incidents decides whether Ajay gets his lady, the Sia-Ria confusion be solved and how Govinda will be able to survive this self-created life-or-death chaos for love.

== Cast ==
*Nana Patekar as Govinda
*Rimi Sen as Ria/Siya
*Muzammil Ibrahim as Ajay Devgan Ali Asgar
*Satish Shah
*Nirmiti Sawant
* Mukesh Tiwari
*Vrajesh Hirjee
*Rakhi Sawant
*Kunika
*Shaurya Chauhan

== Music ==
 NO *  Songs                                      Singer(s)

 01 * Nathani Utaro                          Sunidhi Chauhan ,Toshi

 02 * Love Me Baby                           Shilpa Rao , Earl

 03 * Ayela Ayela                            Sudesh Bhosle, Bela Shende & Nana Patekar

 04 * Awaaj Kunacha                          Govind Cha - Nana Patekar & Jaywant Wadkar

 05 * Nathani Utaro (Remix)                  Shibu & Pinto

 06 * Oya Oya                                Shilpa Rao & Key Key

 07 * Pom Pom Pom                            Neeraj Shreedhar

 08 * Pom Pom Pom (Remix)                    Shibu & Pinto

== Lyricist ==
*Sajid
*Farhad
*Salim Bijnauri
*Srirang Godbole

== External links ==
*  

 
 
 
 
 

 
 