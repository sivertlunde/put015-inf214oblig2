Lilly Pookkal
{{Infobox film 
| name           = Lilly Pookkal
| image          =
| caption        =
| director       = TS Mohan
| producer       = Baby Rathan Lal
| writer         = PR Raveendran
| screenplay     = TS Mohan
| starring       = Jagathy Sreekumar Prameela Shobha Janardanan
| music          = Kottayam Joy
| cinematography = EN Chandru
| editing        = A Sukumaran
| studio         = Concord Movies
| distributor    = Concord Movies
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by TS Mohan and produced by Baby, Rathan and Lal. The film stars Jagathy Sreekumar, Prameela, Shobha and Janardanan in lead roles. The film had musical score by Kottayam Joy.   

==Cast==
*Jagathy Sreekumar
*Prameela
*Shobha
*Janardanan
*Kaduvakulam Antony
*Sukumaran Vincent

==Soundtrack==
The music was composed by Kottayam Joy and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Athyunnathangalil || K. J. Yesudas, Karthikeyan, Raaji || Poovachal Khader || 
|-
| 2 || Solaman padiya || K. J. Yesudas || Poovachal Khader || 
|-
| 3 || Theeyeriyunnoru hridayam || Vani Jairam || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 