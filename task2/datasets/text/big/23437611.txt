Madame Aema 9
 
{{Infobox film
| name           = Madame Aema 9
| image          = Madame Aema 9.jpg
| caption        = Theatrical poster for Madame Aema 9 (1993)
| director       = Kim Sung-su 
| producer       = Choe Chun-ji
| writer         = Song Jae-beom
| starring       = Jin Ju-hui
| music          = Gang In-hyeok
| cinematography = Seo Il-ryong
| editing        = Cho Ki-hyung
| distributor    = Yun Bang Films Co., Ltd.
| released       =  
| runtime        = 95 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name = {{Film name
 | hangul         =   9
 | hanja          =   9
 | rr             = Aemabuin 9
 | mr             = Aemapuin 9}}
}} 
Madame Aema 9 ( ) is a 1993 South Korean film directed by Kim Sung-su. It was the ninth in the Madame Aema series, the longest-running film series in Korean cinema. 

==Plot==
Aema is a bored housewife married to a successful, workaholic businessman. She begins an affair with Jean, a business associate of her husbands. Her husband suspects and begins tormenting Aema, while not letting on that he knows, to protect a business deal. Aema leaves her husband, but is persuaded to return after heeding advice from a friend.   

==Cast==
* Jin Ju-hui: Aema 
* Park Gyeol: Jean
* No Hyeon-u: Hyeon-woo
* Gang Seon-yeong: kang-hee
* Yun Bo-ra: Secretary
* Seo Ji-eun: Model
* Hong Chung-gil: Manager

==Notes==
 

==External links==

===English===
*  
*  
*  

===Korean===
*  
*  
*  
*  

 
 
 
 
 
 


 
 