The Off-Shore Pirate
{{infobox film
| title          = the Off-Shore Pirate
| image          =File:Off Shore Pirate 1921.jpg
| imagesize      =
| caption        =Film poster Dallas Fitzgerald Albert H. Kelley (assistant director)
| producer       = Metro Pictures
| writer         = Waldemar Young (scenario)
| based on        =  
| starring       = Viola Dana  Jack Mulhall
| music          =
| cinematography = John Arnold Lieutenant Joseph Waddell (additional photography)
| editing        =
| distributor    = Metro Pictures
| released       = January 31, 1921
| runtime        = 6 reels
| country        = United States Silent (English intertitles)

}}

 
The Off-Shore Pirate is a 1921 American silent  . 

==Plot==
As summarized in a film publication,    Ardita Farnam (Dana), wealthy and beautiful, had a will of her own and a yacht. When her uncle (Jobson) indicated that he wanted her to meet a certain man, she decided that she wanted to marry a foreigner, saying she wanted a man with a past rather than a future. Alone on her yacht one evening Ardita heard some jazz melodies floating over the waves. A good-looking man (Mulhall) along with six black musicians came aboard. They tell Ardita that they have been giving a charity performance that afternoon and at the end had relieved the audience of their valuables, and now intended to use the yacht to escape. They go out to sea. Later the leader, who calls himself Curtis Caryle, is put off the yacht. Ardita feels sorry for him and follows him. Then her uncle arrives. The man explains that he is Toby Moreland, the man her uncle wanted her to meet. She says that she knew it all along and decides to marry him.

==Cast==
*Viola Dana - Ardita Farnam
*Jack Mulhall - Toby Moreland
*Edward Jobson - Uncle John Farnam
*Edward Cecil - Ivan Nevkova

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 


 
 