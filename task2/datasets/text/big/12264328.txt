Lecture 21
  Italian film written and directed by Alessandro Baricco and starring John Hurt, Noah Taylor and Leonor Watling. The film is in English language|English.

The film is set in 1824, 1997 and 2007 and tells the tale of a Professor (John Hurt) delivering a lecture about a musician (Noah Taylor) coming to a remote village in the mountains to better understand Beethovens 9th symphony before he dies. The film is presented as a Documentary film|documentary, as Leonor Watling and her friends recall the lesson, the audience views what happens as though it were real. The lesson is interspersed with documentary-style interviews of a variety of musicians, who provide a secondary narrative|narration.

It also features Clive Russell, Matthew T. Reynolds, Daniel Tuite, Andy Gathergood, Rasmus Hardiker, Michael Jibson and Natalia Tena.

It had its North American premiere at the Miami International Film Festival in 2009.

==External links==
* 

 
 
 
 
 
 
 
 

 