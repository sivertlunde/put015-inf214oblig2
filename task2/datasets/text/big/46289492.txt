The Dead and the Damned
{{Infobox film
| name           = The Dead and the Damned
| image          =
| alt            =
| caption        =
| director       = Rene Perez
| producer       = Mattia Borrani
| writer         = {{plainlist|
* Rene Perez
* Barry Massoni
}}
| starring       = {{plainlist|
* David Lockhart
* Camille Montgomery
* Rick Mora
* Robert Amstler
}} 
| music          = {{plainlist|
* Mattia Borrani
* Rene Perez
}}
| cinematography = Paul Nordin
| editing        = Rene Perez
| studio         = Mattia Borrani Productions
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Dead and the Damned (also Cowboys & Zombies) is a 2010 American horror film directed by Rene Perez, written by Perez and Barry Massoni, and starring David Lockhart, Camille Montgomery, Rick Mora, and Robert Amstler.  A meteorite unleashes a zombie virus in the American Old West.

== Plot ==
During the California Gold Rush, Mortimer receives a bounty for Brother Wolf, a Native American accused of rape.  Mortimer recruits a prostitute, Rhiannon, as bait to lure out Wolf.  However, Mortimer becomes dubious of the charges once he meets Wolf.  Meanwhile, a group of prospectors unleash a zombie virus when they attempt to mine a meteorite.  Mortimer, Rhiannon, and Wolf must band together to stop the zombies.

== Cast ==
* David Lockhart as Mortimer
* Camille Montgomery as Rhiannon
* Rick Mora as Brother Wolf
* Robert Amstler as The German

== Production ==
Filming took place near Yosemite National Park.   

== Release ==
The Dead and the Damned premiered at the Another Hole In the Head film festival on July 17, 2010.      It was released on DVD in the US on July 26, 2011,  and in the UK on August 1, 2011. 

== Reception ==
Jim Harrington of the San Jose Mercury News called the plot "as ludicrous as it is fun".   Rod Lott of the Oklahoma Gazette called it "proof that bargain-basement zombie flicks, which are dime-a-dozen these days, shouldn’t be made".   Gareth Jones of Dread Central rated it 1.5/5 stars and wrote, "With too little plot and too little visual imagination to justify a feature runtime, were left with plodding scenes of predictable exposition, poorly executed action and lingering gazes at bare breasts peppered with occasional minutes of something approaching genuine entertainment."   Ben Bussey wrote that despite its amateurishness the film "remains a reasonable bit of fun so long as your expectations arent too high".   HorrorTalk rated it 2.5/5 stars and wrote, "The Dead and the Damned had a lot of potential, on both sides of the camera, and it could have succeeded with a bit more experience."   Alex DiVincenzo of HorrorNews.Net wrote, "If there’s not a gun fight or a zombie chase on screen, the movie is boring."   Writing in The Zombie Movie Encyclopedia, Volume 2, academic Peter Dendle called it "mostly a zombie shooting gallery". 

== Sequel ==
A loosely connected sequel, The Dead and the Damned 2, was released in October 2014. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 