Rabba Main Kya Karoon
{{Infobox film
| name=Rabba Main Kya Karoon
| image= Rabba Main Kya Karoon Poster.jpeg
| image_size =
| alt=
| caption=Theatrical release poster
| director=Amrit Sagar Chopra
| producer=Moti Sagar
| co-producer=Rakesh Dang
| story=
| based on=
| screenplay=
| starring=Akash Sagar Chopra Arshad Warsi Paresh Rawal Raj Babbar Shakti Kapoor Riya Sen Himani Shivpuri Anuradha Patel Tinu Anand
| Music=Salim-Sulaiman
| Lyrics=Amitabh Bhattacharya, Akash Sagar,Zaheer Anwar, Sham Balkar, Satyadev Singh
| cinematography=
| editing=
| studio=
| distributor=R.R.Films
| released=  
| runtime=
| country=India
| language=Hindi
}}
 2013 Hindi romantic comedy film directed by Amrit Sagar Chopra and produced by Moti Sagar. The film features    Akash Sagar Chopra, Arshad Warsi, Paresh Rawal, Tahira Kocchar and Riya Sen as main characters. 

==Plot==
Rabba Main Kya Karoon story   revolves around two brothers Arshad Warsi and his younger cousin brother Akash Chopra who is getting married lavishly in a typical Indian style in Delhi.

The story further  leads to a high point when Akash’s elder brother Arshad Warsi and his three mama’s played by Paresh Rawal, Tinnu Anand and Shakti Kapoor teach him the key success to a successful married life and according to them that is “In Order to live a happy married life, one must cheat on his wife!”.

==Cast==
*Akash Sagar Chopra
*Arshad Warsi
*Paresh Rawal
*Raj Babbar
*Shakti Kapoor
*Navni Parihar
* Riya Sen
* Tahira Kocchar
*Himani Shivpuri
*Anuradha Patel
*Tinu Anand

==Soundtrack==
{{Infobox album Name        = Rabba Main Kya Karoon Type        = soundtrack
}}

The official soundtrack was composed by duo Salim-Sulaiman and consists 6 tracks.

{{Track listing extra_column = Singer(s) lyrics_credits = yes total_length = 26:34 title1 = Khulla Saand extra1 = Salim Merchant lyrics1 = Amitabh Bhattacharya length1 = 03:47 title2 = Muh Meetha Kara De  extra2 = Benny Dayal & Monali Thakur lyrics2 = Amitabh Bhattacharya  length2 = 04:23 title3 = Rabba Main Kya Karoon extra3 = Benny Dayal, Raj Pandit & Vidhi Sharma lyrics3 = Amitabh Bhattacharya length3 = 04:33 title4 = Dua extra4 = Akash Sagar Chopra lyrics4 = Zaheer Anwar length4 = 05:18 title5 = Bari Barsi extra5 = Labh Janjua lyrics5 = Sham Balkar  length5 = 05:16 title6 = Brandy extra6 = Akash Sagar Chopra & Satyadev Singh lyrics6 = Akash Sagar Chopra & Satyadev Singh length6 = 03:17
}}

==References==
 

==External links==
*  
* 




 
 

 