The Rabbi's Cat
{{Infobox film
| name = The Rabbis Cat
| image = The-Rabbis-Cat-film-poster.png
| caption = Theatrical release poster
| director = Joann Sfar Antoine Delesvaux
| producer = Clément Oubrerie Antoine Delesvaux Joann Sfar
| screenplay = Joann Sfar Sandrina Jardel
| based on =  
| starring = François Morel (actor)|François Morel Hafsia Herzi Maurice Bénichou Fellag François Damiens Jean-Pierre Kalfon
| music = Olivier Daviaud
| cinematography = 
| editing = Maryline Monthieux
| studio = Autochenille Production UGC Distribution
| released =  
| runtime = 80 minutes
| country = France
| language = French, Russian
| budget = € 12.5 million
| gross =
}}
The Rabbis Cat ( ) is a 2011 French animated film directed by Joann Sfar and Antoine Delesvaux,  based on volume one, two and five of Sfars comics series with the same title. It tells the story of a cat which obtains the ability to speak after swallowing a parrot, and its owner who is a rabbi in 1920s Algeria.  The voice cast includes François Morel (actor)|François Morel, Hafsia Herzi, Maurice Bénichou, Fellag, François Damiens and Jean-Pierre Kalfon.

== Plot ==
The film takes place in the Jewish community of Algeria during the 1920s. One day a Rabbi finds that his talking parrot, which is very noisy, has been eaten by his cat and that the cat has gained the ability to speak in human tongues. However, the Rabbi finds that the cat is very rude and arrogant, so the rabbi teaches the cat about the Torah, with the cat deciding that if he is Jewish then he should receive a bar mitzvah, leading the two to consult with the rabbis rabbi. The cat proceeds to mock and insult the rabbis rabbis strict views, who declares that the cat should be killed for its heresy. The rabbi takes his cat and leaves, mad at the cat for making a fool of his master, but the two eventually reach an agreement where the rabbi will teach the cat all about the Torah, and that someday he might have his bar mitzvah when he is ready.

Later the cat helps the rabbi pass a French language exam so that he might one day become head rabbi, but after the exam the cat loses his ability to speak (as he invoked the name of God to pray for his master). After the discovery of a Russian Jew who has stowed away in a shipment of religious texts, the cat learns he can speak Russian, and eventually regains his ability to speak French, finding he knows many languages and can act as a translator. The Russian reveals that he is a painter who has fled from Soviet Russia. He has come to Africa to search for a hidden city of black Jews deep in the heart of Africa, and convinces the rabbi, his cat, and a former Russian soldier to help him in his search.

==Cast==
* François Morel (actor)|François Morel as the cat
* Maurice Bénichou as the rabbi
* Hafsia Herzi as Zlabya
* Jean-Pierre Kalfon as Malka of the lions
* Fellag as sheik Mohammed Sfar
* Sava Lolov as the Russian painter
* Daniel Cohen as the rabbis master
* François Damiens as the Belgian reporter
* Wojciech Pszoniak as Vastenov
* Marguerite Abouet as the African girl
* Eric Elmosnino as professor Soliman
* Alice Houri as Knidelette
* Mathieu Amalric as the prince

==Production==
Autochenille Production was launched in 2007 by Joann Sfar, Antoine Delesvaux and Clément Oubrerie with the aim to make "author-driven, challenging films to appeal to children and adults."    The Rabbis Cat was the companys first project. The production was made in collaboration with TF1 and France 3. It was pre-bought by Canal+ and CineCinéma and had a budget of 12.5 million euro. It is the second film directed by Sfar, after Gainsbourg (Vie héroïque). 

One of the directors sources of inspiration was American animation from the 1930s, and in particular from the Fleischer Studios, which Sfar described as characterised by multi-ethnical production crews and for portraying also dark aspects of society, in cartoons such as Betty Boop and Popeye. In order to generate more personality for the drawn characters in The Rabbis Cat, some of the scenes were staged in a Parisian suburb loft in the summer of 2008, with props and the cast fully costumed. As the actors performed and invented their characters personal motion habits, the design team observed closely and drew what they picked up. 

The original soundtrack was composed by Olivier Daviaud, who also had composed the music for Gainsbourg (Vie héroïque), and was performed by Enrico Macias and the Amsterdam Klezmer Band. 

==Release== UGC Distribution, which launched it in 243 prints.  It competed at the 2011 Annecy International Animated Film Festival, where it won the top award, the Annecy Crystal for best feature. 

===Critical response===
Pierre Vavasseur of  , Sfar and his cat dont stroke anyone with the hair, but throw as chefs together a broth of cultures with multiple flavours and with nurtured scenery."  Jacques Mandelbaum wrote a negative review in   and The Adventures of Tintin, family chronicle and adventure film, biblical legend and colonial chronicle, historical reconstruction and winks to the contemporary world, the references are superimposed without achieving harmony. In the end, this plea for tolerance is a moral preaching so annoyingly gentle that it struggles to convince us of its legitimacy."  Jordan Mintzer wrote in The Hollywood Reporter: "Though this gorgeously animated affair showcases the artists freewheeling style and colorful arabesque imagery, its rambling episodic structure is not quite the cats meow, even if it remains a thoroughly enjoyable take on Judaism in early 20th century North Africa. ... While the end result is somewhat chaotic, it proves that Sfar can make the jump from page to screen in ways that are both compelling and personal." 

===Awards=== Best Animated Film at the César Awards, France’s equivalent of the Oscars.   
 Best Animated Feature. 

The film won the Prix Jacques Prévert du Scénario for Best Adaptation in 2012.

==See also==
* Cinema of France
* History of French animation
* History of the Jews in Algeria

==References==
 

==External links==
*    
*   at the official website of the comic books
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 