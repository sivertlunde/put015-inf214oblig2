Johnny Stecchino
  
{{Infobox film name        = Johnny Stecchino image       = Johnny_Stecchino.jpg caption     = Film cover director    = Roberto Benigni writer      = Roberto Benigni, Vincenzo Cerami starring    = Roberto Benigni, Nicoletta Braschi producer    = Mario Cecchi Gori, Vittorio Cecchi Gori cinematography = Giuseppe Lanci music = Evan Lurie released    =   24 October 1991   14 September 1992   9 October 1992 runtime  = 122 minutes language = Italian country = Italy
|}}
 Italian comedy film directed by and starring Roberto Benigni.  Sicilian vacation traps him in the middle of a Mafia plot involving a notorious mob informant, the titular Johnny Stecchino.
The film also stars Nicoletta Braschi and Paolo Bonacelli.

== Plot ==

Dante (Roberto Benigni) is a hapless man who drives a school bus for a special school, while earning money through engaging in insurance fraud by claiming an accident gave him an involuntary spasm. Although far from perfect, Dante does respect the children in the special school, and is friends with Lilo, one student whom he allows to use his apartment while he is away. One night, he accidentally meets Maria (Nicoletta Braschi), an attractive and wealthy woman from Palermo, Sicily with whom he immediately falls in love. Soon Dante is lured by Maria into unwittingly impersonating famous mobster Johnny Stecchino, Marias husband, with whom he shares an uncanny resemblance. Meanwhile, the real Stecchino (also played by Benigni) is forced to hide in his home after having testified against several of the local Mafia leaders. Marias plan is to have Dante mistaken for Johnny, publicly murdered by the Mafia, and buried in place of her husband, after which they can both escape to Argentina. However, Dante causes a turn of events both through mistaken identity and corrupt government officials endorsing his actions, as they believe he is Johnny. Meanwhile, Maria starts to doubt her plot, particularly when she sees Dante is nothing like Johnny, and when she is jeered at an opera and Dante shouts down all the hecklers to respect her. She also disagrees with Johnnys reason for turning states evidence; he killed Cozzamara (the local don)s wife in a drunk driving accident and is looking to escape the dons wrath, not out of repentance for his criminal career. On the day of the planned hit, Maria sends Dante to a barber shop owned by Cozzamara. Cozzamaras men see something is not right when they notice there is no facial mole, and when they ask "Johnny" about his mother, Dante replies "She is OK" (the real Johnny gets emotional about that; respecting his late mother more than his wife). The real Johnny, who is in the mens room of a nearby café, is poised for death and reluctantly accepts his doom, seeing it as a chance to end his miserable life. Meanwhile, the other mobsters are laughing uproariously at Dantes jokes and proceed to sing a song that had earlier been sung by the kids on Dantes school bus. 
The film ends with Maria returning Dante to his apartment, now referring to his proper name of Dante, and starting a relationship. Dante tells Lilo of his time in Palermo, considering the experience as wacky customs of the locals.

== Cast ==
* Roberto Benigni: Dante / Johnny Stecchino
* Nicoletta Braschi: Maria
* Paolo Bonacelli: the lawyer / the "uncle"
* Franco Volpi:  minister 
* Ivano Marescotti: Dr. Randazzo
* Turi Scalia: Judge Bernardino Cataratta
* Ignazio Pappalardo: Cozzamara
* Loredana Romito: Gianna
* Tony Sperandeo: mafioso Salvatore Borghese: Ignazio

== External links ==

* 

 

 
 
 
 
 
 
 


 
 