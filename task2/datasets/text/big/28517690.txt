Give Us Tomorrow
Give Us Tomorrow is a 1978 British crime film directed by Donovan Winter and starring Sylvia Syms, Derren Nesbitt and James Kerry. 

==Plot==
A master criminal takes a bank managers family hostage to help him rob the bank.

==Cast==
* Sylvia Syms ...  Wendy
* Derren Nesbitt ...  Ron
* James Kerry ...  Martin
* Derek Anders ...  Police Inspector
* Mark Elwes ...  Assistant Manager
* Donna Evans ...  Nicola Hammond
* Gene Foad ...  Bank Clerk
* Alan Guy ...  The Boy
* Richard Shaw ...  1st Bank Robber Derek Ware ...  2nd Bank Robber
* Victor Brooks ... Superintendent Ogilvie
* Matthew Hanslett ... Jamie Hammond Ken Barker ... Police Sergeant Wilson
* Chris Holroyd ... P.C. McLaren Carol Shaw ... girl driver
* Lolly Cockrell ... Reporter William Parker ... Reporter
* Gil Sutherland ... Reporter

==References==
 

==External links==
* 

 
 
 
 
 



 
 