Gold (1932 film)
{{Infobox film
| name           = Gold
| image          =
| caption        =
| director       = Otto Brower
| producer       = Henry L. Goldstone
| writer         = Scott Darling (continuity) Jack Natteford (story)
| narrator       =
| starring       = Jack Hoxie, Hooper Atchley, Alice Day
| music          = Lee Zahler Arthur Reed
| editing        = S. Roy Luby
| distributor    =
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 B western, the film starred Jack Hoxie in the second of his six sound westerns, featuring Hooper Atchley as the villain Kramer.  The film also marked the last screen appearance of silent movie actress Alice Day.

==Plot==
Kramer (Atchley) works the gold fields by buying up miners claims and then having his henchmen murder them, taking both the money and the gold.  When cowboy-turned-prospector Jack Tarrants (Hoxie) partner Jeff Sellers becomes the next victim to Kramers scam, Tarrant decides to put an end to Kramers gang once and for all.

==Cast==
*Jack Hoxie as Jack Tarrant
*Alice Day as Marion Sellers
*Hooper Atchley as Kramer
*Matthew Betz as Henchman Childress
*Lafe McKee as Jeff Sellers
*Jack Clifford as Elmer Sigmuller
*Tom London as Sheriff
*Bob Kortman as Henchman Without Mustache
*Jack Byron as Henchman With Mustache
*Dynamite the Horse as Dynamite - Jacks new horse

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 