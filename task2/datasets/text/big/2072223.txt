Mezame No Hakobune
 

   Open Your Mind 
  | image = Open-your-mind-dvd-jp.png
  | caption = Mezame no hakobune (DVD) Hiroyuki Hayashi
  | writer = Kenji Kawai  (song) 
  | producer = Atsushi Kubo Masuo Ueda Tomonori Ochikoshi
  | music = Kenji Kawai
  | photography =
  | editing =
  | distributor = Deiz, Aniplex
  | released = March 25, 2005  pavilion Pt.1  April, 2005  DVD special edition  May 25, 2005   pavilion Pt.2  July 25, 2005  pavilion Pt.3  August 24, 2005  DVD complete edition 
  | runtime = 42 minutes Japanese
  | budget =
  | preceded_by =
  | followed_by =
  }}
 musical drama Japanese filmmaker Mamoru Oshii.

An   souvenir shop until it was re-released at the end of the expo as a Complete Edition which included a "Making of" bonus DVD and a color booklet.    

==Concept==
 

==Pavilion version==

===Theme theater=== World Exposition Aichi Japan") 10 minutes episode released bimonthly. 

The theater was designed as a shrine with each side of the floor screens surrounded by four rows of full sized standing Ku-Nu clones. Above the 139 dog-headed generals hovered the Pan doll appearing in the movie. The goddess was surrounded by silk curtains and the doll footage featured in the Intermission: Pan scene was shot in the theater. 

Viewers were divided in two groups with 30 tickets available for the floor (aka "arena",  ) while the remaining 170 viewers had to go to the second level (known as the "slope"  ). 

===Video===
The visual part consisted of a two level multidisplay. The first part was a floor panel linking ninety-six 50" plasma screens. It was suitable for 1:33 video which could be individually displayed by each monitor or as a synchronized 10 m x 9 m large screen. It was made of a horizontal row of 8 monitors and a vertical row of 12 monitors, which gave a 90m² floor screen and was the worlds largest. 

The second part of the facility was a panoramic 3-widescreen 180° multidisplay broadcasting 16:9 footages.  Pictures and light effects were also projected on the ceiling, made from eight silk curtains, or the 5 meter acrylic egg-shapped ( , tamago) screen.

===Audio=== Noh polyphonic Ghost In Zen music with the soundtracks natural audio effects.  

==Special Edition==

===Home cinema===
Due to the technical limitations of video formats the original 360°, bipolar, multidisplay system was reduced to a straight and flat projection which reduces the three dimensional experience and immersion felt in the pavilion version.

===Video===
The Special Edition allows a better visibility of the overall film which was reduced in the pavilion due to viewers walking on the floor screen monitor and slope viewers standing in front of the wall widescreens.  A benefit of the single screen projection is the removal of the mosaic effect, on the main screen, due to the PNP (Plasma Network System) architecture.

===Audio===
The soundtrack of the OVA editions is available in both stereo and Dolby Digital|DD5.1 multichannel versions — the latter being more faithful to the 6.1 channel surround sound original format.

==Overview==

===Musical drama===
The tale is of mixed genres, from Science-Fiction to Ecology and through Mythology to Fantasy. It is a three act musical drama concept with a prologue and epilogue, both named intermission, by Kenji Kawai who is the composer of the score. The characters introduction and drama is narrated through the lyrics of the opera-like Noh chanted recitation, which is self referred to as utai ( ) within the movie.

===Plot===
The plot of Open Your Mind follows the extra–terrestrial origin of life coming from outerspace as six deities (Intermission act), evolving into water (Sho-ho) who then emerge into the air (Hyakkin) and to the ground (Ku-nu). Each of these creatures rules one of the six elements of the Five elements (Japanese philosophy)|godai philosophy — Earth, Water, Fire, Wind (referred to as kaze in the Hyakkin chant), Sky and Consciousness (referred to as "Awakening" in the movies title).

===Characters===
*Pan ( , "Pantheism")
:The three headed goddess sending the generals to Earth. Her six arms physical aspect remembers Benzaiten, Japanese pantheism goddess of music, arts, sciences, wisdom and a protective figure.
*The 6 Riki-Shou ( , "Generals")  
**Sho-Ho ( )
**Hyakkin ( , "Hundred-Bird")
**Ku-Nu ( , "Dog Being")
**Batoh
**Ryo-Ketsu
**Kon-Goh Chimeras ( , kimera)

===Possible interpretations===
Open Your Mind starts as a kind of space journey and exploration of a foreign planet by an advanced civilization. The time is unexplained: it could be an offworld intelligence form visiting our planet, or it could be future human travellers visiting a remote galaxy.
 .]]
Also it could be a fabricated dream, à la   and The Red Spectacles. The pavilion concepts name "Mountain of Dreams" is probably not a coincidence.
 highly menaced, biosphere and ecosystem before it is too late. This film was presented at the World Exposition hence its universal message and collective matter.

The original title "Ark of Awakening" (Mezame no hakobune), is a double reference to the  .

==Story==

===Opening===
This introduction begins with the Buddhist representation of the cosmos through mandala shaped cogwheels emitting a mechanical clockwork sound. The following sequence is set in space with a liquid planet soon entered by six shining globes (the Riki-Shou sent by Pan) moving at fast speed and coming from each side of the screen.

===Intermission: Riki-Shou (prologue)===
This first live-action scene is set in a foggy forest (which is also an Element in the Five elements (Chinese philosophy)|gogyō philosophy), inhabited by a group of three mysterious anthropomorph creatures. They are standing still, silent, glowing in their shining white togas, they look like some kind of anthropomorph gods of Buddhist mythology. They are the Riki-Shou, the animal headed generals, sent by Pan. The creature standing in front of the trio is Ku-Nu, the dog face Riki-Shou, on his right is Hyakkin, the eagle faced, while on his left stands Sho-Ho, the fish head general.  The action is seen in first person view through the eyes of an untold character which makes the viewer the main character of this story. After a while the viewer leaves the woods and the following acts will introduce the Riki-Shou, one after the other, starting with Sho-Ho. This forest scene will follow and end in the last part of the movie which is set after the Ku-Nu act.

This unnarrated introduction is wrapped with sounds of nature and Buddhist music that is in homage to Kawais previous score on the prologue of Oshiis   which takes place in a Cambodian forest and features a giant Buddha statue.

===Act I: Sho-Ho ( )===
(Sho-ho: Mizu no kioku, lit. "Sho-ho: Remembrance of water" 
Open Your Mind episode available from March 25 until May 24, 2005.
 

===Act II: Hyakkin ( )===
( , Hyakkin: toki o retaru, lit. "Hundred-Bird: Across Time") 
Open Your Mind episode available from May 25 until July 24, 2005.

====Like a video game==== shooting game Virtua Fighter. In 2001 he envisioned a "living by the game concept" in the live-action film Avalon (2001 film)|Avalon.
 

===Act III: Ku-Nu ( )===
lit. "Dog Being: Remembrance of Seedling" 
Open Your Mind episode available from July 25 until September 25, 2005.

====Answers==== create new hybrid beings; the anthropomorph animals.

====Controversy====
  separates the humans from the animals.]]
Ku-nu features a controversial sequence morphing human newborns with puppies. The shock comes less from the creation of a human-dog hybrid race through genetical modifications than from the explicit visual presentation of human and dog newborns as equals through morphing.

For both ethic, philosophical and religious concerns human genetic manipulations such as cloning are forbidden and a touchy subject, or sometimes a taboo matter, in many countries in Europe and North America while not so in other parts of the globe.
 animal rights established hierarchy military warfare such as the Anti-tank dogs|mine-dogs (hundminen). On other hand, according to modern philosophers, the psychoanalyst Françoise Dolto and some psychologists such as Didier Pleux the baby figure has become the only remaining holy figure of agnostic societies which sums up the concept of "lEnfant-Roi" or "enfant Roi" (French for "Boy King" literally "Child King").  The 20th century saw the rise and crowning of the Child King which became apparent through various protection measures such as the abolition of child work, the creation of various child care and protection associations, and even the international Convention on the Rights of the Child created in 1989 and applied the following year.

In 1995 Mamoru Oshii invited viewers of Ghost in the Shell to open their mind and think about a new social and legal status for autonomous, evolved Artificial Intelligence creatures. Now, ten years later, his Ark of Awakening work evokes a similar reflection about genetically modified beings and by extension to the status of animals.

===Intermission: Pan (epilogue)===
This second filmed part shows the theater as the silk curtains opens to reveal Pan. The last scene is a return to the now deserted forest.

==Releases==
The pavilion included an Open Your Mind souvenir shop selling figures, books, keyholders, pamphlets, posters, soundtrack CD and the limited premium DVD "Special Edition". 

===Book===
*  2005.04.DD:  : Gem in the Pan, art book
:Mamoru Oshii (text) / Tetsuya Nishio (illus.), Be-next (15.5cm, 32p., color) ISBN 4-906069-39-8

===Audio===
*  2005.08.24:   Open Your Mind: Original Soundtrack, OST CD
:Kenji Kawai, (SVWC7283) Aniplex / Sony Music Dist.

===Video===
*  2005.04.DD:   Open Your Mind: Special Edition, 1DVD (limited)
:(ANZB1195) Aniplex
*  2005.08.24:   Open Your Mind: Complete Edition, 2DVD
:(ANSB1061~2) Aniplex / Sony Pictures Ent. (110 min. making of bonus disc, color booklet)

===Toy===
*  2005.04.DD:  ) ("Riki-Shou figure: Sho-Ho, Hyakkin, Ku-Nu"), 1/12 polystone painted figure (22&nbsp;cm)
:Poppy (Namco Bandai|Bandai-Namco Group)

==Quotes==
*Mamoru Oshii, about his upcoming work (September 2004)

"I decided to take this offer because I wanted to work on something other than films.I am waiting to get the final products for display, the images to show on the monitors from the vendors....Everything should be completed no later than the end of this year....I created the concept of the entire project and I supervised all the designs. I did basically the same thing that a director would do to make a film." 

==Staff==
*Direction supervision: Mamoru Oshii ( )
*Producer: Atsushi Kubo ( ), Masuo Ueda, Tomonori Ochikoshi
*Direction: Hiroyuki Hayashi ( )
*Music: Kenji Kawai ( )
*Arts: Toshihiro Isomi ( )
*Technical director：Yasuhiro Yamaguchi ( )
*Character design: Jun Suemi ( )
*Ceiling iron modelling work:  
*Ceiling doll (Pan):  
*Floor dolls (Riki-Shou):  
*Egg shape screen production:  
*Silk curtain installation work:  
*Project manager:  
*Technical producer:  
*Sound & visual engineer:  
*Lighting director:  
*CG producer:  
*Acoustic supervision: Kazuhiro Wakabayashi ( )
*Sound supervisor:  
*Production manager:  
*Coordination production: Dentsuu Tech
*Distribution: Deiz
*Theme zone exhibitors: Sekisui House, Chubu Nippon Broadcasting, Tokai Television Broadcasting, The Chunichi Shimbun

==References==
 

==External links==
*    
*    
*    
*    
*    
*    
*    
*    
*    
*    

 
 
 
 
 
 
 
 
 