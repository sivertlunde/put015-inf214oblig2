No One Killed Jessica
 
 
{{Infobox film
| name           = No One Killed Jessica
| image          = No One Killed Jessica Movie.jpg
| caption        = Theatrical release poster
| director       = Raj Kumar Gupta
| producer       = Ronnie Screwvala
| writer         = Raj Kumar Gupta
| starring       = Rani Mukerji Vidya Balan
| music          = Amit Trivedi
| cinematography = Anay Goswamy
| based on = Murder of Jessica Lal incident
| narrator = Rani Mukerji
| editing        = Aarti Bajaj
| distributor    = UTV Spotboy
| released       =  
| runtime        = 136 minutes 
| country        = India
| language       = Hindi / English
| budget         =    . Business Standard. 
| gross          =     
}} drama thriller UTV Spotboy Jessica Lal murder case.

The score was composed by Amit Trivedi, who worked on the soundtrack for Dev D (2009).   The character of Jessica’s older sister, Sabrina Lal, is played by Vidya Balan, and the character of the news reporter following the story is played by Rani Mukerji.  Upon release, the film garnered positive critical reviews and enjoyed a successful outing at the box office. 

No One Killed Jessica received four nominations at the 57th Filmfare Awards, including the Filmfare Award for Best Film and the Filmfare Award for Best Actress; it won the Filmfare Award for Best Supporting Actress for Rani Mukerjis performance.

==Cast==
* Rani Mukerji – Meera Gaity
* Vidya Balan – Sabrina Lall
* Myra Karn – Jessica Lall
* Neil Bhoopalam – Vikram Jai Singh Rajesh Sharma – N. K. (police officer investigating the case)
* Shireesh Sharma – Pramod Bharadwaj
* Mohammed Zeeshan Ayyub – Manish Bharadwaj
* Ashu Sharma – Lucky Gill
* Bubbles Sabharwal – Mallika Sehgal
* Samara Chopra – Naina Sehgal
* Yogendra Tikku – Sanjit Lall (Jessicas father)
* Geeta Sudan – June Lall (Jessicas mother)
* Satyadeep Mishra – Meeras boss
* Anuj Tikku – The man in plane with Meera
* Sushil Dahiya – R.D.Rastogi
* Jagat Rawat - Dharam Saxena
* Avijit Dutt - B.M.Pandit

==Production== Jessica Lal murder case. The director explained that the title was inspired by a 2006 headline  of a news story that ran when the accused killer in an infamous murder case was acquitted by a trial court. The events and the subsequent media coverage sparked nationwide protests, resulting in an appeal and re-opening of the murder case and the subsequent sentencing of the killer.  

==Themes==
 
No One Killed Jessica is based on the true story of Jessica Lal, a Delhi-based model and restaurant worker who was shot in 1999 at a New Delhi restaurant by Siddharth Vashisht, better known as Manu Sharma. The killer was the son of the wealthy, influential and intimidating Haryana politician, Venod Sharma, a close aide of the chief minister of Haryana, Bhupinder Singh Hooda.

Jessica, along with actor and model Shayan Munshi, were closing the bar at the restaurant where they worked. They were approached by Manu and his two friends. Infuriated by Jessicas refusal to serve them drinks after the bar closed, Manu fatally shot Jessica. The subsequent investigation and trial were stymied by Manus family, who did not hesitate to use their political, financial, and law enforcement connections to intimidate, coerce, and bribe the majority of the witnesses to the crime. The case then became an uneven battle between Jessica’s family (especially her sister Sabrina) and Manus high-powered family and their associates in the local government.

In February 2006, the court acquitted Manu and his friends, citing a lack of sufficient evidence. This set off a public outcry and widespread protests. The backlash forced the re-opening of the investigation, which was placed on a judicial fast track. In December 2006, Manu was found guilty and sentenced to life imprisonment.

==Release==
The film was released on 7 January 2011. It received positive reviews from critics  and was also well received by the viewing public.  No One Killed Jessica recovered its cost even before release through associated satellite and music rights. 

==Reception==

===Critical response===
No One Killed Jessica received mostly positive reviews from critics.   of CNN-IBN gave it two and a half out of five stars and wrote, "With the exception of a few powerful scenes that leave you with a genuine lump in your throat, Gupta goes for full-on melodrama that doesnt always ring true."  Mayank Shekhar of the Hindustan Times gave it three out of five stars, noting: "Can a feature film, in a couple of hours flat, ever detail an entire truth about anything? Possibly not. But it can entertainingly dramatise it. That’s what the fine director-writer and a consummate raconteur here (Rajkumar Gupta) manages to do."   

Bollywood actor Amitabh Bachchan described No One Killed Jessica as a "pertinent, media friendly and brave effort by the makers. The performances living up to the title and story of the film." 

==Awards==
;Filmfare Awards
*Best Supporting Actor (Female) - Rani Mukerji
;BIG Star Entertainment Awards
*Most Entertaining Social Film
*Best Actress in a Social Role – Rani Mukerji
;Dadasaheb Phalke Academy Awards
*Outstanding Performance Award – Rani Mukerji 
;Anandalok Purushkar
*Best Female Actor of Bollywood – Rani Mukerji 
;Apsara Awards
*Best Editing

==Soundtrack==
{{Infobox album
| Name       = No One Killed Jessica
| Type       = Soundtrack
| Artist     = Amit Trivedi
| Cover      = No-One-Killed-Jessica-Cover.jpg
| Released   = 1 December 2010
| Recorded   = Saregama
| Genres     =
| Label      = Saregama
| Reviews    =
}}
The films score was composed by Amit Trivedi, who also worked with Gupta on Aamir (film)|Aamir. The lyrics were written by Amitabh Bhattacharya. The album received generally positive reviews from critics.   

===Track listing===
{{Track listing
| lyrics_credits  = no
| extra_column = Singer(s)
| title1 = Aitbaar
| extra1 = Vishal Dadlani, Mame Khan, Robert Omulo Bob
| length1 = 4:41
| title2 = Yeh Pal
| extra2 = Shilpa Rao
| length2 = 6:00
| title3 = Aali Re
| extra3 = Tochi Raina, Bonnie Chakraborty, Shriram Iyer, Raja Hassan, Anushka Manchanda, Sonu Kakkar, Aditi Singh Sharma, Sonika Sharma
| length4 = 4:54
| title4 = Dilli
| extra4 = Tochi Raina, Shriram Iyer, Aditi Singh Sharma
| length4 = 3:52
| title5 = Dua
| extra5 = Meenal Jain, Joi Barua, Raman Mahadevan, Amitabh Bhattacharya
| length5 = 5:56
| title6 = Dilli – Hardcore
| extra6 = Tochi Raina, Shriram Iyer, Aditi Singh Sharma
| length6 = 3:51
}}

==Controversy== Dilip Bandopadhyay, was prevented from entering his own campus. Bandopadhyay argued with Bouncer (doorman)|bouncers, on guard outside the main gate, to let him enter while the cameras rolled, allegedly without his permission.  As a result, the university terminated the contract of a faculty member accused of organising the film shoot on campus. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 