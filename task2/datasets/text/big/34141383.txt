Buddy of the Legion
{{Infobox Hollywood cartoon
| series = Looney Tunes (Buddy (Looney Tunes)|Buddy)
| image =
| caption =
| director = Ben Hardaway
| story_artist =
| animator = Bob Clampett Charles M. Jones|Chas. Jones Jack Carr
| musician = Bernard Brown
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = April 6, 1935 (USA)
| color_process = Black-and-white
| runtime = 7 minutes
| movie_language = English
| preceded_by = Buddys Pony Express (1935)
| followed_by = Buddys Lost World (1935)
}}
 animated short film, released April 6, 1935.  It is a Looney Tunes cartoon, featuring Buddy (Looney Tunes)|Buddy, the second star of the series. It was directed by Ben Hardaway; Bernard Brown was musical director. Notably, this is Chuck Joness first credit as animator on a Warner Bros. cartoon.

==Summary== Foreign Legion, daydreams about Amazons, such as the book describes.

Our Hero is now the leader of a detachment of the Legion, marching through the  , sees the legionnaires, and announces their coming to the leader: the leader calls for an attractive genie woman, who travels, by magic carpet, into the path of the Legion.

Dancing seductively, the genie eventually manages to turn the heads of all the soldiers, save trumpeter Buddy; all are led away to their enslavement. Buddy faces about, ordering the company to halt: seeing that his men are gone, Buddy runs off into the distance. The soldiers, in their trance, are brought into the Amazon fortress. As each man enters, the Amazon leader knocks him aside (one legionnaire, wearing  . Then, another Amazon emerges from the same well, shaking Buddy from behind; Buddy awakens, and we find that his new adversary is none other than his boss, returned to her shop. Dragged by the collar, Buddy is, unceremoniously but obligingly, ejected from the store: "Okey-dokey!" he calls back.

==Joe Penner==
This marks the second time that a Buddy cartoon sports a reference to  .

==Use of thorn (Þ)== definite article "Ye," phonetically identical to "The"; the "Y" is a convenient replacement for the originally Old English glyph Þ, called "thorn." In Buddy of the Legion, the article is found in the name of the bookstore: "Ye Olde Book Shoppe."

==References==
 

==External links==
*  

 

 
 
 
 
 