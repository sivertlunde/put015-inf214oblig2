Success Is the Best Revenge
{{Infobox film
| name           = Success Is the Best Revenge
| image          =
| caption        =
| director       = Jerzy Skolimowski
| producer       = Jerzy Skolimowski
| writer         = Jerzy Skolimowski Michael Lyndon Michel Ciment Harriet Pacaud Barbara Elster Michael York
| music          = Stanley Myers Hans Zimmer
| cinematography = Mike Fash
| editing        = Barrie Vince
| distributor    =
| released       =  
| runtime        = 91 minutes
| country        = France United Kingdom
| language       = English
| budget         =
}}

Success Is the Best Revenge ( ) is a 1984 French-British drama film directed by Jerzy Skolimowski. It was entered into the 1984 Cannes Film Festival.   

==Cast== Michael York as Alex Rodak
* Joanna Szczerbic as Alicia Rodak
* Michael Lyndon as Adam Rodak
* Jerry Skol as Tony Rodak
* Michel Piccoli as French Official
* Anouk Aimée as Monique des Fontaines
* John Hurt as Dino Montecurva
* Ric Young as Chinese Waiter
* Claude Le Saché as Monsieur Conio
* Malcolm Sinclair as Assistant Stage Manager
* Hilary Drake as Stage Manager
* Jane Asher as Bank Manager
* Adam French as Martin Sam Smart as Mallett

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 