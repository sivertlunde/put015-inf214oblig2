All'ombra di una colt
{{Infobox film
| name           =Allombra di una colt
| image          = Allombra di una colt.jpg
| image size     =
| caption        =
| director       = Giovanni Grimaldi
| producer       =  Vincenzo Genesi 
| writer     = Giovanni Grimaldi   Maria del Carmen Martinez Roman  Aldo Barni
| narrator       =
| starring       = Stephen Forsyth
| music          = Nico Fidenco
| song           = "Finche il mondo Sara sung by I Cantori Moderni 
| cinematography =Stelvio Massi   Julio Ortas 
| editor       = Alfonso Santacana
| distributor    =
| released       =  
| runtime        = 82 minutes
| country        = Italy Italian
| budget         =
}}
 1965 Italy|Italian spaghetti western film directed and written by Giovanni Grimaldi. 

==Story==
Two professional gunfighters separate after they complete a job in a small Mexican town where they rid it of the Ramirez gang. Steve married Dukes daughter and tries to settle down to a peaceful life on a ranch outside a town controlled by Jackson and Burns.

==Cast==
* Stephen Forsyth - Steve Blaine
* Conrado San Martín -  Duke Buchanan
* Anna Maria Polani -  Susan Buchanan
* Helga Liné -  Fabienne
* Franco Ressel - Harold Jackson
* Virigilio Gazzolo -  Buck
* Aldo Sambrell -  Ramirez
* José Calvo - Sheriff
* Andrea Scotti -  Oliver
* Gino Cassani - Jim
* Franco Lauteri - Burns
* Tito García - Bartender
* José Rosello
* Xan das Bolas
* Javier de Rivera 
* Rafael Albaicín
* Hugo Blanco
* Sancho Gracia
* Hugo Ricardo Alvaro de Luna

==References==
 

== External links ==
*  

 
 
 
 
 


 
 