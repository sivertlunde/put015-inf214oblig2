Umut (film)
{{Multiple issues|
 
 
 
}}

 

{{Infobox film
| name           = Umut
| image          =
| image_size     =
| caption        =
| director       = Yılmaz Güney
| producer       = Güney Film
| writer         =
| narrator       =
| starring       = Yılmaz Güney  Tuncel Kurtiz  Osman Alyanak   Enver Dönmez
| music          =
| cinematography =
| editing        =
| studio         =Güney Film
| distributor    = Filmpot
| released       =
| runtime        =
| country        = Turkey
| language       = Turkish
| budget         =
| preceded_by    =
| followed_by    =
}}

Hope ( ) is a 1970 Turkish drama film, written and directed by Yılmaz Güney and Şerif Gören, featuring Güney as an illiterate horse cab driver, who, after losing one of his horses in an accident, sets out into the desert in a quest for a mythical lost treasure. The film, which wasnt released at the time because of a ban by Film Control Commission in Turkey, won awards at the 2nd Adana Golden Boll Film Festival, the 7th Antalya Golden Orange Film Festival and was screened at the 23rd Cannes Film Festival.

==Overview==

Filming of Umut began in April 1970 in Çukurova, Turkey following the directors return from military service. Güney wanted Umut to be a film showing the defects and contradictions of a reality without any actualization of a revolution and the illness of the socio-economic system of its time.
 Oscar prize in 1950 and is an important example of the Italian neo-realismo stream.

==Plot ==
  phaeton with two exhausted, half-dead horses. The family tries to survive in a damp and dingy living-quarters. Cabbar does not have a good run of business. He is indebted almost to everyone. His single hope is in the lottery tickets, which he continuously buys. He has bound his hope to these pieces of paper.

Life is, on the other hand, getting even harder day by day. A Mercedes hits the horse, while it stands parking alongside sidewalk. In this traffic accident his horse dies. Cabbar is innocent but weak as well. This accident makes him see that the established order is accorded to the “poor and strong”. Even the police finds him guilty.

Henceforth he sells the few sticks, which he owns only. He scratches together some money to buy a new horse. In the meantime, the creditors sell the phaeton and the remaining horse, and shared the money according to the debts of Cabber between themselves.

Cabbar is plunged into despair. A friend of Cabbar, Hasan, has been inculculating the turning up a buried treasure into Cabbar. In his helplessness, Cabbar lends himself to illusions. After a preachers faith healing sessions they -Cabbar, Hasan, and the preacher- go on treasure’s way. In the second half of the film, this search for the unfindable treasure is narrated. This search is a natural continuity of the gradually worsening events, which makes Cabbar take refuge on the preacher, in whom he had no belief at all at the beginning.

As can be seen, Güney set down three alternatives for Cabbar: First, he goes after the preacher and searches the treasure; second, he continues to show attitude of expectancy in the lottery tickets; third, he takes part in an organized opposition with other phaeton drivers. However, Cabbar runs away from political and social activities and lastly he becomes a victim of empty promises. He escaped realities and looks for the shelter in fantasies. Güney could make Cabbar politically conscious, even make him the leader of this opposition, and lastly lead him to success or failure, but Güney chose to leave his protagonist blind.

The film finished with an interesting and striking scene. Cabbar, who left his wife, children, and mother desperate in the cause of treasure search, opens his hands to a God, whom he does not know, he begins to turn around in the middle of arid lands. He has gone mad.

==Analysis==
 
Güney asserted before any political action can be organized to change the economic system, the would-be actors must abandon the idea that they possess a self profoundly different from other selves, that they pursue a destiny which is uniquely their own as individuals. The basic aim of Umut is to demonstrate that people who fight alone have no alternative but to place their trust in luck (lottery) and magic (treasure hunting). Briefly, Güney tried to show ideology as ideology to those who would claim that Cabbar’s situation is a natural state of affairs. As Althusser has stated, consciousness of an ideology as ideology, is the moment in which ideology explodes, revealing the reality it had obscured.

This film, which carries some traces from Güney’s own life, manifested itself with its ability to describe the story with an extraordinary  reality. Güney thought that we had to look at, to grab to, and reflect the realities of the streets we are wandering and passing rapidly through. The people shown in Umut are mostly “real” people, not actors or actresses. The environment  and the living conditions are real, Güney did not use prepared film sets and lightings for Umut. The scenes showing Cabbar at home with his 5 children, wife, and mother are very good examples of his effort to show life and the environment closest to its reality. With this film, Güney introduces a kind of documentarism into the Turkish cinema.

As the neo-realismo movie makers, Güney created a cinema, where the audience is able to interpret the end of the film in whatever direction he/she wants. For instance, there is no definite ends in neo-realismo, likewise Umut’s end is not definite either, the future is uncertain.

Of course, it was not too late that Umut was censored. The prohibition of that film is a very good case to determine the social, economic, and political conditions of Turkey in the 1970s. Although prohibited a copy of the film was smuggled abroad and showed in Cannes Film Festival. Afterwards, with the state council’s final decision the film was featured in Turkey as well as abroad, and attracted great attention. With this film, Güney captured prizes in Altın Portakal and Adana Film Festivals as best actor.

==Awards won==

* Best film, 2nd Adana Golden Boll Film Festival, 1970
* Best director, 2nd Adana Golden Boll Film Festival, 1970
* Best scenario, 2nd Adana Golden Boll Film Festival, 1970
* Best actor, 2nd Adana Golden Boll Film Festival, 1970
* Best photography (Kaya Ererez), 2nd Adana Golden Boll Film Festival, 1970
* Best actor, Antalya Golden Orange Film Festival
* Selectors Commission Special Prize, Grenoble Film Festival

== External links ==
*  
*  

 

 

{{succession box |
  | before = Kuyu (film)|Kuyu
  | after = Ağıt Golden Boll Award for Best Picture
  | years = 1970
|}}

 

 

 
 
 
 
 