Arsène Lupin contra Sherlock Holmes
 
{{Infobox film
| name           = Arsène Lupin contra Sherlock Holmes
| image          =
| caption        =
| director       = Viggo Larsen
| producer       =
| writer         =
| starring       = Viggo Larsen Paul Otto
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 5 episodes
| country        = Germany
| language       = Silent with German intertitles
| budget         =
}}
 drama film serial directed by Viggo Larsen.   

==Cast==
* Viggo Larsen as Sherlock Holmes
* Paul Otto as Arsène Lupin

==List of episodes==
* "Der Alte Sekretar" (The Old Secretary) also known as “Arsene Lupin”, released 20 August 1910 
* "Der Blaue Diamant" (The Blue Diamond), released 17 September 1910 
* "Die Falschen Rembrandts" (The Fake Rembrandts) also known as “The Two Rembrandts”, released 7 October 1910  
* "Die Flucht" (The Escape), released 24 December 1910 
* "Arsene Lupins Ende" (The End of Arsene Lupin), released 4 March 1911

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 


 
 