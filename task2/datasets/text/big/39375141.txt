The Quail Hunt
 

{{Infobox Hollywood cartoon|
| cartoon_name = The Quail Hunt
| series = Oswald the Lucky Rabbit
| image = OswaldInQuailHunt.jpg
| caption = Oswald tries to shoot a quail.
| director = Walter Lantz
| story_artist = Walter Lantz Vic McLeod
| animator = Fred Kopietz Bill Mason Ed Benedict Ray Abrams
| voice_actor = 
| musician = James Dietrich
| producer = 
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = September 23, 1935
| color_process = Black and white
| runtime = 5:45 English
| preceded_by = Amateur Broadcast
| followed_by = Monkey Wretches
}}

The Quail Hunt is a short theatrical cartoon starring Oswald the Lucky Rabbit. It is the 106th Oswald cartoon by Walter Lantz Productions and the 157th in the entire series.

==Plot==
Oswald and his dog Elmer the Great Dane are in the woods hunting for birds, especially quails. Though equipped with a boomstick, Oswald finds it difficult to take down a single quail as the fowls are quite clever. He even has problems trying not to get pushed back each time he fires his gun.

Heading further in the forest, Elmer chases a little quail. The dog follows until he naively runs past a cliff. Instead of letting the hound plummet into the ground, however, the little quail moves and breaks Elmers fall. Elmer is most thankful and therefore befriends the small bird.

While things are going well for Elmer and the little quail, a hawk appears before them and sets sights on the tiny bird. As the hawk goes for a strike, Elmer struggles to defend his little friend. Eventually, the hawk and the dog collide into each other, resulting the buzzard being naked and Elmer covered in feathers.

Oswald finally shows up at the scene. Thinking the dog is a turkey due to the latters feathery exterior, Oswald fires his gun at Elmer, blowing the feathers off. He then notices the little quail, and therefore begins shooting at it too. Elmer immediately intervenes and tells him the small bird is now friends with them. Elmer then embraces Oswald and the little quail in both arms.

==See also==
*Oswald the Lucky Rabbit filmography

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 