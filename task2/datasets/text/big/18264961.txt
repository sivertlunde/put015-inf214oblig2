Assault and Peppered
{{Infobox Hollywood cartoon|
| cartoon_name = Assault and Peppered
| series = Merrie Melodies (Daffy Duck/Speedy Gonzales)
| image = 
| caption = 
| director = Robert McKimson Manny Perez Don Williams
| layout_artist = Dick Ung
| background_artist = Tom OLoughlin John Dunn
| voice_actor = Mel Blanc
| musician = Bill Lava
| producer = David H. DePatie Friz Freleng
| studio = DePatie-Freleng Enterprises
| distributor = Warner Bros. Pictures The Vitaphone Corporation
| release_date = April 24, 1965 (US)
| color_process = Technicolor
| runtime = 6 English
}}
Assault and Peppered  is a Merrie Melodies cartoon featuring Daffy Duck and Speedy Gonzales and other mice.

Released on April 24, 1965, it was directed by Robert McKimson and produced by the award winning DePatie-Freleng, the production house responsible for the Pink Panther and other series of cartoons released by MGM.

Mel Blanc voiced Daffy Duck, Speedy Gonzales, and the mice.

Its run time is six minutes, the standard length of a Warner Brothers cartoon since pre-48 days.

The title of this cartoon is a play on the term "salt and pepper".

==Synopsis==
This cartoon is a semi-remake of Friz Frelengs Bunker Hill Bunny. A group of starving mice are admiring Daffy Ducks Mexican plantation (aptly named El Rancho Rio Daffy), all the while wishing to have some of his home grown food. Evil land baron, Daffy, who isnt particularly fond of beggars, suddenly appears and angrily whips the mice for "starving on his property." Unfortunately, Speedy Gonzales interrupts and startles Daffy, at which the little black duck declares war on the mouse. The two proceed to do battle in private forts, resorting to everything from cannon fire to mine fields. At the end, Speedy (exhausted from doing battle with Daffy) quits and goes home. Daffy then declares victory and rewards himself with a 21 gun salute. Unfortunately, as he pulls the strings to fire his cannons, the cannons flip in his direction ("Mother!") and blast him one-by-one (with Speedy observantly keeping count).

==Edited versions==
*On CBS, the part where Daffy cracks his whip at a peasant mouse and the peasant mouses sombrero splits in two (similar to a gag in the Chuck Jones cartoon "Ali Baba Bunny" that was also edited on CBS and some independent local stations) was cut.

==See also==
* The Golden Age of American animation
* List of Daffy Duck cartoons

==Sources==
* 
* 
* 
 
==External links==
* 

 
 
 
 
 
 


 