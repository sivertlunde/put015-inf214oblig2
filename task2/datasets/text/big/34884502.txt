Portrait of a Mobster
{{Infobox film
| name           = Portrait of a Mobster
| image          = 
| image_size     = 
| caption        = 
| director       = Joseph Pevney
| writer         = 
| narrator       = 
| starring       = Vic Morrow
| music          = 
| cinematography = 
| editing        = 
| studio         = Warner Bros.
| distributor    = 
| released       = January 5, 1962
| runtime        = 108 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Portrait of a Mobster is a 1961 film directed by Joseph Pevney. It stars Vic Morrow and Leslie Parrish. 

==Plot==

Up-and-coming racketeer Dutch Schultz  joins the Legs Diamond gang in Prohibition-era New York. A bootlegger named Murphy is murdered by Dutch, who falls for the dead mans daughter, Iris.

Iris marries her fiancee, Frank Brennan, a police detective. They need money and Frank accepts payoffs from Dutch, who is forming a gang of his own.

After getting rid of Legs, Mad Dog Coll and others standing in his way, Dutch again makes a play for Iris, but she learns that he killed her father and begins to drink. Frank vows to reform and win her back. Betraying his pal Bo to the mob, Dutch discovers that a hit has been put out on himself as well. While fighting for his life, he is shot by Bo by mistake and is killed.

==Cast==
*Vic Morrow as Dutch Schultz
*Leslie Parrish as Iris Murphy
*Peter Breck as Frank Brennan
*Norman Alden as Bo Wetzel
*Robert McQueeney as Michael Ferris
*Frank DeKova  ...  Anthony Parazzo  
*Ray Danton  ...  Jack Diamond (gangster)|Legs Diamond  
*Anthony Eisley  ...  Legal Advisor

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 