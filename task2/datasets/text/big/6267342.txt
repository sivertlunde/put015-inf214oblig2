Witchcraft (1988 film)
{{Infobox film
| name           = Witchcraft
| image          = Witchraft DVD Cover.jpg
| caption        = DVD cover
| director       = Rob Spera
| producer       = Jerry Feifer, Yoram Barzilai
| writer         = Jody Savin
| starring       = Anat Topol, Gary Sloan, Mary Shelley Randy Miller
| cinematography = Jens Sturup Tony Miller
| distributor    = Simitar Entertainment  (USA, DVD) 
| released       =  
| runtime        = 95 min
| country        = United States
| language       = English
}} horror thriller Mary Shelley, Deborah Scott, Alexander Kirkwood, Lee Kissman, and Ross Newton. The movie was released on video in 1988, and re-released October 15, 1997, on DVD.

==Plot==
As Grace Churchill is having her baby, disturbing visions flash in her mind that show two witches being burned at the stake. It is later learned that these two people are John and Elizabeth Stockwell, who were burned in the year 1687. The visions seem to stop once her baby, whom she names William, is born. Things get worse when she, her husband, and the baby temporarily move into her mother-in-law’s creepy old house. It’s here that the visions start returning, and all sorts of spooky events start happening around her, including a priest hanging himself in their backyard.

==Series==
*Witchcraft (1988 film) (1988)
*  (1990)
*  (1991)
*  (1992)
*  (1993)
*  (1994)
*  (1995)
*  (1996)
*  (1997)
*  (1998)
*  (2000)
*  (2002)
*  (2008)

==External links==
* 
* 

 
 
 
 
 


 