Palunkupaathram
{{Infobox film 
| name           = Palunkupaathram
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = AL Sreenivasan
| writer         = Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Sathyan Padmini Padmini Sukumari
| music          = G. Devarajan
| cinematography = V Selvaraj
| editing        = VP Krishnan
| studio         = ALS Productions
| distributor    = ALS Productions
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film, Padmini and Sukumari in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir Sathyan
*Padmini Padmini
*Sukumari
*Jayabharathi
*Kaviyoor Ponnamma
*Adoor Bhasi
*Muthukulam Raghavan Pillai
*T. S. Muthaiah
*Baby Rajani
*K. P. Ummer
*Ushakumari
*Mythili
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arayanname || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Devaloka Radhavumaay || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Maayaajaalakavaathil || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Pachamalayil || P Susheela || Vayalar Ramavarma || 
|-
| 5 || Pachamalayil-Sad || P Susheela || Vayalar Ramavarma || 
|-
| 6 || Sumangali Nee Ormikkumo || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 7 || Vasanthathin Makalallo || K. J. Yesudas, P. Madhuri || Vayalar Ramavarma || 
|-
| 8 || Vasanthathin Makalallo   || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 