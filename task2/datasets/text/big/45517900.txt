Eight Girls in a Boat (1934 film)
{{Infobox film
| name           = Eight Girls in a Boat
| image          = Eight Girls in a Boat poster.jpg
| alt            = 
| caption        = Theatrical release poster Richard Wallace
| producer       = Charles R. Rogers
| screenplay     = Helmut Brandis Lewis R. Foster Casey Robinson Dorothy Wilson Douglass Montgomery Kay Johnson Walter Connolly Ferike Boros James Bush Barbara Barondess
| music          = 
| cinematography = Gilbert Warrenton
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Richard Wallace Dorothy Wilson, Douglass Montgomery, Kay Johnson, Walter Connolly, Ferike Boros, James Bush and Barbara Barondess. The film was released on January 5, 1934, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9A0DE7D6103DE33ABC4B52DFB766838F629EDE|title=Movie Review -
  Eight Girls in a Boat - In a Girls School. - NYTimes.com|work=nytimes.com|accessdate=26 February 2015}}  
 
==Plot==
 

== Cast ==  Dorothy Wilson as Christa Storm
*Douglass Montgomery as David Perrin
*Kay Johnson as Hannah
*Walter Connolly as Storm
*Ferike Boros as Frau Kruger
*James Bush as Paul Lang
*Barbara Barondess as Pickles
*Colin Campbell as Smallman
*Jennifer Gray as The Strange One
*Ruth Heinaman as Goggles
*Baby Peggy as Hortense
*Margaret Marquis as Elizabeth
*Dorothy Drake as The Naughty
*Kathleen Fitz as The Ritzy
*Phyllis Ludwig as The Cry-Baby
*Marjorie L. Carver as Bobby 
*Virginia Hall as Mary Kay Hammond as Katza 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 