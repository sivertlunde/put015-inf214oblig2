Meet the Mormons
 
{{Infobox film
| name           = Meet the Mormons
| image          = Meet_the_Mormons_poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Blair Treu
| producer       = Jeff Roberts
| music          = Sam Cardon
| cinematography = R.J. Hill Brian Sullivan
| editing        = Wynn Hougaard Excel Entertainment The Church of Jesus Christ of Latter-day Saints
| distributor    = Purdie Distribution
| released       =  
| runtime        = 78 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = $5,883,132   
}}
Meet the Mormons is a 2014 American documentary film directed by Blair Treu and produced by The Church of Jesus Christ of Latter-day Saints (LDS Church). The film documents the lives of six devout Mormons living in the United States, Costa Rica, and Nepal. The LDS Church donated all net proceeds from the theatrical release of film to the American Red Cross.

==Production==
The film was originally designed for viewing in the Legacy Theater in the Joseph Smith Memorial Building in Salt Lake City, but after screenings with test audiences, LDS Church leadership decided to release the film first in theaters across the United States.  According to Jeffrey R. Holland, the film is "not a proselytizing effort but informative" and is an "opportunity to share who Mormons really are".  The film is financed and distributed by the LDS Church, a first for the church. It is shot in documentary format and will be translated into 10 languages.   
 American singer-songwriter and actor David Archuleta sang the track "Glorious" for the film. 

==Release== Excel Entertainment. The LDS Church is donating all net proceeds from the theatrical release of film to the American Red Cross.  Beginning January 2015, the LDS Church began showing the film in all of its visitors centers and historical sites. 

==Reception==
 
The film holds an 11% critics rating at Rotten Tomatoes with nine reviews.  Metacritic reports a score of 29/100 from published reviews, indicating "generally unfavorable reviews".   

The film has been criticized as propaganda.   (also   version of same article)     While praised for its "slick" cinematography,        critics felt the documentary lacked information about Mormon history,       its tenets,     and its controversies.      

The film grossed $2,509,808 in its opening weekend, placing it outside the top 10.  As of January 2015, it is listed 31st in revenue-producing documentary films.  

==See also==
 
* List of films of The Church of Jesus Christ of Latter-day Saints

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 