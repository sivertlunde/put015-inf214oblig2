Daughters of the Night
{{infobox film
| name           = Daughters of the Night
| image          =
| imagesize      =
| caption        =
| director       = Elmer Clifton William Fox
| writer         = Willard Robertson (story & screenplay)
| starring       = Orville Caldwell Alyce Mills
| music          = 
| cinematography = 
| editing        =
| distributor    = Fox Film Corporation
| released       =   reels
| country        = United States
| language       = Silent (English intertitles)
}} lost 1924 silent film melodrama produced and distributed by the Fox Film Corporation. It was directed by Elmer Clifton with Alyce Mills as the lead actress. 

==Synopsis==
A story about women telephone operators in the big city.

==Cast==
*Orville Caldwell - Billy Roberts
*Alyce Mills - Betty Blair
*Phelps Decker - Doc Long
*Alice Chapin - Grandma Backer
*Warner Richmond - Lawyer Kilmaster
*Bobbie Perkins - Eloise Dabb
*Clarice Vance - Mrs. Dabb
*Claude Cooper - Mr. Dabb
*Willard Robertson - Professor Woodbury
*Charles Slattery - Dick Oliver
*Henry Sands - Jimmy Roberts

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 