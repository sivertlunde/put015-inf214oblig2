Frozen (1997 film)
 
{{Infobox film
| name           = Frozen
| image          = Frozen (1997 film) DVD boxart.jpg
| caption        = Fox Lorber DVD cover
| director       = Wang Xiaoshuai
| producer       = 
| writer         = Pang Ming Wang Xiaoshuai
| starring       = Jia Hongsheng Ma Xiaoqing
| music          = 
| cinematography = Yang Shu
| editing        = Qing Qing
| distributor    = United States:  
| released       = Netherlands: October 23, 1997 United States (DVD): February 22, 2000
| runtime        = 95 minutes China
| Mandarin
}} 1997 Cinema Chinese film The Days, was screened internationally without government approval. Berry, Michael (2005). "Wang Xiaoshuai: Banned in China" in  , p. 168. ISBN 0-231-13330-8. Google Book Search. Retrieved 2008-10-05.  As such, Wang was forced to use the pseudonym "Wu Ming" (literally "Anonymous") while making this film. 

The film, supposedly based on a true story, follows a young performance artist, Qi Lei, who attempts to create a masterpiece based around the theme of death. After two "acts" where he simulates death, he decides that his final act will be a true suicide through hypothermia. 

Frozen was originally entitled The Great Game ( ). This was meant to reflect the attitude of both the film and the artist portrayed within it to treat death and suicide as a game or a manipulation. 

== Casting == Suzhou River (2000). 
Wang selected Jia in part because he was unconventional looking and in Wangs words, Jia "does not look like an actor."  But because Jia was Wangs friend, he did not demand payment, thus allowing the film to operate on a smaller budget. 

The other lead, Shao Yun, Qi Leis girlfriend, was played by actress Ma Xiaoqing. Her casting was done in part to create parity with Jia. Wang wanted both leads to be professional actors. 

== Production ==
The film proved to be a difficult shoot, much like its predecessor. However, the problems that plagued Frozen were far different from the obstacles of The Days. By far the greatest issue during filming was the content of the film. Several key scenes required actor Jia Hongsheng to recreate performance art, such as soap-eating, and in the films ultimate scene, self-freezing. Both scenes were difficult to capture although the scene which demanded that Jia lay in ice for several minutes was the most dangerous. Indeed Wang had to have Jia sent to the hospital immediately after shooting to check for permanent damage. Berry, p. 170.  

== DVD release ==
Frozen was released on DVD by Fox Lorber on February 22, 2000 in the United States.    The Fox Lorber edition was basic, but included English subtitles and some extra features, including production notes and cast and crew filmographies. 

==Awards and nominations==
Frozen was nominated at the International Film Festival Rotterdam for the Tiger Award,  and won the FIPRESCI Award for Special Mention. {{cite web|url=http://www.filmfestivalrotterdam.com/en/about/awards-and-jury/fipresci-awards/|title=FIPRESCI Award
|publisher=International Film Festival Rotterdam|accessdate=July 23, 2014}} 

== Notes ==
  

== External links ==
*  
*  
*   at the Chinese Movie Databsae
*   from US Distributor, International Film Circuit, Inc.

 

 
 
 
 
 
 
 