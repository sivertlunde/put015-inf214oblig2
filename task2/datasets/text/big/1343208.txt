Code 46
 
 
{{Infobox film
| name                = Code 46
| image               = Code_46_movie.jpg 
| caption             = Code 46 film poster
| writer              = Frank Cottrell Boyce
| starring            = Tim Robbins, Samantha Morton, Jeanne Balibar
| director            = Michael Winterbottom Alwin Kuchler
| producer            = Andrew Eaton
| studio              = BBC Films, Revolution Films MGM United Artists  (USA) 
| released            = 2 September 2003
| runtime             = 92 minutes United Kingdom
| language            = English
| budget              = ~ US$7,500,000
}}
 David Holmes under the name "Free Association". The film was shot in Dubai, Shanghai, Kuala Lumpur, Rajasthan (picturised as Jebel Ali) and many interiors in London, both for logistic reasons and because the juxtaposition of elements of these cities offered a believable futuristic setting.

==Plot==
In the near future the world is divided between those who live "inside", in high-density cities, while the poor underclass live "outside." Access to the cities is highly restricted and regulated through the use of health documents, known as "papelles" in the global pidgin language of the day (composed of elements of English, Spanish, French, Arabic language|Arabic, Italian, Farsi and Mandarin Chinese|Mandarin).
 authoritarian and dystopian. Society is regulated by various "codes". The code of the movie title prohibits "genetically incestuous reproduction", which may occur as a result of the various medical technologies which have become commonplace, such as cloning.

William Geld (  to meet someone she cannot identify. Each birthday she is one station closer to her destination, where she expects to meet the person. William is captivated by her, and instead of turning her over to security, he identifies another employee as the forger.

William then meets up with Maria and they begin an affair. Putting complete trust in a man who could have had her arrested, Maria reveals how she was able to smuggle papelles out of her workplace. In a nightclub they meet Damian (David Fahm), a naturalist who longs to travel to Delhi to study bats. He has applied for cover for eight consecutive years but has always been refused. Maria supplies Damian with a papelle. William is upset by this and indicates that he should turn Maria over to the authorities, but Maria knows he wont. William explains that there are legitimate reasons why Damian is unable to obtain the proper clearances legally. Maria believes that some risks are worth taking to fulfill ones dreams.

Back at her apartment, Maria shows William her "memory scrapbook" (an electronic booklet that records video from the users mind), which contains memories of her parents and friends. Other movies show her passing papelles to various people. She thinks they are beautiful and their eyes are full of desire and dreams, and that they have a different look from "everyday" people. As Maria sleeps, William finds a forged cover in her room and takes it.

Williams travel cover will expire the next day so he returns home to his family. On the way to the airport, he gives the forged cover to a street vendor at the citys perimeter checkpoint, an act of humanity which could change the vendors life. Later, he learns that Damian died in Delhi after exposure to a virus to which he had no immunity. William is reprimanded for not discovering the true Sphinx forger. He explains that he had trouble with his empathy virus and requests that someone else be sent, as there may have been an accomplice to the innocent man he fingered as the guilty party. However, he is ordered to deal with the problem and to return to Shanghai.

Upon his return William discovers that Maria has gone. Her apartment is abandoned and the only clue is a medical clinic appointment. He visits the clinic and using his empathic abilities learns that Maria was pregnant, but that it was terminated due to a violation of Code 46. William knows that this means Maria is somehow genetically related to him, but he has no idea how.
 clone of his mother, who was one of a set of twenty four In vitro fertilisation|in-vitro fertilised clones. This knowledge does not affect Williams feelings, but instead of going back to Maria he decides to go home to his family. However when he tries to leave he is not allowed to do so as his 24-hour cover is now expired.

William then realises that his only hope of returning home is to get a papelle from Maria. He returns to her apartment and tells her about his inability to leave and she agrees to help him. She tells him she must acquire a papelle and that she will meet him at the airport later. She goes to work and obtains a papelle, but is unable to forge one herself, as she was moved to another area of work, so a co-worker makes the cover for her. While taking a train to meet William she remembers her birthday dream, and that he was the person she is looking for in the dream, and she remembers her feelings for him. She meets William and gives him the papelle and then tells him she remembers him. He decides not to leave her.
 Jebel Ali in the Middle East, which does not require special travel clearance. The two hide out in the old city where they book a room. Here William reveals to Maria that as well as the memory wiping she has been given a virus that induces a terrorising adrenaline rush in response to physical contact with the person who brought about the Code 46 violation. However, Maria still wants to make love with William and so he ties her down to prevent her from fleeing once the adrenaline rush kicks in.

Afterward, Maria enters a somnambulistic state also caused by the virus which forces her to report the further Code 46 violation to the authorities. She is unconscious of this, though William is aware of the viruss reaction. They then rent an old car and travel away to escape the authorities who are tracking them. William crashes the car while avoiding a collision with camels and pedestrians and they are both knocked unconscious.

When William awakes in the hospital he finds himself in Seattle with his wife and child. He has no memory of Maria or the Code 46 violation, as all memories of her and their time together have been completely flushed from his mind. The authorities had brought William before a tribunal, but decided the empathy virus had affected his judgment. He attempts to use the empathy virus to read his sons thoughts on the drive back from the hospital, but is unable to. Maria is more severely punished, in effect, by having her memories of William loving her not be erased, essentially forced to remember him and exiled to the place she hated the most, the desert, outside of "cover". Her final words of the film (portrayed through Voice Over of the end-of-film events and a montage of her poor, "beggar" life and existence) are, "I miss you."

==Cast==
* Tim Robbins as William Geld
* Samantha Morton as Maria Gonzalez
* Togo Igawa as a "Driver"
* Natalie Mendoza as a "Sphinx Receptionist"
* Nabil Elouahabi as a "Vendor"
* Om Puri as Bahkland
* Jeanne Balibar as Sylvie
* Nina Wadia as a clinic "Hospital Receptionist"
* Archie Panjabi as a "Check In" agent
* Kerry Shale as Clinic Doctor

==Themes==

===Genetics===
Human DNA has 46 chromosomes (23 pairs). Critic Roger Ebert comments that the character of Maria Gonzalez "suggests ethnic distinctions have been lost in generations of government-supervised DNA matchups".

===Morality===
There are some connections with the Oedipus myth. The most obvious are the inadvertent mother-son sexual relation and the Sphinx, but also Marias exile and Williams loss of empathy (compare to Oedipus loss of sight). Carla Meyer, in her review for the San Francisco Chronicle, argues that "the film updates a classical premise – the struggle for personal freedom – by pairing it with ethical and moral quandaries".

===Language=== Basque word "agur" for hello, and "Khoda Hafez" being Farsi for goodbye. There are also words in Italian (e. g. "ti amo", I love you), in French (e. g. "à bientôt", see you soon), and Arabic traditional greetings like "As-Salamu Alaykum" (السلام عليك; "peace be with you", but generally meant as "hello"). The idea is to portray a society that is not only multi-ethnic but also with a language-fusion with expressions coming from the dominant languages of our present and those that are going to be dominant in our future.
 Mick Jones of The Clash sings The Clash song "Should I Stay or Should I Go?". Earlier in the same scene, a girl singing along with a piano is playing a famous theme of Portugals traditional genre Fado de Coimbra, named "Coimbra Menina e Moça".

==See also==
*List of films featuring surveillance

==References==

 
 
* 
* 
* 
* 
* 
* 
* 
 

==External links==
*  
*  
*  
*   with Michael Winterbottom, first broadcast on Resonance FM
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 