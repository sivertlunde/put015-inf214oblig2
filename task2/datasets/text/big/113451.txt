A Soldier's Story
{{Infobox film
| name           = A Soldiers Story
| image          = Soldiers story poster.jpg
| caption        = Theatrical release poster
| director       = Norman Jewison
| producer       = Norman Jewison
| writer         = Charles Fuller
| starring = {{plainlist| Howard E. Rollins, Jr.
* Adolph Caesar
}} 
| music          = Herbie Hancock
| cinematography = Russell Boyd Mark Warner
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $21,821,347 
}}
 segregation in Jim Crow South, in a time and place where a black officer is unprecedented and bitterly resented by nearly everyone.

The film was first shown at the  ), and Screenplay Adaptation (Fuller).

==Plot==
The time is 1944. during World War II. Vernon Waters (Adolph Caesar), the Army sergeant in charge of Company B, a platoon of black soldiers, is very drunk and staggering along a road along Fort Neal, a segregated Army base in Louisiana. Waters last words amidst his raucous laughter were "They still hate you! They still hate you!" before he is shot to death with a .45 caliber pistol. 
 Howard E. Judge Advocate General is sent to investigate, against the wishes of commanding officer Colonel Nivins (Trey Wilson). While the general consensus is that he was killed by local members of the Ku Klux Klan, others are doubtful, having heard that Waters stripes and insignia were still on his uniform and aware that the Klans typical M.O. is to remove them before lynching their victims.

From the outset, Davenport is faced with obstacles. Colonel Nivins will only give him three days to conduct his investigation. Even Captain Taylor (Dennis Lipscomb), the one white officer in favor of a full investigation, is uncooperative and patronizing, fearing that a black officer will have little success in catching those responsible. While some black soldiers are happy and proud to see one of their own race wearing captains bars, others are distrustful and evasive. 

Davenport learns that the platoon was officially tasked as the 221st Chemical Smoke Generating Company and while they are eager to serve their country overseas, they are often assigned menial jobs in deference to their white counterparts. However, they have also found a good deal of popularity and triumph as a tight-knit baseball team playing against white soldiers, and there is even talk of the platoon playing against the New York Yankees in an exhibition game.
 Larry Riley) But as Davenport probes deeper, he uncovers Waters true tyrannical nature and his disgust with his fellow black soldiers, particularly those from the rural South. 

An interview with Private Peterson (Denzel Washington) revealed how he stood up to Waters when he berated the men after another winning game. In retaliation, the sergeant challenged Peterson to a fight and beat him badly. Davenport then learns through interviews with other soldiers how Waters charged C.J. with the murder of a white MP, after a search conducted by Wilkie turned up a recently discharged pistol under C.J.s bunk. Confronting him with the evidence, Waters provoked C.J. into striking him, whereupon the weapons charge was dismissed and C.J. was then charged with striking a superior officer. 

When C.J.s best friend Bernard Cobb (David Alan Grier) visits him in jail, C.J. is suffering from intense claustrophobia and tells Cobb of a visit from Sgt. Waters, who admitted freely to C.J. that it was a set-up and that Waters had done it at least five times before to others like him, saying "the Black race cant afford you no more...the day of the Geechee is gone, boy. And youre going with it." When Davenport asked Corporal Cobb what happened to C.J., he is told that the man hanged himself in his cell while awaiting trial. In protest, the platoon threw the last game of the season, while Waters was left profoundly shaken by the suicide.

Davenport then finds out that two white officers coming from a military exercise, Captain Wilcox (Scott Paulin) and Lieutenant Byrd (Wings Hauser), had an altercation with the drunk sergeant a short time before his death. When questioned, both officers admit to physical assault when confronted by Waters on a drunken tirade, but deny killing him, revealing that they had not been issued .45 ammunition for the exercise as it was in short supply and it was reserved for MPs and soldiers on special duty. Though Taylor is convinced that Wilcox and Byrd are lying and is eager to arrest them, Davenport releases them. 

While a search has begun for Privates Peterson and Smalls who have both gone AWOL, Davenport questions Wilkie once more, and the demoted private is forced to admit that he planted the gun under C.J.s bunk on Waters orders. Though he hid it from everyone, Waters divulged in private to Wilkie his intense hatred of C.J. and others like him whom Waters felt were an unwelcome weight on the Black race. Davenport then asks why Waters didnt go after Peterson since they had the fight, and Wilkie tells him that Waters liked Peterson because he fought back and was planning to promote him. Davenport has Wilkie placed under arrest just as an impromptu celebration has begun outside after learning that the platoon is to be shipped out to join the fight overseas.

Realizing that Peterson and Smalls were on guard duty the night of Waters murder, and thus had been issued .45 ammunition for their pistols, Davenport interrogates Smalls after he has been found by the MPs and Smalls confesses that it was Peterson who killed Sergeant Waters, as revenge for C.J. When Peterson is captured and brought into the interrogation room, he confesses to the murder, saying "I didnt kill much. Some things need getting rid of." 

The film ends with Taylor congratulating Davenport on getting his man and admitting that he will have to get used to Negroes being in charge. Davenport assures Taylor that hell get used to it. "You can bet your ass on that," he adds, as the platoon marches in preparation for their deployment to the European Theater.

==Cast==
Art Evans plays Private Wilkie, a nervous man too acquiescent for his own good. David Alan Grier plays C.J.s closest friend, bonded by their Mississippi roots. Denzel Washington, in one of his earliest motion picture roles, portrays the deeply embittered Pfc. Peterson. Howard E. Rollins, Jr. as Capt. Davenport
*Adolph Caesar as Sgt. Waters
*Art Evans as Pvt. Wilkie
*David Alan Grier as Cpl. Cobb David Harris as Pvt. Smalls
*Dennis Lipscomb as Capt. Taylor Larry Riley as C.J. Memphis Robert Townsend as Cpl. Ellis
*Denzel Washington as Pfc. Peterson
*William Allen Young as Pvt. Henson John Hancock as Sgt. Washington
*Patti LaBelle as Big Mary
*Trey Wilson as Col. Nivens
*Wings Hauser as Lt. Byrd

==Production== UA and MGM followed suit. Columbias Frank Price read the screenplay and was deeply interested, but the studio was hesitant about its commercial value, so Jewison offered to do the film for a $5 million budget and no salary. When the Directors Guild of America insisted he must have a fee, he agreed to take the lowest possible amount. The film ended up grossing $22.1 million.   
 Broadway careers, but only Adolph Caesar, Denzel Washington, and William Allen Young appeared in both the movie and the original off-Broadway play with the Negro Ensemble Company in the New York version.
 Little Rock at the historic Lamar Porter Field. {{cite book
  | last = Gordon
  | first = William A.
  | title = Shot on This Site
  | publisher = Citadel Press
  | year = 1996
  | page = 146
  | isbn = 0-8065-1647-X}} 
 Reserve base Fort Smith (where Elvis Presley entered the military and received his first haircut).

==Musical score== Larry Riley, who plays guitar, wrote and performed their own songs. The blues played a large role in the films music.

Unfortunately there was no known consideration given to the production of an official soundtrack due to the aforementioned budgetary restraints—this, despite the films relative box office success.

==Awards==

===Won=== Edgar Award, Best Motion Picture Screenplay&nbsp;– Charles Fuller Los Angeles Film Critics Association Awards, Best Supporting Actor&nbsp;– Adolph Caesar
*14th Moscow International Film Festival&nbsp;– Golden Prize 
*NAACP Image Award for Outstanding Motion Picture

===Nominated===
*Academy Award for Best Picture Golden Globe Award for Best Motion Picture - Drama Directors Guild of America Award for Outstanding Directorial Achievement in Motion Pictures&nbsp;– Norman Jewison
*Academy Award for Best Supporting Actor&nbsp;– Adolph Caesar Golden Globe Award Best Actor in a Supporting Role in a Motion Picture&nbsp;– Adolph Caesar
*Academy Award for Writing Adapted Screenplay&nbsp;– Charles Fuller Golden Globe Award for Best Screenplay - Motion Picture&nbsp;– Charles Fuller Writers Guild of America for Best Screenplay Based on Material from Another Medium&nbsp;– Charles Fuller

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 