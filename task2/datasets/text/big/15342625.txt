The Rising of the Moon (film)
 
{{Infobox film
| name           = The Rising of the Moon
| image	         = The Rising of the Moon FilmPoster.jpeg
| caption        = Original Australian film poster
| director       = John Ford
| producer       = Michael Killanin
| writer         = Lady Augusta Gregory Michael J. McHugh Frank S. Nugent Frank OConnor
| narrator       = Tyrone Power Noel Purcell Denis ODea
| music          = 
| cinematography = Robert Krasker
| editing        = 
| distributor    = Warner Bros.
| released       =  
| runtime        = 81 minutes
| country        = Ireland
| language       = English
| budget         = 
}}

The Rising of the Moon is a 1957 Irish anthology film directed by John Ford.      It consists of three episodes all set in Ireland:

*"The Majesty of the Law", based on the short story of that title by Frank OConnor in Bones of Contention
*"A Minutes Wait", based on a 1914 one-act comedy by Martin J. McHugh The Rising of the Moon by Lady Gregory.

==Plot==
===The Majesty of the Law=== Noel Purcell). John Cowley) on the head. OFlaherty refuses to pay the fine, as he feels he has done nothing wrong, nor will he allow OFeeney to pay it for him. Instead, he heads off to prison.

===A Minutes Wait===
A train pulls up to the Dunfaill station, where Paddy Morrisey (Jimmie ODea) announces there will be "a minutes wait". The passengers and crew crowd into the bar for refreshments, served by Pegeen Mallory (Maureen Potter). Later, Paddy finally proposes to his longtime girlfriend Pegeen. 

Mrs. Falsey (May Craig) chats with her old friend Barney Domigan (Harold Goldblatt), while her niece Mary Ann MacMahon (Maureen Connell) becomes acquainted with his son Christy (Godfrey Quigley). Domigan is on his way to arrange a marriage between Christy and a young woman with a substantial dowry. Mrs. Falsey persuades him to change his mind by informing him that the U.S. Army has awarded Mary Ann $10,000 for her fathers death in battle. The young couple, unaware of this development, insist they will only marry each other.

Meanwhile, the train is repeatedly delayed, much to the befuddlement of an older English couple (Anita Sharp-Bolster and Michael Trubshawe). They are first displaced from their first class compartment to make way for a prize-winning goat. Then, they have to share their new compartment with lobsters intended for the bishops golden jubilee. When they finally get off for some tea, they are left behind when the train finally departs.

===1921===
Sean Curran (Donal Donnelly) awaits his execution by the British during the Irish War of Independence|"Black and Tan War". This is very unpopular with the Irish public who consider him a hero. The British warden (Joseph ODea) allows two "nuns" (Doreen Madden and Maureen Cusack), one of them his grieving "sister", to visit him; the false sister (an American citizen) swaps clothes and places with him. Unsuspecting Police Sergeant Michael OHara (Denis ODea) helps the pair into a waiting carriage. He notices that one is wearing high heels, but thinks little of it.

The city is immediately sealed off as the manhunt for the fugitive begins. OHara is assigned to watch a section of the waterfront and daydreams of what he could do with the £500 Bounty (reward)|bounty. Already conflicted by divided loyalties, he is visited by his overtly nationalistic wife (Eileen Crowe). Then, Curran shows up disguised as itinerant ballad singer Jimmy Walsh. OHara is suspicious and has him sing; Curran chooses the patriotic "The Rising of the Moon". Despite his unconvincing rendition, he manages to slip away on a boat sent for him while OHara bickers with his wife. When the policeman sees Curran getting away, he starts to raise the alarm, then reconsiders and starts singing "The Rising of the Moon" himself.

==Cast==
* Tyrone Power as Introducer

===The Majesty of the Law===
* Cyril Cusack as Inspector Dillon Noel Purcell as Dan OFlaherty
* Jack MacGowran as Mickey J. John Cowley as Phelim OFeeney

===A Minutes Wait===
* Jimmy ODea as Paddy Morrisey, the porter
* Maureen Potter as Pegeen Mallory
* Paul Farrell as Mr. OBrien, the train engineer
* Harold Goldblatt as Barney Domigan May Craig as Mrs. Falsey
* Godfrey Quigley as Christy Domigan
* Maureen Connell as Mary Ann McMahon
* Michael Trubshawe as Colonel Frobisher
* Anita Sharp-Bolster as Mrs. Frobisher

===1921===
* Denis ODea as Police Sergeant Michael OHara
* Eileen Crowe as Police Sergeants Wife
* Donal Donnelly as Sean Curran
* Maureen Cusack as "Sister Mary Grace"
* Doreen Madden as "Sister Matthias"
* Joseph ODea as British Warden
* Maureen Delany as Old Woman
* Frank Lawton as British Officer
* Edward Lexy as Quartermaster Sergeant 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 