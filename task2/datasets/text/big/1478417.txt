Portrait of Jennie
 
{{Infobox film
| name           = Portrait of Jennie
| image          = portraitofjennie.jpg
| image_size     = 220px
| caption        = Movie poster
| director       = William Dieterle
| producer       = David O. Selznick David Hempstead
| narrator       = Joseph Cotten
| screenplay     = Paul Osborn Peter Berneis Leonardo Bercovici (adaptation)
| based on       =   
| starring       = Jennifer Jones Joseph Cotten Ethel Barrymore
| music          = Claude Debussy Dimitri Tiomkin
| cinematography = Joseph H. August William Morgan
| studio          =Vanguard Films The Selznick Studio
| distributor    = Selznick Releasing Organization
| released       =  
| runtime        = 86 min.
| country        = United States English
| budget         = $4,041,000 Thomson, David. Showman: The Life of David O. Selznick (Abacus, 1993), p. 521. 
| gross          = $1,510,000 (rentals) 
}}
 1948 fantasy novella by Robert Nathan. The film was directed by William Dieterle and produced by David O. Selznick. It stars Jennifer Jones and Joseph Cotten.

==Plot==
In 1934, impoverished painter Eben Adams (Joseph Cotten) meets a fey little girl named Jennie Appleton (Jennifer Jones) in Central Park, New York. She is wearing old-fashioned clothing. He makes a sketch of her from memory which involves him with art dealer Miss Spinney (Ethel Barrymore), who sees potential in him. This inspires him to paint a portrait of Jennie.

Eben encounters Jennie at intermittent intervals. Strangely, she appears to be growing up much more rapidly than is possible. He soon falls in love with her but is puzzled by the fact that she seems to be experiencing events that he discovers took place many years previously as if they had just happened. Eventually he learns the truth about Jennie and though inevitable tragedy ensues, she continues to be an inspiration to Ebens life and art, and his career makes a remarkable upturn, commencing with his portrait of Jennie.  

==Cast==
* Jennifer Jones as Jennie Appleton
* Joseph Cotten as Eben Adams
* Ethel Barrymore as Miss Spinney
* Lillian Gish as Mother Mary of Mercy
* Cecil Kellaway as Matthews
* David Wayne as Gus OToole
* Albert Sharpe as Moore
* Henry Hull as Eke
* Florence Bates as Mrs. Jekes
* Clem Bevans as Capt. Cobb Nancy Davis as Teenager in Art Gallery 
* Anne Francis as Teenager in Art Gallery  
* Brian Keith as Ice-Skating Extra 
* Nancy Olson as Teenager in Art Gallery  Robert Dudley as Another Old Mariner
* Maude Simmons as Clara Morgan

==Production==
The book on which the film was based first attracted the attention of David O. Selznick, who immediately purchased it as a vehicle for Academy Award winner Jennifer Jones. Filming began in early 1947 in New York City and Boston, Massachusetts, but Selznick was unhappy with the results and scheduled re-shoots as well as hiring and firing five different writers before the film was completed in October 1948. The New York shooting enabled Selznick to use Albert Sharpe and David Wayne who were both appearing on stage in Finians Rainbow, giving an Irish flair to characters and the painting in the bar that wasnt in Nathans novel.
 tinted color sequence for the final scenes. The final shot of the painting, appearing just before the credits, is in full Technicolor.

Portrait of Jennie was highly unusual for its time in that it had no opening credits as such, except for the Selznick Studio logo. All of the other credits appear at the end. Before the film proper begins, the title is announced by the narrator (after delivering a spoken prologue, he says, "And now, Portrait of Jennie").

 
The portrait of Jennie (Jennifer Jones) was painted by artist Robert Brackman. The painting became one of Selznicks prized possessions, and it was displayed in his home after he married Jones in 1949.

The film is notable for  . 

Dimitri Tiomkin used themes by Claude Debussy, including Prélude à laprès-midi dun faune (Prelude to the Afternoon of a Faun), the two Arabesques (Debussy)|Arabesques, "Nuages" and "Sirènes" from the suite Nocturnes, and La fille aux cheveux de lin, with the addition of Bernard Herrmanns "Jennies Theme" to a song featured in Nathans book ("Where I came from, nobody knows, and where I am going everyone goes"), utilizing a theremin. Herrmann was assigned the original composing duties for the film but left during its extended shooting schedule.

A scene of Jennie and Eben having a picnic after witnessing the ceremony in the convent, features in the original screenplay. It was filmed but deleted when it looked as if Jennies hair was blending into the tree next to her. The scene that featured Jennie doing a dance choreographed by Jerome Robbins took over ten days to film,  but wasnt used in the completed film.

==Differences between the novel and the screenplay==
Though the film closely following Nathans novella, there are several differences:  
*In the novella, all the characters can see Jennie; in the film only Eben can.  
*The character of Arne, Ebens friend and fellow artist who appeared in both Nathans work and an original draft of the screenplay, is left out of the completed film.   
*The characters of Gus and the publican were made Irish to accommodate David Wayne and Albert Sharpe, who at that time were appearing in the original stage production of Finians Rainbow.  
*The mural that Eben paints in the book is one of a riverside picnic; a viewer notices that Eben has subconsciously painted what looked like a drowned woman on the bank. 
*The film provides a more thrilling climax where Eben attempts to save Jennie from the storm and a massive tidal wave, whilst the book has Jennie being washed off an ocean liner as she returns from a trip to Europe.

==Reception==
When it was released in December 1948, it was not a success, but today it is considered a classic in the fantasy genre,  with a 91% "fresh" rating on Rotten Tomatoes.  Upon its release, The New York Times reviewer Bosley Crowther called it "deficient and disappointing in the extreme;"  but the Variety (magazine)|Variety reviewers found the story was "told with style, taste and dignity." 

"Portrait of Jennie," the title song written by J. Russell Robinson, subsequently became a hit for Nat King Cole. It was even revisited in 1958 by pianist Red Garland on Manteca (Red Garland album)|Manteca, and again in 1966 by jazz trumpeter Blue Mitchell on his Bring It Home to Me.

Joseph Cottens performance as Eben Adams won the International Prize for Best Actor at the 1949 Venice International Film Festival.

===American Film Institute Lists===
* AFIs 100 Years...100 Passions - Nominated 
* AFIs 10 Top 10 - Nominated Fantasy Film 

== Adaptations ==
Lux Radio Theatre presented an hour-long adaptation of the film on October 31, 1949, again starring Joseph Cotten, but this time with Anne Baxter in the role of Jennie.

== See also ==
* Since You Went Away Love Letters Duel in the Sun

==References==
 

==External links==
*  
*  
*  
*  
* Walker, John A.  . artdesigncafe. (23 February 2011). Retrieved 2 July 2011.

Streaming audio Academy Award Theater: December 4, 1946
*   on Lux Radio Theater: October 31, 1949

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 