Take the Money and Run
 
{{Infobox film
| name           = Take the Money and Run
| image          = Original movie poster for the film Take the Money and Run.jpg
| caption        = Theatrical release poster
| director       = Woody Allen
| producer       = Charles Joffe
| writer         = Woody Allen Mickey Rose
| narrator       = Jackson Beck
| starring       = Woody Allen Janet Margolin Louise Lasser
| music          = Marvin Hamlisch
| cinematography = Lester Shorr
| editing        = Paul Jordan Ron Kalish Palomar Pictures International
| distributor    = Cinerama Releasing Corporation
| released       =  
| runtime        = 85 minutes
| language       = English
| awards         =
| budget         = $1,530,000 "ABC Motion Pictures|ABCs 5 Years of Film Production Profits & Losses", Variety (magazine)|Variety, 31 May 1973 p 3 
| gross          = $3,040,000  (rentals)  
}} comedic mockumentary directed by Woody Allen and starring Allen and Janet Margolin (with Louise Lasser in a small role). Written by Allen and Mickey Rose, the film chronicles the life of Virgil Starkwell (Woody Allen), an inept bank robber.    Filmed in San Francisco and San Quentin State Prison,    Take the Money and Run received Golden Laurel nominations for Male Comedy Performance (Woody Allen) and Male New Face (Woody Allen), and a Writers Guild of America Award nomination for Best Comedy Written Directly for the Screen (Woody Allen, Mickey Rose).   

==Plot==
Virgil Starkwell (Woody Allen) enters a life of crime at a young age. The "plot" traces his crime spree, his first prison term and eventual escape, the birth and growth of his family, as well as his eventual capture at the hands of the Federal Bureau of Investigation|FBI. His multiple crimes include stealing a pane of glass from a jewelry store, robbing a pet store and carving bars of soap into guns to escape from jail. He also robs a man who turns out to be his former friend who reveals he is now a cop, and the movie ends with Woody admitting he got 800 years in prison, but "with good behavior, can get that cut in half". Starkwell grew up in New Jersey, and played the cello (badly) in his towns marching band.

==Cast==
* Woody Allen as Virgil Starkwell
* Janet Margolin as Louise
* Marcel Hillaire as Fritz the Director
* Jacquelyn Hyde as Miss Blair
* Lonny Chapman as Jake the Convict
* Jan Merlin as Al the Bank Robber James Anderson as Chain Gang Warden Howard Storm as Fred
* Mark Gordon as Vince
* Micil Murphy as Frank
* Minnow Moskowitz as Joe Agneta
* Nate Jacobson as The Judge
* Grace Bauer as Farm House Lady
* Ethel Sokolow as Mother Starkwell
* Dan Frazer as Julius Epstein the Psychiatrist
* Henry Leff as Father Starkwell
* Mike ODowd as Michael Sullivan
* Louise Lasser as Kay Lewis   

==Production== Casino Royale (1967), in which he had appeared two years previously. This film marked the first time Allen would perform the triple duties of writing, directing, and acting in a film. The manic, almost slapstick style is similar to that of Allens next several films, including Bananas (film)|Bananas (1971) and Sleeper (1973 film)|Sleeper (1973).

Allen discussed the concept of filming a documentary in an interview with Richard Schickel:
 
 on location in San Francisco,  including one scene set in Ernies restaurant, whose striking red interior was immortalized in Alfred Hitchcocks Vertigo (film)|Vertigo (1958). Other scenes were filmed at San Quentin State Prison,   where 100 prisoners were paid a small fee to work on the film. The regular cast and crew were stamped each day with a special ink that glowed under ultra-violet light so the guards could tell who was allowed to leave the prison grounds at the end of the day. (One of the actors in the San Quentin scenes was Micil Murphy, who knew the prison well: he served five and a half years there, for armed robbery, before being paroled in 1966.)

Allen initially filmed a downbeat ending in which he was shot to death, courtesy of special effects from A.D. Flowers. Reputedly the lighter ending is due to the influence of Allens editor, Ralph Rosenblum, in his first collaboration with Allen.

==Reception==

===Box Office===
By 1973 movie had earned rentals of $2,590,000 in North America and $450,000 in other countries. After all costs were deducted, it reported a loss of $610,000. 

===Critical response===
The film received mostly positive reviews. Vincent Canby of The New York Times described it as "a movie that is, in effect, a feature-length, two-reel comedy—something very special and eccentric and funny", even though toward the end "a certain monotony sets in" with Allens comedy rhythm.    Roger Ebert of the Chicago Sun-Times found the film to have many funny moments, but "in the last analysis it isnt a very funny movie", with the fault lying with its visual humor and editing.    In October 2013, the film was voted by the Guardian (newspaper)|Guardian readers as the sixth best film directed by Woody Allen. 

On the review aggregator web site Rotten Tomatoes, the film holds a 90% positive rating from top film critics based on 18 reviews, with one of the two negative reviews coming from Roger Ebert. The film holds a 77% positive audience rating based on 11,375 ratings.   

===Awards and nominations===
* Golden Laurel Nomination   for Male Comedy Performance (Woody Allen)
* Golden Laurel Nomination for Male New Face (Woody Allen)
* Writers Guild of America Award Nomination for Best Comedy Written Directly for the Screen (Woody Allen, Mickey Rose). 

===American Film Institute recognition===
* AFIs 100 Years... 100 Laughs #67 
* AFIs 100 Years... 100 Movie Quotes:
:Bank Teller #1: "Does this look like gub or gun?"
:Bank Teller #2: "Gun. See? But whats abt mean?"
:Virgil Starkwell: "Its act. A-C-T. Act natural. Please put fifty thousand dollars into this bag and act natural."
:Bank Teller #1: "Oh, I see. This is a holdup?"

==References==
 

==External links==
*  
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 