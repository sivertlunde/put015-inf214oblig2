The Old Homestead
{{Infobox film
| name           = The Old Homestead
| image          = The Old Homestead.jpg
| image_size     =
| caption        =
| director       = William Nigh
| producer       = M. H. Hoffman
| writer         = Scott Darling (story) and Denman Thompson (play)
| narrator       =
| starring       = Howard Jackson (uncredited)
| cinematography = Harry Neumann
| editing        = Mildred Johnston
| distributor    = Liberty Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         =
}}
The Old Homestead is a 1935 American romantic western musical film directed by William Nigh.

==Cast==
*Mary Carlisle ...  Nancy Abbott
*Lawrence Gray ...  Bob Shackleforth
*Willard Robertson ...  Uncle Jed Dorothy Lee ...  Elsie Wilson
*Edward J. Nugent ...  Rudy Nash (as Eddie Nugent)
*Lillian Miles ...  Peggy
*Fuzzy Knight ...  Lem
*Eddie Kane ...  Mr. Wertheimer
*Harry Conley ...  J. Wilberforce Platt, Press Agent Tim Spencer ...  Vern, Member of Sons of the Pioneers (as Vern Spencer)
*Bob Nolan ...  Bob, Member of Sons of the Pioneers
*Roy Rogers ...  Len, Member of Sons of the Pioneers
*Hugh Farr ...  Hugh, Member of Sons of the Pioneers
*Sally Sweet ...  Singer

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 