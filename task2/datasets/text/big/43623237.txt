Moondru Dheivangal
{{Infobox film
| name           = Moondru Dheivangal
| image          =
| image_size     =
| caption        =
| director       = Dada Mirasee
| producer       = K. R. Seenivasan N. Naga Subramaniyam
| writer         = Madhusudan Kalekar
| screenplay     = Chitralaya Gopu
| starring       = Sivaji Ganesan Chandrakala Nagesh R. Muthuraman
| music          = M. S. Viswanathan
| cinematography = K. S. Prasad
| editing        = N. M. Sankar
| studio         =
| distributor    = Sri Bhuvaneswari Movies
| released       =  
| country        = India Tamil
}}
 1971 Cinema Indian Tamil Tamil film,  directed by  Dada Mirasee and produced by K. R. Seenivasan and N. Naga Subramaniyam. The film stars Sivaji Ganesan, Chandrakala, Nagesh and R. Muthuraman in lead roles. The film had musical score by M. S. Viswanathan.    The film was remade in Hindi as Teen Chor.

==Cast==
*Sivaji Ganesan
*Chandrakala
*Nagesh
*R. Muthuraman
*V. K. Ramasamy (actor)|V. K. Ramasamy
*Sivakumar
*V. S. Raghavan
*Senthamarai
*M. R. R. Vasu

==Soundtrack==
The music was composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Kannadasan || 03.30
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 