Stranger Than Paradise
{{Infobox film
| name = Stranger Than Paradise
| image = Strangerthanparadise.jpg
| caption = 1984 movie poster
| alt =
| director = Jim Jarmusch
| producer = Sara Driver
| writer = Jim Jarmusch
| starring = John Lurie  Eszter Balint  Richard Edson  Cecillia Stark
| music = John Lurie
| cinematography = Tom DiCillo
| editing = Jim Jarmusch  Melody London
| studio = Cinesthesia Productions Inc.
| distributor = The Samuel Goldwyn Company
| released = 1984
| runtime = 89 minutes
| country = United States  West Germany English Hungarian Hungarian
| budget = US$100,000
| gross = $2,436,000
}}
Stranger Than Paradise is a 1984 American absurdism|absurdist/deadpan comedy film. It was written and directed by Jim Jarmusch and stars jazz musician John Lurie, former Sonic Youth drummer-turned-actor Richard Edson, and Hungarian-born actress Eszter Balint. The film features a minimalist plot in which the main character, Willie, has a cousin from Hungary, Eva, stay with him for ten days before going to Cleveland. Willie and his friend Eddie eventually go to Cleveland to visit Eva.

==Plot==
The film is a three-act story about self-identified "Hipster (1940s subculture)|hipster" Willie (John Lurie), who lives in New York City, and his interactions with the two other main characters, Eva (Eszter Balint) and Eddie (Richard Edson).
 Hungary to stay with him for ten days because Aunt Lotte, whom she will be staying with, will be in the hospital. Willie at first makes it clear that he does not want her there. He even orders Eva to speak English for the ten-day period, not Hungarian. However, Willie soon begins to enjoy her company. This becomes especially true when Eva steals food items from a grocery store and gets a TV dinner for Willie, "Youre alright." He ends up buying her a dress, which she later discards. After ten days, Eva leaves, and Willie is clearly upset to see her go. Eddie, who had met Eva previously, sees her right before she goes.

The second act starts a year later and opens with a long take showing Willie and Eddie winning a large amount of money by cheating at a game of poker. Willie decides, because of all the money they now have, to leave the city. They decide to go to Cleveland to see Eva. However, when they get there they are just as bored as they were in New York. For example, they end up tagging along with Eva and a friend, Billy, to the movies. They eventually decide to go back to New York.

The final act begins with Willie and Eddie, on their way back to New York, deciding to go to Florida. They turn around and "rescue" Eva. The three of them get to Florida and get a room at a motel. They end up losing all of their money on dog races. At this point, they decide to go back and bet on horse races. Willie refuses to let Eva come along, so she goes out on the beach for a walk. She ends up being mistaken by a drug dealer, and is given a large sum of money. She goes back to the motel, leaves some of the money for Willie and Eddie, and writes them a note explaining that she is going to the airport, and then goes there. When she arrives, she discovers that the only flight to Europe left that day is to Budapest, which is where she originally came from. She decides to wait until the following day, and goes back to the motel. Willie and Eddie end up winning all of their money back at the horse races. But when they get back, Eva is gone, and Willie reads her note and they go to the airport to stop her from leaving. When they get there, Willie conceives a plan: buy a ticket, get on the plane, find Eva and convince her to stay in the states. What Willie didnt know was that at the time Eva made her decision about flying back to Budapest, there was only one free seat left on the plane. The second to last shot shows Eddie outside watching the plane leave, and he realizes what has happened. The final shot shows Eva back at the motel, returning to an empty room.

==Cast==
* John Lurie as Willie
* Eszter Balint as Eva
* Richard Edson as Eddie
* Cecillia Stark as Aunt Lotte
* Danny Rosen as Billy
* Rammellzee as Man With Money
* Tom DiCillo as Airline Agent
* Richard Boes as Factory Worker
* Rockets Redglare, Harvey Perr and Brian J. Burchill as Poker Players
* Sara Driver as Girl With Hat
* Paul Sloane as Motel Owner

==Background and production== Permanent Vacation Der Stand der Dinge (1982) that would enable the young director to shoot the 30-minute short subject film that would become Stranger Than Paradise. This short was released as a standalone film in 1982,  and shown as "Stranger Than Paradise" at the 1983 International Film Festival Rotterdam. When it was later expanded into a three act structure|three-act feature, that name was appropriated for the feature itself, and the initial segment was renamed "The New World".

==Release and reception== Grand Prix of the Belgian Film Critics Association, the Special Jury Prize at the Sundance Film Festival in 1985 and National Society of Film Critics Award for Best Picture of 1985, the film went on to win the Kniema Junpo Award for best foreign language film in 1987, the award for National Film Registry at the National Film Preservation Board, USA in 2002. 

The film made $2,436,000,  significantly more than its budget of around $100,000. 

===Critics===
Film critic Pauline Kael gave the film a generally positive review.
 

The film was voted the Best Picture of 1984 by the National Society of Film Critics. 

===Home media===
Stranger Than Paradise has been released on   film by the directors brother. An accompanying booklet features Jarmuschs 1984 essay "Some Notes on Stranger Than Paradise" as well as critical commentary by Geoff Andrew and J. Hoberman on Stranger Than Paradise and by Luc Sante on Permanent Vacation. 

==Legacy==
Stranger Than Paradise broke many conventions of traditional Hollywood filmmaking,  and became a landmark work in modern independent film.  According to allmovie, it is "one of the most influential movies of the 1980s", and cast "a wide shadow over the new generation of independent American filmmakers to come.  It is cited for giving "an early example of the low-budget independent wave that would dominate the cinematic marketplace a decade later."  The success of the film accorded Jarmusch a certain iconic status within arthouse cinema, as an idiosyncratic and uncompromising auteur exuding the aura of urban cool embodied by downtown Manhattan.   In a 2005 profile of the director for The New York Times, critic Lynn Hirschberg declared the film to have "permanently upended the idea of independent film as an intrinsically inaccessible avant-garde form". 
 100 Years...100 Movies list.  In 2003, Entertainment Weekly ranked the film #26 on their list of "The Top 50 Cult Films".  Empire Magazine put the film at 14 on its list of the 50 greatest independent films of all time. 

==Soundtrack==
{{Infobox album  
| Name = Stranger Than Paradise
| Type = soundtrack
| Artist = John Lurie
| Cover =
| Released = 1986
| Recorded =
| Genre = Experimental music
| Length =
| Label = Enigma
| Producer =
| Last album =
| This album = Stranger Than Paradise (1986)
| Next album = Down By Law (1988)
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =   
| noprose = yes
}}
The film features an original soundtrack written by John Lurie, who also stars in the film. The music is performed by The Paradise Quartet, consisting of Jill B. Jaffe (viola), Mary L. Rowell (violin), Kay Stern (violin), and Eugene Moye (cello). The recording engineer for the sessions was Ollie Cotton. The original song "I Put a Spell on You" by Screamin Jay Hawkins features prominently in the soundtrack.

===Track listing===
{{Track listing
|title1=Bella By Barlight
|title2=Car Cleveland
|title3=Sad Trees
|title4=The Lamposts Are Mine
|title5=Car Florida
|title6=Eva & Willies Room (Beer For Boys – Eva Packing)
|title7=The Good And Happy Army
|title8=A Woman Can Take You To Another Universe (Sometimes She Just Leaves You There)
}}

==References==
{{reflist|2|refs=
   
   
   
   
   
   
   
   
   
   |work=AllMusic.com |publisher=All Media Guide |accessdate=December 30, 2009}} 
 {{cite web
| url= http://community.seattletimes.nwsource.com/archive/?date=20000316&slug=4010335
| title=New on videotape
| work=The Seattle Times
| accessdate=May 11, 2009
| last=Hartl
| first=John
| date=March 16, 2000
}} 
 
{{cite web
| url=http://www.avclub.com/articles/jim-jarmusch,13869/
| title=Jim Jarmusch
| work=The A.V. Club
| accessdate=May 3, 2009
| date=May 19, 2004
| last=Tobias
| first=Scott
}}
 
   
   
 
{{cite news
| url= http://www.nytimes.com/2005/07/31/magazine/31JARMUSCH.html?pagewanted=print
| title=The Last of the Indies
| publisher=The New York Times Company
| accessdate=April 27, 2009
| work=The New York Times
| last=Hirschberg
| first=Lynn
| date=July 31, 2005
}}
 
   
}}

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 