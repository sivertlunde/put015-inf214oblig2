The Killer Is Loose
 
{{Infobox film
| name           = The Killer Is Loose
| image          = The Killer Is Loose 199791.jpg
| caption        = Theatrical release poster
| director       = Budd Boetticher
| producer       = Robert L. Jacks
| screenplay     = Harold Medford
| story          = John Hawkins Ward Hawkins
| starring       = Joseph Cotten Rhonda Fleming Wendell Corey Alan Hale Jr.
| music          = Lionel Newman
| cinematography = Lucien Ballard
| editing        = George A. Gittens
| studio         = Crown Productions
| distributor    = United Artists
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Killer Is Loose is a 1956 American crime film noir directed by Budd Boetticher and starring Joseph Cotten, Rhonda Fleming and Wendell Corey.   

==Plot==
An employee of a savings and loan company successfully robs a bank as an inside job. At first, bank employee Leon Poole (Wendell Corey) is considered a hero during the bank robbery, but the police quickly figure out hes involved in the crime. The cops catch up with Poole and his young wife at their apartment. Pooles wife is accidentally shot to death during a gunfight. Poole is arrested, convicted and sent to prison for the robbery.

While behind bars, Poole plans his escape and revenge on the policeman who killed his wife, Lt. Sam Wagner (Joseph Cotten). Poole figures the best way to exact revenge is to kill Wagners wife, Lila (Rhonda Fleming).

Poole escapes while working on a prison honor farm, murdering a guard. He kills again to gain access to a truck. Managing to avoid a highway roadblock set up by the police, Poole heads for the home of his former Army sergeant, Otto Flanders, holding his wife captive and then killing Flanders in cold blood.

Wagners wife is unaware that she is Pooles target. Wagner manages to get Lila safely out of town, but because she resents her husbands unwillingness to quit law enforcement, she decides to leave him for good. Lila then learns he was merely trying to protect her, so she heads back for their home, where Poole is waiting.

==Cast==
* Joseph Cotten as Det. Sam Wagner
* Rhonda Fleming as Lila Wagner
* Wendell Corey as Leon Foggy Poole
* Alan Hale Jr. as Denny, Detective (as Alan Hale)
* Michael Pate as Det. Chris Gillespie
* John Larch as Otto Flanders
* Dee J. Thompson as Grace Flanders
* John Beradino as Mac, Plainclothes Cop
* Virginia Christine as Mary Gillespie
* Paul Bryar as Greg Boyd

==Critical reaction==
The New York Times film critic, Bosley Crowther, found nothing original about the film, calling the lead actors (Cotten and Corey) "first rate" and the crime film "third rate." 

Critic Bruce Eder gave a more favorable review and wrote, "Budd Boetticher was a filmmaker of consummate skill and many surprises, as anyone whos seen his best Western dramas can attest. The Killer Is Loose (1956) only enhances his reputation in a totally unrelated genre, and in a stylistic mode thats about as far as he could get from his most familiar work. Using a cast of conventional—albeit top-flight—Hollywood professionals, Boetticher takes them out of the studio and puts them into an almost totally location-shot drama, and turns them loose in that naturalistic setting. The result is an array of performances that are as arresting as the script is filled with improbabilities; indeed, the narrative momentum of Boettichers direction, coupled with a handful of excellent performances, overcomes a script that is just a little too heavy on coincidences to otherwise play true." 

Critic Dennis Schwartz wrote, "A typical 1950s noir, distinguished by its rapid pace and taut script, that delves mainly into the character of the villain—making him out to be someone who went over-the-edge when he couldnt take being ridiculed as a failure, anymore...The suburban atmosphere and the no-nonsense style of telling the story add to the blandness of the story and the failure to elicit anything out of the ordinary to the build-up of the suspense that comes with the climax. The result is a watchable film which could be seen for the sense of nostalgia of the 1950s it evokes, a time when it was more receptive for noir to work as well as it does." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 