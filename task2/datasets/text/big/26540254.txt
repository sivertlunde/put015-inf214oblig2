Cardinal Richelieu (film)
{{Infobox film
| name           = Cardinal Richelieu
| image_size     =
| image	         = Cardinal Richelieu FilmPoster.jpeg
| caption        =
| director       = Rowland V. Lee
| producer       = Darryl F. Zanuck Cameron Rogers
| narrator       = Alfred Newman
| editing        = Edward Arnold
| studio         = 20th Century Pictures
| distributor    = United Artists
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
}} American historical Edward Arnold Edward Bulwer-Lytton Louis XIII.

==Cast==
* George Arliss - Cardinal Richelieu
* Maureen OSullivan - Lenore Edward Arnold - Louis XIII
* Cesar Romero - Andre de Pons
* Douglass Dumbrille - Baradas
* Francis Lister - Gaston
* Halliwell Hobbes - Father Joseph
* Violet Kemble Cooper - Queen Marie
* Katharine Alexander - Anne of Austria
* Robert Harrigan - Fontailles
* Joseph Tozer - De Bussy
* Lumsden Hare - Gustavus Adolphus of Sweden Russell Hicks - Le Moyne
* Keith Hitchcock - Duke DEpernon
* Murray Kinnell - Duke of Lorraine
* Herbert Bunston - Duke of Normandy Duke of Buckingham
* Boyd Irwin - Austrian Prime Minister
* Leonard Mudie - Olivares William Worthington - Kings Chamberlain David Clyde as Innkeeper

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 