Querô
{{Infobox film
| name           = Querô
| image          = Querô Film Poster.jpg
| caption        = Theatrical release poster
| director       = Carlos Cortez
| producer       = Caio Gullane   Fabiano Gullane   Débora Ivanov
| writer         = Carlos Cortez
| starring       = Maxwell Nascimento  Leandro Carvalho  Eduardo Chagas  Milhem Cortaz   Nildo Ferreira
| music          = André Abujamra
| cinematography = Hélcio Alemão Nagamine
| editing        = Paulo Sacramento
| studio         = Gullane Filmes 
| distributor    = Downtown Filmes
| released       =  
| runtime        = 88 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$144,823  ($64,575)
}}
Querô is a 2007 Brazilian drama film directed by Carlos Cortez and starring Maxwell Nascimento, Leandro Carvalho, Eduardo Chagas, Milhem Cortaz and Nildo Ferreira. The film is based on the 1976 novel Uma Reportagem Maldita - Querô by Plínio Marcos.   

==Plot==
Querô (Maxwell) is an orphan teenage who lives from side to side, lost in the streets near the port of Santos, São Paulo|Santos. The son of a prostitute (Maria Luisa Mendonça), he is unaware of his father. His mother committed suicide when he was a baby taking kerosene. After being beaten by the owner of a pension (Ângela Leal), he flees. The boy starts living of expedients and involved in petty theft. He ends up on Febem, where he explodes all his revolt against the world.

Out of jail, Querô finds support in Gina (Claudia Juliana), which leads him to an Evangelical Church, where he falls for the pastors niece, Lica (Alessandra Santos). However, the boy lives in a world marked by the determinism, making impossible for him to escape from marginality.   

== Production ==
=== Casting ===
Tests were conducted with more than 1200 kids, from 12 to 21 years, in the cities of Santos, São Paulo|Santos, Cubatão, Guarujá and São Vicente, São Paulo|São Vicente. Approximately 200 attended actors workshops coordinated by the preparer of actors Luiz Mário Vicente. Querô counted with the participation of 40 teenagers from the region of Santos port, which integrated the Querô Workshops.   
==References==
 

 
 
 
 
 
 