Album (film)
{{Infobox film
| name = Album
| image =
| caption =
| director = Vasanthabalan
| producer = Rajam Balachander Pushpa Kandaswamy
| story = Vasanthabalan
| screenplay = Vasanthabalan S. Ramakrishnan (dialogues)
| starring = Aryan Rajesh Shrutika
| music = Karthik Raja
| cinematography = D. K. Bharanidharan
| editor = V. T. Viyayan
| studio = Kavithalayaa Productions
| released = 4 October 2002
| country = India
| language = Tamil
}}

Album is a 2002 Indian Tamil romantic drama film written and directed by Vasanthabalan, starring Aryan Rajesh and Shrutika. The story portrays the love and affection between two families. The film, produced by Kavithalayaa Productions, was Vasanthabalans directorial debut and became a commercial failure, remaining his only unsuccessful venture.  It is however best known for featuring the song "Chellame Chellam" from Karthik Rajas soundtrack to the film, which was award-winning playback singer Shreya Ghoshals first song in Tamil language.

==Cast==
* Aryan Rajesh as Jeevan
* Shrutika as Viji
* Saritha as Shantha
* Prakash Raj Vijayakumar
* Balachandra Menon
* Nizhalgal Ravi
* Karunas

==Soundtrack==
The soundtrack, featuring seven tracks, was composed by Karthik Raja. The song "Chellame Chellam" became very popular and emerged a chartbuster.  It was notably the first Tamil song rendered by acclaimed singer Shreya Ghoshal for a Tamil film soundtrack. Karthik Rajas father, Ilaiyaraaja, and his sister, Bhavatharini, had performed each one song as well.

{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Thathalikudhey 
| lyrics1         = 
| extra1          = Karthik (singer)|Karthik, Sadhana Sargam
| length1         = 5:12 
| title2          = Chellame Chellam
| lyrics2         =  Hariharan
| length2         = 5:13
| title3          = Kadhal Vanoli
| lyrics3         = 
| extra3          = Harish Raghavendra, Sujatha Mohan
| length3         = 6:18
| title4          = Muttaikull
| lyrics4         = 
| extra4          = Bhavatharini
| length4         = 2:03
| title5          = Pillai Thamarai
| lyrics5         = 
| extra5          = Madhu Balakrishnan
| length5         = 5:18
| title6          = Thaazhampoo
| lyrics6         = 
| extra6          = Ilaiyaraaja, Sadhana Sargam
| length6         = 4:47
| title7          = Nilave
| lyrics7         = 
| extra7          = Shankar Mahadevan
| length7         = 5:19
}}

==References==
 

 
 
 

 
 
 
 
 
 


 