Oppam Oppathinoppam
{{Infobox film
| name           = Oppam Oppathinoppam
| image          =
| caption        =
| director       = Soman
| producer       = KH Khan Sahib
| writer         = Bharath Chandran Kaloor Dennis (dialogues)
| screenplay     = Kaloor Dennis Shankar Menaka Menaka Lalu Alex
| music          = Jerry Amaldev
| cinematography = Divakara Menon
| editing        = K Sankunni
| studio         = Krishna Hare Movies
| distributor    = Krishna Hare Movies
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Menaka and Lalu Alex in lead roles. The film had musical score by Jerry Amaldev.   

==Cast==
*Mohanlal Shankar
*Menaka Menaka
*Lalu Alex Madhuri
*Mala Aravindan Meena

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhoomi karangunnundoda || K. J. Yesudas, Bichu Thirumala || Bichu Thirumala || 
|-
| 2 || Kambili megham puthacha || K. J. Yesudas, KS Chithra || Bichu Thirumala || 
|-
| 3 || Puzhayil ninnetho || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 