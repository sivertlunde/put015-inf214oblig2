Madame Aema 8
{{Infobox film
| name           = Madame Aema 8
| image          = Madame Aema 8.jpg
| caption        = Theatrical poster for Madame Aema 8 (1993)
| film name      = {{Film name
 | hangul         =   8
 | hanja          =   8
 | rr             = Aemabuin 8
 | mr             = Aemapuin 8}}
| director       = Suk Do-won 
| producer       = Choe Chun-ji
| writer         = Suk Do-won
| starring       = Ru Mina Kang Eun-ah
| music          = Gang In-hyeok
| cinematography = Ham In-ha
| editing        = Cho Ki-hyung
| distributor    = Yun Bang Films Co., Ltd.
| released       =  
| runtime        = 100 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Madame Aema 8 (hangul|애마부인 8 - Aema Buin 8) is a 1993 South Korean film directed by Suk Do-won. It was the eighth entry in the Madame Aema series, the longest-running film series in Korean cinema. 

==Plot==
Following the multiple-Aema theme started in Madame Aema 6, this entry in the series has two women named Madame Aema. Both women are dancers, and are friends who differ in their thoughts on marriage. One believes in remaining single, and the other believes in marriage, and does so. After being disappointed with her husbands cheating and gambling, she leaves him. They are later reconciled after the husband repents of his behavior.   

==Cast==
* Ru Mina: Madame Aema 
* Kang Eun-ah: Madame Aema
* No Hyeon-u: Hyeon-woo
* Won Seok: Dong-hyeob
* Yoo Seong
* Seo Chang-sook
* Gil Dal-ho
* Cho Hak-ja
* Sue Young-suk

==Bibliography==

===English===
*  
*  
*  

===Korean===
*  
*  
*  
*  

===Contemporary reviews===
* 1993-04-03. "끝없이 춤추는 ‘애마부인’ 시리즈… 제8편 곧 극장개봉 (Endless Aema Buin series, upcoming release of the 8th episode). Hangeorae (한 겨 레) newspaper

==Notes==
 
 
 
 
 
 
 


 
 