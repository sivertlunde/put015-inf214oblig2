The Three Men of Melita Žganjer
{{Infobox film
| name           = The Three Men of Melita Žganjer
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Snježana Tribuson
| producer       = 
| writer         = Snježana Tribuson
| narrator       = 
| starring       = Mirjana Rogina Goran Navojec Suzana Nikolić Sanja Vejnović Ena Begović Filip Šovagović Ivo Gregurević Ljubomir Kerekeš
| music          = Darko Rundek
| cinematography = Goran Mećava
| editing        = Marina Barac
| studio         = Kvadar Film Croatian Radiotelevision
| distributor    = 
| released       = 1998
| runtime        = 97 minutes
| country        = Croatia Croatian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1998 award-winning Croatian film directed by Snježana Tribuson, who also wrote the screenplay for the film. 

==Synopsis== UNPROFOR soldier in a locally produced film dealing with the Croatian War of Independence, Melita succeeds in meeting him, but is immediately disappointed as the Spanish actor is nothing like his character in the telenovela. In the end, Melita learns that Janko likes her and ends up with him.

==Cast==
*Mirjana Rogina as Melita Žganjer
*Goran Navojec as Janko
*Suzana Nikolić as Višnja
*Sanja Vejnović as Eva
*Ena Begović as Maria
*Filip Šovagović as Juan / Felipe Mulero
*Ivo Gregurević as Jura
*Ljubomir Kerekeš as Žac

==Awards== Best Screenplay (Snježana Tribuson), Best Scenography (Vladimir Domitrović), Film Editing (Marina Barac), Best Supporting Actress (Suzana Nikolić) and Best Supporting Actor (Ivo Gregurević).    The film was also screened at the 2000 Cinequest Film Festival. 

==See also==
*Cinema of Croatia

==References==
 

==External links==
* 
* 
*  at Filmski-Programi.hr  

 
 
 
 
 
 