Hum Hain Bemisaal
 
{{Infobox film
| name           = Hum Hain Bemisal
| image          = HumHainBemisaal.jpg
| writer         = Saroj Khan
| starring       = Akshay Kumar Sunil Shetty Shilpa Shirodkar Madhoo
| director       = Deepak Bahry
| producer       = Mrs. Geeta Gupta
| screenplay     = S. Khan
| music          = Anu Malik
| lyrics         = Nasir Hakim
| cinematography = Damodar Naidu
| editor         =
| released       = 16 December 1994
| runtime        = 152 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Hum Hain Bemisal is an Indian film directed by Deepak Bahry and released in 1994. It stars Akshay Kumar, Sunil Shetty,  Shilpa Shirodkar and Madhoo.

==Cast==
* Akshay Kumar ... Vijay Sinha
* Sunil Shetty ... Michael
* Shilpa Shirodkar ... Didi
* Madhoo ... Marya Pran ... DSouza Rami Reddy ... Tutisha
* Avtar Gill ... Kaliya
* Kunika ... Tutishas girl
* Arun Bakshi
* Vikas Anand
* Gavin Packard

==Plot==

Tuti Shah murders Kishan to defend his anti-social business and trap Kishans friend Dsouza (Pran (actor)|Pran) in his murder. DSouza gets life imprisonment. DSouza seven years old son Michael (Sunil Shetty)runs away in frustration when his best friend and Kishans son Vijay Sinha (Akshay Kumar) also kicks him out for being the son of his fathers murderer. Time passes away. DSouza comes out from jail and meets father of the church. Father informs him that he could not trace out his son Michael but according to his wish he had made police inspector to his friend Kishans son Vijay Sinha with the help of the money he used to send from the jail. DSouza decides to bring the truth of the murder of Vijay Sinhas father to his knowledge by getting Tuti Shah arrested. At the same time Tuti Shah comes to know about deprived Michael and his sympathy and attachment towards the poor. He black mails Michael emotionally and gets his rival Bakhshi Jang Bahadur killed by Michael. During his attack a hotel dancer Maria (Madhoo) loses her eyesight by the flames of Michaels pistol. Michael himself feels responsible for this accident and without disclosing his identity starts helping and taking care of Maria. Both fall in love each other Now the motto of Michaels life is to get Marias eyesight back. Tuti Shash again black mails Michael emotionally. He offers him money for the operation of Marias eyes and asks to kill his another enemy Inspector Dharam Das in exchange. But in this deal Tuti Shah deceives Michael. On the other side Inpsector Vijay Sinha chasing Michael for both the murders and wants to arrest the mafia don Tuti Shah also. To defend a pick pocket Meena (Shilpa Shirodkar) from hoodlums Inspector Vijay Sinha himself falls in love with her. Inspector Vijay Sinha arrests Tuti Shah red-handed but fails to do anything against him as a lawman. He resigns from his service and decides to eliminate Tuti Shah. Now DSouza, Michael and Vijay Sinha are at one side and Tuti Shah and his personal army on the other side.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tujhse Kya Chori Hai"
| Kumar Sanu, Sadhana Sargam
|- 
| 2
| "Qatil Aankhon Wale"
| Alisha Chinai
|- 
| 3
| "Mera Lehenga Gher Ghumer" Poornima
|- 
| 4
| "Duma Dum Mast Kalandar"
| Baba Sehgal, Alka Yagnik
|- 
| 5
| "Chaahe Chudi Toot Jaaye"
| Alisha Chinai
|- 
| 6
| "Chori Chori Chori"
| Anu Malik, Alisha Chinai
|}

==External links==
*  

 
 
 


 