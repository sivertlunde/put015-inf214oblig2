Thank You (2013 film)
{{Infobox film
| name = Thank You
| image = ThankYouMalayalamfilm.jpg
| caption =
| director = V. K. Prakash
| producer = Shahul Hammeed Marikar
| writer = Arun Lal Sethu
| cinematography = Arvind Krishna
| editing = Babu Rathnam Uthara
| music = Bijibal
| studio = Marikar Films
| distributor =
| released =  
| runtime = 103 minutes
| country = India
| language = Malayalam
}} Malayalam Drama drama thriller film written by Arun Lal and directed by V. K. Prakash, starring Jayasurya, Honey Rose and Sethu. The film is produced by Shahul Hameed Marikar under the banner of Marikar Films and features music composed by Bijibal, whilst cinematography is handled by Arvind Krishna and is edited by Babu Rathnam.  Although the film earned back its revenue from satellite rights, it did not run well in theatres.

==Plot==
The film is shown to be a reflection of the current socio-political system, based on the violence against women.  It revolves around a person who arrives in Trivandrum city who takes a ride from place to place in an Autorickshaw.  His name or whereabouts are not revealed, and he just travels from place to place in the auto rickshaw. The questions about his whereabouts form the rest of the story.

==Cast==
*Jayasurya
*Honey Rose as Remya 
*Sethu Kailash as Shankar 
*Tini Tom as Sugunan
*Mukundan
*Sudheer Karamana  
*P. Balachandran as Balettan
*Aishwarya Devan as Sruthi 
*Mridul as Gopan
*Saiju Kurup as Arun

==Production==

===Development===
The movie kicked off a series of films by the production firm   scribe Arun Lal, was the first among the eight films to go on floors. On the film, the scriptwriter had revealed before shoot that the film is a thriller that unravels in Trivandrum and the plot revolves around a person who comes to the city. Also that they do not introduce the person by his name or whereabouts, and he just travels from place to place in an auto rickshaw. The questions about his whereabouts form the rest of the story. Mumbai Police and the actor had asked him to approach the director V. K. Prakash, who had been astonished over by the story and requested for a complete script as soon as possible to start work on the film. The project took shape really fast over a few days. Jayasurya told that "he thinks its a gem of a script and Arun Lals a master scriptwriter to come up with it in such a short time. Even VKP is thrilled about the movie." 

===Casting===
Jayasurya was cast by the script writer as he was the first one of cast and crew which the script was told to. Honey Rose who had been in the directors previous film Trivandrum Lodge paired with Jayasurya was cast to do the role as a modern wife. Remya Nambeesan was also considered for a prominent role. Actor Sethu who played the role of police officer Bhaskar in the Tamil film Mynaa was cast to play the one of the lead role as the director was impressed with his performance. There was also rumours of Sunny Wayne at the time a rising actor to be part of the film.  Tini Tom was also cast to play a police officer after being part of the director V. K. Prakashs 2011 film Beautiful (2011 film)|Beautiful. Other people announced as per cast were Kailash (actor)|Kailash, Mukundan, Balachandran and Aishwarya Devan. The actress Aishwarya, who debuted in Tamil films with Yuvan, had now signed her next in prominent and one of her best roles. Aishwarya Devan plays a reporter in the film, a challenging role which is an emotional thriller.

===Filming=== puja of Thank You and seven other films by Marikar Films was conducted on April 9, 2013. Principal photography began on April 11, 2013 at Trivandrum in the residential area Chackai.  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 