Harry in Your Pocket
{{Infobox Film
| name           = Harry in Your Pocket
| image          = Harry_in_your_pocket.jpg
| caption        = original film poster
| director       = Bruce Geller
| producer       = Bruce Geller Alan Godfrey James Buchanan Ronald Austin
| starring       = James Coburn Michael Sarrazin Trish Van Devere Walter Pidgeon
| music          = Lalo Schifrin
| cinematography = Fred J. Koenekamp
| editing        = 
| distributor    = United Artists
| released       =  1973
| runtime        = 103 min. 
| country        = 
| awards         = 
| language       = 
| budget         = 
| gross = $1,500,000 (US/ Canada rentals) 
| 
}}
Harry in Your Pocket is a 1973 film, written by James Buchanan and Ronald Austin and directed by Bruce Geller, starring James Coburn, Michael Sarrazin, Trish Van Devere and Walter Pidgeon.

The movie was filmed in Victoria, BC, Salt Lake City, Utah and Seattle, Washington with the then-mayor of Seattle, Wes Uhlman contributing a cameo appearance.

==Plot== Union Station. Sandy Coletto (Trish Van Devere), waiting for a train to Chicago, watches him with amusement, securing her own possessions when Ray sits close by. He does, however manage to get away with her watch, though she chases him down to get him to confess. While talking with Ray, however, her purse and suitcase, both unwatched, are spirited away by an unseen thief.

Bereft of all her possessions and money, shes stranded in Seattle. Ray promises to help her get back on the road, but his way of raising funds is to sell his inventory of stolen watches – watches so poor that the fence is only willing to pay a fraction of the money Ray promised Sandy. 

As a favor, the fence tips Ray off to the presence of a recruiter for a "wire mob" – a traveling professional pickpocketing band – in town, who will be hanging out at a restaurant in the Pioneer Square district. Ray decides to try it out; Sandy, whos formed an emotional bond with Ray, decides to tag along. At the restaurant they meet Casey, who introduces them to Harry, Caseys protege and "cannon" – the term for a known and skilled professional pickpocket. 

After discussion and doubts on Harrys part, (Sandy proves something of a natural as she was able to lift Caseys cigarette case undetected) Sandy and Ray are given money to buy better clothes and Harry and Casey begin to train them in the parts theyre to play – principally that of the "stall", or the members of the team who will provide distraction in order for Harry to get into the mark and make the "dip".

Harry also inculcates them into the groups modus operandi and operations. The team travels "first-class – everything the best … the best food, the best clothes, the best hotels". In this way they are able to blend into and appear as the classes they are trying to pickpocket. Sandy, being physically attractive, gives the team added advantages in that male marks can presumably mostly have their attention diverted by an attractive young lady in revealing fashions.

During the course of the movie the mob travels from Seattle to Victoria BC to Salt Lake City, Sandy and Ray becoming progressively more adept in their roles. Along the way, Rays ambition to become more than a mere "stall" and the tension between Ray and Harry brought on by the presence of Sandy produce stresses on the group but, by the time the team arrives in Salt Lake City the wire mob have merged into a more-or-less cohesive and successful unit. In the meantime, though, Rays ambition has gotten him, through ingratiation, to convince Casey to take him on as a student. Caseys training turns Ray into a much more accomplished pickpocket and, when in Salt Lake City, Ray and Sandy begin working on their own time and keeping the take. Moreover, Ray keeps the id and effects of the people he lifts from, wanting to study them, two things that threaten the survival of the group and makes Harry furious with Ray.

Events come to a head in Salt Lake City when Casey is arrested when a botched handoff from Ray allows the victim to see his wallet in plain sight protruding from Caseys jacket pocket. Caseys case turns complicated when cocaine is found on him and becomes more than just a case of springing him from jail on a pickpocketing charge. Harry decides to raise extra funds quickly by hitting a regional horse show at the Salt Palace arena (admitting that the take could be high – but also the risk), and Ray, who had decided to split from the group, agrees to go in for Caseys sake. Working over the Salt Palace crowd goes rather smoothly, but building security have been alerted that Harry is in town and its only a matter of time before they catch him – deliberately taking the fall by disposing of the evidence in a wastebasket rather than handing off to Ray.

The film closes as Sandy and Ray, above suspicion, watch as Harry is led away by SLC police and building security to an uncertain future.

==The Ways Of The "Wire Mob"==

::"Harry never holds. Not for a minute, not for thirty seconds. Harry never holds."
::::-"Harrys Law"

The movie is notable for giving a presumably-fictionalized but matter-of-fact way of how a wire mob operates and its jargon. In fact, its said that the actors portraying the "cannon squad" in Seattle that tell Harry its time to leave town were former pickpockets themselves and served as technical advisors to the movie.

Early on in the movie, when being introduced, Harry bluntly and directly describes the lifestyle and image they want to project. Everything, the best – the best food, the best clothes, the best hotels. By adopting an air of sophistication and style, the team appears a member of the class theyre trying to victimize – thus avoiding suspicion. They also never take wallets at the hotel theyre staying at, even though theyd probably be lucrative, in order not to bring attention to themselves. Additionally, Sandys natural attractiveness, accented by fashions designed to highlight sex-appeal, make her a highly attractive distraction.

The movie makes extensive reference to a system of cant – pickpockets jargon. Some examples:

* Poke - The wallet, the object of the theft.
* Kick - any area on a person where a poke commonly resides. For instance, a prat kick is the marks back pocket; the patch kick is the outside pocket in a suit jacket.
* Mark - The intended victim.
* Stall - Members of the team, working under the cannon who distract or "artfully stumble" into people giving the chance for the dip to occur.
* Cannon - The actual pickpocket, the leader of the group in this case. Also, apparently, the police slang term for same.
* Dip - The act of taking the poke.
* Steer - The member of the team who selects marks and telegraphs to the stalls and cannon where the poke will be found.

In the case of Harrys wire mob, Casey spots the mark and communicates to Harry where hell find the poke via gestures; e.g. if the poke is in the marks back pocket hell wipe his brow with a handkerchief and put the handkerchief in that pocket. Sandy and Ray then set up an appropriate stall situation, opening the way for Harry, who dips the poke and secrets it in a folded magazine or newspaper. Harry, as immediately as possible, hands the prize off to Ray, who carries it in a similar way, and Ray relays it to Casey, who retrieves money and valuables and disposes of the rest.

In return for working as stalls, Ray and Sandy get 20% of the groups take. Harry also supervises all transportation arrangements, hotel reservations, and provides "fall money" – bail money to get out of jail on pickpocketing charges and lawyers fees as needed.

===Harrys Law===

Amongst the blunter points Harry makes is what is seen as the most important rule: Harrys Law: Harry never holds. Not for a minute, not for thirty seconds. This means that as soon as Harrys hit the mark, someone must be on hand to take the poke off his hands, providing cover for Harry and allowing him to go on and hit more marks.

==Home Media==

Harry In Your Pocket was brought to DVD (or any other home video format), for the first time, on May 3, 2011, as part of the MGM Limited Edition Collection series.

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 