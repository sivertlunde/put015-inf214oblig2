The Drifter (1932 film)
{{Infobox film
| name           = The Drifter
| image_size     =
| image	=	The Drifter FilmPoster.jpeg
| caption        =
| director       = William A. OConnor
| producer       = Willis Kent Oliver Drake
| narrator       =
| starring       = William Farnum Noah Beery, Sr.
| music          = William Nobles
| editing        = Tom Persons
| distributor    =
| released       = 10 January 1932
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Drifter is a 1932 American film directed by William A. OConnor.

== Cast ==
*William Farnum as The Drifter
*Noah Beery, Sr. as John McNary
*Phyllis Barrington as Bonnie McNary
*Charles Sellon as Whitey
*Bruce Warren as Paul LaTour
*Russell Hopton as Montana
*Ann Brody as Marie
*Ynez Seabury as Yvonne

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 


 