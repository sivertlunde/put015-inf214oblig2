The Last Outpost (1935 film)
{{Infobox film
| name           = The Last Outpost
| image          =
| alt            = 
| caption        = Charles Barton Louis J. Gasnier
| producer       = E. Lloyd Sheldon
| screenplay     = Charles Brackett Frank Partos Philip MacDonald
| starring       = Cary Grant Claude Rains Gertrude Michael Kathleen Burke Colin Tapley Margaret Swope Billy Bevan
| music          = 
| cinematography = Theodor Sparkuhl
| editing        = Jack Dennis
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}} 1935 film Charles Barton, and produced by E. Lloyd Sheldon. Both Grants and Rains character find an interest in a woman played by Gertrude Michael, creating tension between the two close friends, who are both British soldiers in Kurdistan. The film was based on F. Britten Austins novel The Drum. The film was released on October 11, 1935, by Paramount Pictures.  

==Cast==
*Cary Grant as Michael Andrews
*Claude Rains as John Stevenson
*Gertrude Michael as Rosemary Haydon
*Kathleen Burke as Ilya
*Colin Tapley as Lt. Prescott
*Margaret Swope as Nurse Rowland
*Billy Bevan as Cpl. Foster
*Georges Renavent as Turkish major
*Jameson Thoma as Cullen
*Nick Shaid as Haidor
*Meyer Ouhayou as Armenian patriarch
*Frazer Acosta as Armenian officer
*Malay Clu as Armenian guard Robert Adair as Sergeant in generals office
*William Brown as Sgt. Bates Claude King as General
*Olaf Hytten as Doctor Frank Elliott as Colonel
*Ward Lane as Colonel
*Frank Dawson as Surgeon
*Ramsay Hill as Captain
*Mark Strong as Officer
*Carey Harrison as Officer
*Elspeth Dudgeon as Head Nurse
*Gwynne Shipman as Nurse

==References==
 

== External links ==
* 

 
 

 
 
 
 
 
 
 
 
 

 