Tumbling Tumbleweeds (1935 film)
{{Infobox film
| name           = Tumbling Tumbleweeds
| image          = Tumbling_Tumbleweeds_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Joseph Kane
| producer       = Nat Levine Magers 2007, p. 26. 
| screenplay     = Ford Beebe
| story          = Alan Ludwig
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Lucile Browne
}}
| cinematography = {{Plainlist| Ernest Miller William Bradford
}}
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 61 minutes 
| country        = United States
| language       = English
| budget         = $15,344 
}} Western film directed by Joseph Kane and starring Gene Autry, Smiley Burnette, and Lucile Browne.  Written by Ford Beebe, the film is about a cowboy who returns home after a five-year absence to find his father murdered and his boyhood pal accused of the dastardly deed. Tumbling Tumbleweeds features the songs "Riding Down the Canyon", "That Silver-Haired Daddy of Mine", and the Bob Nolan classic "Tumbling Tumbleweeds". 

==Plot==
Gene Autry (Gene Autry) returns to his home after a five-year absence as a singing cowboy with a group of strolling players that includes Smiley (Smiley Burnette) and Eightball (Eugene Jackson), who sell Dr. Parkers Painless Panacea. Genes father, a cattle barron and one of the original "nesters" in the West, was recently murdered during a conflict with his landlord.

While at an abandoned nesters cabin, the group is held up by Harry Brooks (Cornelius Keefe), whom Gene recognizes as his old friend. Wounded and semi-delirious, Harry induces Gene to hide him from the posse headed by Sheriff Manley (George Burton). The deputy later returns and tries to shoot Harry, but Gene chases him away.

In town, the deputy reports to Barney Craven (Edward Hearn), leader of a gang which is trying to silence Harry. Meanwhile, Gene and his friends set up a performance in town, but it is interrupted by Cravens men, who report that Harry is wanted for the murder of Genes father. Hastening to Harrys home, Gene confronts his former sweetheart Janet, now Harrys wife, and meets Janets younger sister Jerry (Lucile Browne), whom he had only known as a girl. They assure Gene of Harrys innocence and reveal that Harry and Genes father were about to sign a settlement over disputed water rights.

Now suspicious of Craven, Gene captures Craven, the deputy, and their cohorts by a series of clever ruses that land them in jail, and thereby vindicates Harry. Gene and Jerry marry and join Smiley and Eightball on the departing Parker wagon. Magers 2007, p. 27. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Smiley
* Lucile Browne as Jerry
* George "Gabby" Hayes as Dr. Parker
* Norma Taylor as Janet Brooks
* Edward Hearn as Barney Craven
* Eugene Jackson as Eightball
* Jack Rockwell as McWade
* George Chesebro as Henchman Connors
* Frankie Marvin as Shorty
* Cornelius Keefe as Harry Brooks (uncredited)
* George Burton as Sheriff Manley (uncredited)
* Bob Card as Deputy (uncredited)
* Champion as Genes Horse (uncredited) Magers 2007, pp. 26–27. 

==Production==

===Filming and budget===
Tumbling Tumbleweeds was filmed July 6–12, 1935. The film had an operating budget of $15,344 (equal to $ }} today), and a negative cost of $18,801. 

===Filming locations===
* Bakersfield, California, USA
* Barstow, California, USA
* Monogram Ranch, 24715 Oak Creek Avenue, Newhall, California, USA
* Victorville, California, USA 

===Stuntwork===
Having learned the art of screen fighting from Yakima Canutt, Gene Autry handled all of the fight scenes himself. Ken Cooper doubled for Autry during the dangerous riding sequences. George-Warren 2007, p. 141. 
* Tommy Coats
* Ken Cooper (Gene Autrys stunt double) Cliff Lyons 

===Soundtrack===
* "Ill Yodel My Troubles Away" (Gene Autry, Smiley Burnette) by Gene Autry
* "Cowboy Medicine Show" (Smiley Burnette) by Gene Autry and the Medicine Show troupe
* "Corn-fed and Rusty" (Smiley Burnette) by Smiley Burnette (vocals and accordion)
* "Riding Down the Canyon" (Gene Autry, Smiley Burnette) by Gene Autry
* "That Silver Haired Daddy of Mine" (Gene Autry, Jimmy Long) by Gene Autry
* "Tumbling Tumbleweeds" (Bob Nolan) by Gene Autry
* "Oh! Susanna" (Stephen Foster) by the Medicine Show troupe 

==Reception==
In his review of the DVD release of the film for DVD Talk, Stuart Galbraith IV wrote:
  }}

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 