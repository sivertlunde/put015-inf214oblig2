Awarapan
 
 

{{Infobox film
| name        = Awarapan 
| image       = Awarapan (DVD cover art).jpg
| caption     = Theatrical release poster
| director    = Mohit Suri
| producer    = Mahesh Bhatt  (Presenter)  Mukesh Bhatt
| co-producer = Vishesh Bhatt
| writer      = Shagufta Rafiq  (Screenplay & Dialogue)
| story       = Mahesh Bhatt
| starring    = Emraan Hashmi Shriya Saran Mrinalini Sharma Ashutosh Rana 
| cinematography = Raaj Chakravarti
| editing     = Aditya Joshi
| music       = Pritam
| released    =  
| country     = India Pakistan
| studio      = Vishesh Films
| distributor = Viacom 18 Motion Pictures
| runtime     = 126 minutes
| language    = Hindi
| gross      =  
| budget       =  
}}
 action crime-thriller thriller film produced by Mukesh Bhatt, Sohyl Khan and directed by Mohit Suri. It is the first joint production film between India and Pakistan.   It stars Emraan Hashmi, Shriya Saran and Mrinalini Sharma and Ashutosh Rana. Awarapan released in India on 29 June 2007 under the banner of Vishesh Films, to widespread critical acclaim. This films success was mostly credited to its immensely popular music score composed by Pritam. The movie is loosely based on a South Korean movie A Bittersweet Life.

The film was given clearance for screening in Pakistan by the Pakistan Film Censor Board and was released on 6 July 2007. The film did well at box office. Emraans performance was appreciated by audience and critics. Through the years Awarapan has acquired a "cult status" among the Indian audience, specially youngsters. Many critics and fans regard Awarapan as the best movie of Emraan Hashmi and Mohit Suris career. 

== Plot ==
The storyline revolves around Shivam (Emraan Hashmi), a heart broken and introvert atheist, gangster, and right-hand man of Malik (Ashutosh Rana), officially a businessman who runs a chain of hotels in Hong Kong. Shivam is given the responsibility of one of these hotels, which he bears capably and well, making it much more successful than Maliks other hotels. Malik holds Shivam in the highest esteem due to Shivams loyalty, reliability, and business acumen, and treats him like an adored son whom he entrusts with almost everything - indeed, more so than his biological son, Ronnie. This is the cause of much jealousy on Ronnies part against Shivam, but due to Maliks outspoken support for Shivam, Ronnie does not dare do openly oppose him. Shivam has a very close childhood friend, Kabir, who assists him with everything.

Maliks brother, Rajan (Ashish Vidyarthi), and Rajans spoilt son Munna nurture a grudge against Shivam because of Shivams repeated conflicts with Munna. Munna is mainly responsible for the start of most of these conflicts; he time and again puts Shivam down as Maliks slave, once by murdering an employee of Shivams hotel who resisted Munna in bed. When Shivam comes to investigate this incident, Munna taunts him, the dead girl, and dares him to tell Malik, resulting in Shivam lashing out against Munna and all his henchmen. This causes an escalation of the feud between Munna, his father Rajan, and Shivam.

One day, Malik asks Shivam to keep an eye on his young mistress Reema (Mrinalini Sharma), whom he suspects of having an affair with someone else, while he is away on a brief business trip. Reema is a young Pakistani girl, a devout Muslim, and a victim of sex trafficking, whom Malik had bought in the Bangkok flesh market. If Reema is found cheating on Malik, Shivam is to inform him immediately.

In Shivams interactions with Reema, he recalls his lost love, Aliyah (Shriya Saran) in a series of flasbacks. Aliyah was a devout Muslim, who Shivam had met by chance in the marketplace one day. The duo eventually fell in love, but when her father saw them together, he wrathfully confronted them, reprimanding Aliyah physically in public for maintaining physical contact with her lover in a non-private place. He also exposed Shivams gangster background to a shocked Aliyah, leading to Shivams instantly offering her father his gun, saying that he was willing to let go of everything, start anew, and legitimately provide for Aliyah if he is allowed to be with her. Aliyahs father recovers from his initial surprise quickly, and shoots at Shivam with the gun, who ducks to avoid the bullet. The bullet hits Aliyah instead, killing her and temporarily driving Shivam over the edge. A feeling of guilt and sorrow, at Aliyah having died because he tried to save his own life, remains with him hereafter. Aliyahs father instantly commits suicide when he realizes what he has done.

Shivam discovers one night that Reema has a secret boyfriend Bilal (Rehaan Khan), who convinces her to run away with him back to Pakistan. Shivam informs Malik, who orders Reemas elimination. However, when he confronts the couple to eliminate them, she dares him to shoot her, claiming that freedom is her right, requesting mercy "for Gods sake". Shivam is reminded of Aliyahs religious mannerisms, which bear a strong resemblance to Reemas, and is unable to shoot her, leaving her alive. Malik tries to reach Shivam, who severs contact with the former, leading to Malik finding out about his disobedience.

Shivam plans to quickly run away from Hong Kong back to India with Kabir, who is extremely unwilling to do so. Eventually, he betrays Shivam to Malik, whose regard for Kabir goes up. Malik has Shivam captured, and orders Ronnie to torture Shivam until he agrees to kill Reema when Shivam refuses to do so even after Malik offers him a second chance. Malik is shown to be genuinely hurt and sorrowful at what he perceives as Shivams betrayal, and hands over Shivams responsibilities in the business to Kabir. Malik makes up the old feud with Rajan and Munna, and merges their gangs and businesses, with Malik demanding that Shivam be brought before him alive so that Malik can kill him personally. This occurs in Kabirs presence, who is bitterly penitent to the point of not wishing to live any more over his betrayal of Shivam.

Shivam manages to escape Ronnie, and seeks refuge in the monastery of a monk whom he had once set free from certain death. He recovers from his injuries, and reminded of Aliyah and her belief in freedom, decides to not continue running all his life to save his own skin, but to "fulfill someone elses dreams" - namely, unite Bilal and Reema and send them to safety. He manages to save Bilal from Maliks henchmen, led by Kabir, whom Shivam still trusts and regards as his best friend, who tearfully reiterates that he is ready to die for Shivam, as he always was. Kabir takes Bilal to the monastery, where arrangements are made to ship Bilal and Reema to Hong Kong.

Shivam tracks Reema, killing Rajan when he taunts Reema and refuses to divulge where she is. At the cost of a knife wound to his side, he tricks Munna into revealing Maliks plans regarding Reema, which are to sell her to the person who "enjoys" her the most. Munna tells Shivam everything about it, including the location, believing Shivam to be crippled by his wounds, and is immediately killed by Shivam when he tries to run away upon realizing that is not the case. Shivam finds Reema and Malik, who is madly enraged at Shivams standing against him, when he had cared for Shivam so much, deriding Shivams new belief in God and freedom. Shivam kills Malik, escapes with Reema to the shipping docks, where Bilal is waiting. Kabir shows up to fight Maliks men and buy Shivam time, and is brutally murdered in the process. Bilal manages to force a reluctant Reema to escape to the ship, who is loath to leave Shivam behind. Ronnie and his men arrive and Shivam manages to kill them all in a gunfight, but is mortally wounded himself. As he lies dying, he comes to terms with his past, forgiving himself and envisioning Aliyah smiling down upon him. The movie ends with Reema in a conference in Pakistan, where she tells her story of the "awara" who sacrificed his life to save her, and incites those present to stand up against unwilling victims of trafficking.

==Cast==
*   gangster with a tragic past whose love has been murdered. Indian and Shivams loved one who induces the idea of God and Freedom into Shivam and was mistakenly murdered by her father due to the criminal life of Shivam.
*   girl who was forced into trafficking by some human trafficking people, Malik bought her from them and Malik even asks Shivam to keep an eye on her.
* Shaad Randhawa as Kabir: Shivams best friend since his childhood and he assists Shivam.
* Ashutosh Rana as Bharat Malik: A crime lord and boss of Shivam who treats Shivam like his own son.
* Salil Acharya as Ronnie Malik
* Rehaan Khan as Bilal
* Ashish Vidyarthi as Rajan Malik (Maliks brother)
* Purab Kohli as Munna (Son of Rajan Malik)

==Music==
The music for the film was composed by Roxen (band) & Annie Khalid and were recreated in musical arrangements by Pritam. All songs were extremely popular in 2007.  Some of the singers who have lent their voices for the album include Mustafa Zahid, Suzanne DMello, Rafaqat Ali Khan and Annie Khalid. The lyrics have been written by Sayeed Quadri, Asif Ali Baig and Annie Khalid.

The movie had a heavy influence of Pakistani music. All four tracks were borrowed from across the border and re-created by Pritam. Due to increased awareness of plagiarism, all the tracks have been properly credited to their original sources. The music was praised highly and appreciated by critics and music followers, it is the first Indian movie which consists all songs by Pakistani singers. Mahiya and To Phir Aao was chartbusters of that time . It was Mustafa Zahids first collaboration with Mohit Suri Later they collaborated for Aashiqui 2 and Ek Villain. Annie Khalids Mahiya was included in movie but with some changes and in voice of Suzzaine De Mello because Mohit Suri wants a proper Pakistani accent for the song Annies accent was a bit British as  per Mohit

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = no
| title1 = Tera Mera Rishta
| extra1 = Mustafa Zahid
| music1 = Pritam
| length1 = 5:47
| title2 = Toh Phir Aao
| extra2 = Mustafa Zahid
| music2 = Pritam
| length2 = 5:48
| title3 = Mahiya
| extra3 = Suzanne DMello
| music3 = Pritam
| length3 = 4:24
| title4 = Maula Maula
| extra4 = Rafaqat Ali Khan
| music4 = Pritam
| length4 = 5:24
| title5 = Tera Mera Rishta
| note5 = Remix
| extra5 = Mustafa Zahid
| music5 = Pritam
| length5 = 4:54
| title6 = Toh Phir Aao
| note6 = Remix
| extra6 = Mustafa Zahid
| music6 = Pritam
| length6 = 5:09
| title7 = Mahiya
| note7 = Remix
| extra7 = Annie Khalid
| music7 = Pritam
| length7 = 5:12
}}

===Rating===
The music has been well received and got rave reviews from prominent critics.

===Release===
Awarapan was released on 29 June 2007 all over India under the banner of Vishesh Films.

====India====
The film opened on an poor basis because of very strong competition from Aap Kaa Suroor and Apne. Additionally, the negativity of Emraans image as a serial kisser also discouraged people to the theatres. Movie was smash hit on online  and DVD . Awarapan went on to collect  .

====Overseas====
Awarapan was huge success in Pakistan and earned 8 crores .The worldwide box office collections of Awarapan were around  

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 