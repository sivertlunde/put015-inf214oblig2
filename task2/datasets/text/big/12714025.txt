Private Confessions
 
{{Infobox film
| name           = Private Confessions
| image          = Private Confessions.jpg
| caption        = Film poster
| director       = Liv Ullmann
| producer       = Ingrid Dahlberg
| writer         = Ingmar Bergman
| starring       = Pernilla August Max von Sydow Samuel Fröler
| music          = 
| cinematography = Sven Nykvist
| editing        = Michal Leszczylowski
| distributor    = 
| released       =  
| runtime        = 200 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

Private Confessions ( ) is a 1996 Swedish drama film directed by Liv Ullmann and written by Ingmar Bergman. It was screened in the Un Certain Regard section at the 1997 Cannes Film Festival.   

==Cast==
* Pernilla August – Anna
* Max von Sydow – Jacob
* Samuel Fröler – Henrik
* Anita Björk – Karin Åkerblom
* Vibeke Falk – Ms. Nylander, housekeeper
* Thomas Hanzon – Tomas Egerman
* Kristina Adolphson – Maria
* Gunnel Fred – Märta Gärdsjö
* Hans Alfredson – Bishop Agrell
* Bengt Schött – Stille, verger

==References==
 

==External links==
*  
 
 

 
 
 
 
 
 
 
 
 