Return from Witch Mountain
 
{{Infobox film
| name = Return from Witch Mountain
| image =Return from Witch Mountain, film poster.jpg
| caption = Theatrical release poster John Hough Ron Miller
| writer = Malcolm Marmorstein Alexander Key (novel) Anthony James
| music = Lalo Schifrin
| cinematography = Frank V. Phillips
| editing = Bob Bring Walt Disney Productions Buena Vista Distribution
| released =  
| runtime = 94 min.
| country = United States
| language = English
}}
 Walt Disney Escape to extraterrestrials with special powers including telepathy and telekinesis. The two main villains are played by Bette Davis as Letha Wedge, a greedy woman using the last of her money to finance the scientific experiments of Dr. Victor Gannon, played by Christopher Lee.

A made-for-television sequel called Beyond Witch Mountain was made in 1982.

==Plot== Rose Bowl stadium in Los Angeles, California, after which the siblings quickly become separated from each other. A man named Dr. Victor Gannon (Lee) and his assistant Letha Wedge (Davis) happen to see Tony using his powers to save their henchman from certain death. Realizing that Tony has supernatural powers, Dr. Gannon drugs the boy with a tranquilizer shot and takes him back to their laboratory. There, Dr. Gannon successfully tests a new mind-control technology on him. Under its influence, Tony is completely hypnotized and does everything that his kidnappers want him to do, like stealing gold from a museum exhibit and stopping Tia from finding them. With Tony at his robotic bidding, Dr. Gannon hopes to achieve recognition within the scientific community and worldwide power, while Letha merely wants a return on her investment.

Tia uses her telepathic powers to locate Tony. She gets additional help from a group of would-be toughs whom she comes across, called the Earthquake Gang, and a hapless truant officer Mr. Yokomoto&nbsp;— whom the toughs call "Yo-Yo"&nbsp;— to find her brother, and foil the villains nefarious plans.

==Filming locations==
The otherwise vacant lot, upon which the childrens dilapidated hideout mansion stands, was at the Alameda Street railroad yard, where the Rochester House (a relic from 1880s)  was waiting for restoration and relocation. The house was used as the hideout in the movie. The house however was never restored and ultimately demolished in 1979 (even though the link above places the house in Virginia, and says that it was listed as a historic building in 2002).

Scenes of Dr. Victor Gannons mansion, the location of his laboratory, were filmed at Moby Castle on Durand Drive, Hollywood Hills, Los Angeles.
 Exposition Park, Echo Park district.

==Cast==
 
 
* Bette Davis&nbsp;— Letha Wedge
* Christopher Lee&nbsp;— Dr. Victor Gannon
* Kim Richards&nbsp;— Tia Malone
* Ike Eisenmann&nbsp;— Tony Malone
* Jack Soo&nbsp;— Mr. "Yo-Yo" Yokomoto Anthony James&nbsp;— Sickle
* Richard Bakalyan&nbsp;— Eddie
* Ward Costello&nbsp;— Mr. Clearcole
* Christian Juttner&nbsp;— Dazzler
* Brad Savage&nbsp;— Muscles
* Poindexter Yothers|Poindexter&nbsp;— Crusher
* Jeffrey Jacquet&nbsp;— Rocky
* Stu Gilliam&nbsp;— Dolan William Bassett&nbsp;— Operations officer
* Tom Scott&nbsp;— Monitor
 
* Helene Winston&nbsp;— Dowager
* Albert Able&nbsp;— Engineer
* Denver Pyle&nbsp;— Uncle Bene
* Brian Part, Pierre Daniel&nbsp;— Goons
* Wally Brooks&nbsp;— Taxi fare
* Mel Gold&nbsp;— Security guard
* Bob Yothers&nbsp;— Cop
* Casse Jaeger&nbsp;— School patrolman
* Larry Mamorstein&nbsp;— Guard
* Bob James&nbsp;— Gate guard
* Ruth Warshawsky&nbsp;— Lady in car
* Adam Anderson&nbsp;— Man in museum
* Rosemary Lord&nbsp;— Woman in museum
* Ted Noose&nbsp;— Policeman
* Wally Berns&nbsp;— Man in car
 

Actors Kim Richards and Ike Eisenmann appear in at least four films together&nbsp;— this one, the original 1975 Disney film Escape to Witch Mountain, and the television film  . Richards portrays the roadside waitress and Eisenmann portrays the Sheriff in a re-imagined remake of the original film, Race to Witch Mountain, released in March 2009.

Jack Soo (Mr. "Yo-Yo" Yokomoto) was diagnosed with esophageal cancer in the Autumn of 1978, several months after the films release. Return from Witch Mountain would be his final movie appearance, as he died the following January.

The emergency voice heard over Yokomotos van radio&nbsp;— announcing the problem at the plutonium plant&nbsp;— is that of Gary Owens.

==Production==
The franchise was continued in 2009 with the release of Race to Witch Mountain, starring Dwayne Johnson and directed by Andy Fickman.

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 