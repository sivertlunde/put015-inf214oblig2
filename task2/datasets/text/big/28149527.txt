Jáchyme, hoď ho do stroje!
 
 
{{Infobox film
| name           = Jáchyme, hoď ho do stroje!
| image          = 
| alt            = 
| caption        = 
| director       = Oldřich Lipský
| producer       = 
| writer         = Oldřich Lipský Ladislav Smoljak Zdeněk Svěrák
| starring       = Luděk Sobota
| music          = 
| cinematography = Jaroslav Kučera
| editing        = Miroslav Hájek
| studio         = 
| distributor    = 
| released       = 1 August 1974
| runtime        = 95 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}
Jáchyme, hoď ho do stroje! is a 1974 Czechoslovak comedy film directed by Oldřich Lipský.

==Cast==
* Luděk Sobota - František Koudelka
* Marta Vančurová - Blanka
* Věra Ferbasová - Františeks aunt
* Josef Dvořák - Bedřich Hudeček
* Ladislav Smoljak - Karfík
* Zdeněk Svěrák - Klasek
* Karel Novák - Inkeeper
* Václav Lohniský - Doc. Chocholoušek
* Eva Fiedlerová - Mrs. Nevyjelová
* František Husák - Attendant Arnošt
* Josef Hlinomaz - Attendant Arnošt
* Miroslav Homola - Speaker
* Lubomír Lipský - Gate-keeper
* Petr Nárožný - Car racer Volejník
* Eva Svobodová - Františeks mother

==External links==
*  

 

 
 
 
 
 
 
 


 
 