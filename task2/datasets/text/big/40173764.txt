The Romancing Star II
 
 
{{Infobox film
| name           = The Romancing Star II
| image          = TheRomancingStarII.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 精裝追女仔之2
| simplified     = 精装追女仔之2
| pinyin         = Jīng Zhuāng Zhuī Nǚ Zǎi Zhī Èr
| jyutping       = Zing1 Zong1 Zeoi1 Neoi5 Zai2 Zi1 Ji6 }}
| director       = Wong Jing
| producer       = Wallace Cheung
| writer         = 
| screenplay     = Wong Jing
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Eric Tsang Natalis Chan Stanley Fung Lo Hoi-pang Wong Jing Carina Lau Elizabeth Lee Ngan Ning Wong Wan Sze Chow Yun-fat
| music          = Sherman Chow
| cinematography = David Choi
| editing        = Robert Choi
| studio         = Wins Movie Production Movie Impact
| distributor    = Wins Entertainment
| released       =  
| runtime        = 102 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$17,096,271
}}
 1988 Cinema Hong Kong romantic comedy film written and directed by Wong Jing and starring Andy Lau, Eric Tsang, Natalis Chan and Stanley Fung. Chow Yun-fat, the star of the films precedent The Romancing Star, makes a brief cameo in the first scene. The film was later followed by The Romancing Star III released in the following year.
 
==Cast==
*Andy Lau as Lau Pei
*Eric Tsang as Silver / Mocking Face
*Natalis Chan as Tony / Traffic Light
*Stanley Fung as Ken Lau
*Lo Hoi-pang as Mr. Chow
*Wong Jing as Simon Hand
*Carina Lau as Fong Fong
*Elizabeth Lee as Ching Po Chu
*Ngan Ning as Ka Yin
*Wong Wan Sze as Sister 13
*Chow Yun-fat as Fred Wong / Big Mouth Fat (cameo)
*Alan Tam as Himself in elevator (cameo)
*Michael Lai as Mr. Lai
*Cheng Kwan Min
*Ben Wong
*Charlie Cho as Mr. Tsang
*Mo-Lin Yu as Lady in phone booth (red dress)
*Manfred Wong as Furniture movers boss
*Lam Fai Wong as Man in toilet (cameo)
*Lily Ng as Simons lover
*Mak Wai Cheung as CID at bank
*William Ho as Mickey in Prison on Fire parody (cameo)
*Victor Hon as Prisoner in Prison on Fire parody (cameo)
*Thomas Sin as Prisoner in Prison on Fire parody (cameo)
*Tony Tam as Gangster boss at hawker stall
*Chun Kwai Po as Gangster at hawker stall
*Chang Sing Kwong as Gangster at hawker stall
*Leung Hak Shun as Councillor Sze To Ming
*Cheung Kwok Wah as Guest at Brother Lungs funeral (cameo)
*Mak Hiu Wai as Guest at Brother Lungs funeral
*Yip Seung Wah as Compere
*Mau Kin Tak as Channel 8 TVs staff
*Shirley Kwan as Woman at Brother Lungs funeral
*Sherman Wong as Chow TVs production staff
*Lee Chuen Hau as Chow TVs interviewee
*Fan Chin Hung as gangster at hawker stall

==Theme songs==
*Flaming Red Lips (烈焰紅唇)
**Composer: Anthony Lun
**Lyricist: Calvin Poon
**Singer: Anita Mui

*Sunshine Big Clearance Sale (陽光大減價)
**Composer: Michael Leeson, Peter Vale
**Lyricist: Lo Wing Keung
**Singer: Bennett Pang

==Box office==
The film grossed HK$17,096,271 at the Hong Kong box office during its theatrical run from 21 January to 10 February 1988 in Hong Kong.

==See also==
*Andy Lau filmography
*Chow Yun-fat filmography
*Wong Jing filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 