Fort Ti
{{Infobox film
| name           = Fort Ti
| image          =Fort Ti 1953 poster.jpg
| image_size     =
| alt            =
| caption        =
| director       = William Castle
| producer       = Sam Katzman
| writer         = Robert E. Kent (story & screenplay)
| narrator       =  George Montgomery Joan Vohs
| music          = Ross DiMaggio
| cinematography = Lester H. White, Lothrop B. Worth
| editing        = William A. Lyon
| studio         = Esskay Pictures Co
| distributor    = Columbia Pictures
| released       =  
| runtime        = 73 min.
| country        = United States English
| budget         =
| gross          =$2.6 million
| preceded by    =
| followed by    =
}} Western film, and was the first Western film to be released in 3D.  It was also the first 3-D feature to be released in Technicolor.  Fort Ti was produced by Esskay Pictures Corporation, and was distributed by Columbia Pictures in the US. 
 House of Wax. 

The film is set during the French and Indian War.

It earned an estimated $2.6 million at the North American box office during its first year of release. 


==Cast== George Montgomery as Capt. Jedediah Horn 
*Joan Vohs as Fortune Mallory
*Irving Bacon as Sgt. Monday Wash
*James Seay as Mark Chesney 
*Ben Astar as François Leroy 
*Phyllis Fowler as Running Otter 
*Howard Petrie as Maj. Robert Rogers 
*Cicely Browne as Bess Chesney
*Lester Matthews as Lord Jeffrey Amherst 
*George Leigh as Capt. Delecroix
*Louis Merrill as Raoul de Moreau
*Rusty Hamer as Chesney child



==References==
 

==External links==
* 
*  
*    at American Film Institute

 

 
 
 
 
 
 
 