A Scene at the Sea
 
 
{{Infobox film
| name = A Scene at the Sea
| image = Kitanoz3rd.jpg
| director = Takeshi Kitano Masayuki Mori
| writer = Takeshi Kitano
| starring = Claude Maki Hiroko Ohshima Sabu Kawahara Susumu Terajima Katsuya Koiso Testsu Watanabe
| music = Joe Hisaishi
| cinematography = Katsumi Yanagishima
| editing = Takeshi Kitano
| distributor = Office Kitano / Totsu EnterOne
| released = October 19, 1991
| runtime = 101 minutes Japanese Japanese Sign Language
| budget = 
}}
 1991 Cinema Japanese film written and directed by Takeshi Kitano.

==Plot==
A deaf garbage collector, played by Claude Maki, is determined to learn how to surfing|surf&mdash;and does so almost at the expense of the girl he loves.

==Production==
This movie was a break from previous Kitano fare in that it features no gangsters or police. However, Kitano did return to darker themes in his next film, Sonatine (1993 film)|Sonatine, as well as many later works. In the film, Kitano develops his more delicate, romantic side along with his trademark deadpan approach. In 2002 the Japanese filmmaker directed a similar movie, Dolls (2002 film)|Dolls, a romantic tale about three pairs of lovers.
 OSTs for all of Kitanos films until Dolls, after which their collaboration ended.

Occasional Office Kitano actor, Claude Maki, who plays the mute main character, went on to appear in Kitanos film Brother (2000 film)|Brother as Ken, a Japanese-American punk set to become leader of a Yakuza clan. In Brother, Claude speaks mostly in American-English with some occasional Japanese.

==Soundtrack==
{{Infobox album
| Name        = A Scene at the Sea
| Type        = soundtrack
| Artist      = Joe Hisaishi
| Cover       = A_sceneOST.jpg 
| Length      = 
| Recorded    =
| Released    = 25 November 1992
| Label       = Toshiba EMI, Milan Records (2001) Wonderland Records (2001)
}}
The soundtrack CD was originally released on November 25, 1992 by Toshiba EMI; then, re-released many times by Milan Records and Wonderland Records.

# "Silent Love (Main Theme)" − 6:52
# "Cliffside Waltz I" − 3:58
# "Island Song" − 3:39
# "Silent Love (In Search of Something)" − 1:10
# "Bus Stop" − 5:11
# "While at Work" − 1:22
# "Cliffside Waltz II" − 3:44
# "Solitude" − 1:12
# "Melody of Love" − 1:41
# "Silent Love (Forever)" − 3:30
# "Alone" − 1:04
# "Next Is My Turn" − 0:45
# "Wave Cruising" − 4:02
# "Cliffside Waltz III" − 3:40

==External links==
* 
*  

 
 
 

 
 
 
 
 
 
 
 
 

 
 