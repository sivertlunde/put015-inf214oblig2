Cradle Robbers
{{Infobox film
| name           = Cradle Robbers
| image          =
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       =
| starring       =
| music          =
| cinematography = Blake Wagner
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States  Silent English intertitles
| budget         =
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 26th Our Gang short subject released.

==Synopsis==
The boys cannot go fishing because they have to take care of their baby brothers and sisters.  After trying unsuccessfully to sell their babies to some traveling gypsies, Mary shows up and tells them that her little sister just won a prize at the baby show.

The boys decide to enter their babies in the show, only to discover that all the prizes are gone except the one for the fattest baby. Mickey comes up with the idea to enter Joe as a baby. After Joe escapes, the gang decides to make their own baby show. When the parents discover their babies are missing they assume that the gypsies stole them. When the gang finds out that their parents are after them, they hide in the gypsy wagon, which drives off. As the police and parents chase the wagon down the street, the babies start falling off the wagon and the parents stop and pick them up as they continue running.

The adults finally catch up with the wagon, the gypsy is arrested, and the kids reunited with their parents.

==Notes==
*The early scene of gang entertaining their younger siblings with fishing poles was reworked in 1933s Forgotten Babies.
*When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into television syndication as "Mischief Makers" in 1960 under the title The Baby Show. About two-thirds of the original film was included.
*This was Ernie Morrisons final appearance in the series.
*This silent Film marks the First appearance of Peggy Ahern

==Cast==

===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickey
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Ernest Morrison as Ernie
* Dick Henchen as Dick
* Sonny Boy Warde as Sing Joy
* Pal the Dog as Himself

===Additional cast===
* Lassie Lou Ahern - Little girl in attic
* Peggy Ahern - Girl at the gangs baby show
* Jannie Hoskins - One of Ernie and Farinas siblings
* Gabe Saienz as Boy at the gangs baby show
* Allan Cavan – Officer / Baby show official
* Beth Darlington – Young woman with bald boyfriend William Gillespie – Officer chasing Joe / Gypsy
* Helen Gilmore – Jackies mother
* Clara Guiol – Baby show official
* Lyle Tayo – Angry mother Dorothy Vernon – Angry mother

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 