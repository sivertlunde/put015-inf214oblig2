The Devil-Stone
 
{{Infobox film
| name           = The Devil-Stone
| image          = Thedevil-stone 1918 newspaper ad.jpg
| caption        = Newspaper advertisement.
| director       = Cecil B. DeMille
| producer       = Jesse L. Lasky Cecil B. DeMille
| writer         = Beatrice DeMille Jeanie MacPherson Leighton Osmun
| starring       = Geraldine Farrar Wallace Reid Hobart Bosworth Tully Marshall
| cinematography = Alvin Wyckoff
| editing        = Cecil B. DeMille
| studio         = Famous Players-Lasky/Artcraft
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent romance film directed by Cecil B. DeMille and co-written by his mother Beatrice and his some time lover Jeanie MacPherson. The film had sequences filmed in the Handschiegl Color Process (billed as the "DeMille-Wyckoff Process").

Only two of six reels are known to survive, in the American Film Institute Collection at the Library of Congress.  

This was the last of Farrars films for Paramount.

==Plot==
Fisherwoman Marcia Manot (Farrar) finds an emerald which belonged to a Norse queen, and is cursed. Fortune hunter Silas Martin (Marshall) marries her to get the emerald.

==Cast==
* Geraldine Farrar as Marcia Manot
* Wallace Reid as Guy Sterling
* Hobart Bosworth as Robert Judson
* Tully Marshall as Silas Martin
* James Neill as Simpson
* Mabel Van Buren
* Lillian Leighton
* Gustav von Seyffertitz as Stephen Densmore
* Horace B. Carpenter
* Ernest Joy
* Burwell Hamrick
* Raymond Hatton
* Theodore Roberts

==See also==
*List of early color feature films
*List of incomplete or partially lost films

==References==
 

==External links==
* 
*  at silentera.com
* 

 

 
 
 
 
 
 
 
 
 


 
 