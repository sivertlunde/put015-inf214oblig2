The Kalemites Visit Gibraltar
  }}

{{infobox film name = The Kalemites Visit Gibraltar image =  caption =  director = Sidney Olcott producer = Kalem Company writer =  starring =  distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 578 ft
| country = United States language = Silent film (English intertitles) 
}}
The Kalemites Visit Gibraltar is a 1912 American silent documentary produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott.

==Production notes==
* The documentary was shot in December 1911, in Gibraltar, Spain.

==References==
* The Moving Picture World, Vol 11, p. 994. 
* Supplement to the Bioscope, May 16, 1912.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 

 