Return to Home Gori
{{Infobox film
| name           = Return to home Gori
| image          = 
| caption        = 
| director       = Alessandro Benvenuti
| producer       = 
| writer         =  
| starring       = Massimo Ceccherini 
| music          = 
| cinematography = Danilo Desideri
| editing        =   	Carla Simoncelli
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Ritorno a casa Gori, internationally known as Return to Home Gori, is a 1996 comedy film directed by Alessandro Benvenuti. It is the sequel of Welcome to Home Gori. 

==Plot==
After 5 years from the fateful day of Christmas at Welcome to Home Gori, the scenery changes during a robbery in a villa in Tuscany by Danilo (Massimo Ceccherini) and his friends addicts (one of whom is played by Alessandro Paci). Back home, he expect a sad circumstance, the death of his mother Adele (Ilaria Occhini). In fear of theft Danilo hiding the loot in the coffin of his mother exposed in the red room in the house for the wake.

==Cast==
*Alessandro Benvenuti: Luciano
*Massimo Ceccherini: Danilo Gori
*Ilaria Occhini: Adele Papini
*Carlo Monni: Gino Gori
*Novello Novelli: Annibale Papini
*Athina Cenci: Bruna Papini
*Alessandro Haber: Libero Salvini
*Sabrina Ferilli: Sandra Salvini Sottili
*Barbara Enrichi: Cinzia Enrichi

== References ==
 

==External links==
* 

 
 


 
 