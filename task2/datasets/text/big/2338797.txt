Three Men and a Cradle
{{Infobox film
| name           = Trois hommes et un couffin
| image          = Three Men and a Cradle.jpg
| caption        = Film poster
| director       = Coline Serreau
| producer       = Jean-Francois Lepetit
| writer         = Coline Serreau
| starring = {{Plainlist|
* Roland Giraud
* Michel Boujenah
* André Dussollier
* Dominique Lavanant
* Philippine Leroy-Beaulieu
}} 
| music          = Franz Schubert
| cinematography = Jean-Jacques Bouhon Jean-Yves Escoffier
| editing        = Catherine Renault
| distributor    = Acteurs Auteurs Associés
| released       =  
| runtime        = 106 minutes
| country        = France
| language       = French
| budget         = 
}}
Three Men and a Cradle ( ) is a 1985 French comedy film by Coline Serreau. The film was remade in Hollywood as Three Men and a Baby in 1987.

==Plot==
Three young men (Jacques, Pierre and Michel) share an apartment in Paris, and have many girlfriends and parties (During the movie, we even learn that they have signed a contract never to allow a girl to spend more than one night at their place). Once, during a party, a friend of Jacques tells him he has a quite compromising package (which turned out to be heroin) to deliver, and asks him if he can leave it discreetly at their place. Jacques agrees and, as he works as a steward, flies away for a one month trip in Japan, telling Pierre and Michel about the package. Then, one of Jacques former girlfriends drops a baby before their door, making Pierre and Michel believing it is the package they are waiting for. Their lives are then completely changed.  This movie follows the bachelors as they deal with angry gangsters, suspicious cops, and the overwhelming responsibility of fatherhood.

==Cast==
*Roland Giraud - Pierre
*Michel Boujenah
*André Dussollier
*Philippine Leroy-Beaulieu

==Trivia==
A second film by Coline Serreau, with the same characters, the same actors and called 18 ans après (18 Years After) was released in 2003.

==Awards and nominations==
*Academy Awards (USA)
**Nominated: Best Foreign Language Film   

*César Awards (France)
**Won: Best Actor &ndash; Supporting Role (Michel Boujenah)
**Won: Best Film
**Won: Best Writing (Coline Serreau)
**Nominated: Best Actress &ndash; Supporting Role (Dominique Lavanant)
**Nominated: Best Director (Coline Serreau)
**Nominated: Most Promising Actress (Philippine Leroy-Beaulieu)

*Golden Globe Awards (USA)
**Nominated: Best Foreign Language Film

*National Academy of Cinema (France)
**Won: Academy Award	(Coline Serreau)

==See also==
* Three Men and a Baby, 1987 American remake 
* Heyy Babyy, 2007 Indian remake
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
;Trois hommes et un couffin
: 
: 
;18 ans après
: 
: 

 
 

 
 
 
 
 
 
 
 
 