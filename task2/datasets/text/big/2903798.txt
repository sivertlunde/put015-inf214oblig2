The Bodyguard (2004 film)
{{Infobox film 
  | name = The Bodyguard
  | image = Bodyguardmumjokmok.jpg
  | image size     = 190px
  | caption =  The cover to the Thailand DVD release of the film. 
  | director = Panna Rittikrai Petchtai Wongkamlao
  | producer = Somsak Techaratanaprasert
  | writer = Petchtai Wongkamlao
  | starring = Petchtai Wongkamlao, Pumwaree Yodkamol, Piphat Apiraktanakorn, Tony Jaa
  | music = 
  | cinematography = Nattawut Kittikhun
  | editing = Thawat Sermsuwittayawong
  | distributor = Sahamongkol Film International
  | released = January 21, 2004  (Thailand) 
  | runtime = 95 min.
  | country = Thailand Thai
  | budget = 
  }} Thai comedian and actor Petchtai Wongkamlao and featuring martial-arts choreography by Panna Ritikrai. It is followed by the 2007 sequel, The Bodyguard 2.

==Plot==

After a shootout with dozens of assassins, Wong Kom, bodyguard to Chot Petchpantakarn, the wealthiest man in Asia, finds his subject killed.

Chaichol, the son and heir to the family fortune, fires the bodyguard and takes it upon himself to find the killers. Hes then ambushed, and the rest of the bodyguard team is wiped out. Chaichol, however, comes out of it alive, and finds himself in a Bangkok slum, living with a volunteer car-accident rescue squad and falling in love with tomboyish Pok.

Meanwhile, Wong Kom is working to clear his name, and stay ahead of the chief villain and his bumbling gang of henchmen.

==Cast==
*Petchtai Wongkamlao as Wong Kom 
*Piphat Apiraktanakorn as Chaichol Petchpantakarn
*Surachai Juntimatorn as Chot Petchpantakarn
*Suthep Prayoonpitak as Choung Petchpantakarn
*Aranya Namwong as Rattana Petchpantakarn
*Pumwaree Yodkamol as Pok
*Apaporn Nakornsawan as Mae Jam
*Wachara Pan-Iom as Songpol
*Yingyong Yodbuangam as Arthit
*Choosak Iamsook as Songpol Gangster No. 1
*Samart Payakaroon as Songpol Gangster No. 2
*Krisranapong Ratchatha as Songpol Gangster No. 3
*Sayan Muangcharoen as Songpol Gangster No. 3
*Tony Jaa as Supermarket fighter / himself (as Panom Yeerum)

===Casting notes===
* Petchtai Wongkamlao, a popular Thai comedian, plays the straight man.
* Some of the films promotional materials imply that this followup to   starring Tony Jaa. In fact, Tony only has a small (but still memorable) role as an anonymous fighter in a supermarket.
* In addition to Tony Jaa, dozens of Thai celebrities, including sports figures, comedians and musicians, have roles and cameos on the film. Among them are: songs for life band, Caravan (Thai band)|Caravan.
**Yingyong Yodbuangam, who portrayed one of the leaders of the bad guys, is a well-known luk thung singer.
**Twin-brother boxers Kaokor Galaxy and Khaosai Galaxy, portraying the "twin assassins".
**Another boxer, Samart Payakaroon, is Songpol Gangster No. 2. Chao Kokaew Thai newspaper society pages until her death in 2005,  portrays a maid.

==Production notes== Victory Monument public transportation. Petchtai sought to have a body-double do the stunt, but couldnt find anyone to do it, so he had to do it himself. On the "making of" documentary on the DVD, he said it was the most embarrassing thing he had done in his life.
*There are several references to the Movie  . An Example is in the Supermarket fight scene, where Tony Jaas character says : You are welcome, Humlae, and         Petchtai Wongkamlao responds by stating that this is another Movie  ( ). Petchtai Wongkamlao played a character named Humlae in this  .

==Awards and nominations==
*Best supporting actress winner, Apaporn Nakornsawan, Thailand National Film Awards, 2005.

==References==
 

==External links==
*  
*  
*  .

 

 
 
 
 
 
 
 