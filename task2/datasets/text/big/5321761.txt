Stardust (1974 film)
{{Infobox film
| name           = Stardust
| image_size     = 160px
| image	=	Stardust FilmPoster.jpeg
| caption        = Original film poster by Arnaldo Putzu
| director       = Michael Apted
| producer       = David Puttnam Sanford Lieberson
| writer         = Ray Connolly
| starring       = David Essex Adam Faith
| cinematography = Anthony B. Richmond
| editing        = Michael Bradsell
| distributor    = EMI Films (UK) Columbia Pictures (U.S.)
| released       = 1974 (UK) 12 November 1975 (U.S.)
| runtime        = 111 min.
| country        = United Kingdom
| language       = English
| budget =£555,000 Alexander Walker, National Heroes: British Cinema in the Seventies and Eighties, Harrap, 1985 p 79  
}}
 1974 United British film directed by Michael Apted and starring David Essex and Adam Faith. The film is the sequel to the 1973 film  Thatll Be the Day (film)|Thatll Be The Day. Its tagline is: "Show me a boy who never wanted to be a rock star and Ill show you a liar."

==Plot==
Following on from the events in the late 1950s/early 60s of Thatll Be The Day, the characters Jim Maclaine, Jeanette and J.D. have moved into the mid 1960s/early 70s and the growing career of aspiring rock star MacLaine. MacLaine has assembled a group of musicians and formed the band The Stray Cats, he seeks out his old friend Mike to become the band’s road manager. Mike acquires a new van, accommodation and a recording session for the group. MacLaine soon becomes a massive star and is plunged into the centre of media attention. He indulges in casual sex and heavy drug use and the film documents the detrimental effects of this success on MacLaine and his relationship with his friends and colleagues. In particular, MacLaine’s long-standing friendship with manager Mike is now soured by money and success.

==Characters==
By the end of the Stardust, the timeline has roughly caught up to the 1974 release of the film.  Many of the characters were played by British musicians who had lived/were living through the era portrayed in the film including Essex, Faith, Marty Wilde, Keith Moon, Dave Edmunds, and Paul Nicholas.

==Main cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| David Essex || Jim MacLaine
|-
| Adam Faith || Mike Menary
|-
| Larry Hagman || Porter Lee Austin
|-
| Ines Des Longchamps || Danielle
|-
| Rosalind Ayres || Jeanette
|-
| Marty Wilde || Colin Day
|-
| Edd Byrnes || TV Interviewer
|-
| Keith Moon || J. D. Clover
|-
| Dave Edmunds || Alex
|-
| Paul Nicholas || Johnny
|-
| Karl Howman || Stevie
|-
| Richard LeParmentier || Felix Hoffman
|- Peter Duncan || Kevin
|-
| John Normington || Harrap
|-
| David Daker || Ralph Woods
|-
| James Hazeldine || Brian
|-
| Charlotte Cornwell || Sally Porter
|- David Jacobs || Himself
|-
| Donald Sumpter || TV Producer
|-
| Bobby Sparrow || Blonde 1
|-
| Claire Russell || Blonde 2
|}

==Reception==
The film was a hit at the box office and by 1985 had earned an estimated £525,000 in profit. 

==Awards and nominations==
BAFTA Writers Guild of Great Britain for Best Original British Screenplay WINNER: Ray Connolly.

BAFTA Best Supporting Actor NOMINATED: Adam Faith.

==Soundtrack==
The Stardust (soundtrack) was released in October on Ronco Records to coincide with the opening.
==Spin-off==
An independent Radio Drama recording project was completed in 2008 entitled   which continues the story of Jimmy MacLaine jr. (son of Jim MacLaine).(Also see external link below).

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 