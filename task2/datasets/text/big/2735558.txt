Tom Sawyer (2000 film)
{{Infobox film
| name = Tom Sawyer
| image = Tomsawyer2000.jpg
| caption = VHS cover
| director = Paul Sabella Phil Mendez  (co-director) 
| producer = Paul Sabella Jonathan Dern Patricia Jones  (co-producer)  Donald Reiker  (co-producer)  Cary Silver  (co-producer) 
| writer = Patricia Jones Donald Reiker Jymn Magon  (additional material) 
| based on =  
| starring = Rhett Akins Charlie Daniels Alecia Elliott Waylon Jennings Richard Kind Don Knotts Mark Nesler Marty Stuart Betty White Hank Williams Jr. Mark Wills Lee Ann Womack
| music = Mark Watters
| studio = Metro-Goldwyn-Mayer Animation
| distributor = MGM Kids|MGM/UA Family Entertainment Metro-Goldwyn-Mayer
| released =  
| runtime = 89 minutes
| country = United States
| language = English
}} MGM Animation and was released in the year 2000. It is an adaptation of Mark Twains The Adventures of Tom Sawyer, with a cast of anthropomorphic animals instead of humans. The characters voices are generally performed by country music singers. 

==Plot==
Tom Sawyer and his half-brother Sid are on their way to school when they see Huckleberry Finn fishing.  Tom skips school to join Huck, but changes his mind after he sees Becky Thatcher.  He tries to sneak into class, but Sid snitches on him to the teacher.  Toms teacher makes him sit with the girls, which Tom actually likes since hes able to sit next to Becky.  He is also sat beside Amy Lawrence, a friend to whom he became "engaged". She still has romantic feelings for him but he is too transfixed by Becky to notice. Toms pet frog Rebel then disrupts the class, meaning they are given early dismissal.

On the way home from school, during the musical number "Hook, Line and Sinker", Tom tries multiple times to steal a kiss from Becky, but is thwarted each time by her father, Judge Thatcher.  The next day, as Tom is about to go fishing with his friends, Aunt Polly makes him paint the house as punishment for what happened at school.  Tom, however, gets his friends to paint the house for him instead.

That night, when Tom and Huck go treasure hunting, they find Injurin Joe and his friend Mutt Potter uncovering a chest of gold.  Deputy Bean, who is visiting his wifes grave, discovers Joe and Mutt. As the boys watch from behind a tombstone, Joe brutally murders Bean who tried to hit him with a shovel, frames Mutt and captures Rebel. Tom knows that Joe can track him down through Rebel, so he and Huck make a pact never to tell anyone what they have seen.
 thrashing with a ruler by the teacher.  After school, Tom becomes "engaged" to Becky, before a musical number by Becky and Amy, "One Dream", where the two individually express their shared love for Tom.  He then admits he did the same with Amy, causing Becky to call off the engagement.  Tom and Huck visit Mutt, who is on death row.  They try to get him to remember Injurin Joe murdering Bean, but Mutt doesnt remember.  Joe meanwhile finds Tom and Huck, but they escape on a raft. They celebrate their survival and friendship with a musical number, "Friends for Life".

When Tom and Huck return to town, they learn that the townspeople are mourning their deaths, believing the boys to have drowning|drowned.  They disrupt the service, showing up at their own funeral, and are welcomed back. Amy, wanting to make Becky more upset at Tom, kisses Tom in front of Becky making her believe that Tom has chosen Amy over her and leaves before Tom can get a chance to explain to her that Amy was the one who kissed him, leaving him heartbroken. Judge Thatcher sentences Mutt to be hanging|hanged, but Huck and Tom testify against Joe at the last minute.  Joe goes after Tom and Huck but fails and is pulled away by a river, while Mutt is freed and the boys are hailed as heroes.

During the celebration, after making up with Tom, Becky talks Tom into exploring a cave, where they get lost and Becky begins to lose hope to finding the exit. Tom sings a number, "Light at the End of the Tunnel" to try and reassure her that they will find a way out. Instead of finding an exit, they find treasure—and Joe.  Meanwhile, the townspeople notice Tom and Becky missing and Amy, who saw Tom and Becky go into the cave, reveals where they are. The townspeople go to look for them in the cave.  With Hucks help, Tom subdues Joe and is reunited with the townspeople and Aunt Polly. In the end Amy becomes Hucks girlfriend (after being impressed with how he assisted Tom in killing Joe) and Becky becomes Toms girlfriend.  The next day, Sid again tries to snitch on Tom, but it backfires, as Aunt Polly makes Sid paint the house instead of Tom.  The movie ends with Tom, Becky, Huck and Amy having a picnic, during which Tom shows the others a gold coin and tells them about another treasure hunt.

==Cast==
*Rhett Akins as Tom Sawyer
*Mark Wills as Huckleberry Finn
*Hynden Walch as Becky Thatcher (speaking)
*Lee Ann Womack as Becky Thatcher (singing)
*Clea Lewis as Amy Lawrence (speaking)
*Alecia Elliott as Amy Lawrence (singing)
*Betty White as Aunt Polly
*Dean Haglund as Sid
*Richard Kind as Mr. Dobbins
*Hank Williams Jr. and Kevin Michael Richardson as Injurin Joe
*Don Knotts as Mutt Potter
*Waylon Jennings as Judge Thatcher
*Dee Bradley Baker as Rebel the Frog
*Pat Corley as Sheriff McGee
*Marty Stuart as Reverend Thom Adcox as Deputy Bean
*Sheryl Bernstein
*Jennifer Hale David Kaufman

==Soundtrack==
*"Leave Your Love Light On" - Marty Stuart
*"Cant Keep a Country Boy Down" - Charlie Daniels
*"Hook, Line, and Sinker" - Mark Nesler
*"Houseboat Painting Song"
*"One Dream" - Lee Ann Womack/Alecia Elliott
*"Friends for Life" - Rhett Akins/Mark Wilis
*"Light at the End of the Tunnel"/Reprise - Bryan White/Rebecca Lynn Howard/Rhett Akins/Lee Ann Womack
*"Never, Ever, and Forever" - Lee Ann Womack/Mark Willis

==Production== Babes in Toyland. The film was not given a theatrical release but was direct-to-video. 

==Critical reception==
Harlene Ellin of The Chicago Tribune gave a negative review, saying that it "stray  too far from Twain."  An uncredited review in the Wichita Eagle was also unfavorable, calling it a "shallow" interpretation of Twains work. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 