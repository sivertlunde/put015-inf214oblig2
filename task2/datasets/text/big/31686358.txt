The Guilt Trip (film)
{{Infobox film
| name           = The Guilt Trip
| image          = The Guilt Trip Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Anne Fletcher
| producer       = Lorne Michaels John Goldwyn Evan Goldberg
| writer         = Dan Fogelman
| starring       = Barbra Streisand Seth Rogen
| music          = Christophe Beck 
| cinematography = Oliver Stapleton
| editing        = Dana E. Glauberman Priscilla Nedd-Friendly
| studio         = Skydance Productions  Michaels-Goldwyn
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95&nbsp;minutes   
| country        = United States
| language       = English
| budget         = $40&nbsp;million
| gross          = $41,030,582	      
}}
The Guilt Trip is a 2012 American comedy-drama film directed by Anne Fletcher from a screenplay written by Dan Fogelman, starring Barbra Streisand and Seth Rogen, who both also served as executive producers on the film.

==Plot== organic chemist and inventor. He is attempting to get his environmentally friendly cleaning product, ScioClean, in a major retail store. However, each retail store he visits dismisses him before he can end his pitch. After a disappointing sales pitch to K-Mart, he visits his mother, Joyce Brewster (Barbra Streisand), in New Jersey before leaving on a cross-country trip to Las Vegas, lying to her that his pitch ended well so she wont worry about him. While there she reveals to him that he was named after a boy she fell in love with in Florida named Andrew Margolis, whom she hoped would object to her marriage with Andys father. However, he never did and she felt that she never mattered to him afterwards. After a little research, he finds Andrew Margolis is still alive and unmarried living in San Francisco. He invites his unknowing mother on the trip, claiming he wants to spend some time with her.

The road trip quickly becomes hard for Andy as his mother continues to intervene in his life. After their car breaks down in Tennessee, Joyce calls Andys ex-girlfriend Jessica (Yvonne Strahovski), whom Joyce insists Andy should get back together with, to pick them up. At a pregnant and married Jessicas house she reveals that Andy proposed to her before college and she turned him down, shocking Joyce, who believed Andy had trouble proposing to women. Andy is glum afterward and Joyce apologizes for calling Jessica, which Andy half-heartedly accepts. In Texas, Andy has a meeting with Costco executive Ryan McFeer (Brandon Keener) however Joyce stays at the meeting and criticizes the products bottling and name along with Ryan to the point that Andy snaps at him, saying "Im not changing the goddamn label, Ryan!" At the motel that night a depressed Andy begins drinking and Joyce attempts to make up with him however Andy snaps at her, only to have Joyce snap back and leave for a nearby bar. Later Andy attempts to retrieve his mother but gets in a fight with a bar patron who attempts to stop her from leaving, receiving a black eye in the process.

At a steak restaurant the next day the two exchange apologies and Andy reveals that he is failing at selling ScioClean. Joyce enters a steak eating challenge where she is noticed by cowboy-styled businessman Ben Graw (Brett Cullen), who gives her tips on eating and helps her finish the challenge. Afterwards he reveals he does a lot of business in New Jersey and asks her to dinner. Joyce, who has never been in a relationship after Andys dad died when he was eight, balks at the offer so Ben merely leaves his number and asks her to call if she reconsiders.

Andy and Joyce begin to genuinely enjoy each others company after, taking time out of their trip to visit the Grand Canyon (which Joyce has always wanted to visit) and having many other adventures.

At Las Vegas, Joyce has such a good time that she asks Andy to leave her while he visits San Francisco, forcing him to reveal that there is no sales pitch in San Francisco and he only invited her to get her to meet Andrew Margolis. Joyce is very distraught as she believed Andy invited her because he actually wanted to spend time with her. He goes to make his pitch at the Home Shopping Network but finds that his science-fact based pitch bores the networks executives and makes them uninterested. He then sees Joyce in the filming crew and takes her advice by appealing to the Networks host family safety and drinking his own product, proving that it is organic and safe for children. Afterwards the Network CEO approaches him and shows genuine interest in selling ScioClean on the Network. Later, a jubilant Andy and Joyce decide to visit Andrew Margoliss house.
 Adam Scott) (whom Andy mistakenly researched instead of the father) that his father died five years ago. After seeing Joyces grief he invites them inside, where he learns that his father and Joyce were close. She asks if Andrews father ever mentioned her, but he says he never did as he only confided personal information to their mother, who is away. However he then introduces his sister (Ari Graynor), who is named Joyce after Andys mom. Joyce is overjoyed by this as she had previously stated her belief that you name your children after someone you cherished and want to remember. This shatters her belief that she didnt matter to Andrew and makes her overjoyed.

Afterwards they part ways at the San Francisco Airport; Andy to make his next sales pitch and Joyce back to New Jersey, where she arranges a date with Ben Graw. The two leave content and much closer than they had been.

==Cast==
*Barbra Streisand as Joyce Brewster
*Seth Rogen as Andy Brewster
*Brett Cullen as Ben Graw Adam Scott as Andrew Margolis, Jr.
*Ari Graynor as Joyce Margolis
*Casey Wilson as Amanda
*Colin Hanks as Rob
*Yvonne Strahovski as Jessica
*Jeff Kober as Jimmy
*Miriam Margolyes as Anita
*Kathy Najimy as Gayle
*Dale Dickey as Tammy
*Nora Dunn as Amy
*Brandon Keener as Ryan McFee
*Danny Pudi as Sanjay

==Production==
{{quote box quote = "Ive met her only a couple of times so far but shes really funny and nice and really reminds me a lot of my actual mother, which is very odd; my girlfriends like a huge Barbra Streisand fan." source = Seth Rogen on Barbra Streisand.  width = 35% align = right
}} Las Vegas years before. {{cite news | url = http://www.jewishjournal.com/hollywood/article/dan_fogelman_explores_romances_range_in_crazy_stupid_love_20110726/ | date = July 26, 2011 | title = 
First crush to midlife dating | work =   

This film marks Streisands first starring role since The Mirror Has Two Faces in 1996.  She appeared in supporting roles in Meet the Fockers in 2004 and Little Fockers in 2010, as well as in a number of television shows.

==Reception==
The film has received mixed reviews from critics. It holds a 38% approval rating on the review aggregator website Rotten Tomatoes, based on 120 reviews. The consensus states: "Seth Rogen and Barbra Streisand have enough chemistry to drive a solidly assembled comedy; unfortunately, The Guilt Trip has a lemon of a script and is perilously low on comedic fuel."  It also received a score of 50/100 on Metacritic with a 6.8/10 userscore.   

However, some critics found the movie to be highly enjoyable: for example, a review from Time Magazine stated that "The Guilt Trip works because we all know and like a Joyce Brewster (or dozens of them)".   

Barbra Streisands performance was generally well received by critics: "Streisand kills it, with dignity. The  funny lady is back!"  However, at the  .   

===Box office===
The film just managed to gross over its $40&nbsp;million budget by $1&nbsp;million, making it a box office disappointment.

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 