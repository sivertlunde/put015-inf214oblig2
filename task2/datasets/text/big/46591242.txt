666 the Devil's Child
{{Infobox film
| name           = 666 the Devils Child
| image          = 
| caption        = 
| director       = Manzie Jones
| producer       = {{plainlist|
* Kevin Clark
* Allen Anthony Jones
* Manzie Jones
* Kristopher Michel
* Chadwick Turner
}}
| writer         = Kristopher Michel
| starring       = {{plainlist|
* Nadya Suleman
* Jeff Kongs
* Chanon Finely
* Valerie Mya
}}
| cinematography = Royce Allen Dudley
| editing        = Charles Dyson
| studio         = {{plainlist|
* Fubot Pictures
}}
| distributor    = Gravitas Ventures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
666 the Devils Child (also stylized as 666: the Devils Child) is a 2012 horror film starring Nadya Suleman in her film debut. Nadya was originally cast for name recognition but producers were surprised by her acting talent.    The movie was originally released in 2012 but re-released in 2014.

==Description==
666 The Devils Child is the story of two friends, a young woman and young man, who visit another young woman who theyve met on the internet. Once they arrive at the womans remote house unexplainable things begin to happen, and the woman who owns the house is anything but what they expect.      

==Cast==
* Nadya Suleman as Vanessa
* Jeff Kongs as Brad
* Chanon Finley as Jessica
* Valerie Mya as Kristen

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 