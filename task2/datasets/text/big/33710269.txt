Queen of Clubs (film)
 
{{Infobox film
| name           = Queen of Clubs
| image          = 
| caption        = 
| director       = George Skalenakis
| producer       = Theophanis A. Damaskinos
| writer         = Giannis Tziotis
| starring       = Elena Nathanail
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}
 Best Foreign Language Film at the 39th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Elena Nathanail as Elena
* Spiros Focás as Alexandros
* Thodoros Roubanis as Vasilis (as Theo Roubanis)
* Despo Diamantidou as Marianthi
* Dimos Starenios as Teacher
* Aris Malliagros as Doctor

==See also==
* List of submissions to the 39th Academy Awards for Best Foreign Language Film
* List of Greek submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 