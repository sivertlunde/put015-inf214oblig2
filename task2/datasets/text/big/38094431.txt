About Time (2013 film)
 
 
{{Infobox film
| name           = About Time
| image          = About Time Poster.jpg
| image_size     = 250
| caption        = Theatrical release poster
| alt            = A girl in a red dress, laughing in the rain, alongside a tall red-haired man wearing a suit. 
| director       = Richard Curtis
| producer       = {{Plainlist|
* Tim Bevan 
* Eric Fellner 
*Nicky Kentish Barnes
}}
| writer         = Richard Curtis
| starring       = {{Plainlist|
* Domhnall Gleeson 
* Rachel McAdams 
* Bill Nighy
* Tom Hollander
* Margot Robbie
 
}}
| music          = Nick Laird-Clowes
| cinematography = John Guleserian Mark Day
| production companies = {{Plainlist|
* Working Title Films
* Relativity Media
}} Universal Pictures
| released       =   
| runtime        = 123 minutes   
| country        = {{Plainlist|
*United Kingdom
*United States  
}}
| language       = English
| budget         = $12 million 
| gross          = $87.1&nbsp;million       
}} romantic comedy-drama film about a young man with the special ability to time travel who tries to change his past in order to improve his future.  The film was written and directed by Richard Curtis,   and stars Domhnall Gleeson, Rachel McAdams and Bill Nighy. It was released in the United Kingdom on 4 September 2013 and in the United States on 1 November 2013.    

== Plot ==
Tim Lake (Domhnall Gleeson) is a young man from Cornwall, England. He grows up in a house by the sea with his father (Bill Nighy), his mother (Lindsay Duncan), his absent-minded uncle (Richard Cordery), and his free-spirited sister, Kit Kat (Lydia Wilson). At the age of 21, Tim is told by his father that the men of his family have a special gift: the ability to travel in time. This supernatural ability is subject to one constraint - they can only travel to places and times they have been before. After his father discourages Tim to use his gift to acquire money or fame, he decides that he will use it to improve his love life.

The following summer, Kit Kats friend Charlotte (Margot Robbie) comes to the house in order to spend her holiday with Tims family. Tim is instantly attracted to her and at the end of her stay, decides to tell her how he feels. She tells him that he should not have waited until the last day, that perhaps if he had told her earlier, something could have happened between them. Tim travels back in time and, the second time around, tells Charlotte in the middle of the holiday how he feels. In this instance, Charlotte uses the exact opposite excuse, saying that it would be better if they waited until the last day of the holiday and then something could potentially happen between them. Heartbroken, Tim realizes that Charlotte is not attracted to him and that time travel will not make it possible for him to change her mind.

After the summer, Tim moves to London to pursue a career as a barrister. He is put up by his fathers old acquaintance, Harry (Tom Hollander), a misanthropic playwright. After some months, Tim visits a Dans le Noir restaurant, where he meets Mary (Rachel McAdams), a young American woman who works for a publishing house. The two flirt in the darkness of the restaurant, and afterward, Mary gives Tim her phone number. Tim returns home to find a distraught Harry. It turns out that the same night as he met Mary, the opening night of Harrys new play had been ruined by one of the actors forgetting his lines at a crucial point. Tim goes back in time to put things right and the play is a triumph.

Having saved Harrys opening night, Tim tries to call Mary, but discovers that her number is no longer in his mobile phone. By going back in time to help Harry, Tim chose a path in which the evening with Mary never happened. However, he recalls that Mary was obsessed with Kate Moss. By attending a Kate Moss exhibition in town, he is able to run into Mary again. He strikes up an acquaintance with her but discovers she now has a boyfriend. Tim finds out when and where they met, turns up early and persuades her to leave the party before she can meet her future boyfriend. Their relationship develops and Tim moves in with Mary. He encounters Charlotte again by accident, and this time she tells him that she would be interested in pursuing a romantic liaison with him. Tim turns her down, realising that he is truly in love with Mary. He proposes marriage; she accepts and is welcomed into his family. Their first child, Posy, is born. Tims sister, Kit, has not been so lucky and her unhappy relationship, failure to find a career, and drinking lead her to crash her car on the same day as Posys first birthday.

Kit is seriously hurt but begins to make a good recovery. Tim decides to intervene in her life and does so by preventing her from meeting her boyfriend, Jimmy (Tom Hughes). When he returns to the present time, he finds Posy has never been born and that he has a son instead. His father explains that travelling back to change things before his children were born would mean those children would not be born. Thus, any events that occurred before Posys birth cannot be changed, and Tim must accept the consequences as a normal person would.  Tim accepts he cannot change Kits life by changing her past but he and Mary help her to change her life in the present. She settles down with an old friend of Tims and has a child of her own. Tim and Mary have another child, a boy.

Tim learns that his father has terminal cancer and that time travel cannot change it. His father has known for quite some time, but kept travelling back in time to effectively extend his life and spend more time with his family. He tells Tim to live each day twice in order to be truly happy: the first time, live it as normal, experiencing each days inherent unpredictability and stress, and the second time to appreciate its small joys and special moments without making any changes to the events that occurred (and avoiding changing any future events). Tim follows this advice and also travels back into the past to visit his father whenever he misses him.

Mary tells Tim she wants a third child. He is reluctant at first because he will not be able to visit his father after the baby is born but agrees. After visiting his father for the following nine months, Tim tells his father that he cannot visit any more. They travel back to when Tim was a small boy, reliving a fond memory of them playing on the beach. After reliving each day, Tim comes to realise that it is better to live each day once, and appreciate everything as if he is living it for the second time. The film ends with Tim leaving Mary in bed and getting his three children ready for school.

== Cast ==
 
* Domhnall Gleeson as Tim 
* Rachel McAdams as Mary
* Bill Nighy as Dad
* Tom Hollander as Harry
* Lindsay Duncan as Mum
* Margot Robbie as Charlotte
* Lydia Wilson as Kit Kat
* Richard Cordery as Uncle Desmond
* Joshua McGuire as Rory Tom Hughes as Jimmy Kincade
* Vanessa Kirby as Joanna
* Will Merrick as Jay
* Lisa Eichhorn as Marys Mother, Jean
 
  
Zooey Deschanel had been in talks for the role of Mary but ultimately the role went to McAdams.     Richard Griffiths also plays a brief part in what would be his final film appearance, with the film being released months after his death in March of 2013.

== Production ==
Curtis has said this is likely to be his last film as director, but that he will continue in the film industry. 

  
The film was initially scheduled to be released on 10 May 2013, release was pushed back to 1 November 2013.  
The film premiered on 8 August 2013 as part of the Film4 Summer Screen outdoor cinema series at Londons historic Somerset House.  It was released in the UK on 4 September 2013 and in the US in limited release on 1 November 2013 and in wide release on 8 November 2013. 

== Reception ==
 
Review aggregation website Rotten Tomatoes gives the film a score of 69% based on reviews from 138 critics, with an average rating of 6.3/10. The sites Audience Score is 4/5, based upon 54,401 ratings with 81% of the raters liking the film.   
Metacritic, which uses a weighted mean, assigned a score of 55/100, based on reviews from 34 film critics. 
Based on responses from 78,018 users, About Time received  a rating of 7.8/10 on the Internet Movie Database. 

 
Catherine Shoard of The Guardian compares the film to Groundhog Day (film)|Groundhog Day noting it "is about as close to home as a homage can get without calling in the copyright team" and describes Domhnall Gleeson as a "ginger Hugh Grant" which "at first, is unnerving; as About Time marches on, Gleesons innate charm gleams through and this weird disconnection becomes quite compelling." Ultimately it is not the familiarity but the "uncarbonated script" that deadens the comedy. Shoard gives the film 2 stars out of 5.  
Robbie Collin of The Daily Telegraph praises the comic timing of McAdams and Gleeson, but criticises the film, comparing it to a quilt, calling it "soft, frayed at the edges, and oh so comfortable" and gives it a score of 3 stars out of 5. 

  The Time Travelers Wife also co-starring McAdams. Unlike that film she has no knowledge of his powers, resulting in a "fundamental lack of honesty in their relationship". Felperin notes British reverse snobbery would put many off this and other Curtis films but that is less of a problem among American Anglophiles and those willing to suspend disbelief, taking the characters as British "versions of Woody Allen’s Manhattanites (but with less angst)". Felperin praises the chemistry of the leading couple "that keeps the film aloft" and the supporting cast, while also criticising the stock characters as being too familiar.  
 

The film became a surprise hit in South Korea, where it was watched by more than 3 million people, one of the highest numbers among the foreign romantic comedy movies released in Korea.  It grossed the total of $23,434,443, which is the highest figure compared to the other countries. 

===Plot holes===
Critic Mark Kermode observes that writer Curtis "sets up his rules of temporal engagement, only to break them willy-nilly whenever the prospect of an extra hug rears its head". 

The rules    as explained to Tim by his father are:

# Only male members of the family can travel in time. (Violated)
# Only travel to the past is possible. (Violated)
# It is impossible to travel back to before you were born. (Violated)
# Travelling back to a time before your child is born may cause a different child to be born and the original child to be lost. (Violated)

The films internal logic about time travel was also criticised in other reviews:
* The Independent says the explanation of time travel is "shockingly inadequate" and that "Curtis keeps leaving questions unanswered – time and time again". 
* MaryAnn Johanson remarks that there are "arbitrary and inconsistent rules of time travel in aid of creepy romantic manipulation and temporal stalking". 
* Steve Cummins of The Irish Post refers to Tim travelling backwards and forwards in time when he says the film is "riddled with plot holes". 
* Megan Gibson writing in Time says: "...sci-fi fans out there likely won’t be able to see   charms through the gaping time-travel plot-holes". 
* Critic Matthew Turner points out the "big problem is the unsightly pile-up of plot holes and logic problems". 
* Kate Erbland of Film School Rejects states "the rules and limitations of Tims gift arent exactly hard and fast, and the final third of the film is rife with complications that never get quite explained. Rules that previously applied suddenly dont apply... the time travel rules arent exactly tight and are occasionally confusing". 

== Soundtrack ==
; Track listing 
{{Track listing
| extra_column    = Artist

| writing_credits =

| title1          = The Luckiest
| note1           = About Time version
| extra1          = Ben Folds
| length1         = 4:04

| title2          = How Long Will I Love You
| extra2          = Jon Boden, Sam Sweeney & Ben Coleman
| length2         = 2:46

| title3          = Mid Air Paul Buchanan
| length3         = 2:28

| title4          = At the River
| note4           = Radio Edit
| extra4          = Groove Armada
| length4         = 3:10

| title5          = Friday Im In Love
| extra5          = The Cure
| length5         = 3:34
 Back To Black
| note6           = Explicit
| extra6          = Amy Winehouse
| length6         = 4:00

| title7          = Gold in them Hills
| extra7          = Ron Sexsmith
| length7         = 3:31

| title8          = The About Time Theme
| extra8          = Nick Laird-Clowes
| length8         = 2:22

| title9          = Into My Arms
| extra9          = Nick Cave & The Bad Seeds
| length9         = 4:13
 Il Mondo
| extra10         = Jimmy Fontana
| length10        = 2:42

| title11         = Golborne Road
| extra11         = Nick Laird-Clowes
| length11        = 2:16
 Push the Button
| extra12         = Sugababes
| length12        = 3:37

| title13         = All the Things She Said
| note13          = Original Edited
| extra13         = t.A.T.u.
| length13        = 3:35

| title14         = When I Fall In Love
| extra14         = Barbar Gough, Sagat Guirey, Andy Hamill & Tim Herniman
| length14        = 3:02

| title15         = Spiegel im Spiegel
| extra15         = Arvo Pärt
| length15        = 9:24

| title16         = How Long Will I Love You
| extra16         = Ellie Goulding
| length16        = 2:34

| title17         = Mr. Brightside
| extra17         = The Killers
| length17        = 3:42
}}

==See also== The Girl Who Leapt Through Time
* The Butterfly Effect

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 