Kurathi Magan
 
{{Infobox film
| name           = Kurathi Magan
| image          =
| image_size     =
| caption        =
| director       = K. S. Gopalakrishnan
| producer       = Balu
| writer         = K. S. Gopalakrishnan
| screenplay     = K. S. Gopalakrishnan
| story          = Kalaignanam
| narrator       =
| starring       = Gemini Ganesan K. R. Vijaya Master Sridhar Jayachitra M. N. Nambiar Kamal Haasan
| music          = K. V. Mahadevan
| cinematography = K. S. Prasad
| editing        = R. Devarajan
| studio         = Ravi Productions
| distributor    = Ravi Productions
| released       = 1972
| runtime        = 172 mins
| country        = India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Kurathi Magan is a Tamil language film  directed by K. S. Gopalakrishnan. The film features Gemini Ganesan, K. R. Vijaya,  Master Sridhar and Jayachitra introduced in the film, Sailasri, V. S. Raghavan, Suruli Rajan and Kamal Haasan in a brief role. R. Muthuraman plays a cameo.

==Plot==
The plot is about how a mother from Kuravar community sacrifices her motherhood for the sake of her sons future.

==Cast==
* Gemini Ganesan
* K. R. Vijaya
* R. Muthuraman  
* M. N. Nambiar
* Master Sridhar
* Jayachitra
* Kamal Haasan
* V. K. Ramasamy (actor)|V. K. Ramasamy
* V. S. Raghavan
* O. A. K. Devar
* Suruli Rajan
* V. Gopalakrishnan
* Shanmuga Sundaram
* G. Sakunthala
* S. R. Janaki
* Shanmuga Sundari
* Baby Kumutha
* Baby Thamizh Selvi
* Master Rajkumar
* Master Bablu

==Soundtrack==
The music composed by K. V. Mahadevan, while lyrics written by Udumalai Narayana Kavi, Kannadasan and Maruthakasi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss)
|-
| 1 || Anjathe Nee || T. M. Soundararajan, P. Susheela || 06:15
|-
| 2 || Geena Geguna || S. C. Krishnan, Krishnamoorthy, Tiruchy Loganathan, Thangappan, M. R. Vijaya || 03:19
|-
| 3 || Jaathigal Illaiyadi || P. Susheela || 05:46
|-
| 4 || Kurathi Vadi || T. M. Soundararajan, P. Susheela || 03:15
|-
| 5 || Nattukkulle || Seerkazhi Govindarajan, L. R. Eswari || 03:13
|}

==References==
 

==External links==
*  

 
 
 
 
 


 