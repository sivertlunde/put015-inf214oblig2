Star Odyssey
 
{{Infobox film
| name           = Sette uomini doro nello spazio (Seven Gold Men in Space)
| image          =Star Odyssey.jpg
| image_size     =
| caption        =
| director       = Alfonso Brescia
| producer       =  Luigi Alessi 
| writer         =  Alfonso Brescia   Massimo Lo Jacono   Giacomo Mazzocchi 
| narrator       =
| starring       = See below
| music          = Marcello Giombini
| cinematography = Silvio Fraschetti 
| editing        =  Mariano Arditi 
| distributor    =
| released       = 1978
| runtime        = 88 minutes (Italy) 103 minutes (USA)
| country        = Italy
| language       = Italian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Star Odyssey (Italian: Sette uomini doro nello spazio / Seven Gold Men in Space) is a 1978 Italian film directed by Alfonso Brescia.
The film is also known as Space Odyssey, Metallica and Captive Planet in other video markets.

== Plot summary ==
 
One of four low-budget Italian science fiction movies produced in the wake of Star Wars by Italian director Alfonso Brescia (under the pseudonym Al Bradley), the film takes place on Earth (now renamed "Sol 3") in the year 2312. 
The planet is sold to an evil despot named Kress, who soon flies to "Sol 3" to start gathering humanoid slaves to sell to his evil counterparts. Defending "Sol 3" against the new owner is the kindly Professor Maury and his ragtag band of human and robot friends. Maury and his defenders set out to reclaim the planet from Kress and his cyborg army. 
Familiar faces include Gianni Garko, Malisa Longo, and Chris Avram, veterans of numerous European thrillers. ~ Robert Firsching, Rov
 War of the Robots (1978, aka Reactor).

== Cast ==
*Yanti Somer as Irene
*Gianni Garko as Dirk Laramie
*Malisa Longo as Bridget
*Chris Avram as Shawn
*Ennio Balbo
*Roberto DellAcqua as Norman
*Aldo Amoroso Pioso
*Nino Castelnuovo
*Gianfranca Dionisi
*Pino Ferrara
*Aldo Funari
*Cesare Gelli
*Claudio Undari
*Filippo Perrone
*Franco Ressel as Commander Barr
*Massimo Righi
*Silvano Tranquilli
*Claudio Zucchet

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 
 