The Victim (2006 film)
{{Infobox film
| name           = The Victim
| image          = The Victim 2006 poster.jpg
| caption        = The Thai theatrical release poster.
| director       = Monthon Arayangkoon
| producer       = Jantima Lieawsirikum
| writer         = Monthon Arayangkoon
| narrator       = 
| starring       = Pitchanart Sakakorn
| music          = Patai Poungchneen
| cinematography = Paiboon Pupradub
| editing        =  RS Film United International Pictures Tartan Films Golden Network Asia
| released       = October 12, 2006 (Thailand)
| runtime        = 108 min Thailand
| Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Thai horror film|horror-thriller film written and directed by Monthon Arayangkoon. The film stars Pitchannart Sakakorn as a struggling young actress who takes a job working for the police department, re-enacting crime scenes, but starts to have frightening experiences when she takes on the role of a murdered beauty queen.

==Plot== Thai newspapers as a means of the police publicizing that they have closed the case and done their jobs.

Ting takes a liking to her job, and is so convincing that even the criminals are moved to remorseful tears. Her popularity soaring, Ting is signed to play the lead in a film based on one of her true-crime re-enactments - the murder of Meen, a former Miss Thailand whose husband, Dr. Charun, is accused of the crime. Ting then starts experiencing some scary visions that lead her believe that Meens longtime friend, Fai, is responsible.

Then it is revealed that the events were actually entirely a film about Meens murder, and Ting is merely a character being played by another actress named May. But while on the set filming the movie, in real life May was really possessed by a spirit that is very obsessed by her.

==Cast==
* Pitchanart Sakakorn as Ting/May
* Apasiri Nitibhon as Meen
* Penpak Sirikul as Fai
* Kiradej Ketakinta as Lieutenant Te
* Chokchai Charoensuk as Dr. Charun
* Sompong Tawee as Lieutenant Bee
* Rashane Limtrakul as Director
* Sompop Vejchapipat as Screenwriter
* Benjaporn Punyaying as Shooting crew manager.

==Production==

===Origins=== Thai media. Such photos are regularly seen in the Media of Thailand#Mass-circulation dailies|mass-circulation daily newspapers of Thailand.  , official website, retrieved 2007-09-26 

"It all began when I watched the police investigating key criminal cases by using stand-ins to re-enact crime scenes," director Monthon Arayangkoon said in the films production notes. "I wondered how those actors or actresses could handle such a horrifying task. Didnt they feel scared at all? For some scenes, like a rape scene, I cant help but wonder why they have to use a real woman to impersonate the victim. Didnt those who acted as the dead feel anything at all?"  , official website, retrieved 2007-09-26 

===Locations===
Some of the filming locations were actual famous crime scenes. These include "The Haunted House at Nong Jork", an unfinished house where a construction worker was killed during a drunken brawl. Work then stopped on the house and it has been used since as a place where criminals dump dead bodies.  , official website, retrieved 2007-09-26 

Another location is a deserted house in Bang Len, where in late 1990s the poorer suitor of a wealthy familys daughter was killed by the young womans father, who opposed his daughters relationship with the man. The dead mans family then hired a gunman to kill the father. The abandoned house was also the scene of a womans hanging death. 

A third location is an abandoned hospital building at Bang Len, where illegal abortions were once performed and a seven-months pregnant woman was raped and killed. The womans ghost was later seen at her own funeral, and the hospital has been abandoned since. 

At all the locations, film crews reported experiencing strange phenomena, such as noises, odd wind and faint images of faces on the film. 

The use of the locations was significant in Thailand, where belief in ghosts is common. Kuipers, Richard. October 18, 2006.  , Variety (magazine), retrieved 2007-09-26 

==Release== Thai cinemas Tartan Asia Extreme on September 18, 2007.

==Reception==
Variety (magazine)|Variety critic Richard Kuipers, who reviewed the film at the 2006 Pusan International Film Festival called The Victim "an entertaining and suspenseful merger of ghost story and crime thriller", and praised the "neat back-flip" of a mid-film twist. 

Jeff Allard, writing for the horror-genre website, Shock Till You Drop, was more muted in this response, saying the film "is a moderate time waster that sports its share of interesting moments but thanks to a convoluted script, it cant deliver the cumulative effect that a good ghost story needs." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 