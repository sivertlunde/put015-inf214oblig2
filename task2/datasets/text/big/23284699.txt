Gagetown (film)
{{Infobox Film
| name           = Gagetown
| image          = 
| image_size     = 
| caption        =
| director       = Daniel Feighery
| producer       = Gregg de Domenico, Jillian Stricker
| writer         = 
| narrator       = 
| starring       = 
| music          = Zain Effendi
| cinematography = Gregg de Domenico
| editing        = Gerald Zecker, Jr.
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Gagetown is a 2009 documentary film that looks into the massive defoliant spray program that was used at CFB Gagetown since 1956. The chemical herbicides used include
50/50 mixtures of 2,4-D/ 2,4,5-T, and Tordon 101, also known as Agent Orange and Agent White. 
 dioxins and hexachlorobenzenes.

Exposure to these byproducts has been linked to increased cases in cancer and blood diseases, especially Non-Hodgkins Lymphoma and leukemia.

According to a Canadian Department of National Defence document acquired through the Access to Information Act, over 3.2 million liters and kilograms of chemical defoliants 
were used at the base between the years 1956 to 1984.

==Press==
*  
*  
*  

==External links==
* 

 
 
 
 
 


 