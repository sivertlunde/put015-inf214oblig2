The Bad Lord Byron
{{Infobox film
| name           = The Bad Lord Byron
| image_size     = 
| image	=	   "The_Bad_Lord_Byron"_(1949).jpg
| caption        =  David MacDonald
| producer       = Aubrey Baring
| writer         = 
| based on = 
| narrator       = 
| starring       = Dennis Price Mai Zetterling
| music          = Cedric Thorpe Davie
| cinematography = Stephen Dade
| editing        = James Needs
| studio         = Triton Films GFD (UK)
| released       = 1949
| runtime        = 85 minutes
| country        = United Kingdom English
| budget         = ₤200,000 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} British historical David MacDonald and starred Dennis Price as Byron with Mai Zetterling, Linden Travers and Joan Greenwood. 
 Greek independence. From his deathbed, Byron remembers his life and many loves, imagining that hes pleading his case before a celestial court.   

==Cast==
* Dennis Price as Lord Byron  Teresa Guiccioli 
* Joan Greenwood as Lady Caroline Lamb
* Linden Travers as Augusta Leigh
* Sonia Holm as Annabella Milbanke John Hobhouse 
* Leslie Dwyer as Fletcher 
* Denis ODea as Prosecuting Counsel  Lady Melbourne  Virgilio Teixeira as Pietro Gamba 
* Ernest Thesiger as Count Guiccioli 
* Gerard Heinz as Austrian Officer 
* Cyril Chamberlain as Defending Counsel 
* Wilfrid Hyde-White as Mr. Hopton 
* Henry Oscar as Count Gamba 
* Richard Molinas as Gondolier 
* Robert Harris as Dallas  Ronald Adam as Judge  Archie Duncan John Murray Barry Jones as Colonel Stonhope 
* Natalie Moya as Lady Milbanke 
* Bernard Rebel as Doctor Bruno John Stone as Lord Clark 
* Nora Swinburne as Lady Jersey 
* John Salew as Samuel Rogers

==Critical reception==
The New York Times wrote, " Roundly ridiculed by British film critics in 1949, The Bad Lord Byron has stood the test of time -- not really a classic, but an acceptable rainy-day wallow." 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 


 