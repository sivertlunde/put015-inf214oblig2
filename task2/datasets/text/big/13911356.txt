João da Mata
{{Infobox film
| name           = João da Mata
| image          =
| caption        =
| director       = Amilar Alves
| producer       = Amilar Alves, Francisco Castelli, Victorino de Oliveira Prata, José Ziggiati
| writer         = Amilar Alves, Felipe Ricci
| starring       = Angelo Fortes
| cinematography = Thomaz de Tullio
| editing        = Amilar Alves and Felipe Ricci
| distributor    = Phoenix Filmes
| released       =  
| runtime        =
| country        = Brazil
| language       = Silent
| budget         =
}} 1923 Brazilian silent political drama film directed by Amilar Alves. 

The film premiered in Rio de Janeiro on October 9, 1923

==Plot==
The film questions the theme of land and Agrarian Reform in Brazil, and depicts the landowners known as "coronéis" in Portuguese as brutal and vindictive men. A lot of money, equipment and resources were employed for the film as it was considered an important topic at the time. Much of the film however has been destroyed and only fragments remain.  

==Cast==
*Amilar Alves   
*Nhá Ana   
*Juracy Aymoré   
*Eugênio Castelli   
*Moacir dos Santos   
*Angelo Fortes as  João da Mata 
*Trajano Guimarães   
*Luiz Laloni   
*Arnaldo Pinheiro   
*Plínio Porto   
*José Rodrigues   
*José Ziggiati   

==External links==
*  

 
 
 
 
 
 
 

 
 