7 mujeres, 1 homosexual y Carlos
{{Infobox film
| name           = 7 mujeres, 1 homosexual y Carlos
| image          = 7 mujeres un homosexual y carlos.PNG
| caption        = 7 mujeres, 1 homosexual y Carlos
| director       = Rene Bueno
| producer       = Jorge Gaxiola   Omar Veytia   Marissa Gómez
| writer         = Rene Bueno
| screenplay     = 
| story          = 
| based on       =  
| starring       = Mauricio Ochmann   Adriana Fonseca   Ninel Conde   Luis Felipe Tovar
| music          = Luis Salazar   Giovanni Arreola   Carlos Tachiquín
| cinematography = Alberto Lee
| editing        = Rene Bueno   Rodrigo Zapién
| studio         = 
| distributor    = 20th Century Fox (Mexico)
| released       =  
| runtime        = 90 minutes
| country        = 
| language       = Spanish pesos (roughly dollars
| gross          = 
}} Mexican comedy movie filmed in Tijuana and released in 2004. Mexican cinema to portray a story typical of the life of middle-class young people in contemporary Tijuana.
 pesos (roughly Madrid Comedy Film Festival of Madrid in 2005.

==Main cast==
* Mauricio Ochmann as Carlos
* Adriana Fonseca as Camila
* Ninel Conde as Mónica
* Luis Felipe Tovar as Carlos boss
* Wasiq Rashid as Humberto
* Veronica Segura as Lucy
* Sujosh Basak as Miguel
* David Eduardo as Felipe
* Beatriz Llamas as Camilas mother
* Paco Rocher as Carlos father
* Anaís Belén as Recepcionista

==Plot==
Carlos (Mauricio Ochmann) is a 21-year old who finds himself married to Camila (Adriana Fonseca), his 18-year old girlfriend after dating for five years and discovering that she is pregnant.

Carlos decides to pursue married life since, he thinks, people marry every day and thus everything will work out fine. He gets a job to support his new family but does not realize the seriousness of his decisions until he discovers that his new boss (Luis Felipe Tovar) is having an affair with his secretary, Lucy (Anaís Belén), and Monica, his attractive new co-worker (Ninel Conde) is attempting to seduce him.

To complicate things even more, Camilas father does not approve of the decision that she and Carlos made. Carlos struggles to defend himself against his father-in-law and from peer-pressure to be unfaithful to his wife. The title of the film derives from Carlos co-workers theory that -in Mexicos demographics- every man is "entitled" to seven women and a homosexual.

==External links==
*  at The New York Times
*   
*    at the Baja California State Commission of Filming
*    ("Tijuana will be seen in Spain") article on Frontera de Tijuana
* 

 
 
 
 
 
 
 