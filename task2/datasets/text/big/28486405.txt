So This Is Africa
{{Infobox film
| name           = So This Is Africa
| image          =
| caption        =
| producer       = Harry Cohn (executive producer)
| director       = Edward F. Cline
| writer         = Lew Lipton Norman Krasna Bert Wheeler Robert Woolsey Raquel Torres Esther Muir
| music          = Bert Kalmer Harry Ruby
| cinematography = Leonard Smith
| editing        = Maurice Wright
| distributor    = Columbia Pictures
| released       =  
| runtime        = 64 mins.
| country        = United States English
}}
 Bert Wheeler, Robert Woolsey, Raquel Torres, and Esther Muir. It was Wheeler and Woolseys only film for Columbia Pictures.

==Plot==
Film studio "Ultimate Pictures" plans on producing an animal picture in Africa. The studio gets the help of animal specialist Mrs. Johnson Martini. Theres just one problem: shes afraid of animals. Martini and the studio soon learn of Wilbur and Alexander, a couple of down on their luck vaudevillians with a trained lion act. The duo agree to join Martini on an expedition to Africa. While there, the trio finds themselves captured by a tribe of violent Amazons.

==Production==
Norman Krasna says he wrote the script in three weeks. *McGilligan, Patrick, "Norman Krasna: The Woolworths Touch", Backstory: Interviews with Screenwriters of Hollywoods Golden Age, University of California Press,1986 p219 

The Motion Picture Division of the Education Board of New York State felt that several lines of dialogue and other sequences in this film were inappropriate. As a result, Columbia Pictures was forced to delete sections of "So This Is Africa" prior to its release.

==Availability==
To date, "So This Is Africa" has not been released into the home video market. It is currently owned by Sony Pictures Entertainment. The film has been shown on Turner Classic Movies in the past.

==Cast==
*Bert Wheeler as Wilbur
*Robert Woolsey as Alexander
*Raquel Torres as Miss More
*Esther Muir as Mrs. Johnson Martini
*Berton Churchill as President of Ultimate Pictures
*Henry Armetta as Street Cleaner
*Spencer Charters as Doctor
*Eddie Clayton as Elevator Operator
*Clarence Moorehouse as Josephine, the gorilla
*Spec ODonnell as Johnny, Office Boy
*Jerome Storm as Production Manager

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 
 