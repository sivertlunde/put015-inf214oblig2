Carry On Teacher
 
 
{{Infobox film| name = Carry On Teacher
  | image = Carry-On-Teacher.jpg
  | caption = Carry On Teacher (Film Poster)
  | director = Gerald Thomas
  | producer = Peter Rogers
  | writer = Norman Hudis Charles Hawtrey Ted Ray Richard OSullivan Bruce Montgomery
  | cinematography = Reginald Wyer
  | editing = John Shirley
  | distributor = Anglo-Amalgamated
  | released = August 1959
  | runtime = 86 min.
  | budget = £71,612
  | country = United Kingdom
  | language = English
  | budget = £78,000
  }} the series Ted Ray Charles Hawtrey, Kenneth Williams and Hattie Jacques. Leslie Phillips and Joan Sims make their second appearances in the series here, having made their debuts in the previous entry, Carry On Nurse. A young Richard OSullivan and a young Larry Dann – making the first of his four Carry on appearances – turn up as pupils.

==Plot== Ted Ray) – who has been at the school for 20 years – is acting headmaster. He spots an advertisement for a headmaster of a brand new school near where he was born and decides to apply for the post.

Because of a coinciding visit by a Ministry of Education Inspector (Miss Wheeler, played by Rosalind Knight) and the noted child psychiatrist Alistair Grigg (Leslie Phillips), he decides to enlist the help of his staff to ensure that the school routine runs smoothly during their visit.

While in conference with his teaching staff (including Gregory Adams (Kenneth Connor), science master; Edwin Milton (Kenneth Williams), English master; Michael Bean (Charles Hawtrey), music teacher; Sarah Allcock (Joan Sims), gym mistress and Grace Short (Hattie Jacques), maths teacher); a senior pupil (Robin Stevens, played by Richard OSullivan) overhears that Wakefield is planning to leave at the end of term. The pupils are fond of the venerable teacher and Stevens immediately rushes this information to his schoolmates. They plan to sabotage every endeavour that might earn Wakefield praise, which would set him on the road to his new post.

On arrival, Grigg and Miss Wheeler are escorted by Wakefield on a tour of inspection and the pupils go out of their way to misbehave in each class they visit. However Griggs tour has not been in vain: he has taken a shine to Sarah Allcock, the gym mistress and it is obvious the feeling is mutual.

Miss Wheeler is disgusted at the behaviour of the children towards the teachers, but is softened when she visits the science masters class, where she feels an instinctive maternal affection for the charm of the nervous science master, Adams.

Wakefield realises his position as headmaster of the new school is in jeopardy and, on seeing Miss Wheeler’s interest in Adams, enlists his help. He asks Adams to make advances to Miss Wheeler to win her over. Adams is aghast at the thought, but eventually agrees to do his best. After many unsuccessful attempts to tell Miss Wheeler of his love, Adams finds an untruth has become truth and finally finds enough courage to declare his love.

The pupils meanwhile, have been doing everything in their power to make things go wrong, and on the last day of term are caught trying to sabotage the prizegiving. They are told to report to Wakefield’s study and after much cross-examination he learns the reason for the weeks events – the pupils simply did not want to see him leave. Wakefield – deeply moved – tells the children he wont leave and will see them all next term.

Miss Wheeler, softened by her newfound love, announces that she intends to tell the Ministry that staff-pupil relationships at the school are excellent.

==Production==
During the filming, Charles Hawtreys mother would often visit the set. While enjoying a cigarette, she accidentally dropped lit ash from the cigarette into her handbag. Joan Sims, who was the first to spot the incident, yelled, "Charlie, Charlie, your mothers bag is on fire!". Charles Hawtrey poured his cup of tea into the bag, snapped it shut, and carried on chatting. 

==Cast== Ted Ray as William Wakefield
*Kenneth Connor as Gregory Adams Charles Hawtrey as Michael Bean
*Leslie Phillips as Alistair Grigg
*Kenneth Williams as Edwin Milton
*Hattie Jacques as Grace Short
*Joan Sims as Sarah Allcock
*Rosalind Knight as Felicity Wheeler
*Cyril Chamberlain as Alf Hodgson
*Richard OSullivan as Robin Stevens
*George Howell as Billy Haig
*Roy Hines as Harry Bird
*Diana Beevers as Penny Lee
*Jacqueline Lewis as Pat Gordon
*Carol White as Sheila Gale

==Filming and locations==

*Filming dates – March 1959

Interiors:
* Pinewood Studios, Buckinghamshire

Exteriors:
* Drayton Green Primary School, Ealing

The name of the school was later alluded to by Morrissey in the song "Late Night, Maudlin Street".

==Footnotes==
 

==Bibliography==
* 
* 
* 
* 
*Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (third edition) (2007) (Reynolds & Hearn Books)
* 
* 
* 
* 
* 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 