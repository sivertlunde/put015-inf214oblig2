Mayavi (2007 film)
{{Infobox film
| name           = Mayavi
| image          = Mayavi (film).jpg Shafi
| producer       = P. Rajan
| writer         = Rafi Mecartin
| narrator       = Vijayaraghavan Salim Saikumar Santhosh Jogi
| music          = Alex Paul
| cinematography = Sanjeev Sankar
| editing        = Hariharaputhran
| studio         = Vaishaka Movies
| distributor    =
| released       =   
| runtime        =
| country        = India
| language       = Malayalam
| budget         = 
}} Malayalam Masala masala film Shafi and Arjun which was directed by A. Venkatesh (director)|A. Venkatesh in 2010.

==Cast==
* Mammootty as  Mahi aka Mayavi
* Gopika as  Indu Saikumar as  Sivasankaran Pillai
* Salim Kumar as  Kannan Srank
* Manoj K. Jayan as  Balan Vijayaraghavan as  Thottappally Surendran
* Santhosh Jogi as Thottappally Sugunan
* Suraj Venjaramood as  Giri
* Spadikam George as  Police Officer
* Keerikkadan Jose as  Yatheedran
* P. Sreekumar as  Advocate
* Cochin Haneefa as  Jailor
* K. P. A. C. Lalitha as  Devaki
* Mamukkoya as  Koya
* Manikuttan as  Satheesh
* Narayanankutty
* Bindu Panikkar as Thottappally Surendrans Wife
* T. P. Madhavan as Home Minister
* Nimisha Suresh as Ammu (Indus Sister)

==Soundtrack==
{{Infobox album 
| Name = Mayavi

| Type = Soundtrack
| Artist = Alex Paul
| Cover =
| Released =
| Recorded = 2007 Feature film soundtrack
| Length =
| Label =
| Producer = Alex Paul
}}

The songs are composed by Alex Paul. The soundtrack album, which was released on 2007, features two songs overall, with lyrics penned by Vayalar Sharath Chandra Varma.

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| lyrics_credits  = yes
| title1          = Sneham Thenalla
| lyrics1         = Vayalar Sharath Chandra Varma
| extra1          =  M. G. Sreekumar, G. Venugopal
| title2          = Muttathe Mulle
| lyrics2         = Vayalar Sharath Chandra Varma Manjari
}}

==Reception==
It had a theatrical run of over 100 days in Kerala and went on to become the top grossing Mayalalam film of 2007.  

==References==
 

== External links ==
*  

 
 
 
 
 