Coin Locker Girl
{{Infobox film
| name           = Coin Locker Girl
| image          = 
| director       = Han Jun-hee
| producer       = Yoon Kyung-hwan
| writer         = Han Jun-hee
| based on       =  
| starring       = Kim Hye-soo Kim Go-eun
| cinematography = 
| music          =  CGV Arthouse 
| studio         = Pollux Pictures
| country        = South Korea
| language       = Korean
| runtime        = 110 minutes
| budget         = 
| gross          = US$4.68 million (South Korea) 
| released       =  
}}
Coin Locker Girl ( ; lit. Chinatown) is a 2015 South Korean film directed by Han Jun-hee, starring Kim Hye-soo and Kim Go-eun.       It has been selected to screen in the International Critics Week section of the 2015 Cannes Film Festival.   

==Plot== coin locker at Western Seoul train station in 1996. A beggar takes her and names her Il-young, then when she turns 10, she is sold off to a woman referred to simply as Mother. Mother is the boss of a loan shark and organ trafficking crime ring in Chinatown, Incheon; she has held on to her position of power by being dispassionate and calculating, and by keeping by her side only those of use to her. Mother raises the young child, eventually grooming her for a position in her organization. By the time Il-young is 18 years old, she has become Mothers trusted right hand. One day, Il-young is given a task: to get close to Suk-hyun, the son of a debtor. Over the course of a few days, she becomes friends with the boy and is exposed to a world she never knew existed. She envies the normalcy that other teenage girls enjoy and opens her heart to Suk-hyun. But when his father flees, Mother orders her to kill Suk-hyun. Il-young cannot go through with it and by making that single choice, she must now flee from the only family she has ever known.

==Cast==
* Kim Hye-soo as Mother
* Kim Go-eun as Il-young
* Um Tae-goo as Woo-gon
* Park Bo-gum as Suk-hyun
* Go Kyung-pyo as Chi-do
* Lee Soo-kyung

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 

 